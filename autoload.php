<?php
namespace Poirot\Std;

function autoload($class)
{
    if ( strpos($class,__NAMESPACE__ . '\\') === 0 ) {
        $file = __DIR__
            . '/'
            . str_replace('\\','/', substr($class,strlen(__NAMESPACE__) + 1))
            . '.php';

        if ( is_file($file) )
            require_once $file;
    }
}

spl_autoload_register(__NAMESPACE__ . '\\autoload');