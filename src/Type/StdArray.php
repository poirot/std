<?php
namespace Poirot\Std\Type;

use Poirot\Std\IteratorWrapper;
use Poirot\Std\Type\Spl\AbstractNSplType;
use Poirot\Std\Type\StdArray\DefaultMergeStrategy;
use Poirot\Std\Type\StdArray\LookupArray;
use function Poirot\Std\flatten;
use function Poirot\Std\swap;

/**
 * @property array $value
 * @property array $asInternalArray
 */
final class StdArray extends AbstractNSplType
    implements \ArrayAccess
    , \Iterator
    , \Countable
{
    const __default = [];

    const InsertDoSkip = 0;
    const InsertDoPush = 1;
    const InsertDoReplace = -1;

    const MergeDoMerging = 0;
    const MergeDoPush    = self::InsertDoPush;
    const MergeDoReplace = self::InsertDoReplace;

    /** @var \Traversable */
    protected $internalValue;
    protected $reservedReferenceArray = [];

    private $pointerIndex;


    /**
     * @inheritDoc
     * @throws \UnexpectedValueException If it's strict
     */
    function __construct($initial_value = self::__default, bool $strict = true)
    {
        if ($initial_value instanceof self)
            $initial_value = $initial_value->asInternalArray;
        elseif ($initial_value instanceof \ArrayIterator)
            $initial_value = $initial_value->getArrayCopy();

        if (! is_array($initial_value) && false === $strict)
            $initial_value = self::_createFromGivenValue($initial_value);

        if (! is_array($initial_value))
            throw new \UnexpectedValueException(sprintf(
                'Type (%s) is unexpected.',
                is_object($initial_value) ? get_class($initial_value) : gettype($initial_value)
            ));

        $this->pointerIndex  = 0;
        $this->internalValue = $initial_value;
    }

    /**
     * Construct Static
     *
     * @param array|\stdClass|object|iterable|string $val
     * @param bool  $strict
     *
     * @return StdArray
     */
    static function of($val, $strict = true)
    {
        return new self($val, $strict);
    }

    /**
     * Construct Static By Php Object
     *
     * @param object $val
     *
     * @return StdArray
     */
    static function ofObject(object $val)
    {
        $initialValue = self::_createFromGivenValue($val);
        return self::of($initialValue);
    }

    /**
     * Construct Static By Php Object
     *
     * @param \Traversable $val
     *
     * @return StdArray
     */
    static function ofTraversable(\Traversable $val)
    {
        $initialValue = self::_createFromGivenValue($val);
        return self::of($initialValue);
    }

    /**
     * Wrap around existing array reference
     *
     * @param string $val
     *
     * @return StdArray
     */
    static function ofJson(string $val)
    {
        $initialValue = self::_createFromGivenValue($val);
        return self::of($initialValue);
    }

    /**
     * Wrap around existing array reference
     *
     * @param array $array
     *
     * @return StdArray
     */
    static function ofReference(array &$array)
    {
        $stdArray = new self();
        $stdArray->internalValue = &$array;
        return $stdArray;
    }

    /**
     * Proxy to get attributes
     * @inheritDoc
     */
    function &__get($name)
    {
        $internalValue = &$this->_getCleanedUpInternalValues();

        $r = null;
        switch ($name) {
            case 'value':
            case 'asInternalArray':
                $r = &$internalValue;
                break;
        }

        return $r;
    }

    /**
     * Lookup Bunch Of Items Regarding To Given Query Path
     *
     * @param string $expression
     *
     * @return mixed|null|\Generator
     */
    function &lookup(string $expression)
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        return LookupArray::lookup($expression, $internalValue);
    }

    /**
     * Walk Through Items And Filter Out Or Manipulate Value Items
     *
     * @param \Closure $filter     mixed func(value, &index, $isTraversed = false) : newValue
     * @param int      $depth      Depth of walk into inner items if value is array
     *
     * @return $this
     * @see IteratorWrapper
     */
    function filter(\Closure $filter, $depth = PHP_INT_MAX, $depthCounter = 0): self
    {
        $internalValue = &$this->_getCleanedUpInternalValues();

        $arr = [];
        $wrapperGen = new IteratorWrapper(
            $internalValue,
            function ($value, &$index) use ($filter, $depth, $depthCounter, &$wrapperGen) {
                if ($depth > 0 && (is_array($value) || $value instanceof self)) {
                    // recursively walk
                    $tValue = $value;
                    if (! $tValue instanceof self)
                        $tValue = StdArray::of($tValue);

                    $tValue = $tValue->filter($filter, $depth-1, $depthCounter+1);
                    if (is_array($value))
                        $tValue = $tValue->asInternalArray;

                    $value = $tValue;
                }

                ## to pass by reference
                #  $value = $filter->call($wrapperGen, $value, $index, $isTraversed);
                $value = $filter->bindTo($wrapperGen)($value, $index, $depthCounter);
                return $value;
            }
        );

        foreach($wrapperGen as $key => $val)
            $arr[$key] = $val;

        $this->internalValue = $arr;
        return $this;
    }

    /**
     * Merge two arrays together, reserve previous values
     *
     * @param array|StdArray $mergeWith
     * @param callable|int|null $comparision Comparision result indicate how to merge value
     *                                       {-1, 0, 1} func($givenValue, $currValue, $key)
     *
     * @return $this
     */
    function merge($mergeWith, $comparision = null): self
    {
        if (! is_array($mergeWith))
            $mergeWith = StdArray::of($mergeWith, false)->asInternalArray;

        if ($comparision === null)
            $comparision = new DefaultMergeStrategy;


        $originArray = &$this->_getCleanedUpInternalValues();
        foreach ($mergeWith as $key => $newValue)
        {
            if (! array_key_exists($key, $originArray)) {
                $this->insert($newValue, $key);
                continue;
            }

            // When comparision instruct is given
            //
            $compareResult = $comparision;
            if (is_callable($comparision)) {
                $compareResult = call_user_func($comparision, $newValue, $originArray[$key], $key);
            } elseif (null !== $comparision) {
                $compareResult = (int) $comparision;
            }

            if (null === $compareResult)
                $compareResult = static::MergeDoReplace;


            // Merge Values
            //
            if (static::MergeDoMerging === $compareResult) {
                if (is_array($originArray[$key]) || is_array($newValue)) {
                    $left  = $originArray[$key];
                    $right = $newValue;

                    if (is_array($left) && is_array($right)) {
                        $left = StdArray::of($left)->merge($right, $comparision);
                    } else {
                        if (is_array($right))
                            swap($left, $right);

                        $left = StdArray::of($left)->insert($right);
                    }

                    $this->insert($left->asInternalArray, $key);
                    continue;
                }

                $this->insert($newValue, $key, StdArray::MergeDoPush);
                continue;

            } elseif (static::MergeDoReplace === $compareResult) {
                $this->insert($newValue, $key, $comparision);
                continue;
            }

            // All other situations are all going to append
            $this->insert($newValue);
        }

        return $this;
    }

    /**
     * Insert to the current array.
     *
     * @param mixed             $value
     * @param string|null       $key
     * @param callable|int|null $comparision Comparision result indicate how to merge value
     *                                       {-1, 0, 1} func($givenValue, $currValue, $key)
     * @return $this
     */
    function insert($value, $key = null, $comparision = null): self
    {
        $internalValue = &$this->_getCleanedUpInternalValues();

        // When key not given; we simply append given value to array
        //
        if ($key === null) {
            $internalValue[] = $value;
            return $this;
        }

        // When key is given based on the key type check if any instruction is given
        //
        if ($comparision !== null && array_key_exists($key, $internalValue))
        {
            // When comparision instruct is given
            //
            $compareResult = $comparision;
            if (is_callable($comparision)) {
                $compareResult = call_user_func($comparision, $value, $internalValue[$key], $key);
            } elseif (null !== $comparision) {
                $compareResult = (int) $comparision;
            }

            if (null === $compareResult)
                $compareResult = static::InsertDoReplace;

            if ($compareResult >= static::InsertDoPush) {
                // Merge current value as array
                $currValue = $internalValue[$key];
                if (! is_array($currValue))
                    $currValue = [$currValue];
                $currValue[] = $value;
                $value = $currValue;

            } elseif ($compareResult <= static::InsertDoReplace) {
                // We let value to be set on given key
                $internalValue[$key] = $value;
                return $this;

            } elseif ($compareResult === static::InsertDoSkip) {
                // Skip and not insert to array; for instance in cases which same value exists
                return $this;
            }
        }


        $internalValue[$key] = $value;
        return $this;
    }

    /**
     * Prepend a (key) + value to the current array.
     * Note: will re-index integer array keys
     *
     * @param mixed $value
     * @param mixed $key
     *
     * @return $this
     */
    function prepend($value, $key = null): self
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        if ($key === null) {
            \array_unshift($internalValue, $value);
        } else {
            unset($internalValue[$key]);
            $this->internalValue = [$key => $value] + $internalValue;
        }

        return $this;
    }

    /**
     * Append a (key) + value to the current array.
     *
     * @param mixed $value
     * @param mixed $key
     *
     * @return $this
     */
    function append($value, $key = null)
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        if ($key === null) {
            \array_push($internalValue, $value);
        } else {
            unset($internalValue[$key]);
            $this->internalValue = $internalValue + [$key => $value];
        }

        return $this;
    }

    /**
     * Remove Multiple Keys From Array
     *
     * @param array $keys
     *
     * @return $this
     */
    function except(...$keys): self
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        if ( empty($internalValue) )
            return $this;

        $this->internalValue = array_diff_key($internalValue, array_flip($keys));
        return $this;
    }

    /**
     * Keep Given Keys from array and remove rest
     *
     * @param mixed ...$keys
     *
     * @return $this
     */
    function keep(...$keys): self
    {
        $arr = [];
        $internalValue = &$this->_getCleanedUpInternalValues();
        foreach ($keys as $k) {
            if (! array_key_exists($k, $internalValue) )
                continue;

            $arr[$k] = $internalValue[$k];
        }

        $this->internalValue = $arr;
        return $this;
    }

    /**
     * Keep array items of the intersection of given array values
     *
     * @param array|StdArray $search
     * @param \Closure|null  $compareCallable
     *
     * @return $this
     */
    function keepIntersection($search, \Closure $compareCallable = null): self
    {
        if (! is_array($search))
            $search = StdArray::of($search, false)->asInternalArray;

        if (empty($search))
            return $this;


        if ($compareCallable === null)
            $compareCallable = function ($a, $b) {
                return ($a === $b ? 0 : ($a > $b ? 1 : -1));
            };

        $internalValue = &$this->_getCleanedUpInternalValues();
        $this->internalValue = \array_uintersect(
            $internalValue,
            $search,
            $compareCallable
        );

        return $this;
    }

    /**
     * Returns the values from a single column of the input array, identified by
     * the $columnKey, can be used to extract data-columns from multi-arrays.
     *
     * Info: Optionally, you may provide an $indexKey to index the values in the returned
     * array by the values from the $indexKey column in the input array.
     *
     * @param mixed $columnKey
     * @param mixed $indexKey
     *
     * @return StdArray
     */
    function getColumn($columnKey = null, $indexKey = null): StdArray
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        return self::of(
            \array_column($internalValue, $columnKey, $indexKey)
        );
    }

    /**
     * Get First Array Item
     *
     * @return StdArray
     */
    function getFirst(): StdArray
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        return self::of(
            array_slice($internalValue, 0, 1, true)
        );
    }

    /**
     * Get First Array Item and Remove Element
     *
     * @return StdArray
     */
    function getFirstAndRemove(): StdArray
    {
        $firstItem = $this->getFirst();

        // rewind to beginning of array and remove first element
        $internalValue = &$this->_getCleanedUpInternalValues();
        $this->internalValue = array_slice(
            $internalValue,
            1,
            count($internalValue)-1,
            true
        );

        return $firstItem;
    }

    /**
     * Get Last Array Item
     *
     * @return StdArray
     */
    function getLast(): StdArray
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        return self::of(
            array_slice($internalValue, -1, count($internalValue)-1,  true)
        );
    }

    /**
     * Get Last Array Item and Remove Element
     *
     * @return StdArray
     */
    function getLastAndRemove(): StdArray
    {
        $internalValue = &$this->_getCleanedUpInternalValues();

        $lastItem = $this->getLast();
        array_pop($internalValue);
        return $lastItem;
    }

    /**
     * Get all keys from the current array.
     *
     * @return StdArray
     */
    function getKeys(): StdArray
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        return self::of((function() use ($internalValue) {
            foreach ($internalValue as $key => $_)
                yield $key;
        })(), false);
    }

    /**
     * Get all values from a array.
     *
     * @return StdArray
     */
    function getValues(): StdArray
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        return self::of(array_values($internalValue));
    }

    /**
     * Re-Index array
     *
     * @return $this
     */
    function reindex(): self
    {
        $newArr = [];
        $mapArr = [];
        $internalValue = &$this->_getCleanedUpInternalValues();
        array_walk($internalValue,
            function ($item, $key) use (&$newArr, &$mapArr) {
                if (is_int($key))
                    $newArr[] = $item;
                else
                    $mapArr[$key] = $item;
            }
        );

        ksort($mapArr);
        $this->internalValue = $newArr + $mapArr;
        return $this;
    }

    /**
     * Clean all falsy values from the current array.
     *
     * @return $this
     */
    function clean(): self
    {
        $this->filter(function ($value) {
            if (! (bool)$value )
                /** @var IteratorWrapper $this */
                return $this->skipCurrentIteration();

            return $value;
        });

        return $this;
    }

    /**
     * Flatten An Array
     *
     * @param string $notation Compatible with sprintf notation, exp. "[%s]" or ".%s"
     *
     * @return StdArray
     */
    function getFlatten($notation = '[%s]'): StdArray
    {
        $internalValue = &$this->_getCleanedUpInternalValues();

        if (false === strpos($notation, '%s'))
            $notation .= '%s';

        $flattenArray = $this->_makeFlattenArray($internalValue, $notation);
        return new static($flattenArray);
    }

    /**
     * Check check if two arrays contain the same values
     *
     * @param array|StdArray $array
     *
     * @return bool
     */
    function isHavingEqualValuesWith($array): bool
    {
        if (! is_array($array))
            $array = StdArray::of($array, false)->asInternalArray;

        $internalValue = &$this->_getCleanedUpInternalValues();
        return array_diff($internalValue, $array) === array_diff($array, $internalValue);
    }

    /**
     * Check if the current array is equal to the given "$array" or not.
     *
     * @param array|StdArray $array
     *
     * @return bool
     */
    function isEqualWith($array): bool
    {
        if (! is_array($array))
            $array = StdArray::of($array, false)->asInternalArray;

        $internalValue = &$this->_getCleanedUpInternalValues();
        return $internalValue === $array;
    }

    /**
     * Check if a given key or keys are empty.
     *
     * @return bool
     */
    function isEmpty(): bool
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        return empty($internalValue);
    }

    /**
     * Is This An Associative Array?
     *
     * @return bool
     */
    function isAssoc(): bool
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        $data = $internalValue;
        return array_values($data) !== $data;
    }


    // Implement ArrayAccess:

    /**
     * @inheritDoc
     */
    function offsetExists($offset)
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        if ( empty($internalValue) )
            return false;

        if ((bool) $offset === $offset)
            // booleans index will cast to integer by php
            $offset = (int) $offset;

        return isset($internalValue[$offset]);
    }

    /**
     * @inheritDoc
     */
    function &offsetGet($offset)
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        if (! array_key_exists($offset, $internalValue) ) {
            $this->reservedReferenceArray[$offset] = $this->_voidVal();
            $r = &$this->reservedReferenceArray[$offset];
            return $r;
        }

        if ((bool) $offset === $offset)
            // booleans index will cast to integer by php
            $offset = (int) $offset;

        return $internalValue[$offset];
    }

    /**
     * @inheritDoc
     */
    function offsetSet($offset, $value)
    {
        $internalValue = &$this->_getCleanedUpInternalValues();

        if ($offset === null) {
            $internalValue[] = $value;
        } else {
            if ((bool) $offset === $offset)
                // booleans index will cast to integer by php
                $offset = (int) $offset;

            $internalValue[$offset] = $value;
        }
    }

    /**
     * @inheritDoc
     */
    function offsetUnset($offset)
    {
        if ((bool) $offset === $offset)
            // booleans index will cast to integer by php
            $offset = (int) $offset;

        unset($this->internalValue[$offset]);
        unset($this->reservedReferenceArray[$offset]);
    }


    // Implement Iterator

    /**
     * Proxy method to current array value
     * @return mixed
     */
    function value()
    {
        return $this->current();
    }

    /**
     * @inheritDoc
     */
    function current()
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        if (! $this->valid())
            // point to array end if it's already point to nowhere because of iterate and next function
            return end($internalValue);

        return current($internalValue);
    }

    /**
     * @inheritDoc
     */
    function next()
    {
        if ($this->pointerIndex++ > $this->count() - 1)
            $this->pointerIndex = $this->count();

        $internalValue = &$this->_getCleanedUpInternalValues();
        return next($internalValue);
    }

    /**
     * @inheritDoc
     */
    function key()
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        if (! $this->valid())
            // point to array end if it's already point to nowhere because of iterate and next function
            end($internalValue);

        return key($internalValue);
    }

    /**
     * @inheritDoc
     */
    function valid()
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        return !(false === current($internalValue) && $this->pointerIndex >= count($internalValue) - 1);
    }

    /**
     * Proxy method to rewind
     * @return $this
     */
    function reset()
    {
        return $this->rewind();
    }

    /**
     * @inheritDoc
     */
    function rewind()
    {
        $internalValue = &$this->_getCleanedUpInternalValues();
        $this->pointerIndex = 0;
        reset($internalValue);
        return $this;
    }


    // Implement Countable:

    /**
     * @inheritDoc
     */
    function count()
    {
        return count($this->_getCleanedUpInternalValues(), \COUNT_NORMAL);
    }


    // ..

    // Build PHP Array Request Params Compatible With Curl Post
    private function _makeFlattenArray(array $givenArray, $notation, $prefix = '') : array
    {
        $flattenArray = [];
        foreach ($givenArray as $key => $value) {
            $key = $prefix ? $prefix . sprintf($notation, $key) : $key;
            if (! is_array($value) ) {
                $flattenArray[$key] = $value;
            } else {
                $fa = $this->_makeFlattenArray($value, $notation, $key);
                foreach ($fa as $k => $v)
                    $flattenArray[$k] = $v;
            }
        }

        return $flattenArray;
    }

    /**
     * Import Data From Given None-Array Value
     *
     * @param \stdClass|object|iterable|string $initial_value
     *
     * @return array
     */
    private static function _createFromGivenValue($initial_value)
    {
        if (is_iterable($initial_value)) {
            $initial_value = self::_createFromTraversable($initial_value);

        } elseif ($initial_value instanceof \stdClass) {
            $initial_value = self::_convertStdClassToArrayRecursively($initial_value);

        } elseif (is_object($initial_value)) {
            $initial_value = get_object_vars($initial_value);

        } elseif (is_string($initial_value) && function_exists('json_decode')) {
            $initial_value = @json_decode($initial_value, true);
            if ( JSON_ERROR_NONE !== json_last_error() )
                throw new \InvalidArgumentException('JSON: ' . json_last_error_msg());
        }

        return $initial_value;
    }

    /**
     * Convert Iterator To An Array
     *
     * @param iterable $initial_value
     *
     * @return array
     */
    private static function _createFromTraversable(iterable $initial_value)
    {
        $arr = []; foreach($initial_value as $key => $val) {
            if (! \Poirot\Std\Type\StdString\isStringify($key) )
                // some poirot Traversable is able to handle objects as key
                $key = flatten($key);

            $arr[$key] = $val;
        }

        return $arr;
    }

    private static function _convertStdClassToArrayRecursively($data)
    {
        if ( $data instanceof \stdClass)
            $data = get_object_vars($data);

        if ( is_array($data) )
            return array_map(function($v) {
                return self::_convertStdClassToArrayRecursively($v);
            }, $data);

        return $data;
    }

    private function &_getCleanedUpInternalValues(): array
    {
        foreach ($this->reservedReferenceArray as $offset => $value) {
            if ($value !== $this->_voidVal()) {
                $this->internalValue[$offset] = $value;
                unset($this->reservedReferenceArray[$offset]);
            }
        }

        $r = &$this->internalValue;
        return $r;
    }

    private function _voidVal()
    {
        return null;
    }
}
