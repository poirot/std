<?php
namespace Poirot\Std\Type\StdString\Wrappers;

use Poirot\Std\Type\StdString\Encodings;

final class ConverterStringWrapper
    extends aStringWrapper
{
    private static $_supportedEncoding = ['UTF-8'];

    /** @var array|null */
    private static $ORD;
    /** @var array|null */
    private static $CHR;

    /**
     * @inheritDoc
     */
    static function isSupported(string $encoding, $convertEncoding = null) : bool
    {
        $encoding = self::_normalizeEncoding($encoding);

        $supportedEncodings = self::_getSupportedEncodings();
        if (! in_array($encoding, $supportedEncodings) )
            return false;
        elseif ($convertEncoding === null)
            return true;


        // convert between ISO-8859-1 <- UTF-8 encodings
        $convertEncoding = self::_normalizeEncoding($convertEncoding);
        if ($convertEncoding != $encoding)
            return ( $encoding == Encodings::Charset['UTF-8'] &&
                     $convertEncoding == Encodings::Charset['ISO-8859-1'] );

        return false;
    }


    /**
     * @inheritDoc
     */
    protected function doConvert($str, $fromEncoding, $toEncoding)
    {
        if ($fromEncoding === 'UTF-8' && $toEncoding === 'ISO-8859-1')
            // allow convert UTF-8 to ISO-8859-1
            return $this->utf8_decode($str);

    }

    // ..

    private static function _getSupportedEncodings()
    {
        return self::$_supportedEncoding;
    }

    /**
     * Decodes a UTF-8 string to ISO-8859-1.
     *
     * @param string $str
     *
     * @return string
     */
    private function utf8_decode(string $str): string
    {
        if ($str === '')
            return '';

        $len = \strlen($str);

        if (self::$ORD === null)
            self::$ORD = include __DIR__ . '/_files/ord.php';

        if (self::$CHR === null)
            self::$CHR = include __DIR__ . '/_files/chr.php';

        $no_char_found = '?';
        for ($i = 0, $j = 0; $i < $len; ++$i, ++$j) {
            switch ($str[$i] & "\xF0") {
                case "\xC0":
                case "\xD0":
                    $c = (self::$ORD[$str[$i] & "\x1F"] << 6) | self::$ORD[$str[++$i] & "\x3F"];
                    $str[$j] = $c < 256 ? self::$CHR[$c] : $no_char_found;
                    break;

                /** @noinspection PhpMissingBreakStatementInspection */
                case "\xF0":
                    ++$i;
                case "\xE0":
                    $str[$j] = $no_char_found;
                    $i += 2;
                    break;

                default:
                    $str[$j] = $str[$i];
            }
        }

        if ( false === $return = \substr($str, 0, $j) )
            $return = '';

        return $return;
    }
}
