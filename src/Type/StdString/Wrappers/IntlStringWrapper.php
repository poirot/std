<?php
namespace Poirot\Std\Type\StdString\Wrappers;


final class IntlStringWrapper
    extends aStringWrapper
{
    private static $_supportedEncoding = ['UTF-8'];


    /**
     * @inheritDoc
     */
    static function isSupported(string $encoding, $convertEncoding = null) : bool
    {
        if (! extension_loaded('intl') )
            return false;

        $encoding = self::_normalizeEncoding($encoding);
        $supportedEncodings = self::_getSupportedEncodings();
        if (! in_array($encoding, $supportedEncodings) )
            return false;

        if ( $convertEncoding !== null && !self::isSupported($convertEncoding) )
            return false;

        return true;
    }


    /**
     * @inheritDoc
     */
    function strlen($str)
    {
        return grapheme_strlen($str);
    }

    /**
     * @inheritDoc
     */
    function substr($str, $offset = 0, $length = null)
    {
        if ($length !== null)
            return grapheme_substr($str, $offset, $length);

        return grapheme_substr($str, $offset);
    }

    /**
     * @inheritDoc
     */
    function strpos($haystack, $needle, $offset = 0)
    {
        return grapheme_strpos($haystack, $needle, $offset);
    }

    /**
     * @inheritDoc
     */
    protected function doConvert($str, $fromEncoding, $toEncoding)
    {
        // Convert Method never reach here because wrapper only support UTF-8 encoding.
    }

    // ..

    private static function _getSupportedEncodings()
    {
        return self::$_supportedEncoding;
    }
}
