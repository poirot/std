<?php
namespace Poirot\Std\Type\StdString\Wrappers;


final class IconvStringWrapper
    extends aStringWrapper
{
    private static $_supportedEncoding;


    /**
     * @inheritDoc
     */
    static function isSupported(string $encoding, $convertEncoding = null) : bool
    {
        if (! extension_loaded('iconv') )
            return false;

        $encoding = self::_normalizeEncoding($encoding);
        $supportedEncodings = self::_getSupportedEncodings();
        if (! in_array($encoding, $supportedEncodings) )
            return false;

        if ( $convertEncoding !== null && !self::isSupported($encoding) )
            return false;

        return true;
    }


    /**
     * @inheritDoc
     */
    function strlen($str)
    {
        return iconv_strlen($str, $this->getEncoding());
    }

    /**
     * @inheritDoc
     */
    function substr($str, $offset = 0, $length = null)
    {
        $length = $length ?? $this->strlen($str);
        return iconv_substr($str, $offset, $length, $this->getEncoding());
    }

    /**
     * @inheritDoc
     */
    function strpos($haystack, $needle, $offset = 0)
    {
        return iconv_strpos($haystack, $needle, $offset, $this->getEncoding());
    }

    /**
     * @inheritDoc
     */
    protected function doConvert($str, $fromEncoding, $toEncoding)
    {
        return iconv($fromEncoding, $toEncoding . '//IGNORE', $str);
    }

    // ..

    private static function _getSupportedEncodings()
    {
        if (static::$_supportedEncoding)
            return static::$_supportedEncoding;


        $encodings = preg_split('/[\s]+/', file_get_contents(__DIR__ . '/_files/iconv-l.txt') );
        return static::$_supportedEncoding = $encodings;
    }
}
