<?php
namespace Poirot\Std\Type\StdString\Wrappers;

use Poirot\Std\Type\StdString\Encodings;

final class NativeStringWrapper
    extends aStringWrapper
{
    /**
     * @inheritDoc
     */
    static function isSupported(string $encoding, $convertEncoding = null) : bool
    {
        $encoding = self::_normalizeEncoding($encoding);
        $supportedEncodings = self::_getSupportedEncodings();

        // convert between ISO-8859-1 -> UTF-8 encodings
        if ( $convertEncoding !== null ) {
            $convertEncoding = self::_normalizeEncoding($convertEncoding);
            if ($convertEncoding != $encoding)
                return ( $encoding == Encodings::Charset['ISO-8859-1'] &&
                         $convertEncoding == Encodings::Charset['UTF-8'] );
        }

        if (! isset($supportedEncodings[$encoding]) )
            return false;

        return true;
    }

    /**
     * @inheritDoc
     */
    function strtolower($str)
    {
        return strtolower($str);
    }

    /**
     * @inheritDoc
     */
    function strtoupper($str)
    {
        return strtoupper($str);
    }

    /**
     * @inheritDoc
     */
    function strlen($str)
    {
        return strlen($str);
    }

    /**
     * @inheritDoc
     */
    function substr($str, $offset = 0, $length = null)
    {
        if ($length === null)
            return substr($str, $offset);

        return substr($str, $offset, $length);
    }

    /**
     * @inheritDoc
     */
    function strpos($haystack, $needle, $offset = 0)
    {
        return strpos($haystack, $needle, $offset);
    }

    /**
     * @inheritDoc
     */
    protected function doConvert($str, $fromEncoding, $toEncoding)
    {
        // allow convert ISO-8859-1 to UTF-8
        return $this->utf8_encode($str);
    }

    // ..

    private static function _getSupportedEncodings()
    {
        return Encodings::CharsetSingleByte;
    }

    /**
     * Encodes an ISO-8859-1 string to UTF-8.
     *
     * @param string $str
     *
     * @return string
     */
    private function utf8_encode(string $str): string
    {
        if ($str === '')
            return '';

        if ( false === $str = \utf8_encode($str) )
            return '';

        return $str;
    }
}
