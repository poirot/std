<?php
namespace Poirot\Std\Type\StdString\Wrappers;


final class MbStringWrapper
    extends aStringWrapper
{
    private static $_supportedEncoding;


    /**
     * @inheritDoc
     */
    static function isSupported(string $encoding, $convertEncoding = null) : bool
    {
        if (!\extension_loaded('mbstring') || !function_exists('mb_strlen') )
            return false;

        $encoding = self::_normalizeEncoding($encoding);
        $supportedEncodings = self::_getSupportedEncodings();
        if (! isset($supportedEncodings[$encoding]) )
            return false;

        if ( $convertEncoding !== null && !self::isSupported($encoding) )
            return false;

        return true;
    }

    /**
     * @inheritDoc
     */
    function strtolower($str)
    {
        return mb_strtolower($str);
    }

    /**
     * @inheritDoc
     */
    function strtoupper($str)
    {
        return mb_strtoupper($str);
    }

    /**
     * @inheritDoc
     */
    function strlen($str)
    {
        return mb_strlen($str, $this->getEncoding());
    }

    /**
     * @inheritDoc
     */
    function substr($str, $offset = 0, $length = null)
    {
        return mb_substr($str, $offset, $length, $this->getEncoding());
    }

    /**
     * @inheritDoc
     */
    function strpos($haystack, $needle, $offset = 0)
    {
        return mb_strpos($haystack, $needle, $offset, $this->getEncoding());
    }

    /**
     * @inheritDoc
     */
    protected function doConvert($str, $fromEncoding, $toEncoding)
    {
        return mb_convert_encoding($str, $toEncoding, $fromEncoding);
    }

    // ..

    private static function _getSupportedEncodings()
    {
        if (static::$_supportedEncoding)
            return static::$_supportedEncoding;


        return static::$_supportedEncoding = array_flip(array_map('strtoupper', mb_list_encodings()));
    }
}
