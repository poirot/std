<?php
namespace Poirot\Std\Type\StdString\Wrappers;

use Poirot\Std\Exceptions\Type\StdString\MethodNotImplementedError;
use Poirot\Std\Exceptions\Type\StdString\StringEncodingNotSupportedError;
use Poirot\Std\Interfaces\Type\iStringWrapper;
use Poirot\Std\Type\StdString\Encodings;

abstract class aStringWrapper
    implements iStringWrapper
{
    /** @var string */
    protected $encoding;


    /**
     * Constructor.
     *
     * @param string $encoding
     *
     * @throws StringEncodingNotSupportedError
     */
    function __construct(string $encoding)
    {
        if (! static::isSupported($encoding) )
            throw new StringEncodingNotSupportedError(sprintf(
                'Encoding (%s) is not supported by string wrapper.', $encoding
            ));

        $this->encoding = self::_normalizeEncoding($encoding);
    }

    /**
     * Convert a string to the given convert encoding
     *
     * @param string $str
     * @param string $fromEncoding
     * @param string $toEncoding
     *
     * @return string
     */
    abstract protected function doConvert($str, $fromEncoding, $toEncoding);

    /**
     * @inheritDoc
     */
    function convert($str, $convertEncoding)
    {
        $encoding = $this->getEncoding();
        if (! static::isSupported($this->getEncoding(), $convertEncoding))
            throw new StringEncodingNotSupportedError(sprintf(
                'Convert not possible, Encoding (%s) is not supported by string wrapper.', $convertEncoding
            ));


        $convertEncoding = self::_getActualEncodingFromNormalized(
            self::_normalizeEncoding($convertEncoding));

        if ($encoding === $convertEncoding)
            return $str;

        return static::doConvert($str, $encoding, $convertEncoding);
    }

    /**
     * Get the defined character encoding to work with (upper case)
     *
     * @return string
     */
    function getEncoding()
    {
        return self::_getActualEncodingFromNormalized($this->encoding);
    }

    // Methods:

    /**
     * @inheritDoc
     */
    function strtolower($str)
    {
        throw new MethodNotImplementedError(sprintf("Method %s not implemented.", __FUNCTION__));
    }

    /**
     * @inheritDoc
     */
    function strtoupper($str)
    {
        throw new MethodNotImplementedError(sprintf("Method %s not implemented.", __FUNCTION__));
    }

    /**
     * @inheritDoc
     */
    function strlen($str)
    {
        throw new MethodNotImplementedError(sprintf("Method %s not implemented.", __FUNCTION__));
    }

    /**
     * @inheritDoc
     */
    function substr($str, $offset = 0, $length = null)
    {
        throw new MethodNotImplementedError(sprintf("Method %s not implemented.", __FUNCTION__));
    }

    /**
     * @inheritDoc
     */
    function strpos($haystack, $needle, $offset = 0)
    {
        throw new MethodNotImplementedError(sprintf("Method %s not implemented.", __FUNCTION__));
    }

    // ..

    protected static function _normalizeEncoding($encoding) : string
    {
        return strtoupper($encoding);
    }

    protected static function _getActualEncodingFromNormalized($normalizedEncoding) : string
    {
        return Encodings::Charset[$normalizedEncoding] ?? $normalizedEncoding;
    }
}
