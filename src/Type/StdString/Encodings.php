<?php
namespace Poirot\Std\Type\StdString;

final class Encodings
{
    /**
     * Constructor.
     * Declared private, as we have no need for instantiation.
     */
    private function __construct()
    { }

    const Charset = self::CharsetSingleByte + [
        'PASS' => 'pass', 'WCHAR' => 'wchar', 'BYTE2BE' => 'byte2be', 'BYTE2LE' => 'byte2le', 'BYTE4BE' => 'byte4be',
        'BYTE4LE' => 'byte4le', 'BASE64' => 'BASE64', 'UUENCODE' => 'UUENCODE',
        'HTML-ENTITIES' => 'HTML-ENTITIES', 'QUOTED-PRINTABLE' => 'Quoted-Printable',
        'UCS-4' => 'UCS-4', 'UCS-4BE' => 'UCS-4BE', 'UCS-4LE' => 'UCS-4LE', 'UCS-2' => 'UCS-2', 'UCS-2BE' => 'UCS-2BE',
        'UCS-2LE' => 'UCS-2LE',
        'UTF-32' => 'UTF-32', 'UTF-32BE' => 'UTF-32BE', 'UTF-32LE' => 'UTF-32LE', 'UTF-16' => 'UTF-16',
        'UTF-16BE' => 'UTF-16BE', 'UTF-16LE' => 'UTF-16LE', 'UTF-8' => 'UTF-8', 'UTF-7' => 'UTF-7',
        'UTF7-IMAP' => 'UTF7-IMAP',
        'EUC-JP' => 'EUC-JP', 'SJIS' => 'SJIS', 'EUCJP-WIN' => 'eucJP-win', 'EUC-JP-2004' => 'EUC-JP-2004',
        'SJIS-WIN' => 'SJIS-win', 'SJIS-MOBILE#DOCOMO' => 'SJIS-Mobile#DOCOMO', 'SJIS-MOBILE#KDDI' => 'SJIS-Mobile#KDDI',
        'SJIS-MOBILE#SOFTBANK' => 'SJIS-Mobile#SOFTBANK', 'SJIS-MAC' => 'SJIS-mac', 'SJIS-2004' => 'SJIS-2004',
        'UTF-8-MOBILE#DOCOMO' => 'UTF-8-Mobile#DOCOMO', 'UTF-8-MOBILE#KDDI-A' => 'UTF-8-Mobile#KDDI-A',
        'UTF-8-MOBILE#KDDI-B' => 'UTF-8-Mobile#KDDI-B', 'UTF-8-MOBILE#SOFTBANK' => 'UTF-8-Mobile#SOFTBANK',
        'CP932' => 'CP932', 'CP51932' => 'CP51932', 'JIS' => 'JIS',
        'ISO-2022-JP' => 'ISO-2022-JP', 'ISO-2022-JP-MS' => 'ISO-2022-JP-MS',
        'GB18030' => 'GB18030', 'WINDOWS-1252' => 'Windows-1252', 'WINDOWS-1254' => 'Windows-1254',
        'EUC-CN' => 'EUC-CN', 'CP936' => 'CP936', 'HZ' => 'HZ', 'EUC-TW' => 'EUC-TW', 'BIG-5' => 'BIG-5', 'CP950' => 'CP950',
        'EUC-KR' => 'EUC-KR', 'UHC' => 'UHC',
        'ISO-2022-KR' => 'ISO-2022-KR', 'WINDOWS-1251' => 'Windows-1251', 'CP866' => 'CP866', 'KOI8-R' => 'KOI8-R',
        'KOI8-U' => 'KOI8-U', 'ARMSCII-8' => 'ArmSCII-8', 'CP850' => 'CP850', 'JIS-MS' => 'JIS-ms',
        'ISO-2022-JP-2004' => 'ISO-2022-JP-2004', 'ISO-2022-JP-MOBILE#KDDI' => 'ISO-2022-JP-MOBILE#KDDI',
        'CP50220' => 'CP50220', 'CP50220RAW' => 'CP50220raw', 'CP50221' => 'CP50221', 'CP50222' => 'CP50222',
    ];

    const CharsetSingleByte = [
        'ASCII' => 'ASCII', '7BIT' => '7bit', '8BIT' => '8bit',
        'ISO-8859-1' => 'ISO-8859-1', 'ISO-8859-2' => 'ISO-8859-2', 'ISO-8859-3' => 'ISO-8859-3',
        'ISO-8859-4' => 'ISO-8859-4', 'ISO-8859-5' => 'ISO-8859-5', 'ISO-8859-6' => 'ISO-8859-6',
        'ISO-8859-7' => 'ISO-8859-7', 'ISO-8859-8' => 'ISO-8859-8', 'ISO-8859-9' => 'ISO-8859-9',
        'ISO-8859-10' => 'ISO-8859-10', 'ISO-8859-11' => 'ISO-8859-11', 'ISO-8859-13' => 'ISO-8859-13',
        'ISO-8859-14' => 'ISO-8859-14', 'ISO-8859-15' => 'ISO-8859-15', 'ISO-8859-16' => 'ISO-8859-16',
        'CP-1251' => 'CP-1251', 'CP-1252' => 'CP-1252',
    ];

    /**
     * 2 bytes byte order map representing charset encoding
     */
    protected const CharsetByteOrder128 = [
        'UNKNOWN' => "\x00\x00",
        'ASCII' => "\x00\x01", '7bit' => "\x00\x02", '8bit' => "\x00\x03",
        'ISO-8859-1' => "\x00\x04", 'ISO-8859-2' => "\x00\x05", 'ISO-8859-3' => "\x00\x06",
        'ISO-8859-4' => "\x00\x07", 'ISO-8859-5' => "\x00\x08", 'ISO-8859-6' => "\x00\x09",
        'ISO-8859-7' => "\x00\x0A", 'ISO-8859-8' => "\x00\x0B", 'ISO-8859-9' => "\x00\x0C",
        'ISO-8859-10' => "\x00\x0D", 'ISO-8859-11' => "\x00\x0E", 'ISO-8859-13' => "\x00\x0F",
        'ISO-8859-14' => "\x00\x10", 'ISO-8859-15' => "\x00\x11", 'ISO-8859-16' => "\x00\x12",
        'CP-1251' => "\x00\x13", 'CP-1252' => "\x00\x14",
        'pass' => "\x00\x15", 'wchar' => "\x00\x16", 'byte2be' => "\x00\x17", 'byte2le' => "\x00\x18",
        'byte4be' => "\x00\x19", 'byte4le' => "\x00\x1A", 'BASE64' => "\x00\x1B", 'UUENCODE' => "\x00\x1C",
        'HTML-ENTITIES' => "\x00\x1D", 'Quoted-Printable' => "\x00\x1E", 'UCS-4' => "\x00\x1F",
        'UCS-4BE' => "\x00\x20", 'UCS-4LE' => "\x00\x21", 'UCS-2' => "\x00\x22", 'UCS-2BE' => "\x00\x23",
        'UCS-2LE' => "\x00\x24", 'UTF-32' => "\x00\x25", 'UTF-32BE' => "\x00\x26", 'UTF-32LE' => "\x00\x27",
        'UTF-16' => "\x00\x28", 'UTF-16BE' => "\x00\x29", 'UTF-16LE' => "\x00\x2A", 'UTF-8' => "\x00\x2B",
        'UTF-7' => "\x00\x2C", 'UTF7-IMAP' => "\x00\x2D", 'EUC-JP' => "\x00\x2E", 'SJIS' => "\x00\x2F",
        'eucJP-win' => "\x00\x30", 'EUC-JP-2004' => "\x00\x31", 'SJIS-win' => "\x00\x32",
        'SJIS-Mobile#DOCOMO' => "\x00\x33", 'SJIS-Mobile#KDDI' => "\x00\x34", 'SJIS-Mobile#SOFTBANK' => "\x00\x35",
        'SJIS-mac' => "\x00\x36", 'SJIS-2004' => "\x00\x37", 'UTF-8-Mobile#DOCOMO' => "\x00\x38",
        'UTF-8-Mobile#KDDI-A' => "\x00\x39", 'UTF-8-Mobile#KDDI-B' => "\x00\x3A", 'UTF-8-Mobile#SOFTBANK' => "\x00\x3B",
        'CP932' => "\x00\x3C", 'CP51932' => "\x00\x3D", 'JIS' => "\x00\x3E", 'ISO-2022-JP' => "\x00\x3F",
        'ISO-2022-JP-MS' => "\x00\x40", 'GB18030' => "\x00\x41", 'Windows-1252' => "\x00\x42",
        'Windows-1254' => "\x00\x43", 'EUC-CN' => "\x00\x44", 'CP936' => "\x00\x45", 'HZ' => "\x00\x46",
        'EUC-TW' => "\x00\x47", 'BIG-5' => "\x00\x48", 'CP950' => "\x00\x49", 'EUC-KR' => "\x00\x4A", 'UHC' => "\x00\x4B",
        'ISO-2022-KR' => "\x00\x4C", 'Windows-1251' => "\x00\x4D", 'CP866' => "\x00\x4E", 'KOI8-R' => "\x00\x4F",
        'KOI8-U' => "\x00\x50", 'ArmSCII-8' => "\x00\x51", 'CP850' => "\x00\x52", 'JIS-ms' => "\x00\x53",
        'ISO-2022-JP-2004' => "\x00\x54", 'ISO-2022-JP-MOBILE#KDDI' => "\x00\x55", 'CP50220' => "\x00\x56",
        'CP50220raw' => "\x00\x57", 'CP50221' => "\x00\x58", 'CP50222' => "\x00\x59"
    ];

    /**
     * Get Byte Order Of Given Charset
     *
     * @param string $charset
     *
     * @return mixed|null
     */
    static function getByteOrderOfCharset($charset)
    {
        return self::CharsetByteOrder128[$charset] ?? self::CharsetByteOrder128['UNKNOWN'];
    }

    /**
     * Get Charset Representing the given Byte Order
     *
     * @param mixed $byteOrder
     *
     * @return string|null
     */
    static function getCharsetOfByteOrder($byteOrder)
    {
        $byteOrders = array_flip(self::CharsetByteOrder128);
        return $byteOrders[$byteOrder] ?? 'UNKNOWN';
    }
}
