<?php
namespace Poirot\Std\Type;

use Poirot\Std\IteratorWrapper;
use Poirot\Std\Interfaces\Pact\ipFactory;
use Poirot\Std\Type\Spl\AbstractNSplType;


final class StdTraversable
    extends AbstractNSplType
    implements \IteratorAggregate, ipFactory
{
    /** @var \Traversable */
    protected $internalValue;


    /**
     * Construct Static
     *
     * @param iterable $val
     * @param bool $strict
     *
     * @return StdTraversable
     */
    static function of($val, $strict = true)
    {
        return new self($val, $strict);
    }

    /**
     * @inheritDoc
     *
     * @throws \UnexpectedValueException If it's strict
     */
    function __construct($initial_value = null, bool $strict = true)
    {
        if ($initial_value === null)
            $initial_value = new \ArrayIterator;

        if (! $initial_value instanceof \Traversable && false === $strict)
            $initial_value = $this->_createFromGivenValue($initial_value);

        if (! $initial_value instanceof \Traversable )
            throw new \UnexpectedValueException(sprintf(
                'Type (%s) is unexpected.',
                is_object($initial_value) ? get_class($initial_value) : gettype($initial_value)
            ));


        $this->internalValue = $initial_value;
    }

    // Implement Features:

    /**
     * Walk Through Items And Filter Out Or Manipulate Items
     *
     * @param \Closure $filter     mixed func(value, &index, $isTraversed = false)
     * @param bool     $recursive  Recursively convert values that can be iterated
     *
     * @return StdTraversable
     */
    function each(\Closure $filter, $recursive = false)
    {
        $arr = [];
        $wrapperGen = new IteratorWrapper(
            $this->getIterator(),
            function ($value, &$index) use ($filter, $recursive, &$wrapperGen) {
                $isTraversed = false;
                if ( $recursive && is_iterable($value) ) {
                    // recursively walk
                    $value = StdTraversable::of($value)->each($filter, true);
                    $isTraversed = true;
                }

                $value = $filter->call($wrapperGen, $value, $index, $isTraversed);
                return $value;
            }
        );

        foreach($wrapperGen as $key => $val)
            $arr[$key] = $val;

        return new self($arr, false);
    }

    // Implement IteratorAggregate:

    function getIterator()
    {
        foreach ($this->internalValue as $index => $value)
            yield $index => $value;
    }

    // ..

    /**
     * Import Data From Given None-Array Value
     *
     * @param \stdClass|object|iterable $initial_value
     *
     * @return \Traversable
     */
    private function _createFromGivenValue($initial_value)
    {
        if ( is_object($initial_value) )
            $initial_value = get_object_vars($initial_value);
        else
            $initial_value = (array) $initial_value;


        return new \ArrayIterator($initial_value);
    }
}
