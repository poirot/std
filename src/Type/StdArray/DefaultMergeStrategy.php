<?php
namespace Poirot\Std\Type\StdArray;

use Poirot\Std\Type\StdArray;


class DefaultMergeStrategy
{
    /**
     * Proxy Call To Invoke Method,
     * Comparision result indicate how to merge value
     *
     * @param $value
     * @param $currVal
     * @param $key
     *
     * @return int {-1, 0, 1}
     */
    static function call($value, $currVal, $key): ?int
    {
        $self = new static;
        return $self->__invoke($value, $currVal, $key);
    }

    /**
     * Comparision result indicate how to merge value
     *
     * @param $value
     * @param $currVal
     * @param $key
     *
     * @return int {-1, 0, 1}
     */
    function __invoke($value, $currVal, $key): ?int
    {
        if ($value === $currVal)
            // We keep current value if given value is exactly same as current value
            return StdArray::MergeDoReplace;

        if (is_int($key) && (!is_array($value) || !is_array($currVal)))
            // We merge [ 0 => 'foo'] and [ 0 => 'bar'] to [ 0 => 'foo', 1 => 'bar']
            return StdArray::MergeDoPush;

        if (is_array($value) || is_array($currVal))
            return StdArray::MergeDoMerging;

        // It's a default behaviour
        return StdArray::MergeDoReplace;
    }
}
