<?php
namespace Poirot\Std\Type\StdArray;

final class LookupArray
{
    /** @var array */
    protected $array;


    /**
     * Constructor.
     *
     * @param array $array
     */
    private function __construct(array &$array)
    {
        $this->array = &$array;
    }

    /**
     * Lookup Bunch Of Items Regarding To Given Query Path
     *
     * @param string $path
     * @param array $array
     *
     * @return mixed
     */
    static function &lookup(string $path, array &$array)
    {
        return (new self($array))
            ->_lookupArray($path);
    }

    // ..

    protected function &_lookupArray(string $path, array &$array = null)
    {
        if ($array === null)
            $array = &$this->array;


        # every request path will start from array root
        $path = rtrim($path, '/') . '/';

        $regTokens   = preg_quote('/>*:');
        $regArrayKey = "^$regTokens";

        $command = null;
        $currentPos = 0;
        $lookupCallable = function & (&$r) {return $r;};
        $nullValAsReference = null; # to avoid Notice: only variable by reference must return
        while ($currentPos < strlen($path) && is_iterable($array)) {
            preg_match("(\G(?P<_key_>[$regArrayKey]*)(?P<_token_>[$regTokens]|$))"
                , $path
                , $matches
                , 0
                , $currentPos
            );

            $command = $this->_createLookupCommandByParsedToken($matches, $command, $arguments);

            if ($command == 'Tick') {
                $currentPos++;
                $lResult = &$lookupCallable($array);
                unset($array);
                $array = &$lResult;

            } elseif ($command == 'Any' || $command == 'TickToc') {
                // move pointer and not reset command to notify next command
                $currentPos++;

            } elseif ($command == 'Values') {
                $currentPos++;
                $lookupCallable = function & (&$array) use ($arguments) {
                    @$r = &$this->_doValues($array);
                    return $r;
                };

            } elseif ($command == 'Match') {
                $currentPos+= strlen($arguments['match_criteria']);
                $lookupCallable = function & (&$array) use ($arguments) {
                    $matchCriteria = $arguments['match_criteria'];
                    @$r = &$this->_doMatch($array, $this->_assertLookupCriteria($matchCriteria));
                    return $r;
                };

            } elseif ($command == 'MatchAny') {
                $currentPos+= strlen($arguments['match_criteria']);
                $lookupCallable = function & (&$array) use ($arguments) {
                    $matchCriteria = $arguments['match_criteria'];
                    @$x = $this->_doMatchMultiple($array, $this->_assertLookupCriteria($matchCriteria));
                    $y = &$x;
                    return $y;
                };

            } elseif ($command == 'MatchRecursive') {
                $currentPos+= strlen($arguments['match_criteria']);
                $lookupCallable = function & (&$array) use ($arguments) {
                    $matchCriteria = $arguments['match_criteria'];
                    @$x = $this->_doMatchRecursive($array, $this->_assertLookupCriteria($matchCriteria));
                    $y = &$x;
                    return $y;
                };

            } elseif ($command == 'Depth') {
                // move pointer and not reset command to empty until the next tick
                $currentPos+= $arguments['depth_criteria_forward'] ?? 1;
            } elseif ($command == 'DepthTick') {
                // prepare callable and reset command
                $lookupCallable = function & (&$array) use ($arguments) {
                    $depthLevel = $arguments['depth_level'] ?? 1;
                    @$x = $this->_doDepth($array, $depthLevel);
                    $y = &$x;
                    return $y;
                };

            } else {
                throw new \RuntimeException(sprintf(
                    'Invalid Token Found For (%s) Command, Right Before %s:"%s"', $command, $currentPos, $path
                ));
            }
        }

        if ($currentPos >= strlen($path)) {
            // given path fully traced
            return $array;
        } else {
            $y = null;
            return $y;
        }
    }

    private function &_doValues(&$array)
    {
        foreach ($array as $_ => &$v) {
            yield $v;
        }
    }

    private function &_doMatchMultiple(&$array, $matchCriteria)
    {
        foreach ($array as $k => &$v) {
            if (!preg_match($matchCriteria, $k))
                continue;

            yield $k => $v;
        }
    }

    private function &_doMatch(&$array, $matchCriteria)
    {
        $r = null; foreach ($array as $k => &$v) {
            if ( preg_match($matchCriteria, $k) ) {
                $r = &$v;
                break;
            }
        }

        return $r;
    }

    private function &_doMatchRecursive(&$array, $matchCriteria)
    {
        foreach($array as $key => &$value) {
            if (is_array($value) || $value instanceof \Generator) {
                foreach ($this->_doMatchRecursive($value, $matchCriteria) as $k => &$v)
                    yield $k => $v;
            }

            if (preg_match($matchCriteria, $key))
                yield $key => $value;
        }
    }

    private function &_doDepth(&$array, $depth) : ?\Generator
    {
        if ($depth > 1) {
            $tArray = $this->_doDepth($array, $depth - 1);
            unset($array);
            $array = &$tArray;
        }

        foreach ($array as $key => &$value) {
            if ( is_array($value) ) {
                foreach ($value as $k => &$v)
                    yield $k => $v;
            }
        }
    }

    private function _assertLookupCriteria($matchCriteria)
    {
        if (false === preg_match_all("/~(?P<_delimiter_>[^~]+)~/"
                , $matchCriteria
                , $matches
            ))
            throw new \RuntimeException(sprintf('Regex Match (%s) is invalid.', $matchCriteria));

        if (!empty($matches['_delimiter_'])) {
            foreach ($matches['_delimiter_'] as $delimiter)
                $matchCriteria = str_replace("~$delimiter~", "($delimiter)", $matchCriteria);
        }

        $matchCriteria = "(^{$matchCriteria}$)";
        return $matchCriteria;
    }

    private function _createLookupCommandByParsedToken(array $matches, $prevCommand, &$arguments = [])
    {
        $key   = $matches['_key_'];
        $token = $matches['_token_'];

        if (!empty($key) || $key === '0') {
            switch ($prevCommand) {
                case 'Depth':
                    // >2 change depth level by argument
                    $arguments['depth_level'] = (int) $key;
                    $arguments['depth_criteria_forward'] = strlen($key);
                    return 'Depth';

                case 'Any':
                    $arguments['match_criteria'] = $key;
                    return 'MatchAny';
                case 'TickToc':
                    $arguments['match_criteria'] = $key;
                    return 'MatchRecursive';

                default:
                    $arguments['match_criteria'] = $key;
                    return 'Match';
            }
        }

        if ($token) {
            $token = $matches['_token_'];
            if ($token == '>') {
                $command = 'Depth';
                $arguments['depth_level'] = (int) $key;
            } elseif ($token == '/') {
                $command = 'Tick';
                if ($prevCommand == 'Tick')
                    $command = 'Toc';
                elseif ($prevCommand == 'Depth')
                    $command = 'Tick';
                elseif ($command == 'Tick')
                    $prevCommand = '';

                $command = $prevCommand . $command;
            } elseif ($token == '*') {
                $command = 'Any';
            } elseif ($token == ':') {
                $command = 'Values';
            }
        }

        return $command;
    }
}
