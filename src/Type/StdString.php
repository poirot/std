<?php
namespace Poirot\Std\Type;

use Poirot\Std\Exceptions\ObjectImmutableError;
use Poirot\Std\Exceptions\Type\StdString\MethodNotImplementedError;
use Poirot\Std\Exceptions\Type\StdString\StringEncodingNotSupportedError;
use Poirot\Std\Interfaces\Pact\ipFactory;
use Poirot\Std\Interfaces\Type\iStringWrapper;
use Poirot\Std\Type\Spl\NSplString;
use Poirot\Std\Type\StdString\Encodings;
use Poirot\Std\Type\StdString\Wrappers\ConverterStringWrapper;
use Poirot\Std\Type\StdString\Wrappers\IconvStringWrapper;
use Poirot\Std\Type\StdString\Wrappers\IntlStringWrapper;
use Poirot\Std\Type\StdString\Wrappers\MbStringWrapper;
use Poirot\Std\Type\StdString\Wrappers\NativeStringWrapper;


/**
 * @property string $charset
 * @property string $encoding
 */
final class StdString
    extends NSplString
    implements ipFactory, \ArrayAccess, \Countable
{
    const CharsetDefault = Encodings::Charset['UTF-8'];

    /** @var string */
    protected $charset;

    static private $stringWrappers = [
        ConverterStringWrapper::class, # lowest priority  ^
        NativeStringWrapper::class,
        IntlStringWrapper::class,
        IconvStringWrapper::class,
        MbStringWrapper::class,       # highest priority  v
    ];

    static private $instantiatedWrapper = [];


    /**
     * @inheritDoc
     *
     * @param string $charset the charset to use ISO-8859-1, UTF-8, ...
     */
    function __construct($initial_value = self::__default, string $charset = self::CharsetDefault, $strict = true)
    {
        parent::__construct($initial_value, $strict);

        $this->charset = $this->_normalizeEncoding($charset);
    }

    /**
     * Construct Static
     *
     * @param string $val
     * @param string $charset the charset to use
     * @param bool   $strict
     *
     * @return StdString
     */
    static function of($val = '', string $charset = Encodings::Charset['UTF-8'], $strict = true)
    {
        return new self($val, $charset, $strict);
    }

    /**
     * @param string $serialized
     *
     * @return StdString
     */
    static function ofSerializedByteOrder($serialized = '')
    {
        $charset = $serialized[0] . $serialized[1];
        $charset = Encodings::getCharsetOfByteOrder($charset);
        $string  = substr($serialized, 3); # skip 2 first byte + space after byte code

        return new self($string, $charset);
    }

    /**
     * Export as Serialized Byte Order
     *
     * @return string
     */
    function asSerializedByteOrder()
    {
        return Encodings::getByteOrderOfCharset($this->charset) . ' ' . $this;
    }

    /**
     * Instantiate Supported String Wrapper
     *
     * @param string      $charset
     * @param null|string $convertEncoding
     * @param array       $exclude
     *
     * @return iStringWrapper
     */
    static function wrapper(string $charset, ?string $convertEncoding = null, array $exclude = [])
    {
        $wrapper = null;
        $arguments = [$charset, $convertEncoding];
        for ($i = count(self::$stringWrappers)-1; !$wrapper && $i >= 0; $i--) {
            $tWrapper = self::$stringWrappers[$i];
            if ( in_array($tWrapper, $exclude) )
                continue;

            if ( forward_static_call_array([$tWrapper, 'isSupported'], $arguments) )
                $wrapper = new $tWrapper($charset);
        }

        if (! $wrapper )
            throw new StringEncodingNotSupportedError("Not Any Wrapper Found To Encoding ($charset).");


        return $wrapper;
    }

    /**
     * Proxy to get attributes
     * @inheritDoc
     */
    function __get($name)
    {
        $charset = null;
        switch ($name) {
            case 'charset':
            case 'encoding':
                $charset = $this->charset;
                break;
        }

        return $charset;
    }


    /**
     * Register String Wrapper
     *
     * @param string $wrapperClass
     *
     * @return bool
     */
    static function registerWrapper(string $wrapperClass)
    {
        try {
            $class = new \ReflectionClass($wrapperClass);
            if (! $class->implementsInterface(iStringWrapper::class) )
                return false;

        } catch (\ReflectionException $_) {
            return false;
        }

        self::$stringWrappers[] = $wrapperClass;
    }

    /**
     * Un Register String Wrapper
     *
     * @param string $wrapperClass
     *
     * @return void
     */
    static function unregisterWrapper(string $wrapperClass)
    {
        foreach (self::$stringWrappers as $i => $wrapper) {
            if ($wrapper == $wrapperClass)
                unset(self::$stringWrappers[$i]);
        }
    }

    /**
     * Safe implode strings with given delimiter
     *
     * @param string[] $parts
     * @param string   $delimiter
     *
     * @return StdString
     */
    function concatSafeImplode(array $parts, string $delimiter = '')
    {
        if ('' === $delimiter)
            return new self($this . implode($delimiter, $parts));


        if ('' !== (string) $this)
            array_unshift($parts, (string) $this);

        $r = '';
        if ( 1 !== $cp = count($parts) ) {
            // when we have just one item let it be as is
            for ($i = 1; $i <= $cp; $i++) {
                $p = $parts[$i-1];

                if ($i == 1)
                    // don't left trim first member, let it as is
                    $p = rtrim($p, $delimiter);
                elseif ($i != $cp)
                    $p = trim($p, $delimiter);

                $r .= $p;

                if ($i < count($parts))
                    // append delimiter to parts except last one
                    $r .= $delimiter;
            }
        }

        return new self($r, true, $this->charset);
    }

    /**
     * Makes string's first char lowercase.
     *
     * @return StdString
     */
    function lowerFirstChar()
    {
        $self = $this;
        $str  = $this->wrapperCall(function(iStringWrapper $wrapper) use ($self) {
            return $wrapper->strtolower($self[0]) . $wrapper->substr((string) $self, 1);
        });

        return new self($str, $this->charset);
    }

    /**
     * Makes string's first char uppercase.
     *
     * @return StdString
     */
    function upperFirstChar()
    {
        $self = $this;
        $str  = $this->wrapperCall(function(iStringWrapper $wrapper) use ($self) {
            return $wrapper->strtoupper($self[0]) . $wrapper->substr((string) $self, 1);
        });

        return new self($str, $this->charset);
    }

    /**
     * Convert the given string to lowercase
     *
     * @return StdString
     */
    function lower()
    {
        $str = (string) $this;
        if ($str === '')
            return new self('', $this->charset);


        $substr = $this->wrapperCall(function(iStringWrapper $wrapper) use ($str) {
            return (string) $wrapper->strtolower($str);
        });

        return new self($substr, $this->charset);
    }

    /**
     * Convert the given string to uppercase
     *
     * @return StdString
     */
    function upper()
    {
        $str = (string) $this;
        if ($str === '')
            return new self('', $this->charset);


        $substr = $this->wrapperCall(function(iStringWrapper $wrapper) use ($str) {
            return (string) $wrapper->strtoupper($str);
        });

        return new self($substr, $this->charset);
    }

    /**
     * Sanitize To PascalCase
     *
     * @param string $delimiter
     *
     * @return StdString
     */
    function pascalCase($delimiter = '_')
    {
        ## prefix and postfix __ will remains; like: __this__
        #
        $pos = 'prefix';
        $prefix = '';
        $posfix = '';

        $i = 0; while( isset($this[$i]) ) {
            if ($this[$i] == $delimiter) {
                $$pos .= $delimiter;
            } else {
                $posfix = ''; // reset posix, _ may found within string
                $pos = 'posfix';
            }

            $i++;
        }


        $str = (string) $this->upperFirstChar();

        if (self::isSingleByteEncoding($this->charset)) {
            // same but performing faster
            $str = str_replace(' ', '', ucwords(str_replace($delimiter, ' ', $str)));

        } else {
            // every char that started with space, - or _
            $str = (string) \preg_replace_callback('/[' . $delimiter . '\\s]+(.)?/u',
                function (array $match) : string {
                    // $match ['-c', 'c']
                    if (! isset($match[1]) ) return '';
                    return $this->wrapperCall(function(iStringWrapper $wrapper) use ($match) {
                        return (string) $wrapper->strtoupper($match[1]);
                    });
                }, $str );
        }

        $str = $prefix . $str . $posfix;
        return new self($str, $this->charset);
    }

    /**
     * Sanitize Underscore To Camelcase
     *
     * @param string $delimiter
     *
     * @return StdString
     */
    function camelCase($delimiter = '_')
    {
        ## prefix and postfix __ will remains; like: __this__
        #
        $prefix = ''; $i = 0; while( isset($this[$i]) ) {
            if ($this[$i] != $delimiter)
                break;

            $prefix .= $delimiter;
            $i++;
        }

        $prefixLength = $this->wrapperCall(function(iStringWrapper $wrapper) use ($prefix) {
            return (string) $wrapper->strlen($prefix);
        });

        $str = $prefix . (string) $this->subString($prefixLength)->pascalCase()->lowerFirstChar();
        return new self($str, $this->charset);
    }

    /**
     * Sanitize CamelCase To under_score
     *
     * @param string $delimiter
     *
     * @return StdString
     */
    function snakeCase($delimiter = '_')
    {
        $str = (string) $this;

        ## prefix and postfix __ will remains; like: __this__
        #
        $pos = 'prefix';
        $prefix = '';
        $posfix = '';

        $i = 0; while( isset($this[$i]) ) {
            if ($this[$i] == $delimiter) {
                $$pos .= $delimiter;
            } else {
                $posfix = ''; // reset posix, _ may found within string
                $pos = 'posfix';
            }

            $i++;
        }

        $str = (string) \preg_replace_callback('/([\\p{N}|\\p{Lu}])/u',
            function (array $matches) use ($delimiter) : string {
                $match = $matches[1];

                $match_int = (int) $match;
                if ((string) $match_int === $match)
                    // when word matched is int
                    return $match;

                return $this->wrapperCall(function(iStringWrapper $wrapper) use ($match, $delimiter) {
                    return $delimiter . $wrapper->strtolower($match);
                });
            }, $str );

        $str = (string) \preg_replace(
            [
                '/\\s+/u',               // convert spaces to "_"
                '/^\\s+|\\s+$/u',        // trim leading & trailing spaces
                '/' . $delimiter . '+/', // remove double "_"
            ],
            [
                $delimiter,
                $delimiter,
                $delimiter,
            ],
            $str
        );

        // trim leading & trailing "_"
        $str = \trim($str, $delimiter);

        $str = $prefix . $str . $posfix;
        return new self($str, $this->charset);
    }

    /**
     * Get Prefix String If String Started With That?
     *
     * @param string[] $prefix
     *
     * @return StdString
     */
    function hasStartedWith(string... $prefix)
    {
        $str = (string) $this;
        foreach ($prefix as $p) {
            if ( 0 === strpos($str, $p) )
                return new self($p, $this->charset);
        }

        return new self('', $this->charset);
    }

    /**
     * Remove a prefix string from the left of a string
     *
     * @param string $subject
     * @param bool   $removeWhitespaces
     *
     * @return StdString
     */
    function stripPrefix(?string $subject = null, bool $removeWhitespaces = true)
    {
        $str = (string) $this;
        if ($removeWhitespaces)
            $str = ltrim($str);

        $str = $this->wrapperCall(function(iStringWrapper $wrapper) use ($subject, $str, $removeWhitespaces) {
            $prLen = $wrapper->strlen($subject);
            if ($prLen && $wrapper->substr($str, 0, $prLen) == $subject)
                $str = $wrapper->substr($str, $prLen);

            if ($removeWhitespaces)
                $str = ltrim($str);

            return $str;
        });

        return new self($str, $this->charset);
    }

    /**
     * Remove a prefix string from the right of a string
     *
     * @param string $subject
     * @param bool   $removeWhitespaces
     *
     * @return StdString
     */
    function stripPostfix(?string $subject = null, bool $removeWhitespaces = true)
    {
        $str = (string) $this;
        if ($removeWhitespaces)
            $str = rtrim($str);

        $str = $this->wrapperCall(function(iStringWrapper $wrapper) use ($subject, $str, $removeWhitespaces) {
            $prLen = $wrapper->strlen($subject);
            if ($prLen && $wrapper->substr($str, -$prLen) == $subject)
                $str = $wrapper->substr($str, 0, $wrapper->strlen($str) - $prLen);

            if ($removeWhitespaces)
                $str = rtrim($str);

            return $str;
        });

        return new self($str, $this->charset);
    }

    /**
     * Returns the portion of string specified by the start and length parameters.
     *
     * @param int      $start
     * @param null|int $length
     *
     * @return StdString
     */
    function subString($start, $length = null)
    {
        $str = (string) $this;
        if ($str === '')
            return new self('', $this->charset);


        $substr = $this->wrapperCall(function(iStringWrapper $wrapper) use ($str, $start, $length) {
            // when string is less than start characters long, FALSE will be returned.
            return (string) $wrapper->substr($str, $start, $length);
        });

        return new self($substr, $this->charset);
    }

    /**
     * Determine if a given string contains a given substring.
     *
     * @param string $needle
     *
     * @return StdString
     */
    function hasContains(string $needle)
    {
        $haystack = (string) $this;
        if ($needle !== '' && strpos($haystack, $needle) !== false)
            return new self($needle, $this->charset);

        return new self('', $this->charset);
    }

    /**
     * Determine if a given string contains a given substring.
     *
     * @param string $needle
     *
     * @return bool
     */
    function isContains(string $needle): bool
    {
        $haystack = (string) $this;
        return ($needle !== '' && strpos($haystack, $needle) !== false);
    }

    /**
     * Trim and remove all new lines from string
     *
     * @return StdString
     */
    function normalizeExtraNewLines()
    {
        $s = preg_replace(
            '/\t+/',
            '',
            preg_replace('/(\r\n|\n|\r){3,}/',
                "$1$1",
                (string) $this )
        );

        return new self($s, $this->charset);
    }

    /**
     * Strip all whitespace characters. This includes tabs and newline
     * characters, as well as multibyte whitespace such as the thin space
     * and ideographic space.
     *
     * @return StdString
     */
    function normalizeWhitespaces()
    {
        $str = (string) $this;

        if ($str != '') {
            $str = (string) \preg_replace('/[[:space:]]+/u', ' ', $str);
            $str = trim($str);
        }

        return new self($str, $this->charset);
    }

    /**
     * Convert a string to the given convert encoding
     *
     * @param string $convertEncoding
     *
     * @return StdString
     */
    function convert($convertEncoding)
    {
        $str = (string) $this;

        try {
            $wrapper = $this->stringWrapper($convertEncoding);
        } catch (StringEncodingNotSupportedError $e) {
            throw new \RuntimeException('Can`t convert to "%s"; not suitable wrapper found.', null, $e);
        }

        $str = $wrapper->convert($str, $convertEncoding);
        return new self($str, $convertEncoding);
    }

    // ..

    /**
     * Is String Started With Given Prefixes?
     *
     * @param string[] $prefix
     *
     * @return bool
     */
    function isStartedWith(string... $prefix): bool
    {
        $str = (string) $this;
        foreach ($prefix as $p) {
            if (0 === strpos($str, $p))
                return true;
        }

        return false;
    }

    /**
     * Get Length of String
     *
     * @return int
     */
    function length(): int
    {
        return $this->count();
    }

    // Implement ArrayAccess

    /**
     * @inheritDoc
     */
    function offsetExists($offset)
    {
        return $offset >= 0 && $offset <= $this->count() - 1;
    }

    /**
     * @inheritDoc
     */
    function offsetGet($offset)
    {
        return (string) $this->subString($offset, 1);
    }

    /**
     * @inheritDoc
     */
    function offsetSet($offset, $value)
    {
        throw new ObjectImmutableError('Can`t change a char inside StdString.');
    }

    /**
     * @inheritDoc
     */
    function offsetUnset($offset)
    {
        throw new ObjectImmutableError('Can`t remove a char from StdString.');
    }

    // Implement Countable

    /**
     * @inheritDoc
     */
    function count()
    {
        $str = (string) $this;

        if ($str === '')
            return 0;

        return $this->wrapperCall(function(iStringWrapper $wrapper) use ($str) {
            return $wrapper->strlen($str);
        });
    }

    // ..

    /**
     * Handle Supported Wrapper
     *
     * @param \Closure $callable
     * @param array    $exclude
     *
     * @return mixed
     */
    private function wrapperCall(\Closure $callable, array $exclude = [])
    {
        $wrapper = $this->stringWrapper(null, $exclude);

        try {
            $r = call_user_func($callable, $wrapper);
        } catch (MethodNotImplementedError $e) {
            // when method is not implemented try to fallback to another wrapper which is support the
            // method call.
            array_push($exclude, get_class($wrapper));
            return $this->wrapperCall($callable, $exclude);
        }

        return $r;
    }

    /**
     * Instantiate Supported String Wrapper
     *
     * @param null|string $convertEncoding
     * @param array       $exclude
     *
     * @return iStringWrapper
     */
    private function stringWrapper(?string $convertEncoding = null, array $exclude = []) : iStringWrapper
    {
        if ( isset(self::$instantiatedWrapper[$this->charset . $convertEncoding]) ) {
            $wrapper = self::$instantiatedWrapper[$this->charset . $convertEncoding];
            if (! in_array(get_class($wrapper), $exclude) )
                return $wrapper;
        }

        $wrapper = self::wrapper($this->charset, $convertEncoding, $exclude);
        return self::$instantiatedWrapper[$this->charset . $convertEncoding] = $wrapper;
    }

    /**
     * Is given encoding a known single-byte character encoding?
     *
     * @param string $normalizedCharset
     *
     * @return bool
     */
    private function isSingleByteEncoding($normalizedCharset) : bool
    {
        return isset(Encodings::CharsetSingleByte[$normalizedCharset]);
    }

    private function _normalizeEncoding(string $charset) : string
    {
        $charsetUpper = strtoupper($charset);
        return Encodings::Charset[$charsetUpper] ?? $charset;
    }
}
