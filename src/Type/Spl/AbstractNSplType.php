<?php
namespace Poirot\Std\Type\Spl;


abstract class AbstractNSplType
{
    const __default = null;

    /**
     * Creates a new value of some type
     *
     * @param mixed $initial_value
     * @param bool $strict  If set to true then will throw UnexpectedValueException if value of other type will be assigned. True by default
     */
    abstract function __construct ($initial_value = self::__default, bool $strict = true);
}
