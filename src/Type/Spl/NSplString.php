<?php
namespace Poirot\Std\Type\Spl;


class NSplString 
    extends AbstractNSplType
{
    const __default = '';

    /** @var string */
    protected $internalValue;


    /**
     * @inheritDoc
     */
    function __construct ($initial_value = self::__default, $strict = true)
    {
        if ($strict && !$this->isStringify($initial_value))
            throw new \UnexpectedValueException(sprintf(
                'Type (%s) is unexpected.', gettype($initial_value)
            ));


        $initial_value = (!is_bool($initial_value))
            ? $initial_value
            : ($initial_value ? 'true' : 'false');

        $this->internalValue = (string) $initial_value;
    }

    function __toString()
    {
        return $this->internalValue;
    }

    // ..

    /**
     * Check whether given value can be converted to string?
     *
     * @param mixed $var
     *
     * @return bool
     */
    private function isStringify($var): bool
    {
        $value = $var; // don't manipulate given variable value by settype

        return (
            (!is_array($value) && !is_bool($value))
            && (
                (! is_object($value) && false !== @settype($value, 'string'))
                || ( is_object($value) && method_exists($value, '__toString') )
            )
        );
    }
}
