<?php
namespace Poirot\Std\Type\Spl;


abstract class NSplEnum
{
    /**
     * Enum value
     * @var mixed|null
     */
    protected $value;

    /**
     * existing constants per class
     * @var array
     */
    protected static $cachedConstantList = [];


    /**
     * @inheritDoc
     */
    function __construct($initial_value = null)
    {
        if ($initial_value instanceof static)
            $initial_value = $initial_value();

        if (! in_array($initial_value, static::initConstantList(true)) )
            throw new \UnexpectedValueException(sprintf(
                'Value (%s) is unexpected.',
                is_object($initial_value) ? gettype($initial_value) : $initial_value
            ));

        $this->value = $initial_value;
    }

    function __invoke()
    {
        return $this->value;
    }

    /**
     * Returns a value when called statically
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return static
     * @throws \BadMethodCallException
     */
    static function __callStatic($name, $arguments)
    {
        $constList = static::initConstantList();
        if (! \array_key_exists($name, $constList) )
            throw new \UnexpectedValueException(sprintf(
                'Value (%s) is unexpected.', $name));

        return new static($constList[$name]);
    }


    /**
     * Determines if Enum considered as equal
     *
     * @param NSplEnum $enumVal
     *
     * @return bool
     */
    final function isEqualTo(NSplEnum $enumVal): bool
    {
        return $this() === $enumVal() && $enumVal instanceof $this;
    }

    /**
     * Returns instances of the Enum class of all Enum constants
     *
     * @return static[] Constant name in key, Enum instance in value
     */
    final static function listEnums()
    {
        $values = [];
        foreach (static::initConstantList() as $key => $value)
            $values[$key] = new static($value);

        return $values;
    }

    // ..

    /**
     * Returns all possible values as an array
     *
     * @param bool $includeDefault
     *
     * @return array
     * @throws
     */
    protected static function initConstantList(bool $includeDefault = false)
    {
        $class = static::class;
        if (! isset(static::$cachedConstantList[$class]) ) {
            $reflection = new \ReflectionClass($class);
            $constList = []; foreach ($reflection->getReflectionConstants() as $refConst) {
                if ($refConst->isPublic()) {
                    $constList[$refConst->getName()] = $refConst->getValue();
                }
            }

            static::$cachedConstantList[$class] = $constList;
        }

        $constants = static::$cachedConstantList[$class];
        if (! $includeDefault )
            unset($constants['__default']);

        return $constants;
    }
}
