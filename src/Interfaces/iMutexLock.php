<?php
namespace Poirot\Std\Interfaces;

interface iMutexLock
{
    /**
     * Put Lock On Resource
     *
     * @param float $ttl the expiration delay of locks in seconds
     *
     * @return bool
     */
    function acquire(float $ttl = 0);

    /**
     * Check Weather Resource Locked?!
     *
     * @return bool
     */
    function isLocked();

    /**
     * Unlock Resource Locked
     *
     * @return bool True on success and wise versa
     */
    function release();
}
