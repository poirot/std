<?php
namespace Poirot\Std\Interfaces\Pact;


interface ipBuilder
    extends ipConfigurable
{
    /**
     * Build Given Class and also return that class object
     *
     * @param mixed $class
     *
     * @return mixed
     */
    #function build($class);
}
