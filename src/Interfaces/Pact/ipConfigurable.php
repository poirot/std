<?php
namespace Poirot\Std\Interfaces\Pact;

use Poirot\Std\Exceptions\Configurable\ConfigurationError;
use Poirot\Std\Exceptions\Configurable\ResourceParseError;
use Poirot\Std\Exceptions\Configurable\UnknownResourceError;

interface ipConfigurable
{
    /**
     * Build Object With Provided Options
     *
     * @param array $options        Usually associated array
     * @param array $skipExceptions List of exception classes to skip on error
     *
     * @return $this
     * @throws ConfigurationError Invalid Option(s) value provided
     */
    function setConfigs(array $options, array $skipExceptions = []);

    /**
     * Parse Build Options From Given Resource; then parsed options can be
     * passed to ::with method to set options
     *
     * - usually it used in cases that we have to support more than one configure situation
     *
     *   ```php
     *     Configurable->with(Configurable::parse(path\to\file.conf))
     *   ```
     *
     * @param array|mixed $optionsResource
     *
     * @throws UnknownResourceError|ResourceParseError When given resource is not supported
     * @return array
     */
    static function parse($optionsResource);
}
