<?php
namespace Poirot\Std\Interfaces\Pact;

use Poirot\Std\Exceptions\Validator\ValidationError;

interface ipValidator
{
    /**
     * Assert Validate Entity
     *
     * @return void
     * @throws ValidationError
     */
    function assertValidate(): void;
}
