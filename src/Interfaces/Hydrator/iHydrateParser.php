<?php
namespace Poirot\Std\Interfaces\Hydrator;


interface iHydrateParser
{
    /**
     * Parse given resource
     *
     * @param mixed $optionsResource
     *
     * @return iterable|null
     */
    function parse($optionsResource): ?iterable;
}
