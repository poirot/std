<?php
namespace Poirot\Std\Interfaces\InvokableResponder;


interface iResponderResult
{
    /**
     * Get Data
     *
     * @return mixed
     */
    function getPreparedResult();
}
