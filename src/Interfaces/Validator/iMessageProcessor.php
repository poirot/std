<?php
namespace Poirot\Std\Interfaces\Validator;

interface iMessageProcessor
{
    function getProcessedErrMessage($message, string $param = null, $value = null) : string;
}
