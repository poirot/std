<?php
namespace Poirot\Std\Interfaces\Type;

use Poirot\Std\Exceptions\Type\StdString\StringEncodingNotSupportedError;

interface iStringWrapper
{
    /**
     * Check if the given character encoding is supported by this wrapper
     * and the character encoding to convert to is also supported.
     * note: when extension not loaded this method also should return false
     *
     * @param string      $encoding
     * @param string|null $convertEncoding
     *
     * @return bool
     */
    static function isSupported(string $encoding, $convertEncoding = null) : bool;

    /**
     * Get the defined character encoding to work with (upper case)
     *
     * @return string
     */
    function getEncoding();

    /**
     * Convert the given string to lowercase
     *
     * @param string $str
     *
     * @return string
     */
    function strtolower($str);

    /**
     * Convert the given string to uppercase
     *
     * @param string $str
     *
     * @return string
     */
    function strtoupper($str);

    /**
     * Returns the length of the given string
     *
     * @param string $str
     *
     * @return int|false
     */
    function strlen($str);

    /**
     * Returns the portion of string specified by the start and length parameters
     *
     * @param string   $str
     * @param int      $offset
     * @param int|null $length
     *
     * @return string|false
     */
     function substr($str, $offset = 0, $length = null);

    /**
     * Find the position of the first occurrence of a substring in a string
     *
     * @param string $haystack
     * @param string $needle
     * @param int    $offset
     *
     * @return int|false
     */
     function strpos($haystack, $needle, $offset = 0);

    /**
     * Convert a string to the given convert encoding
     *
     * @param string $str
     * @param string $convertEncoding
     *
     * @return string
     * @throws StringEncodingNotSupportedError
     */
     function convert($str, $convertEncoding);
}
