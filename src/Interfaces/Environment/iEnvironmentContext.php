<?php
namespace Poirot\Std\Interfaces\Environment;

use Poirot\Std\Interfaces\Struct\iData;

interface iEnvironmentContext
    extends iData
{
    function getTimeZone(): ?string;

    function getMaxExecutionTime(): ?int;

    function getDisplayErrors();

    function getErrorReporting();

    function getDisplayStartupErrors();

    function getHtmlErrors();

    function getDefineConst();

    function getEnvGlobal();
}
