<?php
namespace Poirot\Std\Interfaces\Struct\Collection;


interface iDatumId
{
    /**
     * Factory With Valuable Parameter
     *
     * @param mixed $datum
     *
     * @return iDatumId
     */
    static function of($datum);

    /**
     * Calculate a unique identifier for the contained datum object
     *
     * @return string
     */
    function __toString();
}
