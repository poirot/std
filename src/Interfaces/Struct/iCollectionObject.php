<?php
namespace Poirot\Std\Interfaces\Struct;

use Poirot\Std\Exceptions\Struct\CollectionNotSupportDatum;
use Poirot\Std\IteratorWrapper;
use Poirot\Std\Interfaces\Struct\Collection\iDatumId;


interface iCollectionObject
    extends \Countable
{
    /**
     * Add Object To Collection
     *
     * - will replace object with newly given data if exists
     *
     * @param mixed $datum
     * @param array $metaData
     *
     * @return iDatumId ETag Hash Identifier of object
     * @throws CollectionNotSupportDatum
     */
    function add($datum, array $metaData = []): iDatumId;

    /**
     * Set Data For Stored Object
     *
     * @param iDatumId $objectId
     * @param array    $metaData
     *
     * @return $this
     * @throws \Exception Object not stored
     */
    function setMetaData(iDatumId $objectId, array $metaData);

    /**
     * Checks if the collection contains a specific object
     *
     * @param iDatumId      $objectId
     * @param \Closure|null $callable function($datum, array $data, iDatumId $objectId)
     *                                callable will call if object exists result is the
     *                                return part of callable
     * @return mixed|null
     */
    function get(iDatumId $objectId, \Closure $callable = null);

    /**
     * Search for datum objects that match given accurate data
     *
     * @param array         $data
     * @param \Closure|null $callable function($datum, array $data, iDatumId $objectId)
     *                                callable will apply to each iteration item
     *
     * @return IteratorWrapper[object]
     */
    function find(array $data, \Closure $callable = null): IteratorWrapper;

    /**
     * Detach By ETag Hash Or Object Match
     *
     * @param iDatumId $objectId
     *
     * @return $this
     */
    function del(iDatumId $objectId);
}
