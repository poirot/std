<?php
namespace Poirot\Std\Interfaces\Struct;

interface iIterableObject
    extends \Traversable
{ }
