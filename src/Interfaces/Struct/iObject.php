<?php
namespace Poirot\Std\Interfaces\Struct;

/**
 * Objects that implement this interface can interchange provided
 * data with each others.
 */
interface iObject
    extends iIterableObject
{
    /**
     * @inheritDoc
     */
    function __set($key, $value);

    /**
     * @inheritDoc
     */
    function __get($key);

    /**
     * @inheritDoc
     */
    function __isset($key);

    /**
     * @inheritDoc
     */
    function __unset($key);
}
