<?php
namespace Poirot\Std\Interfaces\Struct;

interface iData
    extends iObject, \Countable
{
    /**
     * Set Struct Data From Array
     *
     * @param iterable $data
     *
     * @return $this
     */
    function import(iterable $data);

    /**
     * If a variable is declared
     *
     * @param mixed $key
     *
     * @return bool
     */
    function has(...$key);

    /**
     * Unset a given variable
     *
     * @param mixed $key
     *
     * @return $this
     */
    function del(...$key);

    /**
     * Clear all values
     *
     * @return $this
     */
    function empty();

    /**
     * Is Empty?
     *
     * @return bool
     */
    function isEmpty();
}
