<?php
namespace Poirot\Std\Interfaces\Struct;

use Poirot\Std\Exceptions\ValueObject\ObjectInitializeError;
use Poirot\Std\Exceptions\ValueObject\PropertyMissingOrNotFulFilledError;
use Poirot\Std\Exceptions\ValueObject\ResourceParseError;
use Poirot\Std\Exceptions\ValueObject\UnknownResourceError;


interface iValueObject
    extends iIterableObject
    , \Serializable
{
    /**
     * Create Value Object From Given Data
     *
     * @param iterable $data
     *
     * @return iValueObject
     * @throws PropertyMissingOrNotFulFilledError
     * @throws ObjectInitializeError
     */
    static function of(iterable $data);

    /**
     * Parse given data source to iterable data
     *
     * @param mixed $origin
     * @param bool $strict
     *
     * @return iterable Empty iterable on non-strict parse
     * @throws ResourceParseError|UnknownResourceError When it's strict
     */
    static function parseWith($origin, $strict = true): iterable;

    /**
     * Check whether the given object has equal values?
     *
     * @param iValueObject $object
     *
     * @return bool
     * @throws PropertyMissingOrNotFulFilledError
     */
    function isEqualTo(iValueObject $object): bool;

    // Serializable:

    /**
     * @inheritDoc
     * @throws PropertyMissingOrNotFulFilledError
     */
    function serialize();
}
