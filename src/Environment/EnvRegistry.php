<?php
namespace Poirot\Std\Environment;

use Poirot\Std\Environment\Contexts\DevelopmentContext;
use Poirot\Std\Environment\Contexts\PhpServerContext;
use Poirot\Std\Environment\Contexts\ProductionContext;
use Poirot\Std\Exceptions\Environment\FactoryEnvironmentError;
use Poirot\Std\Interfaces\Environment\iEnvironmentContext;
use Poirot\Std\Traits\tPropertySanitizeKey;


class EnvRegistry
{
    use tPropertySanitizeKey;

    const Development = 'development';
    const Production  = 'production';
    const PhpServer   = 'php-server';

    protected static $EnvAndAliases = [
        'development'     => DevelopmentContext::class,
        'dev'             => 'development',
        'debug'           => 'development',

        'production'      => ProductionContext::class,
        'prod'            => 'production',

        'php-server'      => PhpServerContext::class,
        'php'             => 'php-server',
        'default'         => 'php',
    ];

    protected static $AppliedEnv;

    /**
     * Apply Php Environment Settings
     *
     * @param string        $alias
     * @param iterable|null $overrideConf
     *
     * @throws \Throwable
     */
    static function apply(string $alias = self::PhpServer, ?iterable $overrideConf = null): void
    {
        $environment = static::getContextNameByAlias($alias);

        if (static::$AppliedEnv)
            throw new \RuntimeException(sprintf(
                'Environment "%s:%s" Already Provisioned.'
                , $alias, $environment
            ));


        $self = new static;
        $envObject = static::createEnvInstance($environment, $overrideConf);
        foreach($envObject as $prop => $value) {
            if ($value === null)
                continue;

            $method = 'do' . $self->_sanitizeBackFromPropertyKey($prop);
            if (! method_exists($self, $method))
                continue;

            $self->{$method}($value);
        }

        static::$AppliedEnv = [
            'name' => $environment,
            'data' => $envObject,
        ];
    }

    /**
     * Register New Environment With Given Name
     *
     * - it can override current environment with existence name
     *
     * @param iEnvironmentContext|string $environment Environment class instance or class name
     * @param string                     $name        Registered Name
     * @param array                      $aliases     Aliases
     */
    static function register($environment, $name, array $aliases = [])
    {
        if ( is_string($environment) && !class_exists($environment)
            || is_object($environment) && !$environment instanceof iEnvironmentContext
        )
            throw new \InvalidArgumentException(sprintf(
                'Invalid Environment Provided; It must be class name or object instance of iEnvironmentContext. given: (%s).'
                , \Poirot\Std\flatten($environment)
            ));

        static::$EnvAndAliases[$name] = $environment;
        static::setAliases($name, $aliases);
    }

    /**
     * Set Alias Or Name Aliases
     *
     * @param string   $name    Alias or name
     * @param string[] $aliases Alias(es)
     */
    static function setAliases(string $name, string...$aliases)
    {
        foreach($aliases as $a)
            self::$EnvAndAliases[$a] = $name;
    }

    /**
     * Get Current Applied Environment
     *
     * @return object
     */
    static function currentEnvironment()
    {
        if (! self::$AppliedEnv) {
            try {
                static::$AppliedEnv = [
                    'name' => self::PhpServer,
                    'data' => static::createEnvInstance(self::PhpServer),
                ];
            } catch (\Throwable $e) { }
        }


        $name = static::$AppliedEnv['name'];
        $data = clone static::$AppliedEnv['data'];
        return new class($name, $data) {
            private $name, $context;

            function __construct(string $name, iEnvironmentContext $context) {
                $this->name = $name;
                $this->context = $context;
            }

            function getName(): string {
                return $this->name;
            }

            function getContext(): iEnvironmentContext {
                return $this->context;
            }

            function __toString() {
                return $this->getName();
            }
        };
    }

    /**
     * Find Context Environment Name By Given Alias/Environment Name
     *
     * @param string $alias
     *
     * @return string|null
     */
    static function getContextNameByAlias(string $alias): ?string
    {
        $environment = null;
        while (is_string($alias) && isset(static::$EnvAndAliases[$alias])
            && $environment = $alias
        ) {
            $alias = static::$EnvAndAliases[$alias];
        }

        return $environment;
    }

    // Environment Provision Actions

    /**
     * Called On Apply With Option Value of getDisplayErrors
     *
     * @param int $value
     *
     * @return $this
     */
    function doDisplayErrors(int $value): self
    {
        ini_set('display_errors', $value);
        return $this;
    }

    function doDisplayStartupErrors($value): self
    {
        ini_set('display_startup_errors', $value);
        return $this;
    }

    function doDefineConst(array $constantList): self
    {
        foreach ($constantList as $const => $value) {
            $const = (string) $const;
            if (defined($const) && constant($const) !== $value)
                throw new \RuntimeException(sprintf(
                    'Constant (%s) is defined and can`t override.'
                    , $const
                ));

            define($const, $value);
        }

        return $this;
    }

    function doEnvGlobal(array $values): self
    {
        foreach ($values as $name => $value)
        {
            $notHttpName = 0 !== strpos($name, 'HTTP_');

            $_ENV[$name] = $value;
            putenv("$name=$value");

            // If PHP is running as an Apache module and an existing
            if (function_exists('apache_getenv')
                && function_exists('apache_setenv')
                && apache_getenv($name) !== false
            )
                apache_setenv($name, $value);


            if ($notHttpName)
                $_SERVER[$name] = $value;
        }


        return $this;
    }

    /**
     * @param int|string $value String as defined constant like E_ALL
     * @return $this
     */
    function doErrorReporting($value): self
    {
        error_reporting(defined($value) ? constant($value) : (int) $value);
        return $this;
    }

    function doHtmlErrors($value): self
    {
        ini_set('html_errors', $value);
        return $this;
    }

    /**
     * Called On Apply With Option Value of getTimeZone
     *
     * @param $value
     *
     * @return $this
     */
    function doTimeZone(string $value): self
    {
        if (false === date_default_timezone_set($value))
            trigger_error(
                sprintf('Timezone (%s) is`nt valid.', $value)
                , E_USER_WARNING
            );

        return $this;
    }

    /**
     * Called On Apply With Option Value of getMaxExecutionTime
     *
     * @param $value
     *
     * @return $this
     */
    function doMaxExecutionTime(int $value): self
    {
        if (false === set_time_limit($value))
            trigger_error(
                sprintf('Max Execution Time (%s) Has Failed.', $value)
                , E_USER_WARNING
            );

        return $this;
    }

    // ...

    /**
     * Create Instance Of Environment Object
     *
     * @param string   $alias Environment name or alias
     * @param iterable $settings
     *
     * @return iEnvironmentContext
     * @throws FactoryEnvironmentError
     * @throws \Throwable
     */
    protected static function createEnvInstance(string $alias, ?iterable $settings = null)
    {
        $envClass = null;
        if (null !== $environment = self::getContextNameByAlias($alias)) {
            $envClass = static::$EnvAndAliases[$environment];
            if (is_string($envClass) && class_exists($envClass))
                $envClass = new $envClass();
        }

        if (! $envClass instanceof iEnvironmentContext)
            throw new FactoryEnvironmentError(sprintf(
                '"%s" Is Invalid Alias Or Environment Name Provided.'
                , $alias
            ));

        $envClass = clone $envClass;
        if ($settings !== null)
            $envClass->import($settings);

        return $envClass;
    }
}
