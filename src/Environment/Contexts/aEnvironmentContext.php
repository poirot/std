<?php
namespace Poirot\Std\Environment\Contexts;

use Poirot\Std\Interfaces\Environment\iEnvironmentContext;
use Poirot\Std\Struct\DataDynamicSet;


abstract class aEnvironmentContext
    extends DataDynamicSet
    implements iEnvironmentContext
{
    protected $timeZone;
    protected $maxExecutionTime;
    protected $displayErrors;
    protected $errorReporting;
    protected $displayStartupErrors = 1;
    protected $htmlErrors;
    protected $defineConst = [];
    protected $envGlobal = [];

    protected $dataSetMethodVisibilities = \ReflectionMethod::IS_PUBLIC;


    function setTimeZone(?string $value): self
    {
        $this->timeZone = $value;
        return $this;
    }

    function getTimeZone(): ?string
    {
        return $this->timeZone;
    }

    // ..

    function setMaxExecutionTime(?int $value): self
    {
        $this->maxExecutionTime = $value;
        return $this;
    }

    function getMaxExecutionTime(): ?int
    {
        return $this->maxExecutionTime;
    }

    // ..

    function setDisplayErrors($displayErrors): self
    {
        $this->displayErrors = $displayErrors;
        return $this;
    }

    function getDisplayErrors()
    {
        return $this->displayErrors;
    }

    // ..

    /**
     * @param int|string $errorReporting String as defined constant like E_ALL
     * @return $this
     */
    function setErrorReporting($errorReporting)
    {
        $this->errorReporting = $errorReporting;
        return $this;
    }

    function getErrorReporting()
    {
        return $this->errorReporting;
    }

    // ..

    function setDisplayStartupErrors($displayStartupErrors)
    {
        $this->displayStartupErrors = $displayStartupErrors;
        return $this;
    }

    function getDisplayStartupErrors()
    {
        return $this->displayStartupErrors;
    }

    // ..

    function setHtmlErrors($htmlErrors)
    {
        $this->htmlErrors = $htmlErrors;
        return $this;
    }

    function getHtmlErrors()
    {
        return $this->htmlErrors;
    }

    // ..

    function setDefineConst($defineConst)
    {
        $this->defineConst = $defineConst;
        return $this;
    }

    function getDefineConst()
    {
        return $this->defineConst;
    }

    //..

    function setEnvGlobal($definedConst)
    {
        $this->envGlobal = (array) $definedConst;
        return $this;
    }

    function getEnvGlobal()
    {
        return $this->envGlobal;
    }
}
