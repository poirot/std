<?php
namespace Poirot\Std\Environment\Contexts;

/**
 * - Display Errors Off
 * - Advised to use error logging in place of error displaying on production
 *
 */
class ProductionContext
    extends aEnvironmentContext
{
    protected $displayErrors  = 0;
    protected $errorReporting = 0;
    protected $displayStartupErrors = 0;
}
