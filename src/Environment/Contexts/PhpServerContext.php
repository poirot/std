<?php
namespace Poirot\Std\Environment\Contexts;


class PhpServerContext
    extends aEnvironmentContext
{
    /**
     * @inheritDoc
     */
    function getDisplayErrors()
    {
        if($this->errorReporting === null)
            $this->setDisplayErrors( (int) ini_get('display_errors'));

        return $this->displayErrors;
    }

    /**
     * @inheritDoc
     */
    function getDisplayStartupErrors()
    {
        if($this->displayStartupErrors === null)
            $this->setDisplayStartupErrors( (int) ini_get('display_startup_errors'));

        return $this->displayStartupErrors;
    }

    /**
     * @inheritDoc
     */
    function getDefinedConst()
    {
        return get_defined_constants(true)['user'];
    }

    /**
     * @inheritDoc
     */
    function getEnvGlobal()
    {
        return $_ENV;
    }

    /**
     * @inheritDoc
     */
    function getErrorReporting()
    {
        if ($this->errorReporting === null)
            ## current php settings
            $this->setErrorReporting( (int) ini_get('error_reporting'));

        return $this->errorReporting;
    }

    /**
     * @inheritDoc
     */
    function getHtmlErrors()
    {
        if ($this->htmlErrors === null)
            $this->setHtmlErrors((int) ini_get('html_errors'));

        return $this->htmlErrors;
    }

    /**
     * @inheritDoc
     */
    function getTimeZone(): ?string
    {
        if ($this->timeZone === null)
            $this->setTimeZone(date_default_timezone_get());

        return $this->timeZone;
    }

    /**
     * @inheritDoc
     */
    function getMaxExecutionTime(): ?int
    {
        if ($this->maxExecutionTime === null)
            $this->setMaxExecutionTime(ini_get('max_execution_time'));

        return $this->maxExecutionTime;
    }
}
