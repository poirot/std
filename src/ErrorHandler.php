<?php
namespace Poirot\Std;

use Poirot\Std\Exceptions\ErrorException;

!defined('E_ERROR') && define('E_ERROR', 1);
!defined('E_RECOVERABLE_ERROR') && define('E_RECOVERABLE_ERROR', 4096);

class ErrorHandler
{
    const E_ScreamAll  = 268435456; # handle php errors regarding of current error_reporting level
    const E_Exceptions = 16777216;  # handle exceptions and throwable errors
    const E_FatalError = E_ERROR | E_RECOVERABLE_ERROR | E_USER_ERROR | E_PARSE | E_COMPILE_ERROR; # handle exceptions and throwable errors
    const E_HandleAll  = E_ALL | self::E_FatalError | self::E_Exceptions;

    protected static $handlerStack = [];
    protected static $reservedMemory;

    /**
     * Constructor.
     * Declared private, as we have no need for instantiation.
     */
    private function __construct()
    { }

    /**
     * Start Handling PHP Errors and Exceptions Based On Given Error Type
     *
     * @param int $errorTypes
     * @param callable|null $errorHandler A handler with \Throwable|\ErrorException as first argument that will be called on Error
     *                                    Exception handler will use to handle the abortion of your script gracefully.
     *
     * @return void
     */
    static function handle(int $errorTypes = self::E_HandleAll, callable $errorHandler = null)
    {
        if ($errorTypes === null)
            $errorTypes = self::E_HandleAll;


        $controlBits = 0;

        // Handle Throwable, Exceptions
        //
        if (self::assertErrorTypeContainHandleThrowable($errorTypes, $errorTypes))
            // keep applied control bit
            $controlBits |= self::E_FatalError;

        // Handle PHP Errors
        //
        if (self::assertErrorTypeContainScreamAllErrors($errorTypes, $errorTypes))
            // keep applied control bit
            $controlBits |= self::E_ScreamAll;

        // Register Error Handlers
        //
        register_shutdown_function(function() {
            if (PHP_SAPI === 'cli' || PHP_SAPI === 'phpdbg')
                return;

            if (null === self::$reservedMemory)
                self::$reservedMemory = str_repeat('x', 10240);

            self::handleFatalError();
        });

        self::safeRegisterExceptionHandler();
        self::safeRegisterErrorHandler();

        static::$handlerStack[] = [
            'control_bits' => $controlBits,
            'error_type'   => $errorTypes,
            'callable'     => $errorHandler,
            'errors'      => [
                'error' => [],
                'exception' => [],
            ],
        ];
    }

    /**
     * Stopping The Error Handling and Return PHP Errors if it has
     *
     * @return null|IteratorWrapper
     */
    static function handleDone()
    {
        if (null === $errStack = array_pop(self::$handlerStack)) {
            self::destroy();
            return null;
        }

        if (empty(self::$handlerStack))
            self::destroy();

        if (empty($errStack['errors']['error']))
            return null;

        return new IteratorWrapper(array_reverse($errStack['errors']['error']), function($error) {
            return $error;
        });
    }

    /**
     * Stop all active handler and clean stack
     *
     * @return void
     */
    static function destroy()
    {
        // To be sure that Error handler registered by self class is still active
        self::safeRegisterErrorHandler();
        restore_error_handler();

        self::safeRegisterExceptionHandler();
        restore_exception_handler();

        self::$handlerStack = [];
    }

    /**
     * Get Stack Nested Level
     *
     * @return int
     */
    static function getNestedLevel()
    {
        return count(self::$handlerStack);
    }

    // ...

    /**
     * @internal
     *
     * Fallback function to Handles PHP errors
     *
     * @return bool Returns false when no handling happens so that the PHP engine can handle the error itself
     * @throws \ErrorException|\Throwable
     */
    protected static function handleError(int $type, string $message, string $file, int $line): bool
    {
        $errStack = &self::$handlerStack[count(self::$handlerStack)-1];

        if ( $type !== ($type & $errStack['error_type']) )
            // handler not suppose to handle this type of error
            return false;

        if (! self::assertErrorTypeContainScreamAllErrors($errStack['control_bits']) ) {
            // Check error_reporting level to see user has silenced this error type?!
            $currErrorReporting = error_reporting();
            if ( $type !== ($type & $currErrorReporting) )
                // User silenced this error type; nothing to do!
                return false;
        }

        $errStack['errors']['error'][] = $errorException = self::prepareErrorTrace(
            self::makeErrorExceptionFromPhpError($type, $message, $file, $line));

        if ( isset($errStack['callable']) ) {
            try {
                if (false === call_user_func($errStack['callable'], $errorException))
                    // Let it go handled by php default handler
                    return false;

            } catch (\Throwable $e) {
                self::handleDone();
                throw $e;
            }
        }

        return true;
    }

    /**
     * @internal
     *
     * Fallback function to Handles Throwable Errors
     *
     * @throws \Throwable
     */
    protected static function handleException(\Throwable $error)
    {
        if (null === $errStack = array_pop(self::$handlerStack))
            throw $error;

        if (! self::assertErrorTypeContainHandleThrowable($errStack['control_bits']) )
            // Nothing to do with Exception let it flow
            throw $error;


        $errStack['errors']['exception'][] = $error;
        try {
            // Throw Exception and bubble up to next handler
            if (! isset($errStack['callable']) || false === call_user_func($errStack['callable'], $error)) {
                self::handleDone();
                throw $error;
            }
        } catch (\Throwable $e) {
            self::handleException($e);
        }

        self::handleDone();
        return true;
    }

    /**
     * @internal
     *
     * Handle Fatal Errors
     *
     * @throws \Throwable
     */
    protected static function handleFatalError()
    {
        if (0 === count(self::$handlerStack))
            return;

        if (null === $errStack = self::$handlerStack[count(self::$handlerStack)-1])
            return;

        $error = error_get_last();
        if (! is_array($error))
            return;

        if ( $error['type'] !== (self::E_FatalError & $error['type'])
             || self::E_FatalError !== (self::E_FatalError & $errStack['control_bits'])
        )
            return false;


        $error = self::makeErrorExceptionFromPhpError($error['type'], $error['message'], $error['file'], $error['line']);
        try {
            self::handleException($error); // if not handled already by exception handler we catch those here
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Check weather given error control is meant to handle throwable or not;
     * if yes it would remove the control bits from errorType into the passed variable
     *
     * @param int $errType
     * @param null $errTypeWithoutThrowable
     *
     * @return bool
     */
    protected static function assertErrorTypeContainHandleThrowable(int $errType, &$errTypeWithoutThrowable = null)
    {
        if ( E_ERROR !== (E_ERROR & $errType)
            && E_RECOVERABLE_ERROR !== (E_RECOVERABLE_ERROR & $errType)
            && self::E_Exceptions !== (self::E_Exceptions & $errType)
        )
            return false;


        if ($errTypeWithoutThrowable)
            // remove Throwable bit controls from given errorType
            $errTypeWithoutThrowable = $errType & ~self::E_FatalError;

        return true;
    }

    /**
     * Check weather given error control has consist of ScreamAll bit mask
     * if yes it would remove the control bits from errorType into the passed variable
     *
     * @param int  $errType
     * @param null $errTypeWithoutScreamAll
     *
     * @return bool
     */
    protected static function assertErrorTypeContainScreamAllErrors(int $errType, &$errTypeWithoutScreamAll = null)
    {
        if (self::E_ScreamAll !== (self::E_ScreamAll & $errType))
            return false;


        if ($errTypeWithoutScreamAll)
            // remove ScreamAll bit controls from given errorType
            $errTypeWithoutScreamAll = $errType & ~self::E_ScreamAll;

        return true;
    }

    /**
     * @internal
     *
     * Make an error exception from php error arguments
     *
     * @return ErrorException
     */
    protected static function makeErrorExceptionFromPhpError(int $type, string $message, string $file, int $line): \ErrorException
    {
        return new ErrorException($message, 0, $type, $file, $line);
    }

    private static function safeRegisterExceptionHandler()
    {
        $prevHandler = set_exception_handler(function(\Throwable $exception) {
            self::handleException($exception);
        });

        // Check the previous one is same handler?
        if ($prevHandler instanceof \Closure) {
            $reflectionFunction = new \ReflectionFunction($prevHandler);
            $scopedClass = $reflectionFunction->getClosureScopeClass();
            if ( $scopedClass && $scopedClass->name == __CLASS__ ) {
                restore_exception_handler();
                return true; // safely replaced
            }
        }

        return $prevHandler;
    }

    private static function safeRegisterErrorHandler()
    {
        $prevHandler = set_error_handler(function(int $type, string $message, string $file, int $line) {
            return self::handleError($type, $message, $file, $line);
        }, E_ALL);

        // Check the previous one is same handler?
        if ($prevHandler instanceof \Closure) {
            $reflectionFunction = new \ReflectionFunction($prevHandler);
            $scopedClass = $reflectionFunction->getClosureScopeClass();
            if ( $scopedClass && $scopedClass->name == __CLASS__ ) {
                restore_error_handler();
                return true; // safely replaced
            }
        }

        return $prevHandler;
    }

    protected static function prepareErrorTrace(\ErrorException $error): \ErrorException
    {
        $trace = debug_backtrace();
        $trace = array_slice($trace, 3);

        $traceReflector = new \ReflectionProperty('Exception', 'trace');
        $traceReflector->setAccessible(true);

        $traceReflector->setValue($error, $trace);

        return $error;
    }
}
