<?php
namespace Poirot\Std\Validator;

use Poirot\Std\Exceptions\Validator\ValidationError;
use Poirot\Std\Interfaces\Pact\ipValidator;
use function Poirot\Std\flatten;

trait tValidator
{
    /**
     * Do Assertion Validate and Return An Array Of Errors
     *
     * @param array $exceptions Add validation errors here
     *
     * @return ValidationError[]|void
     */
    abstract protected function doAssertValidate(&$exceptions);


    /**
     * Assert Validate Entity
     * @see ipValidator
     *
     * @return void
     * @throws ValidationError|\Exception
     */
    final function assertValidate(): void
    {
        $exceptions = [];
        if ($tExceptions = $this->doAssertValidate($exceptions))
            $exceptions = $tExceptions;

        if (! is_array($exceptions) )
            throw new \Exception(sprintf(
                'Unknown Assertion Value returned Of Type (%s).'
                , \Poirot\Std\flatten($exceptions)
            ));

        if (empty($exceptions))
            return;


        // Chain Exception:
        //
        throw $this->wrapExceptionsIntoErrorObject($exceptions);
    }

    // ..

    protected function wrapExceptionsIntoErrorObject(array $exceptions) : ?ValidationError
    {
        /** @var ValidationError $exception */
        if (null === $validationError = array_pop($exceptions))
            return null;


        if (! $validationError instanceof \Throwable)
            throw new \RuntimeException(sprintf(
                'Validation expect to get ValidationError instance got: (%s).'
                , flatten($validationError)
            ));

        if (! $validationError instanceof ValidationError)
            // When exception is not Validation Error, create an unknown error.
            $validationError = $this->createError(
                $validationError->getMessage(),
                ValidationError::UnknownError
            );

        // Because Exception interface doesn't allow to set previous we create it again
        $validationError = $this->createError(
            $validationError->getMessage(),
            $validationError->getError(),
            $validationError->getParameter(),
            $validationError->getValue(),
            $this->wrapExceptionsIntoErrorObject($exceptions)
        );

        return $validationError;
    }

    /**
     * Create Exception
     *
     * @param string $message
     * @param string $errType
     * @param null $parameter
     * @param mixed|VOID $value
     * @param ValidationError|null $previous
     *
     * @return ValidationError
     */
    protected function createError(
        $message = '',
        $errType = ValidationError::Error,
        $parameter = null,
        $value = null,
        ?ValidationError $previous = null
    ) {
        $validationErr = new ValidationError(
            $message,
            $errType,
            $parameter,
            $value,
            $previous
        );

        return $validationErr;
    }
}
