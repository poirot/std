<?php
namespace Poirot\Std\Validator\MessageProcessor;

use Poirot\Std\Interfaces\Validator\iMessageProcessor;
use Poirot\Std\Type\StdString;


class DataInterchange
    implements iMessageProcessor
{
    const PlaceHolderValue = '%value%';
    const PlaceHolderLabel = '%param%';


    function getProcessedErrMessage($message, string $param = null, $value = null): string
    {
        // Replace Value Placeholder
        //
        if ( StdString::of($message)->isContains('%value%') )
        {
            if (is_object($value))
                $value = get_class($value) . ' Object';
            if (is_object($value) && method_exists($value, '__toString'))
                $value = (string) $value;
            else
                $value = var_export($value, 1);

            $message = str_replace('%value%', $value, $message);
        }


        // Replace Parameter Name Placeholder
        //
        $message = str_replace("%param%", $param ?? 'unknown parameter', $message);

        return $message;
    }
}
