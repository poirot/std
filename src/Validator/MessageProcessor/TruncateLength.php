<?php
namespace Poirot\Std\Validator\MessageProcessor;

use Poirot\Std\Interfaces\Validator\iMessageProcessor;


class TruncateLength
    implements iMessageProcessor
{
    /**
     * Limits the maximum returned length of an error message
     * @var int
     */
    protected $messageLength = -1;


    /**
     * Constructor.
     *
     * @param int $messageLength
     */
    function __construct(int $messageLength)
    {
        $this->messageLength = $messageLength;
    }


    function getProcessedErrMessage($message, string $param = null, $value = null): string
    {
        $length = $this->messageLength;
        if ( ($length > -1) && (strlen($message) > $length) )
            $message = substr($message, 0, ($length - 3)) . '...';

        return $message;
    }
}
