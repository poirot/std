<?php
namespace Poirot\Std;

use Poirot\Std\Interfaces\InvokableResponder\iResponderResult;
use Poirot\Std\InvokableResponder\MergeResult;
use Poirot\Std\Type\StdArray;
use function Poirot\Std\Invokable\resolveCallable;

final class ResponderChain
{
    /** @var ResponderChain */
    protected $prevLinked;
    /** @var ResponderChain */
    protected $nextLinked;
    /** @var int */
    protected $stackCounter = 0;

    /** @var callable */
    protected $callable;
    /** @var callable */
    protected $onFailure;
    /** @var array */
    protected $defaultParams = [];

    /**
     * Create New Invokable Responder Instance An Can Bind Optionally
     * All Callables To An External Object
     *
     * @param iterable|null $defaultParams
     *
     * @return ResponderChain
     */
    static function new(iterable $defaultParams = [])
    {
        return (new static)
            ->withDefaultParams($defaultParams);
    }

    /**
     * Constructor.
     * Declared private, as we have no need for instantiation.
     */
    private function __construct()
    { }

    /**
     * Proxy to call method
     *
     * @param iterable|iResponderResult $params Result from previous invokable
     *
     * @return array|iResponderResult|mixed
     * @throws \Exception
     */
    function __invoke(iterable $params = [])
    {
        return $this->handle($params);
    }

    /**
     * Invoke Chain Responder From First Invokable as Root to Current Invokable
     *
     * @param iterable|iResponderResult|null $params Result from previous invokable
     *
     * @return array|iResponderResult|mixed
     * @throws \Exception
     */
    function handle(iterable $params = [])
    {
        if (! empty($this->defaultParams))
            $params = StdArray::of($params)->merge($this->defaultParams)->asInternalArray;


        $lastResult = null;
        $invokable  = $this->getRootInvokable();
        while ($invokable = $invokable->hasNestLink())
        {
            $prevResult = $lastResult;

            // Prepare parameters chain from previous stage to current
            //
            if (isset($tParams)) {
                if (! is_array($lastResult))
                    $tParams = [$lastResult];

                $tParams['last_result'] = $lastResult;
                $params = array_merge($params, $tParams);
            }

            try {
                $lastResult = resolveCallable($invokable->callable, $params)();
            } catch (\Exception $e) {
                if ($this->onFailure) {
                    $params['exception'] = $e;
                    $params['e'] = $e;
                    return resolveCallable($this->onFailure, $params)();
                }

                throw $e;
            }

            // Create Result
            //
            if (is_array($lastResult) || $lastResult instanceof StdArray) {
                $lastResult = new MergeResult($lastResult);
            }

            while ($lastResult instanceof iResponderResult) {
                $lastResult->setLastState($prevResult);
                $lastResult = $lastResult->getPreparedResult();
            }

            // Parameters for next stage
            $tParams = $lastResult;
        }

        return $lastResult;
    }

    /**
     * Add New Callable To Stack Chain
     *
     * @param callable            $callable
     * @param callable|null|false $onFailureCallable
     *
     * @return ResponderChain if callable not given
     * @throws \Exception
     */
    function then(callable $callable, callable $onFailureCallable = null)
    {
        $new = $this
            ->withDefaultParams($this->defaultParams)
            ->onFailure($onFailureCallable)
            ->withNestTo($this);

        $new->callable = $callable;
        return $new;
    }

    /**
     * Attach to invokable responder
     *
     * @param ResponderChain $responder
     *
     * @return $this
     */
    function withNestTo(ResponderChain $responder)
    {
        $new = clone $this;
        $new->prevLinked = $responder;
        $new->stackCounter = $responder->stackCounter++;

        $responder->nextLinked = $new;

        return $new;
    }

    /**
     * Set Default Params
     *
     * @param iterable $defaultParams
     *
     * @return ResponderChain
     */
    function withDefaultParams(iterable $defaultParams)
    {
        $new = clone $this;
        $new->defaultParams = StdArray::of($defaultParams)->asInternalArray; # ensure that it's array
        return $new;
    }

    /**
     * Get Parent Link If Has
     *
     * @return ResponderChain|null
     */
    function hasParentLink()
    {
        return $this->prevLinked;
    }

    /**
     * Get Nested Link If Has
     *
     * @return ResponderChain|null
     */
    function hasNestLink()
    {
        return $this->nextLinked;
    }

    /**
     * Handle Exceptions
     *
     * callable:
     *  mixed function(\Exception $exception, InvokableResponder $self)
     *
     * @param callable|false $callable Given false will remove current failure callback
     *
     * @return $this
     * @throws \Exception
     */
    function onFailure(?callable $callable)
    {
        if ($callable === null)
        {
            // Disable On Failure Handler
            $this->onFailure = $callable;
            return $this;
        }


        if (! is_callable($callable))
            throw new \Exception(sprintf(
                'The argument given is not callable; given: (%s).'
                , \Poirot\Std\flatten($callable)
            ));

        $this->onFailure = $callable;
        return $this;
    }

    // ..

    protected function getRootInvokable(): self
    {
        $root = $this;
        while ($parent = $root->hasParentLink())
            $root = $parent;

        return $root;
    }
}
