<?php
namespace Poirot\Std\CriteriaLexer;

use Poirot\Std\Interfaces\Pact\ipFactory;
use Poirot\Std\Type\StdTraversable;
use function Poirot\Std\Type\StdString\isStringify;


final class ParsedCriteria
    implements ipFactory
{
    const RegexMatchExact  = 1;
    const RegexMatchPrefix = 3;
    const RegexPattern     = 5;

    protected $parsedCriteria;


    /**
     * Factory With Valuable Parameter
     *
     * @param mixed $valuable
     *
     * @throws \Exception
     * @return ParsedCriteria
     */
    static function of($valuable)
    {
        if (! is_array($valuable) )
            throw new \InvalidArgumentException(sprintf(
                'Value Must Be Array; given(%s).'
                , \Poirot\Std\flatten($valuable)
            ));


        $self = new static($valuable);
        return $self;
    }


    /**
     * ParsedCriteria constructor.
     *
     * @param array $parsedCriteria
     */
    protected function __construct(array $parsedCriteria)
    {
        $this->parsedCriteria = $parsedCriteria;
    }


    /**
     * Parsed Criteria
     *
     * @return array
     */
    function asArray()
    {
        return $this->parsedCriteria;
    }

    /**
     * Build the matching regex from parsed parts.
     *
     * - then can match using:
     *   $result = @preg_match($thisRegex, $against, &$matches);
     *
     * @param int $matchExact
     *
     * @return string
     */
    function asRegex($matchExact = self::RegexPattern)
    {
        return $this->_buildRegexFromParsed($this->parsedCriteria, $matchExact);
    }

    /**
     * Build String Representation From Given Parts With Given Params
     *
     * @param array|\Traversable $params
     *
     * @return string
     * @throws \InvalidArgumentException|\Exception
     */
    function asString($params = [])
    {
        $parts = $this->parsedCriteria;
        return $this->_buildStringFromParsed($parts, $params);
    }

    // ..

    /**
     * Build the matching regex from parsed parts.
     *
     * @param array $parts
     * @param bool $matchExact
     *
     * @return string
     */
    protected function _buildRegexFromParsed(array $parts, $matchExact = null)
    {
        $regex = '';

        // [0 => ['_literal_' => 'localhost'], 1=>['_optional' => ..] ..]
        foreach ($parts as $parsed) {
            $definition_name  = key($parsed);
            $definition_value = $parsed[$definition_name];
            // $parsed can also have extra parsed data options
            // _parameter_ String(3) => tld \
            // tld String(4)         => .com
            switch ($definition_name) {
                case '_regex_':
                    $regex .= $definition_value;
                    break;
                case '_token_':
                case '_literal_':
                    $regex .= preg_quote($definition_value);
                    break;

                case '_parameter_':
                    $groupName = '?P<' . $definition_value . '>';

                    if ( isset($parsed[$definition_value]) )
                        // Delimiter: localhost:port{{\d+}}
                        $regex .= '(' . $groupName . $parsed[$definition_value] . ')';
                    else
                        $regex .= '(' . $groupName . '[^.]+)';

                    break;

                case '_optional_':
                    $regex .= '(?:' . $this->_buildRegexFromParsed($definition_value, self::RegexPattern) . ')?';
                    break;
            }
        }

        if ($matchExact != self::RegexPattern) {
            $regex = ( $matchExact == self::RegexMatchExact )
                ? "(^{$regex}$)" ## exact match
                : "(^{$regex})"; ## only start with criteria "/pages[/other/paths]"
        }

        return $regex;
    }

    /**
     * Build String Representation From Parsed Criteria With Params
     *
     * @param array $parts
     * @param iterable $params
     * @param bool $isOptional recursive function call for optional part
     *
     * @return string
     * @throws \InvalidArgumentException|\Exception
     */
    protected function _buildStringFromParsed(array $parts, iterable $params = [], $isOptional = false)
    {
        if ($params instanceof \Traversable)
            $params = StdTraversable::of($params)->toArray();


        ## Check For Presented Values in Optional Segment
        #
        // consider this "/[@:username~\w+~][-:userid~\w+~]"
        // if username not presented as params injected then first part include @ as literal
        // not rendered.
        // optional part only render when all parameter is present
        if ($isOptional) {
            foreach ($parts as $parsed) {
                if (! isset($parsed['_parameter_']) )
                    continue;

                ## need parameter
                $neededParameterName = $parsed['_parameter_'];
                if (! (isset($params[$neededParameterName]) && $params[$neededParameterName] !== '') )
                    return '';
            }
        }


        $return = '';
        // [0 => ['_literal_' => 'localhost'], 1=>['_optional' => ..] ..]
        foreach ($parts as $parsed) {
            $definition_name  = key($parsed);
            $definition_value = $parsed[$definition_name];
            // $parsed can also have extra parsed data options
            // _parameter_ String(3) => tld \
            // tld String(4)         => .com
            switch ($definition_name)
            {
                case '_literal_':
                    $return .= $definition_value;
                    break;

                case '_parameter_':
                    if (! isset($params[$definition_value]) ) {
                        if ($isOptional)
                            return '';

                        throw new \InvalidArgumentException(sprintf(
                            'Missing parameter (%s).'
                            , $definition_value
                        ));
                    }

                    if (! isStringify($params[$definition_value]) )
                        throw new \Exception(sprintf(
                            'Parameter %s is not stringify; given: (%s).'
                            , $definition_value
                            , \Poirot\Std\flatten($params[$definition_value])
                        ));

                    if ( isset($parsed[$definition_value]) ) {
                        // regex part of parameter exp. ~\d+~ is given
                        $regex = $parsed[$definition_value];
                        if (! preg_match("(^{$regex}$)", $params[$definition_value]) )
                            throw new \InvalidArgumentException(sprintf(
                                'value "%s" given for (%s) dose not match the defined "%s".',
                                $params[$definition_value],
                                $definition_value,
                                $regex
                            ));
                    }

                    $return .= $params[$definition_value];
                    break;

                case '_optional_':
                    $optionalPart = $this->_buildStringFromParsed($definition_value, $params, true);
                    if ($optionalPart !== '')
                        $return .= $optionalPart;

                    break;
            }
        }

        return $return;
    }
}
