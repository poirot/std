<?php
namespace Poirot\Std\CriteriaLexer;


class Criteria
{
    const LiteralAlphabet  = 'A-Za-z0-9_';
    const LiteralPrintable = '/@\+\-\.';
    const LiteralDefault   = self::LiteralAlphabet . self::LiteralPrintable;

    const Tokens = [
        'escapeChar' => '\\',
        'variable' => ':',
        'regularExpr' => '~',
        'optional' => '<>'
    ];
    
    /** @var string */
    protected $criteria;
    /** @var string */
    protected $literalChars;

    
    /**
     * Tokenize Parse String Definition Into Parts
     *
     * @param string $criteria
     * @param string $literalChars Quoted preg_quote
     *
     * @throws \InvalidArgumentException
     */
    function __construct(
        string $criteria, 
        string $literalChars = self::LiteralDefault
    ) {
        if ('' === $criteria)
            throw new \InvalidArgumentException('The Criteria that given is empty.');

        $this->criteria     = $criteria;
        $this->literalChars = $literalChars;
    }
    

    /**
     * Parse Criteria
     *
     * @return ParsedCriteria
     * @throws \Exception
     */
    function parse()
    {
        $LITERAL = $this->getLiteralChars();
        $TOKENS  = preg_quote( implode('', static::Tokens) );

        $criteria   = $this->getCriteria();
        $currentPos = 0;
        $length     = strlen($criteria);
        $parts      = [];
        $levelParts = [&$parts];
        $level      = 0;

        while ($currentPos < $length)
        {
            preg_match("(\G(?P<_literal_>[$LITERAL]*)(?P<_token_>[$TOKENS]|$))"
                , $criteria
                , $matches
                , 0
                , $currentPos
            );

            if ( empty($matches) )
                break;

            if (! empty($matches['_literal_']) ) {
                $levelParts[$level][] = ['_literal_' => $matches['_literal_']];
                $currentPos += strlen($matches['_literal_']);
            }

            ## Deal With Token(s):
            #
            if ( !isset($matches['_token_']) || $matches['_token_'] == '' )
                // There is no token matched yet! parse rest of criteria.
                continue;

            $currentPos += strlen($matches['_token_']);
            $Token = $matches['_token_'];

            ## Regular Expression Token
            #
            if ($Token == '~') {
                // ^ match any character that is not in list
                $pmatch = preg_match("/~(?P<_delimiter_>[^~]+)~/"
                    , $criteria
                    , $matches
                    , 0
                    , 0
                );
                if (! $pmatch )
                    throw new \RuntimeException('Miss usage of ~ ~ token.');

                $val['_regex_'] = $matches['_delimiter_'];
                $levelParts[$level][] = $val;

                $currentPos += strlen($matches[0]) + 2; # ~x~ 2 for opening and closing ~
            }

            ##
            # Variable
            # /:hash_id~\w{24}~
            #
            elseif ($Token == ':') {
                // ^$TOKENS  match any character that is not in list
                $pmatch = preg_match("(\G(?P<_name_>[^$TOKENS]+)(?:~(?P<_delimiter_>[^~]+)~)?:?)"
                    , $criteria
                    , $matches
                    , 0
                    , $currentPos
                );

                if (! $pmatch && !(isset($matches['_name_']) && isset($matches['_delimiter_'])) )
                    throw new \RuntimeException(
                        'Found empty parameter name or delimiter on (%s).'
                        , $criteria
                    );

                $parameter = $matches['_name_'];
                $val       = ['_parameter_' => $parameter];
                if (isset($matches['_delimiter_']))
                    $val[$parameter] = $matches['_delimiter_'];

                $levelParts[$level][] = $val;
                $currentPos += strlen($matches[0]);
            }

            ## EscapeChar
            #  localhost\:port
            #
            elseif ($Token == '\\') {
                // Consider next character as Literal
                $levelParts[$level][] = ['_literal_' => $criteria[$currentPos]];
                $currentPos++;
            }

            ## Optional Part
            #  /<u/:username~[a-zA-Z0-9._]+~><-:userid~\w{24}~>
            #
            elseif ($Token == '<') {
                if (! isset($va) ) {
                    $va = [];
                    $n = 0;
                }

                $n++;
                $va[$n] = [];
                $levelParts[$level][] = ['_optional_' => &$va[$n]];
                $level++;
                $levelParts[$level] = &$va[$n];
            }
            ## Close Optional ##
            elseif ($Token === '>') {
                unset($levelParts[$level]);
                $level--;

                if ($level < 0)
                    throw new \RuntimeException('Found closing bracket without matching opening bracket');
            }

            else
                // Recognized unused token return immanently
                $levelParts[$level][]   = ['_token_' => $Token];

        } // end while

        if ($level > 0)
            throw new \RuntimeException('Found unbalanced brackets');


        return ParsedCriteria::of($parts);
    }
    
    // Options:

    /**
     * New Instance with given characters literal
     *
     * @param string $characters
     *
     * @return Criteria
     */
    function withLiteralChars(string $characters)
    {
        $new = clone $this;
        $new->literalChars = $characters;
        return $new;
    }

    /**
     * Get Criteria
     * 
     * @return string
     */
    function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * Get Characters Known as Literal
     * 
     * @return string
     */
    function getLiteralChars()
    {
        return $this->literalChars;
    }
}
