<?php
namespace Poirot\Std;


class IteratorWrapper
    implements \Iterator
{
    protected $innerIterator;
    protected $callable;

    /** @var bool */
    protected $isStopPropagation;
    /** @var bool */
    protected $isSkipped;
    /** @var bool */
    protected $isAttemptedIteration = false;
    /** @var \Generator */
    protected $_wrapperGenerator;
    /** @var \ArrayIterator */
    protected $_bufferIterator;


    /**
     * Constructor
     *
     * @param iterable|\Generator $iterator
     * @param \Closure $filterCallable mixed func(value, &index)
     */
    function __construct($iterator, \Closure $filterCallable)
    {
        if (!is_iterable($iterator) && !$iterator instanceof \Generator)
            throw new \InvalidArgumentException(sprintf(
                'Given argument should be iterator or instance of Generator; but %s given.',
                is_object($iterator) ? get_class($iterator) : gettype($iterator)
            ));

        $this->innerIterator = $iterator;
        $this->callable      = $filterCallable->bindTo($this, static::class);
    }

    /**
     * Skip Current Iteration To Next
     *
     * @return $this
     */
    function skipCurrentIteration()
    {
        $this->isSkipped = true;
        return $this;
    }

    /**
     * Stop Propagation
     *
     * @return $this
     */
    function stopAfterThisIteration()
    {
        $this->isStopPropagation = true;
        return $this;
    }

    /**
     * Is Stopped?
     *
     * @return boolean
     */
    function isIterationStopped()
    {
        return $this->isStopPropagation;
    }

    // Implement Iterator

    /**
     * @inheritDoc
     */
    function current()
    {
        if (! $this->isAttemptedIteration )
            $this->rewind();

        return $this->_getBufferIterator()->current();
    }

    /**
     * @inheritDoc
     */
    function next()
    {
        $buffer = $this->_getBufferIterator();
        $buffer->next();

        if ( false === $buffer->valid() ) {
            // buffer end; fill with new values from inner iterator
            $this->_getInnerIterator()->next();
            $this->_loadToBuffer();
        }
    }

    /**
     * @inheritDoc
     */
    function key()
    {
        if (! $this->isAttemptedIteration )
            $this->rewind();

        return $this->_getBufferIterator()->key();
    }

    /**
     * @inheritDoc
     */
    function valid()
    {
        if (! $this->isAttemptedIteration )
            $this->rewind();

        return $this->_getBufferIterator()->valid()
            && !$this->isIterationStopped();
    }

    /**
     * @inheritDoc
     */
    function rewind()
    {
        $buffer = $this->_getBufferIterator();
        if (! $this->isAttemptedIteration ) {
            $this->isAttemptedIteration = true;
            $this->_loadToBuffer();

            return;
        }

        $buffer->rewind();
    }

    // ..

    protected function _loadToBuffer()
    {
        $buffer = $this->_getBufferIterator();
        $innerIterator = $this->_getInnerIterator();
        if ( $innerIterator->valid() ) {
            $this->isSkipped = null;
            $key = $innerIterator->key();
            $value = $innerIterator->current();
            $value = ($this->callable)($value, $key);
            if ($this->isSkipped || $key === null) {
                $this->next();
                return;
            }

            $buffer->offsetSet($key, $value);
        }
    }

    protected function _getInnerIterator(): \Generator
    {
        if ($this->_wrapperGenerator)
            return $this->_wrapperGenerator;

        return $this->_wrapperGenerator = (function () {
            foreach ($this->innerIterator as $index => $value) {
                $key = &$index;
                yield $key => $value;
            }
        })();
    }

    protected function _getBufferIterator(): \ArrayIterator
    {
        if (! $this->_bufferIterator )
            $this->_bufferIterator = new \ArrayIterator;

        return $this->_bufferIterator;
    }
}
