<?php
namespace Poirot\Std\InvokableResponder;

use Poirot\Std\Type\StdArray;

class MergeResult
    extends AggregateResult
{
    /**
     * Get Data
     *
     * @return mixed
     */
    function getPreparedResult()
    {
        $data = parent::getPreparedResult();

        if (null === $lastResult = $this->lastState)
            return $data;

        if (! is_array($lastResult)) {
            if ($lastResult instanceof StdArray)
                $lastResult = $lastResult->asInternalArray;
            else
                $lastResult = [$lastResult];
        }

        return StdArray::of($lastResult)->merge($data)->asInternalArray;
    }
}
