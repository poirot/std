<?php
namespace Poirot\Std\InvokableResponder;

use Poirot\Std\Interfaces\InvokableResponder\iResponderResult;

abstract class aResponderResult
    implements iResponderResult
{
    protected $lastState;

    /**
     * Set Last State By Last Responder Result
     *
     * @param $result
     *
     * @return aResponderResult
     */
    function setLastState($result)
    {
        $this->lastState = $result;
        return $this;
    }
}
