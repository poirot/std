<?php
namespace Poirot\Std\InvokableResponder;

use Poirot\Std\Interfaces\InvokableResponder\iResponderResult;
use Poirot\Std\Type\StdArray;

class AggregateResult
    extends aResponderResult
{
    protected $data;

    /**
     * Constructor.
     *
     * @param array $data
     */
    function __construct(...$data)
    {
        $this->data = $data;
    }

    /**
     * Get Data
     *
     * @return mixed
     */
    function getPreparedResult()
    {
        $result = [];
        foreach ($this->data as $data) {
            if ($data instanceof iResponderResult)
                $data = $data->getPreparedResult();

            $data = (is_array($data) || $data instanceof StdArray) ? $data : [$data]; # let data merge
            $data = StdArray::of($data)->filter(function ($value) {
                while ($value instanceof iResponderResult)
                    $value = $value->getPreparedResult();
                return $value;

            })->asInternalArray;

            $result = StdArray::of($result)
                ->merge($data)
                ->asInternalArray;
        }

        return $result;
    }
}
