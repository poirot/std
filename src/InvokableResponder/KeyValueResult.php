<?php
namespace Poirot\Std\InvokableResponder;

class KeyValueResult
    extends aResponderResult
{
    private $data;
    private $identity;

    /**
     * Constructor.
     *
     * @param string $identity
     * @param mixed  $data
     */
    function __construct(string $identity, $data)
    {
        $this->data = $data;
        $this->identity = $identity;
    }

    /**
     * Get Data
     *
     * @return mixed
     */
    function getPreparedResult()
    {
        return [
            $this->identity => $this->data
        ];
    }
}
