<?php
namespace Poirot\Std\ArgumentsResolver;

use function Poirot\Std\Invokable\makeReflectFromCallable;


class CallableResolver
    extends aResolver
{
    /** @var callable */
    protected $callable;

    /**
     * Constructor
     *
     * @param callable $callable
     * @param array $defaultOptions
     */
    function __construct(callable $callable, array $defaultOptions = [])
    {
        $this->callable = $callable;
        parent::__construct($defaultOptions);
    }

    /**
     * Get Reflection Method To Argument Resolver
     *
     * @return \ReflectionFunction|\ReflectionMethod
     */
    function getReflectionMethod()
    {
        return makeReflectFromCallable($this->callable);
    }

    /**
     * Make call or instantiate reflection method by given arguments
     *
     * @param array $arguments
     *
     * @return \Closure
     */
    function resolve(array $arguments)
    {
        $callbackResolved = function() use ($arguments) {
            return call_user_func_array($this->callable, $arguments);
        };

        return $callbackResolved;
    }
}
