<?php
namespace Poirot\Std\ArgumentsResolver;


class ReflectionResolver
    extends aResolver
{
    /** @var \ReflectionFunctionAbstract */
    protected $reflection;


    /**
     * Constructor
     *
     * @param \ReflectionFunctionAbstract $reflectionMethod
     * @param array $defaultOptions
     */
    function __construct(\ReflectionFunctionAbstract $reflectionMethod, array $defaultOptions = [])
    {
        $this->reflection = $reflectionMethod;
        parent::__construct($defaultOptions);
    }


    /**
     * Get Reflection Method To Argument Resolver
     *
     * @return \ReflectionFunction|\ReflectionMethod|\ReflectionFunctionAbstract
     */
    function getReflectionMethod()
    {
        return $this->reflection;
    }

    /**
     * Make call or instantiate reflection method by given arguments
     *
     * @param array $arguments
     *
     * @return \Closure
     * @throws \Exception
     */
    function resolve(array $arguments)
    {
        if ($this->getReflectionMethod() instanceof \ReflectionMethod)
            throw new \RuntimeException('Invoke cant called on reflections method.');


        $callbackResolved = function() use ($arguments) {
            return $this->getReflectionMethod()->invokeArgs($arguments);
        };

        return $callbackResolved;
    }
}
