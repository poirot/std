<?php
namespace Poirot\Std\ArgumentsResolver;

use function Poirot\Std\Invokable\makeReflectFromCallable;


class InstantiatorResolver
    extends aResolver
{
    /** @var object|string */
    protected $class;
    /** @var \ReflectionFunction|\ReflectionFunctionAbstract|\ReflectionMethod */
    protected $reflection;


    /**
     * Constructor
     *
     * @param mixed $className Either a string containing the name of the class to reflect, or an object
     * @param array $defaultOptions
     *
     * @throws \InvalidArgumentException
     */
    function __construct(string $className, array $defaultOptions = [])
    {
        if (! class_exists($className))
            throw new \InvalidArgumentException(sprintf('Invalid class name "%s" is given.', $className));

        $this->class = $className;
        parent::__construct($defaultOptions);
    }

    /**
     * Get Reflection Method To Argument Resolver
     *
     * @return \ReflectionFunction|\ReflectionMethod
     * @throws
     */
    function getReflectionMethod()
    {
        if ($this->reflection)
            return $this->reflection;


        $classReflection = new \ReflectionClass($this->class);
        if (null === $reflectionMethod = $classReflection->getConstructor()) {
            $reflectionMethod = makeReflectFromCallable(function () {
                return new $this->class;
            });
        }

        return $this->reflection = $reflectionMethod;
    }

    /**
     * Make call or instantiate reflection method by given arguments
     *
     * @param array $arguments
     *
     * @return \Closure
     */
    function resolve(array $arguments)
    {
        $refConstructor = $this->getReflectionMethod();
        if ($refConstructor->getName() == '__construct') {
            $classReflection = $refConstructor->getDeclaringClass();
            $instance = $classReflection->newInstanceArgs($arguments);
        } else {
            $instance = $refConstructor->invoke($arguments);
        }

        return $instance;
    }
}
