<?php
namespace Poirot\Std\ArgumentsResolver;


abstract class aResolver
{
    /** @var array */
    protected $defaultOptions = [];


    /**
     * Constructor.
     *
     * @param array $defaultOptions
     */
    function __construct(array $defaultOptions = [])
    {
        $this->defaultOptions = $defaultOptions;
    }

    /**
     * Get Reflection Method To Argument Resolver
     *
     * @return \ReflectionFunction|\ReflectionMethod
     */
    abstract function getReflectionMethod();

    /**
     * Make call or instantiate reflection method by given arguments
     *
     * @param array $arguments
     *
     * @return mixed
     */
    abstract function resolve(array $arguments);

    /**
     * Resolver Default Options which get merged into argument resolver options
     *
     * @return array
     */
    function getDefaultOptions(): array
    {
        return $this->defaultOptions;
    }
}
