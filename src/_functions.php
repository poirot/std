<?php
namespace
{
    !defined('DS')   and define('DS', DIRECTORY_SEPARATOR);

    /**
     * Return Unmodified Argument
     *
     * usage:
     *
     *    __(new Classes())->callMethods();
     *
     * @param mixed $var
     * @return mixed
     */
    function __($var)
    {
        return $var;
    }
}

namespace Poirot\Std\Type\StdString
{
    /**
     * Byte order mark
     * INFO: https://en.wikipedia.org/wiki/Byte_order_mark
     */
    const BomUtf8               = "\xef\xbb\xbf";     // UTF-8
    const BomUtf8_Len           = 3;                  // Character length
    const BomUtf8Win1252        = 'ï»¿';              // UTF-8 as "WINDOWS-1252"
    const BomUtf8Win1252_Len    = 6;                  //

    const BomUtf16Be            = "\xfe\xff";         // UTF-16 (Big-endian)
    const BomUtf16Be_Len        = 2;                  //
    const BomUtf16BeWin1252     = 'þÿ';               // UTF-16 (Big-endian) as "WINDOWS-1252"
    const BomUtf16BeWin1252_Len = 4;                  //

    const BomUtf16Le            = "\xff\xfe";         // UTF-16 (Little-endian)
    const BomUtf16Le_Len        = 2;                  //
    const BomUtf16LeWin1252     = 'ÿþ';               // UTF-16 (Little-endian) as "WINDOWS-1252"
    const BomUtf16LeWin1252_Len = 4;                  //

    const BomUtf32Be            = "\x00\x00\xfe\xff"; // UTF-32 (Big-endian)
    const BomUtf32Be_Len        = 4;                  //
    const BomUtf32BeWin1252     = '  þÿ';             // UTF-32 (Big-endian) as "WINDOWS-1252"
    const BomUtf32BeWin1252_Len = 6;                  //

    const BomUtf32Le            = "\xff\xfe\x00\x00"; // UTF-32 (Little-endian)
    const BomUtf32Le_Len        = 4;                  //
    const BomUtf32LeWin1252     = 'ÿþ  ';             // UTF-32 (Little-endian) as "WINDOWS-1252"
    const BomUtf32LeWin1252_Len = 6;                  //


    /**
     * Check Variable/Object Is String
     *
     * @param mixed $var
     *
     * @return bool
     */
    function isStringify($var)
    {
        return ( (!is_array($var)) && (
                (!is_object($var) && @settype($var, 'string') !== false) ||
                (is_object($var)  && method_exists($var, '__toString' ))
            ));
    }

    /**
     * Check if the string is "printable"
     *
     * @param string $str
     *
     * @return bool
     */
    function isPrintable($str) : bool
    {
        return ctype_print( (string) $str );
    }

    /**
     * Is Contains UTF-8 Encoding?
     *
     * @param string $str
     *
     * @return bool
     */
    function isUTF8($str)
    {
        $string = (string) $str;
        return $string === '' || preg_match('/^./su', $string) == 1;
    }

    /**
     * Get a binary representation of a specific string.
     *
     * @param string $str The input string.
     *
     * @return false|string false on error
     */
    function toBinary(string $str)
    {
        $value = \unpack('H*', $str);
        if ($value === false)
            return false;

        return \base_convert($value[1], 16, 2);
    }

    /**
     * Convert binary into a string.
     *
     * @param mixed $bin 1|0
     *
     * @return string
     */
    function binaryToString($bin): string
    {
        if (! isset($bin[0]) )
            return '';

        $convert = \base_convert($bin, 2, 16);
        if ($convert === '0')
            return '';

        return \pack('H*', $convert);
    }

    /**
     * Remove None Printable Characters From String
     *
     * @param string $str
     *
     * @return string
     */
    function removeNonPrintableCharacters(string $str)
    {
        return preg_replace('/(\p{C}||\p{M})+/u', "", $str);
    }

    /**
     * Replaces HTML tag "<br>" with new line
     *
     * @param $htmlString
     *
     * @return string
     */
    function replaceBrTagToNewLine(string $htmlString)
    {
        return preg_replace('#<br\s*/?>#i', "\n", $htmlString);
    }

    // TODO Detect RLT?LTR
    //      polyfill to https://www.php.net/manual/en/intlchar.ord.php
}

namespace Poirot\Std\StdArray
{
    /**
     * Pop an element from associative array
     *
     * @param array  $arr
     * @param string $key
     *
     * @return mixed|null
     */
    function keyPop(array &$arr, string $key)
    {
        $r = null;
        if (isset($arr[$key])) {
            $r = $arr[$key];
            unset($arr[$key]);
        }

        return $r;
    }

    /**
     * Convert an array into a object.
     *
     * @param array $array PHP array
     *
     * @return \stdClass
     */
    function toObject(array $array = []): \stdClass
    {
        $object = new \stdClass;
        if (\count($array, \COUNT_NORMAL) <= 0)
            return $object;

        foreach ($array as $name => $value) {
            if (\is_array($value) === true)
                $object->{$name} = toObject($value);
            else
                $object->{$name} = $value;
        }

        return $object;
    }

    /**
     * Insert Into Array Pos
     *
     * @param $array
     * @param $element
     * @param $offset
     *
     * @return int
     */
    function positionalInsert(&$array, $element, $offset)
    {
        if ($offset == 0)
            return array_unshift($array, $element);

        if ( $offset + 1 >= count($array) )
            return array_push($array, $element);


        // [1, 2, x, 4, 5, 6] ---> before [1, 2], after [4, 5, 6]
        $beforeOffsetPart = array_slice($array, 0, $offset);
        $afterOffsetPart  = array_slice($array, $offset);
        # insert element in offset
        $beforeOffsetPart = $beforeOffsetPart + [$offset => $element];
        # glue them back
        $array = array_merge($beforeOffsetPart , $afterOffsetPart);
        arsort($array);
    }
}

namespace Poirot\Std\Invokable
{
    use Poirot\Std\ArgumentsResolver;
    use Poirot\Std\Interfaces\Pact\ipInvokableCallback;

    /**
     * Factory Reflection From Given Callable
     *
     * $function
     *   'function_name' | \closure
     *   'classname::method'
     *   [className_orObject, 'method_name']
     *
     * @param callable $callable
     *
     * @return \ReflectionFunction|\ReflectionMethod
     * @throws
     */
    function makeReflectFromCallable(callable $callable) : \ReflectionFunctionAbstract
    {
        if ( $callable instanceof ipInvokableCallback )
            $callable = $callable->getCallable();

        // [className_orObject, 'method_name']
        if ( is_array($callable) )
            $reflection = new \ReflectionMethod($callable[0], $callable[1]);

        // 'classname::method' | // 'function_name'
        elseif ( is_string($callable) ) {
            if ( strpos($callable, '::') )
                $reflection = new \ReflectionMethod($callable);
            else
                $reflection = new \ReflectionFunction($callable);
        }

        // Closure and Invokable
        elseif ( method_exists($callable, '__invoke') ) {
            if ($callable instanceof \Closure)
                $reflection = new \ReflectionFunction($callable);
            else
                $reflection = new \ReflectionMethod($callable, '__invoke');
        }

        return $reflection;
    }

    /**
     * Resolve Arguments Matched With Callable Arguments
     *
     * @param callable $callable
     * @param iterable $options Params to match with function arguments
     *
     * @return \Closure
     */
    function resolveCallable(callable $callable, iterable $options)
    {
        return (new ArgumentsResolver(new ArgumentsResolver\CallableResolver($callable)))
            ->withOptions($options)
            ->resolve();
    }

    /**
     * Resolve Class Instant Matched With Given Arguments
     *
     * @param string $className
     * @param iterable $options Params to match with constructor arguments
     *
     * @return object
     */
    function resolveInstantClass(string $className, iterable $options)
    {
        return (new ArgumentsResolver(new ArgumentsResolver\InstantiatorResolver($className)))
            ->withOptions($options)
            ->resolve();
    }
}

namespace Poirot\Std
{
    use Closure;
    use Poirot\Std\Type\StdString;

    const CODE_NUMBERS       = 2;
    const CODE_STRINGS_LOWER = 4;
    const CODE_STRINGS_UPPER = 8;
    const CODE_STRINGS       = CODE_STRINGS_LOWER | CODE_STRINGS_UPPER;

    /**
     * Catch Callable Exception
     *
     * @param callable $fun
     * @param callable $catch mixed function(\Exception $exception)
     *
     * @return mixed value returned by $fun or $catch
     * @throws \Exception
     */
    function catchIt(callable $fun, callable $catch = null)
    {
        try {
            $return = call_user_func($fun);
        } catch (\Exception $e) {
            if ($catch)
                return call_user_func($catch, $e);

            throw $e;
        }

        return $return;
    }

    /**
     * Retry Callable
     *
     * @param Closure $f
     * @param int     $maxTries
     * @param int     $usleep
     *
     * @return mixed
     * @throws \Exception
     */
    function reTry(\Closure $f, $maxTries = 5, $usleep = 0)
    {
        $attempt = 0;

        do {
            try {
                // Pop Payload form Queue
                return $f();

            } catch (\Exception $e) {

                if ($usleep) usleep($usleep);
                $attempt++;
                continue;
            }

        } while ($attempt >= $maxTries);

        throw $e;
    }

    /**
     * Is Given Mime Match With Mime-Type Lists
     *
     * @param array  $mimesList
     * @param string $against
     *
     * @return bool|null
     */
    function isMimeMatchInList(array $mimesList, $against)
    {
        $exMimeType = explode('/', $against);

        $flag = null; // mean we have no list
        foreach ($mimesList as $mimeDef)
        {
            foreach (explode('/', $mimeDef) as $i => $v) {
                if ($v == '*')
                    // Skip This mimeType Definition Part, try next ...
                    continue;

                $flag = false; // mean we have a list

                if (isset($exMimeType[$i]) && strtolower($v) === strtolower($exMimeType[$i]))
                    return $against; // mean we have given mime in list
            }
        }

        return $flag;
    };

    /**
     * Timer Diff
     *
     * @param $timeStart
     *
     * @return string
     */
    function timerDiff($timeStart)
    {
        return number_format(microtime(true) - $timeStart, 3);
    }

    /**
     * Prettified Bytes
     *
     * @param int $bytes
     *
     * @return string
     */
    function prettyBytes($bytes)
    {
        $kb = 1024; $mb = $kb * 1024; $gb = $mb * 1024;

        switch(true) {
            case $bytes > $gb: $bytes = sprintf('%sG', number_format($bytes / $gb, 2)); break;
            case $bytes > $mb: $bytes = sprintf('%sM', number_format($bytes / $mb, 2)); break;
            case $bytes > $kb: $bytes = sprintf('%sK', number_format($bytes / $kb, 2)); break;
        }

        return $bytes;
    }

    function prettyFileSize($size, $precision = 2)
    {
        static $units = array('B','KB','MB','GB','TB','PB','EB','ZB','YB');
        $step = 1024;
        $i = 0;
        while (($size / $step) > 0.9) {
            $size = $size / $step;
            $i++;
        }

        return round($size, $precision).' '.$units[$i];
    }

    /**
     * Prettified Durations
     *
     * @param int $seconds
     *
     * @return string
     */
    function prettyDuration($seconds)
    {
        $m = 60; $h = $m * 60; $d = $h * 24;

        $out = '';
        switch(true) {
            case $seconds > $d:
                $out .= intval($seconds / $d) . 'd ';
                $seconds %= $d;
            case $seconds > $h:
                $out .= intval($seconds / $h) . 'h ';
                $seconds %= $h;
            case $seconds > $m:
                $out .= intval($seconds / $m) . 'm ';
                $seconds %= $m;
            default:
                $out .= "{$seconds}s";
        }

        return $out;
    }

    /**
     * Generate Random Strings
     *
     * @param int $length
     * @param int $contains
     *
     * @return string
     */
    function generateShuffleCode($length = 8, $contains = CODE_NUMBERS | CODE_STRINGS)
    {
        $characters = (string) $contains;

        if ( is_int($contains) )
        {
            // CODE_NUMBERS | CODE_STRINGS
            if (($contains & CODE_NUMBERS) == CODE_NUMBERS)
                $characters .= '0123456789';

            if (($contains & CODE_STRINGS_LOWER) == CODE_STRINGS_LOWER)
                $characters .= 'abcdefghijklmnopqrstuvwxyz';

            if (($contains & CODE_STRINGS_UPPER) == CODE_STRINGS_UPPER)
                $characters .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }

        if ( empty($characters) )
            throw new \InvalidArgumentException('Invalid Contains Argument Provided; Does Not Match Any Condition.');


        $randomString = '';
        for ($i = 0; $i < $length; $i++)
            $randomString .= $characters[rand(0, strlen($characters) - 1)];

        return $randomString;
    }

    /**
     * Generate a unique identifier
     *
     * @param int $length
     *
     * @return string
     * @throws \Exception
     */
    function generateUniqueIdentifier($length = 40)
    {
        try {
            // Converting bytes to hex will always double length. Hence, we can reduce
            // the amount of bytes by half to produce the correct length.
            return bin2hex(random_bytes($length / 2));
        } catch (\TypeError $e) {
            throw new \Exception('Server Error While Creating Unique Identifier.');
        } catch (\Error $e) {
            throw new \Exception('Server Error While Creating Unique Identifier.');
        } catch (\Exception $e) {
            // If you get this message, the CSPRNG failed hard.
            throw new \Exception('Server Error While Creating Unique Identifier.');
        }
    }

    /**
     * Swap Value Of Two Variable
     *
     * @param mixed $a
     * @param mixed $b
     *
     * @return void
     */
    function swap(&$a, &$b)
    {
        list($a, $b) = [$b, $a];
    }

    /**
     * Represent Stringify From Variable
     *
     * @param mixed $var
     *
     * @return string
     * @throws \Exception
     */
    function toStrVar($var)
    {
        $r = null;

        if (is_bool($var))
            $r = ($var) ? 'True' : 'False';
        elseif (is_null($var))
            $r = 'null';
        elseif (StdString\isStringify($var))
            $r = (string) $var;
        else
            throw new \Exception(sprintf('Variable (%s) cant convert to string.', $var));

        return $r;
    }

    /**
     * Flatten Value
     *
     * @param mixed $value
     *
     * @return string
     */
    function flatten($value)
    {
        if ($value instanceof Closure) {
            $closureReflection = new \ReflectionFunction($value);
            $value = sprintf(
                '(Closure at %s:%s)',
                $closureReflection->getFileName(),
                $closureReflection->getStartLine()
            );
        } elseif (is_object($value)) {
            $value = sprintf('%s:object(%s)', spl_object_hash($value), get_class($value));
        } elseif (is_resource($value)) {
            $value = sprintf('resource(%s-%s)', get_resource_type($value), $value);
        } elseif (is_array($value)) {
            foreach($value as $k => &$v)
                $v = flatten($v);

            $value = 'Array: ['.implode(', ', $value).']';
        } elseif (is_scalar($value)) {
            $value = sprintf('%s(%s)', gettype($value), $value);
        } elseif ($value === null)
            $value = 'NULL';

        return $value;
    }
}

namespace Poirot\Std\Lexer
{
    use Poirot\Std\CriteriaLexer\Criteria;
    use Poirot\Std\CriteriaLexer\ParsedCriteria;


    /**
     * Tokenize Parse String Definition Into Parts
     *
     * @param string $criteria
     * @param string $expectedLiteral
     *
     * @return array
     * @throws \Exception
     */
    function parseCriteria($criteria, $expectedLiteral = null)
    {
        $criteria = new Criteria($criteria, $expectedLiteral);
        $parsed   = $criteria->parse();
        return $parsed->asArray();
    }

    /**
     * Build the matching regex from parsed parts.
     *
     * @param array $parts
     * @param bool $matchExact
     *
     * @return string
     * @throws \Exception
     */
    function buildRegexFromParsed(array $parts, $matchExact = null)
    {
        $parsedCriteria = ParsedCriteria::of($parts);
        return $parsedCriteria->asRegex($matchExact);
    }

    /**
     * Build String Representation From Given Parts and Params
     *
     * @param array $parts
     * @param array|\Traversable $params
     *
     * @return string
     * @throws \Exception
     */
    function buildStringFromParsed(array $parts, $params = array())
    {
        $parsedCriteria = ParsedCriteria::of($parts);
        return $parsedCriteria->asString($params);
    }
}
