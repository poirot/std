<?php
namespace Poirot\Std\Configurable;

use Poirot\Std\Interfaces\Pact\ipConfigurable;
use Poirot\Std\Traits\tConfigurableSetter;

abstract class aConfigurableSetter
    extends    aConfigurable
    implements ipConfigurable
{
    use tConfigurableSetter;

}
