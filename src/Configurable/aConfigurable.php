<?php
namespace Poirot\Std\Configurable;

use Poirot\Std\Exceptions\Configurable\ConfigurationError;
use Poirot\Std\Exceptions\Configurable\ResourceParseError;
use Poirot\Std\Interfaces\Pact\ipConfigurable;
use Poirot\Std\Traits\tConfigurable;


abstract class aConfigurable
    implements ipConfigurable
{
    use tConfigurable;


    /**
     * Construct
     *
     * @param mixed|\Traversable $options
     * @param array $skipExceptions List of exception classes to skip on error
     *
     * @throws ConfigurationError|ResourceParseError
     */
    function __construct($options = null, array $skipExceptions = [])
    {
        if ($options !== null)
            $this->setConfigs(static::parse($options), $skipExceptions);
    }

    /**
     * @inheritDoc
     */
    abstract function setConfigs(array $options, array $skipExceptions = []);
}
