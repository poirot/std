<?php
namespace Poirot\Std\Configurable;

class Params
{
    private $params = [];

    function __construct(iterable $params)
    {
        foreach ($params as $key => $val)
            $this->params[$key] = $val;
    }

    function asArray(): array
    {
        return $this->params;
    }
}
