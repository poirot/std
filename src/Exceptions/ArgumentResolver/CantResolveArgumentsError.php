<?php
namespace Poirot\Std\Exceptions\ArgumentResolver;


class CantResolveArgumentsError
    extends ArgumentResolverError
{
    const ErrorCode_NoOptionsGiven = 2;

    protected $methodReflection;

    function __construct(
        \ReflectionFunctionAbstract $reflectionMethod,
        string $message = '',
        int $code = 0,
        \Throwable $previous = null
    ) {
        $this->methodReflection = $reflectionMethod;

        parent::__construct($message, $code, $previous);
    }

    /**
     * Get Reflection Method That Needed To Be Resolved Arguments
     *
     * @return \ReflectionFunctionAbstract
     */
    function getReflectionMethod()
    {
        return $this->methodReflection;
    }
}
