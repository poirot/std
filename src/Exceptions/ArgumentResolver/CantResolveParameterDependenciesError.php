<?php
namespace Poirot\Std\Exceptions\ArgumentResolver;


class CantResolveParameterDependenciesError
    extends CantResolveArgumentsError
{
    protected $parameterReflection;

    function __construct(
        \ReflectionFunctionAbstract $reflectionMethod,
        \ReflectionParameter $reflectionParameter,
        string $message = '',
        int $code = 0,
        \Throwable $previous = null
    ) {
        $this->parameterReflection = $reflectionParameter;

        parent::__construct($reflectionMethod, $message, $code, $previous);
    }

    // ..

    /**
     * Argument That Actually Not Resolved
     *
     * @return \ReflectionParameter
     */
    function getReflectionParameter()
    {
        return $this->parameterReflection;
    }
}
