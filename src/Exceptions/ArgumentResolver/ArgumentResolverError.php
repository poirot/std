<?php
namespace Poirot\Std\Exceptions\ArgumentResolver;

class ArgumentResolverError
    extends \RuntimeException
{
    static function getCantResolveArgumentsError(\ReflectionFunctionAbstract $reflectionMethod, int $errorCode): CantResolveArgumentsError
    {
        if ($errorCode === CantResolveArgumentsError::ErrorCode_NoOptionsGiven)
            $errorCodeMessage = 'There is no options given to resolve to Callable (%s).';
        else
            $errorCodeMessage = 'Unknown Error while try to resolve arguments for Callable (%s).';

        $message = sprintf(
            $errorCodeMessage
            , ($reflectionMethod instanceof \ReflectionMethod)
                ? $reflectionMethod->getDeclaringClass()->getName() . '::' . $reflectionMethod->getName()
                : $reflectionMethod->getName()
        );

        return new CantResolveArgumentsError($reflectionMethod, $message, $errorCode);
    }

    static function getCantResolveParameterDependenciesError(
        \ReflectionFunctionAbstract $reflectionMethod,
        \ReflectionParameter $reflectionParameter
    ): CantResolveParameterDependenciesError
    {
        $message = sprintf(
            'Callable (%s) has no match found on parameter (%s) from given params.'
            , ($reflectionMethod instanceof \ReflectionMethod)
                ? $reflectionMethod->getDeclaringClass()->getName() . '::' . $reflectionMethod->getName()
                : $reflectionMethod->getName()
            , $reflectionParameter->getName()
        );

        return new CantResolveParameterDependenciesError($reflectionMethod, $reflectionParameter, $message);
    }
}
