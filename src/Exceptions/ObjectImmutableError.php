<?php
namespace Poirot\Std\Exceptions;


class ObjectImmutableError
    extends \RuntimeException
{ }
