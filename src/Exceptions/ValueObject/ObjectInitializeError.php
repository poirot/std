<?php
namespace Poirot\Std\Exceptions\ValueObject;

class ObjectInitializeError
    extends ValueObjectError
{ }
