<?php
namespace Poirot\Std\Exceptions\ValueObject;

class ValueObjectError
    extends \RuntimeException
{
    static function getPropertyMissingOrNotFulFilledError(\Throwable $e): PropertyMissingOrNotFulFilledError
    {
        return new PropertyMissingOrNotFulFilledError($e->getMessage(), $e->getCode(), $e);
    }

    static function getUnknownResourceError($resource): UnknownResourceError
    {
        return new UnknownResourceError(sprintf(
            'Resource Origin (%s) is unknown by configurable and unable to parse.'
            , is_object($resource) ? get_class($resource) : gettype($resource)
        ));
    }
}
