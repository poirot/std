<?php
namespace Poirot\Std\Exceptions\ValueObject;

class UnknownResourceError
    extends ResourceParseError
{ }
