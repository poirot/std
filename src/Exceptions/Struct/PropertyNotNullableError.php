<?php
namespace Poirot\Std\Exceptions\Struct;


class PropertyNotNullableError
    extends PropertyTypeError
{ }
