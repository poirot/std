<?php
namespace Poirot\Std\Exceptions\Struct;


class PropertyIsSetterOnlyError
    extends PropertyIsUnknownError
{ }
