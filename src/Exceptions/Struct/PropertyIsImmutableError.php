<?php
namespace Poirot\Std\Exceptions\Struct;


class PropertyIsImmutableError
    extends PropertyError
{ }
