<?php
namespace Poirot\Std\Exceptions\Struct;


class PropertyIsUnknownError
    extends PropertyError
{ }
