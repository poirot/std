<?php
namespace Poirot\Std\Exceptions\Struct;


class PropertyIsGetterOnlyError
    extends PropertyIsUnknownError
{ }
