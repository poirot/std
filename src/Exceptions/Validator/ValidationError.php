<?php
namespace Poirot\Std\Exceptions\Validator;

use Poirot\Std\Interfaces\Validator\iMessageProcessor;
use Poirot\Std\Struct\PriorityQueue;
use Poirot\Std\Type\StdArray;
use Poirot\Std\Validator\MessageProcessor\DataInterchange;


/**
 * @method ValidationError getPrevious()
 */
final class ValidationError
    extends \Exception
{
    const Error           = 'error';
    const UnknownError    = 'unknown';
    const RequiredError   = 'required';
    const MinLengthError  = 'length-min';
    const NotInRangeError = 'not-in-range';

    protected $errorType;
    protected $parameter;
    protected $paramValue;
    protected $message;
    protected $code = 422;
    /* @var ValidationError|null */
    protected $previous;

    /** @var PriorityQueue */
    static protected $queue;


    /**
     * Constructor.
     *
     * @param string               $errType
     * @param string               $parameter
     * @param mixed|null           $paramValue
     * @param string               $message
     * @param ValidationError|null $previous
     */
    function __construct(
        string $message,
        string $errType = self::Error,
        string $parameter = null,
        $paramValue = null,
        ?ValidationError $previous = null
    ) {
        $this->parameter  = $parameter;
        $this->errorType  = $errType;
        $this->paramValue = $paramValue;

        parent::__construct($this->_getMessage($message), $this->getCode(), $previous);
    }


    /**
     * Get Parameter Name
     *
     * @return string|null
     */
    function getParameter() : ?string
    {
        return $this->parameter;
    }

    /**
     * Get Parameter Value That Cause Validation Error
     *
     * @return mixed|void
     */
    function getValue()
    {
        return $this->paramValue;
    }

    /**
     * Get Error Reason Type
     *
     * @return string|null
     */
    function getError() : ?string
    {
        return $this->errorType;
    }

    /**
     * Get Validation Error Message and All Nested
     * Validation Error Messages.
     *
     * @return array
     */
    function asErrorsArray()
    {
        $messages = [];

        $index = $this->getParameter() ?: 0;

        $messages[$index] = [
            'type' => $this->getError(),
            'message' => $this->getMessage(),
            'value' => $this->getValue(),
        ];

        if ( $this->getPrevious() ) {
            $messages = StdArray::of($messages)
                ->merge($this->getPrevious()->asErrorsArray(), function ($value, $currVal) {
                    if ($value === $currVal)
                        return StdArray::MergeDoReplace;

                    return StdArray::MergeDoMerging;
            })->asInternalArray;
        }

        return $messages;
    }


    /**
     * Add Message Processor
     *
     * @param iMessageProcessor $messageProcessor
     * @param int null          $weight           Priority weight
     */
    static function addMessageProcessor(iMessageProcessor $messageProcessor, int $weight = 10)
    {
        self::_priorityQueue()->insert($messageProcessor, $weight);
    }

    /**
     * Reset Message Processors To Default
     *
     */
    static function resetMessageProcessorsToDefault()
    {
        self::$queue = null;
        self::_attachDefaultMessageProcessors(self::_priorityQueue());
    }

    /**
     * List Registered Message Processors
     *
     * @return \Generator
     */
    static function listMessageProcessors()
    {
        /** @var iMessageProcessor $ms */
        foreach (self::_priorityQueue() as $ms)
            yield $ms;
    }

    // ..

    protected function _getMessage($message)
    {
        /** @var iMessageProcessor $ms */
        foreach (static::listMessageProcessors() as $ms)
            $message = $ms->getProcessedErrMessage($message, $this->getParameter(), $this->getValue());

        return $message;
    }

    /**
     * Get Priority Queue To Store Message Processors
     *
     * @return PriorityQueue
     */
    static protected function _priorityQueue(): PriorityQueue
    {
        if (! self::$queue ) {
            $queue = new PriorityQueue;
            self::_attachDefaultMessageProcessors($queue);
            self::$queue = $queue;
        }

        return self::$queue;
    }

    static protected function _attachDefaultMessageProcessors(PriorityQueue $queue)
    {
        $queue->insert(new DataInterchange, PHP_INT_MIN); // last thing to process
    }
}
