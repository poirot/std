<?php
namespace Poirot\Std\Exceptions;

use Poirot\Std\Type\StdString;

abstract class aExceptionsFacade
{
    /**
     * Constructor.
     * Declared private, as we have no need for instantiation.
     */
    private function __construct()
    { }

    protected static function getProcessedErrMessage($message, array $data): string
    {
        foreach ($data as $key => $value) {
            // Replace Value Placeholder
            //
            if (is_object($value))
                $value = get_class($value) . ' Object';
            elseif (is_string($value)) {
                if (interface_exists($value)) {
                    $value = $value . ' Interface';
                } elseif (class_exists($value)) {
                    $value = $value . ' Class';
                }
            } else {
                $value = var_export($value, 1);
            }

            $data[$key] = $value;

            $dataPlaceholder = "%$key%";
            if (! StdString::of($message)->isContains($dataPlaceholder))
                continue;

            $message = str_replace($dataPlaceholder, $value, $message);
            unset($data[$key]);
        }

        return sprintf($message, ...array_values($data));
    }
}
