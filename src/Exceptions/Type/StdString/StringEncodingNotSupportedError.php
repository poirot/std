<?php
namespace Poirot\Std\Exceptions\Type\StdString;

class StringEncodingNotSupportedError
    extends \InvalidArgumentException
{

}
