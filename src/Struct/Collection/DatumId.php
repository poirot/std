<?php
namespace Poirot\Std\Struct\Collection;

use Poirot\Std\Interfaces\Struct\Collection\iDatumId;


class DatumId
    implements iDatumId
{
    /** @var string */
    protected $hashId;


    /**
     * Constructor.
     *
     * @param $hashId
     */
    function __construct($hashId)
    {
        $this->hashId = (string) $hashId;
    }

    /**
     * @inheritDoc
     */
    static function of($datum)
    {
        $hashId = md5(\Poirot\Std\flatten($datum));
        return new self($hashId);
    }

    /**
     * @inheritDoc
     */
    function __toString()
    {
        return $this->hashId;
    }
}
