<?php
namespace Poirot\Std\Struct\Spl;


class SplAscendingQueue
    extends SplDescendingQueue
{
    /**
     * @override reverse priority
     * @inheritdoc
     */
    function compare($priority1, $priority2)
    {
        if ($priority1 === $priority2)
            return 0;

        return $priority1 > $priority2 ? -1 : 1;
    }
}
