<?php
namespace Poirot\Std\Struct\Spl;


class SplDescendingQueue
    extends \SplPriorityQueue
{
    /**
     * @var int Seed used to ensure queue order for items of the same priority
     */
    protected $serial = PHP_INT_MAX;

    /**
     * Insert a value with a given priority
     *
     * Utilizes {@var $serial } to ensure that values of equal priority are
     * emitted in the same order in which they are inserted.
     *
     * @param mixed $datum
     * @param mixed $priority
     *
     * @return true
     */
    function insert($datum, $priority)
    {
        if (! is_array($priority) )
            $priority = [$priority, $this->serial--];

        return parent::insert($datum, $priority);
    }

    /**
     * @inheritDoc
     */
    function next()
    {
        $this->serial++;
        parent::next();
    }
}
