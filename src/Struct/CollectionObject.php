<?php
namespace Poirot\Std\Struct;

use Poirot\Std\IteratorWrapper;
use Poirot\Std\Interfaces\Struct\Collection\iDatumId;
use Poirot\Std\Interfaces\Struct\iCollectionObject;
use Poirot\Std\Struct\Collection\DatumId;


class CollectionObject
    implements iCollectionObject
{
    protected $storedDatum = [
        /*
        '0xc33223Etag' => [
            'data' => [
                'etag'   => <iDatumId>,
                'extra'  => 'This is extra added data',
            ],
            'object'     => StoredObject,
        ],
        // ...
        */
    ];

    /**
     * @inheritDoc
     */
    function add($datum, array $metaData = []): iDatumId
    {
        $objectId = DatumId::of($datum);
        $objectHash = (string) $objectId;
        if ( isset($this->storedDatum[$objectHash]) )
            ## merge data if object exists
            $metaData = array_merge($this->storedDatum[$objectHash]['data'], $metaData);

        $this->storedDatum[$objectHash] = ['object' => $datum, 'data' => $metaData];
        return $objectId;
    }

    /**
     * @inheritDoc
     */
    function setMetaData(iDatumId $objectId, array $metaData)
    {
        if (null === $this->get($objectId) )
            throw new \Exception('Object Not Found.');


        $objectId = (string) $objectId;
        $this->storedDatum[$objectId]['data'] = $metaData;
        return $this;
    }

    /**
     * @inheritDoc
     */
    function get(iDatumId $objectId, \Closure $callback = null)
    {
        $objectId = (string) $objectId;
        if (! isset($this->storedDatum[$objectId]) )
            return null;

        $storedObject = $this->storedDatum[$objectId]['object'];
        return (null === $callback)
            ? $storedObject
            : $callback($storedObject, $this->storedDatum[$objectId]['data']);
    }

    /**
     * @inheritDoc
     */
    function find(array $data, \Closure $callable = null): IteratorWrapper
    {
        $storedObjects = $this->storedDatum;
        $iteration = (function() use ($storedObjects, $data) {
            foreach($storedObjects as $objHash => $internalData) {
                if ( $data == array_intersect_assoc($internalData['data'], $data) ) {
                    yield $objHash => $storedObjects[$objHash]['object'];
                }
            }
        })();

        return $this->_wrapIterationGenerator($iteration, $callable);
    }

    /**
     * @inheritDoc
     */
    function del(iDatumId $objectId)
    {
        unset($this->storedDatum[(string) $objectId]);
        return $this;
    }

    // Implement Countable:

    /**
     * @inheritdoc
     */
    function count()
    {
        return count($this->storedDatum);
    }

    // ..

    protected function _wrapIterationGenerator(iterable $iteration, ?\Closure $callable)
    {
        $storedDatum = $this->storedDatum;
        return new IteratorWrapper($iteration, function ($object, string $objHash) use ($callable, $storedDatum) {
            if (! $callable )
                return $object;

            $callable = $callable->bindTo($this, IteratorWrapper::class);
            return $callable($object, $storedDatum[$objHash]['data'], $objHash);
        });
    }
}
