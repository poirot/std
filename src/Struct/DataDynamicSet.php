<?php
namespace Poirot\Std\Struct;

use Poirot\Std\Exceptions\Struct\PropertyError;
use Poirot\Std\Exceptions\Struct\PropertyIsGetterOnlyError;
use Poirot\Std\Exceptions\Struct\PropertyNotNullableError;
use Poirot\Std\Exceptions\Struct\PropertyIsSetterOnlyError;
use Poirot\Std\Exceptions\Struct\PropertyTypeError;
use Poirot\Std\Interfaces\Struct\iData;
use Poirot\Std\Struct\DataSet\DataSetPropertyObject;
use Poirot\Std\Traits\tSetterGetterPropertyReceptive;


class DataDynamicSet
    extends DataStruct
    implements \IteratorAggregate, iData
{
    use tSetterGetterPropertyReceptive;

    protected $propertyUnset = [];

    protected $dataSetMethodVisibilities = \ReflectionMethod::IS_PROTECTED;
    /** @var array */
    protected $defaultImportSkipErrors;

    protected $_cachedPropertyGetterMethodsCount;
    /** @var \ReflectionClass */
    protected $_cachedSelfReflectionClass;


    /**
     * @inheritDoc
     * @param array $skipExceptions List of exception classes to skip on error
     */
    function __construct(iterable $data = null, array $skipExceptions = [])
    {
        $this->setSkipImportErrors($skipExceptions);
        parent::__construct($data);

        // Add to default ignored methods
        $this->excludePropertyMethod(...[
            'isEmpty',
            'getIterator',
            'setSkipImportErrors',
            'getSkipImportErrors',
        ]);
    }

    // Implement Data Object Methods:

    /**
     * @inheritDoc
     * @throws PropertyIsGetterOnlyError|PropertyError
     */
    function __set($key, $value)
    {
        if ( $setter = $this->_hasPropertyOperatorMethod($key, DataSetPropertyObject::Writable) ) {
            try {
                $this->$setter($value);
                unset($this->propertyUnset[$key]);
                $this->_cachedPropertyGetterMethodsCount = null;
            } catch (\TypeError $e) {
                $e = new PropertyTypeError($e->getMessage(), $e->getCode(), $e);
            } catch (\Throwable $e) {
                if (! $e instanceof PropertyError)
                    $e = new PropertyError($e->getMessage(), $e->getCode(), $e);
            } finally {
                if ( isset($e) )
                    throw $e;
            }

        } elseif (! $this->_hasPropertyOperatorMethod($key, DataSetPropertyObject::Readable) ) {
            parent::__set($key, $value);

        } else {
            throw new PropertyIsGetterOnlyError(sprintf(
                'Property %s is getter only property.'
                , $key
            ));
        }
    }

    /**
     * @inheritDoc
     * @throws PropertyIsSetterOnlyError
     */
    function &__get($key)
    {
        if ( $getter = $this->_hasPropertyOperatorMethod($key, DataSetPropertyObject::Readable)  ) {
            $result = $this->$getter();
            $fakeReference = &$result;
            return $fakeReference;

        } elseif (! $this->_hasPropertyOperatorMethod($key, DataSetPropertyObject::Writable) ) {
            return parent::__get($key);

        } else {
            throw new PropertyIsSetterOnlyError(sprintf(
                'Property %s is setter only property.'
                , $key
            ));
        }
    }

    /**
     * @inheritDoc
     * @throws PropertyIsSetterOnlyError
     */
    function __isset($key)
    {
        if ( isset($this->propertyUnset[$key]) )
            return false;

        elseif ( $this->_hasPropertyOperatorMethod($key, DataSetPropertyObject::Readable) )
            return null !== $this->__get($key);

        return parent::__isset($key);
    }

    /**
     * @inheritDoc
     */
    function __unset($key)
    {
        if ( $setter = $this->_hasPropertyOperatorMethod($key, DataSetPropertyObject::Writable) ) {
            try {
                $this->$setter(null);
            } catch (\TypeError $e) {
                throw new PropertyNotNullableError(sprintf(
                    'Property "%s" is not nullable and cant unset.'
                    , $key
                ), $e->getCode(), $e);
            }

            $this->_cachedPropertyGetterMethodsCount = null;
            $this->propertyUnset[$key] = true;

        } elseif (! $this->_hasPropertyOperatorMethod($key, DataSetPropertyObject::Readable) ) {
            return parent::__unset($key);

        } else {
            throw new PropertyIsGetterOnlyError(sprintf(
                'Property %s is getter only property.'
                , $key
            ));
        }
    }

    // Implementation:

    /**
     * @inheritDoc
     * @param array $skipExceptions List of exception classes to skip on error
     * @throws \Exception|\Throwable
     */
    function import(iterable $data, array $skipExceptions = [])
    {
        $skipExceptions = array_merge($this->getSkipImportErrors(), $skipExceptions);

        foreach($data as $k => $v) {
            try {
                $this->__set($k, $v);

            } catch (\Throwable $e) {
                $throw = true;
                foreach ($skipExceptions as $error) {
                    if ($e instanceof $error) {
                        $throw = false;
                        break;
                    }
                }

                if ($throw)
                    throw $e;
            }
        }

        return $this;
    }

    /**
     * Set Errors skip on import
     *
     * @param array $skipExceptions List of exception classes to skip on error
     *
     * @return $this
     */
    function setSkipImportErrors(array $skipExceptions = []): self
    {
        $this->defaultImportSkipErrors = $skipExceptions;
        return $this;
    }

    /**
     * Get Errors skip on import
     *
     * @return array
     */
    function getSkipImportErrors(): array
    {
        return $this->defaultImportSkipErrors;
    }

    // Implement Iterator:

    /**
     * @inheritdoc
     */
    function &getIterator()
    {
        foreach($this->_getDataSetPropertyObjects($this->dataSetMethodVisibilities) as $property => $availableMethods) {
            /** @var DataSetPropertyObject $propObject */
            foreach ($availableMethods as $propObject) {
                if (! $propObject->isReadable() )
                    continue;

                $propName = $propObject->getPropertyName();
                if ( isset($this->propertyUnset[$propName]) )
                    continue;

                yield $propName => $this->__get($propName);
            }
        }

        foreach (parent::getIterator() as $key => &$val)
            yield $key => $val;
    }

    // Implement Countable:

    /**
     * @inheritDoc
     */
    function count()
    {
        if ($this->_cachedPropertyGetterMethodsCount === null) {
            $this->_cachedPropertyGetterMethodsCount = 0;
            foreach($this->_getDataSetPropertyObjects($this->dataSetMethodVisibilities) as $property => $availableMethods) {
                /** @var DataSetPropertyObject $propObject */
                foreach ($availableMethods as $propObject) {
                    if (! $propObject->isReadable() )
                        continue;

                    $propName = $propObject->getPropertyName();
                    if ( isset($this->propertyUnset[$propName]) )
                        continue;

                    $this->_cachedPropertyGetterMethodsCount++;
                }
            }
        }

        return $this->_cachedPropertyGetterMethodsCount + parent::count();
    }
}
