<?php
namespace Poirot\Std\Struct;

use Poirot\Std\Exceptions\ArgumentResolver\CantResolveParameterDependenciesError;
use Poirot\Std\Exceptions\ValueObject\PropertyMissingOrNotFulFilledError;
use Poirot\Std\Exceptions\ValueObject\ObjectInitializeError;
use Poirot\Std\Exceptions\ValueObject\ValueObjectError;
use Poirot\Std\Interfaces\Struct\iValueObject;
use Poirot\Std\Struct\DataSet\DataSetPropertyObject;
use Poirot\Std\Traits\tSetterGetterPropertyReceptive;
use function Poirot\Std\Invokable\resolveCallable;
use function Poirot\Std\Invokable\resolveInstantClass;


class aValueObject
    implements \IteratorAggregate, iValueObject
{
    use tSetterGetterPropertyReceptive;

    protected $dataSetMethodVisibilities = \ReflectionMethod::IS_PROTECTED;

    private $isInitialized;

    /**
     * Constructor.
     *
     * Note:
     * should be called by extended classes on construct to initialize class
     *
     * @throws ObjectInitializeError
     */
    function __construct()
    {
        $this->_initProperties();
        $this->_assertObjectInitialized();
    }

    /**
     * @throws ObjectInitializeError
     */
    final function __call($name, $arguments)
    {
        $this->_assertObjectInitialized();

        $propertyMethods = $this->_getDataSetPropertyObjects($this->dataSetMethodVisibilities);
        foreach ($propertyMethods as $property => $availableMethods) {
            if (! isset($availableMethods[$name]))
                continue;

            /** @var DataSetPropertyObject $method */
            $method = $availableMethods[$name];
            if ( $method->isWritable() ) {
                if ('with' != $method->getAccessorPrefix())
                    throw new \BadMethodCallException(sprintf(
                        'Only call to immutable setters are possible.',
                    ));

                $new = clone $this;
                try {
                    call_user_func_array([$new, $name], $arguments);
                } catch (\TypeError $e) {
                    throw ValueObjectError::getPropertyMissingOrNotFulFilledError($e);
                }

                return $new;
            }

            return call_user_func_array([$this, $name], $arguments);
        }

        throw new \BadMethodCallException(sprintf(
            'Method %s not implemented.',
            $name
        ));
    }

    /**
     * @inheritDoc
     */
    static function parseWith($origin, $strict = true): iterable
    {
        if (! is_iterable($origin))
            throw ValueObjectError::getUnknownResourceError($origin);

        return $origin;
    }

    /**
     * @inheritDoc
     */
    final static function of(iterable $data)
    {
        try {
            $new = resolveInstantClass(static::class, $data);
        } catch (CantResolveParameterDependenciesError | \TypeError $e) {
            throw ValueObjectError::getPropertyMissingOrNotFulFilledError($e);
        }

        return $new;
    }

    /**
     * Check whether the given object has equal values?
     *
     * @param iValueObject $object
     *
     * @return bool
     * @throws PropertyMissingOrNotFulFilledError
     * @throws ObjectInitializeError
     */
    function isEqualTo(iValueObject $object): bool
    {
        $this->_assertObjectInitialized();

        return get_class($this) === get_class($object) && $this == $object;
    }

    // Implement Iterator:

    /**
     * @inheritdoc
     * @throws ObjectInitializeError
     */
    final function getIterator()
    {
        $this->_assertObjectInitialized();

        foreach($this->_getDataSetPropertyObjects($this->dataSetMethodVisibilities) as $property => $availableMethods) {
            foreach ($availableMethods as $propObject) {
                /** @var DataSetPropertyObject $propObject */
                if (! $propObject->isReadable() )
                    continue;

                $propName = $propObject->getPropertyName();
                yield $propName => call_user_func([$this, $propObject->getMethodName()]);
            }
        }
    }

    // Implement Serializable

    /**
     * @inheritDoc
     */
    function serialize()
    {
        return serialize(iterator_to_array($this));
    }

    /**
     * @inheritDoc
     */
    function unserialize($serialized)
    {
        resolveCallable([$this, '__construct'], unserialize($serialized))();
    }

    // ..

    /**
     * Initialize object by checking which available setter
     * properties used on construct as arguments
     *
     */
    private function _initProperties()
    {
        if (null !== $this->isInitialized)
            throw new \RuntimeException('Constructor called twice, object is already initialized.');


        $properties    = $this->_getDataPropertiesName();
        $constructArgs = $this->_getConstructorArguments();

        sort($properties);
        sort($constructArgs);

        if ($properties != $constructArgs)
            throw new ObjectInitializeError('The properties accessor and construct definition does not match.');

        $this->isInitialized = true;
    }

    /**
     * Assert object to see if it's initialized and it's okay?
     *
     */
    private function _assertObjectInitialized()
    {
        if (false === $this->isInitialized)
            throw new ObjectInitializeError(sprintf(
                '(%s) object should be initialized first, perhaps parent __construct method is not called.',
                get_called_class()
            ));
    }

    /**
     * Get Object Properties By Name
     *
     * @return array
     */
    private function _getDataPropertiesName()
    {
        $properties = [];
        foreach ($this->_getDataSetPropertyObjects($this->dataSetMethodVisibilities) as $availableMethods) {
            foreach ($availableMethods as $propObject) {
                /** @var DataSetPropertyObject $propObject */
                if ($propObject->isWritable()) {
                    if ('with' == $propObject->getAccessorPrefix())
                        $properties[] = $propObject->getPropertyName();
                }
            }
        }

        return $properties;
    }

    /**
     * Get Constructor Arguments By Name
     *
     * @return array
     * @throws
     */
    private function _getConstructorArguments()
    {
        $params = [];
        $reflectionClass = new \ReflectionClass($this);
        foreach ($reflectionClass->getConstructor()->getParameters() as $argument) {
            $params[] = $argument->getName();
        }

        return $params;
    }
}
