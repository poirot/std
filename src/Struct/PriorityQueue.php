<?php
namespace Poirot\Std\Struct;

use Poirot\Std\Struct\Spl\SplAscendingQueue;
use Poirot\Std\Struct\Spl\SplDescendingQueue;


class PriorityQueue
    implements \IteratorAggregate
{
    const Ascending  = SORT_ASC;
    const Descending = SORT_DESC;

    /** @var SplDescendingQueue */
    protected $innerQueue;


    /**
     * Construct
     *
     * @param int $order
     */
    function __construct($order = self::Descending)
    {
        if ($order === self::Ascending)
            $queue = new SplAscendingQueue;
        else
            $queue = new SplDescendingQueue;

        $this->innerQueue = $queue;
    }

    function __clone()
    {
        if (null !== $this->innerQueue)
            $this->innerQueue = clone $this->innerQueue;
    }

    /**
     * Inserts an element in the queue by sifting it up.
     * @param mixed $value <p>
     * The value to insert.
     * </p>
     * @param mixed $priority <p>
     * The associated priority.
     * </p>
     * @return true
     */
    function insert($value, $priority)
    {
        return $this->innerQueue->insert($value, $priority);
    }

    /**
     * @inheritDoc
     */
    function getIterator()
    {
        return clone $this->innerQueue;
    }
}
