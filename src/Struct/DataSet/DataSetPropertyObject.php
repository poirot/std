<?php
namespace Poirot\Std\Struct\DataSet;

use Poirot\Std\Exceptions\Struct\DataSet\InvalidPropertyName;
use Poirot\Std\Traits\tPropertySanitizeKey;
use Poirot\Std\Type\StdString;

final class DataSetPropertyObject
{
    use tPropertySanitizeKey;

    const Readable = 001;
    const Writable = 010;

    const PropertyMethodPrefixes = [
        'readable' => ['has', 'get', 'is'],
        'writable' => ['with', 'give', 'set'],
    ];

    /** @var string */
    protected $methodName;
    /** @var \ReflectionMethod */
    protected $reflectionMethod;
    /** @var string property_name */
    protected $propertyName;

    protected $accumulate = 000; // its r/w 011


    /**
     * Constructor
     *
     * @param \ReflectionMethod $method
     *
     * @throws InvalidPropertyName
     */
    function __construct(\ReflectionMethod $method)
    {
        $methodName = $method->getName();
        if (! self::isDataSetPropertyMethod($methodName) )
            throw new InvalidPropertyName(sprintf(
                '"%s" Is Invalid DataSet Method Property.'
                , $methodName
            ));


        $this->methodName = $methodName;
        $this->reflectionMethod = $method;

        ## mark readable/writable for property
        $prefix = self::getPrefixOperation($methodName);
        if ( in_array($prefix, self::PropertyMethodPrefixes['writable']) ) {
            $this->setWritable();
            if (! $this->isImmutableWritable()) {
                // except of immutable property method condition which start with 'giveSomething'
                foreach ($this->reflectionMethod->getParameters() as $arg) {
                    if (! $arg->allowsNull() )
                        throw new \RuntimeException(sprintf(
                            'Method %s should be nullable.',
                            $this->methodName
                        ));
                }
            }
        } else {
            $this->setReadable();
       }
    }

    function __toString()
    {
        return $this->getPropertyName();
    }

    /**
     * Check whether given method name can be considered as data set property?
     *
     * @param string $methodName
     *
     * @return bool
     */
    static function isDataSetPropertyMethod(string $methodName): bool
    {
        $prefix = self::getPrefixOperation($methodName);
        return '' !== $prefix && $prefix !== $methodName;
    }

    /**
     * Property Name Based On The Method
     *
     * @return string
     */
    function getPropertyName(): string
    {
        if ($this->propertyName)
            return $this->propertyName;


        // getAttributeName -> AttributeName -> attribute_name
        $propertyName = substr($this->methodName, strlen(self::getPrefixOperation($this->methodName)));
        $propertyName = $this->_sanitizeToPropertyKey($propertyName);

        return $this->propertyName = $propertyName;
    }

    /**
     * Get Method Name
     *
     * @return string
     */
    function getMethodName(): string
    {
        return $this->methodName;
    }

    /**
     * Get Property Accessor Prefix
     *
     * @return string
     */
    function getAccessorPrefix(): string
    {
        return self::getPrefixOperation($this->methodName);
    }

    /**
     * Attain to method reflection
     *
     * @return \ReflectionMethod
     */
    function getReflectionMethod(): \ReflectionMethod
    {
        return $this->reflectionMethod;
    }

    /**
     * Is a Property Readable through associated method?
     *
     * @return bool
     */
    function isReadable(): bool
    {
        return (bool) ($this->accumulate & self::Readable);
    }

    /**
     * Is a Property Writable and able to set a value with associated method?
     *
     * @return bool
     */
    function isWritable(): bool
    {
        return (bool) ($this->accumulate & self::Writable);
    }

    /**
     * Is a Property Immutable Writable?
     *
     * @return bool
     */
    function isImmutableWritable(): bool
    {
        $prefix = $this->getAccessorPrefix();
        return 'give' == $prefix || 'with' == $prefix;
    }

    // ..

    static protected function getPrefixOperation($methodName): string
    {
        $readablePrefixes = self::PropertyMethodPrefixes['readable'];
        $writablePrefixes = self::PropertyMethodPrefixes['writable'];
        $prefix = StdString::of($methodName)
            ->hasStartedWith(...array_merge($readablePrefixes, $writablePrefixes));

        return (string) $prefix;
    }

    protected function setReadable($flag = true)
    {
        if ($flag)
            $this->accumulate |= self::Readable;
        else
            $this->accumulate ^= self::Readable;
    }

    protected function setWritable($flag = true)
    {
        if ($flag)
            $this->accumulate |= self::Writable;
        else
            $this->accumulate ^= self::Writable;
    }
}
