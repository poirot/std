<?php
namespace Poirot\Std\Struct;

use Poirot\Std;
use Poirot\Std\Interfaces\Struct\iData;


class DataHashMap
    extends DataStruct
    implements \IteratorAggregate, iData
{
    // Hash Map of None scalar property keys
    protected $_mapNonScalarPropObjects = [];


    // Implementation

    /**
     * @inheritDoc
     */
    function __set($key, $value)
    {
        if ( $key !== $hash = $this->_normalizeKey($key) ) {
            $this->_mapNonScalarPropObjects[$hash] = $key /* none scalar given key */;
            $key = $hash;
        }

        parent::__set($key, $value);
        return $this;
    }

    /**
     * Set Entity
     *
     * @param mixed $key
     * @param mixed $value
     *
     * @return $this
     */
    function set($key, $value): self
    {
        return $this->__set($key, $value);
    }

    /**
     * Get Entity Value
     *
     * @param mixed      $key
     * @param null|mixed $default Return value if key not exists
     *
     * @return mixed
     */
    function &get($key, $default = null)
    {
        if (! $this->has($key) )
            return $default;

        return $this->__get($this->_normalizeKey($key));
    }

    /**
     * @inheritDoc
     */
    function has(...$key)
    {
        foreach ($key as $k) {
            if (! parent::has($this->_normalizeKey($k)) )
                return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    function del(...$key)
    {
        foreach ($key as $k) {
            $k = $this->_normalizeKey($k);
            unset($this->_mapNonScalarPropObjects[$k]);

            parent::del($k);
        }

        return $this;
    }

    // Implement Iterator

    /**
     * @inheritDoc
     */
    function &getIterator()
    {
        foreach (parent::getIterator() as $k => &$v) {
            if ( array_key_exists($k, $this->_mapNonScalarPropObjects) )
                $k = $this->_mapNonScalarPropObjects[$k];

            yield $k => $v;
        }
    }

    // ...

    /**
     * Make hash string for none scalars
     *
     * @param string|mixed $key
     *
     * @return string
     */
    protected function _normalizeKey($key): string
    {
        if (! is_scalar($key) )
            $key = md5( Std\flatten($key) );

        return $key;
    }
}
