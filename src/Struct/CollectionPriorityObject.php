<?php
namespace Poirot\Std\Struct;

use Poirot\Std\IteratorWrapper;
use Poirot\Std\Interfaces\Struct\Collection\iDatumId;
use Poirot\Std\Struct\Collection\DatumId;


class CollectionPriorityObject
    extends CollectionObject
{
    protected $orderingIndex = [];
    protected $shouldReIndex = false;


    /**
     * @inheritdoc
     */
    function add($datum, array $metaData = [], int $order = null): iDatumId
    {
        $objectId = parent::add($datum, $metaData);

        $order = (isset($metaData['__order']) && $order === null) ? (int) $metaData['__order'] : $order;
        $this->orderingIndex[(string) $objectId] = $order ?? 0;
        $this->shouldReIndex = true;
        return $objectId;
    }

    /**
     * @inheritDoc
     */
    function find(array $data, \Closure $callable = null): IteratorWrapper
    {
        $this->_sortIndexedPriority();
        return parent::find($data, $callable);
    }

    /**
     * @inheritdoc
     */
    function del(iDatumId $objectId)
    {
        parent::del($objectId);
        unset($this->orderingIndex[(string) $objectId]);
        return $this;
    }

    // ...

    /**
     * Sorts the page index according to page order
     *
     * @return void
     */
    protected function _sortIndexedPriority()
    {
        if (! $this->shouldReIndex)
            return;

        $prioritizedObjects = [];
        arsort($this->orderingIndex);
        foreach ($this->orderingIndex as $objectHash => $order) {
            $prioritizedObjects[$objectHash] = $this->get(new DatumId($objectHash), function ($obj, $data) {
                return [
                    'data' => $data,
                    'object' => $obj,
                ];
            });
        }

        $this->storedDatum = $prioritizedObjects;
        $this->shouldReIndex = false;
    }
}
