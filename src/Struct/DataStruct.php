<?php
namespace Poirot\Std\Struct;

use Poirot\Std\Interfaces\Struct\iData;
use Poirot\Std\Traits\tDataCommon;


class DataStruct
    implements \IteratorAggregate, iData
{
    use tDataCommon;

    protected $properties = [];
    protected $reservedProperties = [];


    /**
     * Wrap around existing array reference
     *
     * @param array $array
     *
     * @return DataStruct
     */
    static function ofReference(array &$array)
    {
        $dataStruct = new self;
        $dataStruct->properties = &$array;
        return $dataStruct;
    }

    // Magic Methods to cover -> access to class properties:

    function __set($key, $value)
    {
        $this->_cleanUpReservedProperties();
        $this->properties[$key] = $value;
        return $this;
    }

    function &__get($key)
    {
        $this->_cleanUpReservedProperties();

        if (! array_key_exists($key, $this->properties) ) {
            $this->reservedProperties[$key] = $this->_voidVal();
            $r = &$this->reservedProperties[$key];
        } else {
            $r = &$this->properties[$key];
        }

        return $r;
    }

    function __isset($key)
    {
        $this->_cleanUpReservedProperties();
        return array_key_exists($key, $this->properties);
    }

    function __unset($key)
    {
        unset($this->properties[$key]);
        unset($this->reservedProperties[$key]);
    }

    // Implementation:

    /**
     * @inheritDoc
     */
    function import(iterable $data)
    {
        foreach($data as $k => $v)
            $this->__set($k, $v);

        return $this;
    }

    /**
     * @inheritDoc
     */
    function has(...$key)
    {
        foreach ($key as $k) {
            if (! $this->__isset($k))
                return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    function del(...$key)
    {
        foreach ($key as $k)
            $this->__unset($k);

        return $this;
    }

    // Implement Countable

    /**
     * @inheritDoc
     */
    function count()
    {
        $this->_cleanUpReservedProperties();
        return count($this->properties);
    }

    // Implement Iterator

    /**
     * @inheritDoc
     */
    function &getIterator()
    {
        $this->_cleanUpReservedProperties();
        foreach($this->properties as $key => $v) {
            $value = &$this->__get($key);
            yield $key => $value;
        }
    }

    // ..

    private function _cleanUpReservedProperties()
    {
        foreach ($this->reservedProperties as $key => $value) {
            if ( $value !== $this->_voidVal() ) {
                $this->properties[$key] = $value;
                unset($this->reservedProperties[$key]);
            }
        }
    }

    private function _voidVal()
    {
        return null;
    }
}
