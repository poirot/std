<?php
namespace Poirot\Std;

use Poirot\Std\Exceptions\ObjectImmutableError;
use Poirot\Std\Interfaces\iMutexLock;


class MutexLock
    implements iMutexLock
{
    protected $realm;
    /** @var string|null*/
    private $lockDirectory;

    /** @var resource|null */
    protected $lockedFile;
    protected $lockedFileData;

    private static $defLockDirectory;


    /**
     * Constructor
     *
     * @param string $realm
     */
    function __construct(string $realm)
    {
        $this->realm = preg_replace('/[^a-z0-9\._-]+/i', '-', $realm);
    }

    function __destruct()
    {
        if ( $this->lockedFile )
            // The class that put lock should force to release lock on exit
            $this->release();
    }

    private function __clone()
    {
        throw new \LogicException('Cloning Mutex Lock is not allowed.');
    }


    /**
     * @inheritDoc
     */
    function acquire(float $ttl = 0)
    {
        if ( $this->isLocked() )
            // resource is locked; if resource locked and owned by self return true; other wise false.
            return $this->lockedFile ? true : false;


        $lockFilePath = $this->getAbsoluteLockFilePath();
        if (false === $file = fopen($lockFilePath, 'w+') )
            throw new \RuntimeException(sprintf(
                'Failed To Open Resource (%s).'
                , $lockFilePath
            ));


        // Write Lock Data Into File:
        //
        ftruncate($file, 0);
        $data = implode(PHP_EOL, [
            sprintf("%s - pid(%s) has Accrued Locked.", date('Y-m-d H:i:s'), getmypid()),
            serialize($this->lockedFileData = [getmypid(), $ttl, microtime(true)])
        ]);
        fwrite($file, $data);
        fflush($file);

        $this->lockedFile = $file;
        return true;
    }

    /**
     * @inheritDoc
     */
    function isLocked()
    {
        $lockFilePath = $this->getAbsoluteLockFilePath();
        if (! file_exists($lockFilePath) ) {
            // Lock file not exists at all.
            return false;
        }

        if ($this->lockedFile)
            // Lock was made by current class; check for expiration.
            return !$this->isTTLExpiredFromFileData($this->lockedFileData);


        if (false === $file = fopen($lockFilePath, 'r+') )
            throw new \RuntimeException(sprintf(
                'Failed To Open Resource (%s).'
                , $lockFilePath
            ));

        if (! flock($file, LOCK_EX | LOCK_NB, $wouldBlock) ) {
            if ($wouldBlock)
                // another process holds the lock
                return false;

            return flock($file, LOCK_EX); // doesn't support unblock mode!
        }


        // Check TTL to determine lock
        //
        $fileContent = '';
        while (! feof($file) )
            $fileContent .= fgets($file);

        /*
           2019-10-06 16:41:04 - pid(502) has Accrued Locked.
           a:2:{i:0;d:0;i:1;d:1570380064.993115;}
        */
        $fileContent = explode(PHP_EOL, $fileContent);
        $fileData    = @unserialize($fileContent[1]);

        return ! $this->isTTLExpiredFromFileData($fileData);
    }

    /**
     * @inheritDoc
     */
    function release()
    {
        $lockFilePath = $this->getAbsoluteLockFilePath();
        if (false === $lockFileResource = fopen($lockFilePath, 'c'))
            return false;


        ## Release Lock Res.
        #
        $this->lockedFile = null;

        fclose($lockFileResource);
        unlink($lockFilePath);

        return true;
    }

    // Options:

    /**
     * Set Immutable Directory Path To Lock Files
     *
     * @param string $dirPath
     *
     * @return $this
     * @throws ObjectImmutableError
     */
    function giveLockDirPath(string $dirPath)
    {
        if ( isset($this->lockDirectory) )
            throw new ObjectImmutableError(sprintf(
                'Lock Directory Currently Has The Value: (%s).'
                , $this->lockDirectory
            ));

        $this->lockDirectory = $dirPath;
        return $this;
    }

    /**
     * Get Dir Path To Store Lock Files
     *
     * @return string
     */
    function getLockDirPath() : string
    {
        if (! isset($this->lockDirectory) )
            $this->giveLockDirPath( MutexLock::getDefaultLockDirPath() );

        return $this->lockDirectory;
    }

    /**
     * Set Default Locks Directory
     *
     * @param string $dirPath
     */
    static function giveDefaultLockDirPath(string $dirPath)
    {
        if ( isset(static::$defLockDirectory) )
            throw new ObjectImmutableError(sprintf(
                'Default Lock Directory Has Value: (%s).'
                , static::$defLockDirectory
            ));

        static::$defLockDirectory = $dirPath;
    }

    static function getDefaultLockDirPath() : string
    {
        if (! static::$defLockDirectory ) {
            $lockDir = sys_get_temp_dir() . '/.locks';
            static::giveDefaultLockDirPath($lockDir);
        }

        return static::$defLockDirectory;
    }

    // ..

    private function isTTLExpiredFromFileData(array $fileData)
    {
        if ($fileData[1] <= 0)
            // ttl is not given; means lock forever.
            return false;

        return $fileData[1] + $fileData[2] <= microtime(true);
    }

    /**
     * Get Absolute Path To Lock File
     *
     * @return string
     */
    private function getAbsoluteLockFilePath()
    {
        self::_ensureDirectoryPathIsWritable($this->getLockDirPath());

        return rtrim($this->getLockDirPath(), '/\\') . '/' . "{$this->realm}.lock";
    }

    static private function _ensureDirectoryPathIsWritable($dirPath)
    {
        if (! is_dir($dirPath) ) {
            // try to create folder
            if ( false === mkdir($dirPath) )
                throw new \RuntimeException(sprintf(
                    'Can`t create lock directory at "%s".'
                    , $dirPath
                ));
        }

        if (! is_writable($dirPath) )
            throw new \RuntimeException(sprintf(
                'Lock Folder (%s) Is Not A Writable Directory.'
                , $dirPath
            ));
    }
}
