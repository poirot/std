<?php
namespace Poirot\Std\Hydrator;

use Poirot\Std\Configurable\aConfigurableSetter;
use Poirot\Std\Exceptions\Configurable\ConfigurationError;
use Poirot\Std\Exceptions\Configurable\UnknownConfigurationPropertyError;
use Poirot\Std\Exceptions\Configurable\ResourceParseError;
use Poirot\Std\Exceptions\Configurable\UnknownResourceError;
use Poirot\Std\Interfaces\Hydrator\iHydrateParser;
use Poirot\Std\Interfaces\Struct\iIterableObject;


abstract class aHydrateEntity
    extends aConfigurableSetter
    implements \IteratorAggregate, iIterableObject
{
    /** @var iHydrateParser[] */
    protected static $registeredParser = [];

    /** @var HydrateGetters */
    private $iterator;


    /**
     * Construct
     *
     * @param mixed|\Traversable $options
     * @param mixed|\Traversable $defaults
     * @param array $skipExceptions List of exception classes to skip on error
     *
     * @throws ConfigurationError|ResourceParseError
     */
    function __construct(
        $options = null,
        $defaults = null,
        array $skipExceptions = [UnknownConfigurationPropertyError::class]
    ) {
        if ($defaults !== null)
            $this->setConfigs(static::parse($defaults), $skipExceptions);

        parent::__construct($options, $skipExceptions);
    }

    function __clone()
    {
        $this->iterator = null;
    }

    // Implement Hydrate Setter(s):
    // ..


    // Implement Hydrate Getter(s):
    // ..


    // Implement Configurable

    /**
     * @inheritdoc
     *
     * @param iterable|mixed $optionsResource
     *
     * @return array
     * @throws ResourceParseError|UnknownResourceError
     */
    final static function parse($optionsResource)
    {
        if (! is_iterable($optionsResource)) {
            foreach (self::$registeredParser as $parser) {
                try {
                    if ($parsedResource = $parser->parse($optionsResource)) {
                        $optionsResource = $parsedResource;
                        break;
                    }
                } catch (\Exception $e) {
                    throw new ResourceParseError($e->getMessage(), $e->getCode(), $e);
                }
            }
        }

        return parent::parse($optionsResource);
    }

    /**
     * Register Parser
     *
     * @param iHydrateParser $parser
     */
    final static function registerParser(iHydrateParser $parser)
    {
        self::$registeredParser[] = $parser;
    }

    /**
     * List Registered Parser
     *
     * @return iHydrateParser[]
     */
    final static function listParsers(): array
    {
        return self::$registeredParser;
    }

    /**
     * Clear All Registered Parsers
     *
     */
    final static function clearAllParsers()
    {
        self::$registeredParser = [];
    }

    /**
     * @inheritDoc
     */
    function setConfigs(array $options, array $skipExceptions = [UnknownConfigurationPropertyError::class])
    {
        parent::setConfigs($options, $skipExceptions);
        return $this;
    }

    /**
     * Configure The Current Iterator And Return New Object Instance
     *
     * @param \Closure $closure function(HydrateGetters $iterator)
     *
     * @return self
     */
    final function withIteratorSpec(\Closure $closure): self
    {
        $new = clone $this;
        $iterator = $new->getIterator();
        $closure($iterator);
        $new->iterator = $iterator;
        return $new;
    }

    // Implement IteratorAggregate

    /**
     * @ignore Ignore from getter hydrator
     * @inheritdoc
     */
    final function getIterator()
    {
        if (! $this->iterator) {
            $hydrator = new HydrateGetters($this);
            $hydrator->setExcludeNullValues();
            return $hydrator;
        }

        return $this->iterator;
    }
}
