<?php
namespace Poirot\Std\Hydrator\Parser;

use Poirot\Std\Interfaces\Hydrator\iHydrateParser;


class FunctorHydrateParser
    implements iHydrateParser
{
    /** @var \Closure */
    private $callable;

    /**
     * Constructor
     *
     * @param \Closure $callable
     */
    function __construct(\Closure $callable)
    {
        $this->callable = $callable;
    }

    /**
     * @inheritDoc
     */
    function parse($optionsResource): ?iterable
    {
        $callable = $this->callable;
        return $callable($optionsResource);
    }
}
