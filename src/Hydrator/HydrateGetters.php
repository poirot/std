<?php
namespace Poirot\Std\Hydrator;

use Poirot\Std\Interfaces\Struct\iIterableObject;
use Poirot\Std\Struct\DataSet\DataSetPropertyObject;
use Poirot\Std\Traits\tSetterGetterPropertyReceptive;

class HydrateGetters
    implements \IteratorAggregate
    , iIterableObject
{
    use tSetterGetterPropertyReceptive;

    const IncludeIgnoredMethods = 'refuse.exclude';
    const ExcludeIgnoreMethods  = 'exclude';

    /** @var object */
    protected $givenObject;


    /**
     * Constructor
     *
     * @param object $object
     * @param array  $config
     */
    function __construct($object, array $config = [])
    {
        if (! is_object($object) )
            throw new \InvalidArgumentException(sprintf(
                'Hydration give an Object as Argument; given: (%s).'
                , \Poirot\Std\flatten($object)
            ));


        $this->excludePropertyMethod('getIterator');

        if ( isset($config[self::ExcludeIgnoreMethods]) ) {
            $this->excludePropertyMethod(...$config[self::ExcludeIgnoreMethods] );
            unset($config[self::ExcludeIgnoreMethods]);
        }

        if ( isset($config[self::IncludeIgnoredMethods]) ) {
            $this->refuseExcludedPropertyMethod(...$config[self::IncludeIgnoredMethods] );
            unset($config[self::IncludeIgnoredMethods]);
        }

        if (! empty($config))
            $this->excludePropertyMethod(...$config);

        $this->givenObject = $object;
    }

    // Implement IteratorAggregate

    /**
     * @inheritdoc
     */
    function getIterator()
    {
        foreach ($this->_getDataSetPropertyObjects() as $property => $availableMethods)
        {
            /** @var DataSetPropertyObject $propObject */
            foreach ($availableMethods as $propObject) {
                if (! $propObject->isReadable() )
                    continue;

                $getter = $propObject->getMethodName();
                $result = $this->givenObject->$getter();
                yield $property => $result;
            }
        }
    }

    protected function _makePropertyReceptiveReflection(): \ReflectionClass
    {
        if ($this->_cachedSelfReflectionClass === null)
            $this->_cachedSelfReflectionClass = new \ReflectionClass($this->givenObject);

        return $this->_cachedSelfReflectionClass;
    }
}
