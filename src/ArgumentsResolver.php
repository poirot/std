<?php
namespace Poirot\Std;

use Poirot\Std\ArgumentsResolver\aResolver;
use Poirot\Std\Exceptions\ArgumentResolver\ArgumentResolverError;
use Poirot\Std\Exceptions\ArgumentResolver\CantResolveArgumentsError;
use Poirot\Std\Exceptions\ArgumentResolver\CantResolveParameterDependenciesError;
use Poirot\Std\Type\StdArray;
use Poirot\Std\Type\StdString;


class ArgumentsResolver
{
    /** @var aResolver */
    protected $resolver;
    /** @var array */
    protected $options;
    /** @var bool */
    protected $strict = false;

    /**
     * null or other values can't be considered as undefined
     * as any other value may used as default argument value
     *
     * @var string
     */
    private $undefinedValue;
    private $tmpMappedArguments = [];
    private $tmpPositionalArguments = [];

    /**
     * Constructor
     *
     * @param aResolver $resolver
     */
    function __construct(aResolver $resolver)
    {
        $this->resolver = $resolver;
        $this->undefinedValue = uniqid('argumentResolver_');
    }

    /**
     * Set Params to match by Arguments To Resolve To Origin
     *
     * @param iterable $options
     * @param bool $matchHint Resolve argument with type from parameters; if false just
     *                        resolve arguments by name
     *
     * @return ArgumentsResolver
     */
    function withOptions(iterable $options, bool $matchHint = true): ArgumentsResolver
    {
        $new = clone $this;
        $new->options = $options;
        $new->strict = $matchHint;
        return $new;
    }

    /**
     * Invoke Resolved Arguments By Resolver
     *
     * @return \Closure|mixed
     * @throws CantResolveParameterDependenciesError
     */
    function resolve()
    {
        return $this->resolver->resolve(
            $this->getResolvedArguments()
        );
    }

    /**
     * Resolve Arguments Matched With Reflection Method/Function
     *
     * @return array Of matching arguments
     * @throws CantResolveArgumentsError
     */
    function getResolvedArguments()
    {
        $givenArgs = $this->resolver->getDefaultOptions();
        if ($this->options)
            $givenArgs = StdArray::of($givenArgs)->merge($this->options)->asInternalArray;

        $reflectFunc = $this->resolver->getReflectionMethod();

        $this->_createPositionalAndMappedArgumentList($reflectFunc, $givenArgs);

        $matchedArguments = [];
        foreach ($reflectFunc->getParameters() as $reflectParameter)
        {
            $parameterName = $reflectParameter->getName();

            if ($this->undefinedValue === $matchedValue = $this->_matchWithNamedParameter($reflectParameter))
                if ($this->undefinedValue === $matchedValue = $this->_matchWithTypedParameter($reflectParameter))
                    if (!$this->strict)
                        $matchedValue = $this->_matchWithPositionalParameter($reflectParameter);

            if ($this->undefinedValue === $matchedValue && $reflectParameter->isDefaultValueAvailable())
                $matchedValue = $reflectParameter->getDefaultValue();


            if ($this->undefinedValue === $matchedValue)
                throw ArgumentResolverError::getCantResolveParameterDependenciesError($reflectFunc, $reflectParameter);

            $matchedArguments[$parameterName] = $matchedValue;
        }

        return $matchedArguments;
    }

    // ..

    private function _createPositionalAndMappedArgumentList(
        \ReflectionFunctionAbstract $reflectionFunc,
        array $givenArgumentsOptions
    ) {
        $mappedArgs = [];
        $positionalArgs = [];

        $functionParameterNames = [];
        foreach ($reflectionFunc->getParameters() as $parameter)
            $functionParameterNames[strtolower($parameter->getName())] = true;

        foreach ($givenArgumentsOptions as $argumentName => $argumentValue)
        {
            if (! is_int($argumentName)) {
                $isMappedArgumentFounded = false;
                foreach ($this->_getParameterNameAndAliases($argumentName) as $argumentAlias => $_) {
                    if (isset($functionParameterNames[$argumentAlias])) {
                        $isMappedArgumentFounded = true;
                        $mappedArgs[$argumentAlias] = $argumentValue;
                        break;
                    }
                }

                if ($isMappedArgumentFounded)
                    continue;
            }

            $positionalArgs[] = $argumentValue;
        }

        $this->tmpMappedArguments = $mappedArgs;
        $this->tmpPositionalArguments = $positionalArgs;
    }

    private function _matchWithNamedParameter(\ReflectionParameter $reflectParameter)
    {
        $matchedValue = $this->undefinedValue;
        $parameterName = strtolower($reflectParameter->getName());
        if (array_key_exists($parameterName, $this->tmpMappedArguments)) {
            $matchedValue = $this->tmpMappedArguments[$parameterName];
            unset($this->tmpMappedArguments[$parameterName]);
        }

        return $matchedValue;
    }

    private function _matchWithTypedParameter(\ReflectionParameter $reflectParameter)
    {
        $matchedValue = $this->undefinedValue;

        if ($reflectParameter->getClass()) {
            foreach ($this->tmpPositionalArguments as $index => $value) {
                if (is_object($value) && $reflectParameter->getClass()->isInstance($value)) {
                    $matchedValue = $value;
                    unset($this->tmpPositionalArguments[$index]);
                    break;
                }
            }

        } elseif ($reflectParameter->isCallable()) {
            foreach ($this->tmpPositionalArguments as $index => $value) {
                if (is_callable($value)) {
                    $matchedValue = $value;
                    unset($this->tmpPositionalArguments[$index]);
                    break;
                }
            }

        } elseif ($reflectParameter->hasType()) {
            foreach ($this->tmpPositionalArguments as $index => $value) {
                $type = (string) $reflectParameter->getType();
                if ($type === 'int')
                    // Reflection Type return 'int' but gettype give 'integer'
                    $type = 'integer';

                if ($type === gettype($value)) {
                    $matchedValue = $value;
                    unset($this->tmpPositionalArguments[$index]);
                    break;
                }
            }
        }

        return $matchedValue;
    }

    private function _matchWithPositionalParameter(\ReflectionParameter $reflectParameter)
    {
        $matchedValue = $this->undefinedValue;
        foreach ($this->tmpPositionalArguments as $index => $value) {
                $matchedValue = $value;
                unset($this->tmpPositionalArguments[$index]);
                break;
        }

        return $matchedValue;
    }

    private function _getParameterNameAndAliases(string $paramName): array
    {
        $parameterNameAliases = [];
        $parameterNameAliases[$paramName] = true;
        $parameterNameAliases[strtolower($paramName)] = true;
        $parameterNameAliases[strtolower(StdString::of($paramName)->camelCase())] = true;
        $parameterNameAliases[strtolower(StdString::of($paramName)->snakeCase())] = true;

        return $parameterNameAliases;
    }
}
