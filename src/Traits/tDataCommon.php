<?php
namespace Poirot\Std\Traits;

trait tDataCommon
{
    /**
     * Construct
     *
     * @param iterable $data
     */
    function __construct(iterable $data = null)
    {
        if ($data !== null)
            $this->import($data);
    }

    // Implement Magic Method For var_export Initialization:

    /**
     * @inheritDoc
     */
    function empty()
    {
        foreach($this as $k => $_)
            $this->del($k);

        return $this;
    }

    /**
     * @inheritDoc
     */
    function isEmpty()
    {
        return $this->count() === 0;
    }
}
