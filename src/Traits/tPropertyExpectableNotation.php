<?php
namespace Poirot\Std\Traits;

/*
$expected = $this
    ->_hasClassDefinedExpectedProperty($propKey)      // first look for class defined property
    ->_hasMethodDefinedExpectedProperty($propMethod)  // then within method docblock
    ->_hasClassFieldDefinedExpectedProperty($propKey) // then class defined field variables
    ->isMatchedToValue($propValue);
*/

/**
 * Expected Definitions Can Defined as Below:
 *
 * @property string sanitizedProperty @required description of property usage
 */
trait tPropertyExpectableNotation
{
    use tPropertySanitizeKey;

    protected $_c__previousExpectedObject;

    /** @var string|\DateTime @required yyyy-mm-ddThh:mm:ss (1983-08-13) */
    # protected $birthDate;
    /** @var string */
    # protected $mobile;
    /** @var int @required Gender 1=male|2=female */
    # protected $gender;
    /** @var string @required */
    # protected $passportNo;
    /** @var int @required description about field */
    # protected $planCode;

    /**
     * As a sample method definition we can define expected type:
     *
     * @return string|null|object|\Stdclass|void
     */
    # function sampleMethod()


    /**
     * Has Any Definition Expectation On Class DocBlock?
     *
     * @param string $key
     *
     * @return Definition|null
     * @throws
     */
    protected function _hasClassDefinedExpectedProperty($key)
    {
        $ref = new \ReflectionClass($this);
        $classDocComment = $ref->getDocComment();

        // detect required expected from Class DocBlock:
        $regex = '/(@property\s*)(?P<EXPECTED>[\w\|]+\s*)(' . $key . '+\s*)@required/';

        $definition = null;
        if (false !== $classDocComment
            && preg_match($regex, $classDocComment, $matches)
        )
            $definition = $matches['EXPECTED'];

        return $this->__createExpectedObject($definition);
    }

    /**
     * Has Any Definition Expectation On Method DocBlock return notation?
     *
     * @param string $methodName
     *
     * @return Definition|null
     * @throws
     */
    protected function _hasMethodDefinedExpectedProperty($methodName)
    {
        $ref = new \ReflectionClass($this);

        try {
            $methodReflect = $ref->getMethod($methodName);
        } catch (\Exception $e) {
            return null;
        }


        $definition = null;
        $methodComment = $methodReflect->getDocComment();
        $regex = '/(@required\s)(.*\s+|)+(@return\s(?P<EXPECTED>[\w\s\|]*))/';
        if (false !== $methodComment
            && preg_match($regex, $methodComment, $matches)
        )
            $definition = $matches['EXPECTED'];

        return $this->__createExpectedObject($definition);
    }

    /**
     * Has Any Definition Expectation On Class Field DocBlock return notation?
     *
     * @param string $key
     *
     * @return Definition|null
     * @throws
     */
    protected function _hasClassFieldDefinedExpectedProperty($key)
    {
        $ref = new \ReflectionClass($this);

        try {
            $propReflect = $ref->getProperty($key);
        } catch (\Exception $e) {
            return null;
        }

        $definition = null;
        $propComment = $propReflect->getDocComment();
        $regex = '/(@var\s+)(?P<EXPECTED>[\w\s\|]*)(@required)/';
        if (false !== $propComment
            && preg_match($regex, $propComment, $matches)
        )
            $definition = $matches['EXPECTED'];

        return $this->__createExpectedObject($definition);
    }

    // ...

    private function __createExpectedObject($definition)
    {
        return $this->_c__previousExpectedObject = new Definition(
            $definition,
            $this->_c__previousExpectedObject,
            $this
        );
    }
}

final class Definition
{
    use tPropertyExpectableNotation;

    private $definition;
    /** @var tPropertyExpectableNotation */
    private $mainClass;
    /** @var Definition */
    private $previous;


    /**
     * Definition constructor.
     *
     * @param string $definition
     * @param Definition $previous
     * @param tPropertyExpectableNotation $mainClass
     */
    function __construct($definition, $previous, $mainClass)
    {
        $this->definition = $definition;
        $this->previous   = $previous;
        $this->mainClass  = $mainClass;
    }

    /**
     * Match a value against expected docblock comment
     *
     * @param mixed $value
     *
     * @return bool
     */
    function isMatchedToValue($value)
    {
        // reset chain
        $this->mainClass->_c__previousExpectedObject = false;

        ## Run the result from self
        #
        $valueType = gettype($value);
        foreach($this->getDefinitions() as $def) {
            if ($valueType == $def && $value != null)
                return true;

            elseif ($valueType === 'object' && is_a($value, $def))
                return true;
        }

        return false;
    }

    /**
     * Get Definitions
     *
     * @return array|false
     */
    protected function getDefinitions()
    {
        ## Try to get result from previous definitions on the chain
        #
        if ($this->previous && $definitions = $this->previous->getDefinitions())
            return $definitions;


        $definitions = [];
        foreach ( explode('|', $this->definition) as $def) {
            $def = trim($def);
            if ('' != $def)
                $definitions[] = $def;
        }

        return !empty($definitions) ? $definitions : false;
    }

    // Proxies call to main class

    /**
     * @return tPropertyExpectableNotation
     */
    function _hasClassDefinedExpectedProperty($key)
    {
        return $this->previous->_hasClassDefinedExpectedProperty($key);
    }

    /**
     * @return tPropertyExpectableNotation
     */
    function _hasMethodDefinedExpectedProperty($methodName)
    {
        return $this->previous->_hasMethodDefinedExpectedProperty($methodName);
    }

    /**
     * @return tPropertyExpectableNotation
     */
    function _hasClassFieldDefinedExpectedProperty($key)
    {
        return $this->previous->_hasClassFieldDefinedExpectedProperty($key);
    }
}
