<?php
namespace Poirot\Std\Traits;

use Poirot\Std\Type\StdString;

trait tPropertySanitizeKey
{
    /**
     * Sanitize Property Key exp. AttributeName -> attribute_name
     *
     * @param string $key
     *
     * @return string
     */
    protected function _sanitizeToPropertyKey($key)
    {
        return (string) StdString::of($key)->snakeCase();
    }

    /**
     * Sanitize Back exp. AttributeName <- attribute_name
     *
     * @param string $key
     *
     * @return string
     */
    protected function _sanitizeBackFromPropertyKey($key)
    {
        return (string) StdString::of($key)->pascalCase();
    }
}
