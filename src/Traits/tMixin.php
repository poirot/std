<?php
namespace Poirot\Std\Traits;

use Poirot\Std\ArgumentsResolver;

trait tMixin
{
    /**
     * Registered Custom Methods
     * @var array
     */
    static private $_t__methods = [];

    /**
     * Proxy Call To Registered Mixin Methods
     *
     * @param $methodName
     * @param array $args
     *
     * @return mixed
     * @throws \BadMethodCallException
     */
    function __call($methodName, array $args)
    {
        if (! static::hasMixinMethod($methodName) )
            throw new \BadMethodCallException(sprintf(
                'Method %s::%s does not exist.', static::class, $methodName
            ));

        $callable = static::$_t__methods[$methodName];
        if ($callable instanceof \Closure)
            return call_user_func_array($callable->bindTo($this, static::class), $args);

        return $callable(...$args);
    }

    /**
     * Mix another object into the class.
     *
     * @param  object $object
     * @param  bool  $replace
     *
     * @throws \ReflectionException
     */
    static function importMethodsFrom($object, $replace = true)
    {
        $refClass = new \ReflectionClass($object);
        $methods = ( $refClass )
            ->getMethods(\ReflectionMethod::IS_PUBLIC);

        foreach ($methods as $method) {
            if ( $replace || !static::hasMixinMethod($method->name) ) {
                $methodName = $method->name;
                static::addMixinMethod($methodName, function (...$args) use ($methodName, $method, $object)
                {
                    $argumentResolver = (new ArgumentsResolver(
                        new ArgumentsResolver\CallableResolver([$object, $method->name])
                    ))->withOptions($args);

                    $r = $argumentResolver->resolve()->__invoke();
                    if (! $r instanceof \Closure)
                        return $r;

                    // Classes may provide closures to bind to the mixin class; to have access to protected or other
                    // resources inside class.
                    // Replace method again with provided \Closure and return the result
                    static::addMixinMethod($methodName, $r);
                    $arguments = $argumentResolver->getResolvedArguments();
                    return call_user_func_array($r->bindTo($this, static::class), $arguments + $args);
                });
            }
        }
    }

    /**
     * Attach Custom Method To Mixin Object
     *
     * @param string   $methodName
     * @param callable $methodCallable
     */
    static function addMixinMethod($methodName, callable $methodCallable)
    {
        self::$_t__methods[$methodName] = $methodCallable;
    }

    /**
     * Check If Method With Given Name Exists?
     *
     * @param string $methodName
     *
     * @return bool
     */
    static function hasMixinMethod(string $methodName) : bool
    {
        return isset(static::$_t__methods[$methodName]);
    }

    /**
     * List Mixin Available Methods
     *
     * @return array[string]
     */
    static function listMixinMethods()
    {
        return array_keys(self::$_t__methods);
    }
}
