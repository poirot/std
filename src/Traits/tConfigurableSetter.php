<?php
namespace Poirot\Std\Traits;

use Poirot\Std\ArgumentsResolver;
use Poirot\Std\Configurable\Params;
use Poirot\Std\Exceptions\Configurable\ConfigurationPropertyTypeError;
use Poirot\Std\Exceptions\Configurable\UnknownConfigurationPropertyError;
use Poirot\Std\Struct\DataSet\DataSetPropertyObject;
use ReflectionMethod;

trait tConfigurableSetter
{
    use tConfigurable;
    use tPropertySanitizeKey;

    protected $writableMethodPrefixes;


    /**
     * @inheritDoc
     */
    function setConfigs(array $options, array $skipExceptions = [])
    {
        if ( empty($options) )
            // nothing to do!
            return $options;


        if (! isset($this->writableMethodPrefixes))
            $this->writableMethodPrefixes = DataSetPropertyObject::PropertyMethodPrefixes['writable'];

        foreach($options as $key => $val)
        {
            try {
                $methodExecuted = false;
                foreach ($this->writableMethodPrefixes as $prefix) {
                    /*
                       It can be public, protected or private methods
                       usually protected methods can be used as helper
                       to build class with options.
                    */
                    $setterMethodName = $prefix . $this->_sanitizeBackFromPropertyKey($key);
                    if (! method_exists($this, $setterMethodName))
                        continue;

                    try {
                        $this->_setConfigThroughMethod($setterMethodName, $val);
                        $methodExecuted = true;
                    } catch (\Throwable $e) {
                        throw new ConfigurationPropertyTypeError(
                            'The option has error while trying to set value.', $e->getCode(), $e);
                    }
                }

                if (! $methodExecuted) {
                    throw new UnknownConfigurationPropertyError(sprintf(
                        'Configuration Setter option "%s" not found.'
                        , $key
                    ));
                }
            } catch (\Exception $e) {
                if ($this->_shouldThrowConfigurationException($e, $skipExceptions))
                    throw $e;
            }
        }

        return $this;
    }

    // ..

    protected function _setConfigThroughMethod(string $methodName, $value)
    {
        $reflectionMethod = new ReflectionMethod($this, $methodName);
        if ($value instanceof Params
            || is_array($value) && $this->isSetterMethodShouldResolvedByParams($reflectionMethod)) {
            $argumentsResolver = new ArgumentsResolver(new ArgumentsResolver\ReflectionResolver(
                $reflectionMethod,
                $value instanceof Params ? $value->asArray() : $value
            ));

            return call_user_func_array([$this, $methodName], $argumentsResolver->getResolvedArguments());
        }

        return call_user_func([$this, $methodName], $value);
    }

    protected function isSetterMethodShouldResolvedByParams(\ReflectionFunctionAbstract $reflectionMethod): bool
    {
        return count($reflectionMethod->getParameters()) > 1;
    }
}
