<?php
namespace Poirot\Std\Traits;

use Poirot\Std\Struct\DataSet\DataSetPropertyObject;

/**
 * On main class that uses this trait you should put this docblock to enable support:
 * @excludeByNotation enabled
 *
 * Method exclude notation can put on class docblock
 * @method set_Id($value) @ignore
 * @method set__Pclass($value) @ignore
 *
 * or can be placed on the method dockblock
 * @ignore
 */
trait tSetterGetterPropertyReceptive
{
    use tPropertySanitizeKey;

    /** @var array */
    protected $excludedPropertyMethodNames = [];
    protected $excludedPropertyMethodNamesByNotation = [];

    protected $refuseExcludedPropertyMethodNames = [];

    protected $_cachedPropertiesProcessed = null; // it must be null
    protected $_isExcludedPropertyNotationProcessed = false; // used as internal cache
    /** @var \ReflectionClass */
    protected $_cachedSelfReflectionClass;


    /**
     * Exclude given method names to not consider as Property Method
     *
     * @param string[] $methodName
     *
     * @return $this
     */
    function excludePropertyMethod(string...$methodName)
    {
        $this->_cachedPropertiesProcessed = null;
        foreach($methodName as $ignoredMethodName)
            $this->excludedPropertyMethodNames[$ignoredMethodName] = true;

        return $this;
    }

    /**
     * Include Excluded method names to not consider as Property Method
     *
     * @param string[] $methodName
     *
     * @return $this
     */
    function refuseExcludedPropertyMethod(string...$methodName)
    {
        $this->_cachedPropertiesProcessed = null;
        foreach($methodName as $includedMethodName)
            $this->refuseExcludedPropertyMethodNames[$includedMethodName] = true;

        return $this;
    }

    /**
     * Check Whether Any Setter Method Exists For This Key?
     *
     * @param string $key
     * @param $operator
     *
     * @return false|string Name of method
     */
    protected function _hasPropertyOperatorMethod($key, $operator)
    {
        $propertyMethods = $this->_getDataSetPropertyObjects($this->dataSetMethodVisibilities);
        foreach ($propertyMethods as $property => $availableMethods) {
            if ( $property !== $this->_sanitizeToPropertyKey($key) )
                continue;

            /** @var DataSetPropertyObject $method */
            foreach ($availableMethods as $method) {
                if ( $operator === DataSetPropertyObject::Writable && $method->isWritable() )
                    return $method->getMethodName();
                elseif ( $operator === DataSetPropertyObject::Readable && $method->isReadable() )
                    return $method->getMethodName();
            }
        }

        return false;
    }

    /**
     * Get Setter/Getter Properties Objects
     *
     * @param int $methodsReflectionFiltering Any bitwise disjunction of reflection method filter
     *
     * @return \Generator
     */
    protected function _getDataSetPropertyObjects($methodsReflectionFiltering = \ReflectionMethod::IS_PUBLIC)
    {
        if ($this->_cachedPropertiesProcessed === null)
        {
            $props = [];
            $methods = $this->_makePropertyReceptiveReflection()->getMethods($methodsReflectionFiltering);
            foreach($methods as $method) {
                $methodName = $method->getName();
                if (! DataSetPropertyObject::isDataSetPropertyMethod($methodName) )
                    // Method is not Readable nor Writable Property method.
                    continue;

                $excludedMethods = $this->_getExcludedPropertyMethodNames();
                if ( isset($excludedMethods[$methodName]) )
                    // Method is Ignored to be a Property method.
                    continue;

                $propertyObject = new DataSetPropertyObject($method);
                $props[$propertyObject->getPropertyName()][$methodName] = $propertyObject;
            }

            $this->_cachedPropertiesProcessed = $props;
        }

        foreach ($this->_cachedPropertiesProcessed as $name => $properties)
            yield $name => $properties;
    }

    /**
     * Get List Of Excluded Methods By Parsing Annotation Definition
     * From Class and Then Look on Each Method Doc.
     *
     * Note:
     * Class dockblock should contains:
     * @excludeByNotation enabled|Enabled|ENABLED
     * to start parsing ignored methods form notations.
     *
     * @return array
     */
    protected function _getExcludedPropertyMethodNames(): array
    {
        ## Parse Annotation Doc
        #
        if (!$this->_isExcludedPropertyNotationProcessed && $this->_isAnnotationEnabled())
        {
            $ref = $this->_makePropertyReceptiveReflection();
            $classDocComment = $ref->getDocComment();

            ## Ignored methods from Class DocComment:
            #
            preg_match_all('/.*[\n]?/', $classDocComment, $lines);
            $lines = $lines[0];
            $regex = '/.+(@method).+((?P<method_name>\b\w+)(\(.*\))*)\s@ignore\s*$/';
            foreach($lines as $line) {
                if (preg_match($regex, $line, $matches))
                    $this->excludedPropertyMethodNamesByNotation[$matches['method_name']] = true;
            }

            ## ignored methods from Method DocBlock
            #
            $methods = $ref->getMethods(\ReflectionMethod::IS_PUBLIC);
            foreach($methods as $m) {
                $mc = $m->getDocComment();
                if ($mc !== false && preg_match('/@ignore/', $mc, $matches))
                    $this->excludedPropertyMethodNamesByNotation[$m->getName()] = true;
            }

            if ( empty($this->excludedPropertyMethodNamesByNotation) )
                throw new \RuntimeException(sprintf(
                    'Class "%s" is meant to have excluded property methods by @ignore notation, nothing found.'
                    , get_called_class()
                ));
        }

        $this->_isExcludedPropertyNotationProcessed = true;
        $excludedMethods = array_merge($this->excludedPropertyMethodNamesByNotation, $this->excludedPropertyMethodNames);
        // remove methods which forced to be considered as method property
        $excludedMethods = array_diff_key($excludedMethods, $this->refuseExcludedPropertyMethodNames);
        return $excludedMethods;
    }

    // ..

    protected function _isAnnotationEnabled()
    {
        $ref = $this->_makePropertyReceptiveReflection();
        $classDocComment = $ref->getDocComment();
        // Check whether checking for Ignored Methods through dockblocks is Enabled?
        return $classDocComment !== false
            && preg_match_all('/.+(@excludeByNotation)\s+(enabled)|(Enabled)|(ENABLED)\s*$/', $classDocComment);
    }

    protected function _makePropertyReceptiveReflection(): \ReflectionClass
    {
        if ($this->_cachedSelfReflectionClass === null)
            $this->_cachedSelfReflectionClass = new \ReflectionClass($this);

        return $this->_cachedSelfReflectionClass;
    }
}
