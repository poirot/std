<?php
namespace Poirot\Std\Traits;

use Poirot\Std\Exceptions\Configurable\UnknownResourceError;
use Poirot\Std\Type\StdArray;

trait tConfigurable
{
    /**
     * @inheritDoc
     * @see ipConfigurable
     */
    abstract function setConfigs(array $options, array $skipExceptions = []);


    /**
     * @inheritDoc
     * @see ipConfigurable
     */
    static function parse($optionsResource)
    {
        if (is_iterable($optionsResource))
            $optionsResource = StdArray::of($optionsResource, false)->asInternalArray;

        if (! is_array($optionsResource))
            throw new UnknownResourceError(sprintf(
                'Resource %s is unknown by configurable and unable to parse.'
                , is_object($optionsResource) ? get_class($optionsResource) : gettype($optionsResource)
            ));

        return $optionsResource;
    }

    protected function _shouldThrowConfigurationException(\Exception $e, array $skipExceptions): bool
    {
        $throw = true;
        foreach ($skipExceptions as $exceptionSkipped) {
            if ($e instanceof $exceptionSkipped) {
                $throw = false;
                break;
            }
        }

        return $throw;
    }
}
