<?php
namespace PoirotTest\Std;

use PHPUnit\Framework\TestCase;
use Poirot\Std\InvokableResponder\AggregateResult;
use Poirot\Std\InvokableResponder\KeyValueResult;
use Poirot\Std\InvokableResponder\MergeResult;
use Poirot\Std\ResponderChain;
use Poirot\Std\Type\StdArray;
use Poirot\Std\Type\StdString;

/**
 * @see ResponderChain
 */
class ResponderChainTest
    extends TestCase
{
    function testArrayResultsByDefaultGetMergedToLastResult()
    {
        $result = ResponderChain::new()
            ->then(function () {
                return 'first';
            })->then(function() {
                return ['second'];
            })->then(function() {
                return ['third'];
            })
            ->handle();

        $this->assertEquals(['first', 'second', 'third'], $result);

        // Same behaviour on StdArray object

        $result = ResponderChain::new()
            ->then(function () {
                return 'first';
            })->then(function() {
                return StdArray::of(['second']);
            })->then(function() {
                return StdArray::of(['third']);
            })
            ->handle();

        $this->assertEquals(['first', 'second', 'third'], $result);

        // Check None Scalar Values

        $stringObject = StdString::of('first');
        $result = ResponderChain::new()
            ->then(function () use ($stringObject) {
                return $stringObject;
            })->then(function() {
                return StdArray::of(['second']);
            })->then(function() {
                return StdArray::of(['third']);
            })
            ->handle();

        $this->assertEquals([$stringObject, 'second', 'third'], $result);
    }

    function testTheResultsWhichGetReplacedByLastResult()
    {
        $result = ResponderChain::new()
            ->then(function () {
                return 'first';
            })->then(function() {
                return 'second';
            })->then(function() {
                return 'third';
            })
            ->handle();

        $this->assertEquals('third', $result);


        $result = ResponderChain::new()
            ->then(function () {
                return ['first' => 1];
            })
            ->then(function () {
                return 'second will replaced first';
            })
            ->then(function () {
                return ['third' => 3]; // get merged, because it's array
            })
            ->handle();

        $this->assertEquals(['second will replaced first', 'third' => 3], $result);
    }

    function testKeyValueAsCallableResult()
    {
        // Test will converted to an single key associative array
        $result = ResponderChain::new()
            ->then(function () {
                return new KeyValueResult('name', 'PHP');
            })
            ->handle();

        $this->assertEquals(['name' => 'PHP'], $result);

        // Test will get replace the last result
        $result = ResponderChain::new()
            ->then(function () {
                return 'first';
            })
            ->then(function () {
                return ['second'];
            })
            ->then(function () {
                return new KeyValueResult('name', 'Python');
            })
            ->then(function () {
                return new KeyValueResult('name', 'PHP');
            })
            ->handle();

        $this->assertEquals(['name' => 'PHP'], $result);
    }

    function testAggregateAsCallableResult()
    {
        // test will replace the last result
        $result = ResponderChain::new()
            ->then(function() {
                return ['first_release' => '1995'];
            })
            ->then(function () {
                return new AggregateResult(['name' => 'PHP', 'type' => 'Dynamic Language']);
            })
            ->handle();

        $this->assertEquals([
            'name' => 'PHP',
            'type' => 'Dynamic Language',
        ], $result);

        //
        $result = ResponderChain::new()
            ->then(function () {
                return new AggregateResult(
                    new AggregateResult('PHP', 'Dynamic Language', ['name' => 'PHP']),
                    ['designed_by' => 'Rasmus Lerdorf'],
                    ['first_release' => '1995']
                );
            })
            ->handle();

        $this->assertEquals([
            'PHP',
            'Dynamic Language',
            'name' => 'PHP',
            'designed_by' => 'Rasmus Lerdorf',
            'first_release' => '1995',
        ], $result);
    }

    function testMergeAsCallableResult()
    {
        $result = ResponderChain::new()
            ->then(function () {
                return 'first';
            })->then(function() {
                return new MergeResult(['second']);
            })->then(function() {
                return new MergeResult(['third']);
            })
            ->handle();

        $this->assertEquals(['first', 'second', 'third'], $result);


        $result = ResponderChain::new()
            ->then(function() {
                return ['first_release' => '1995'];
            })
            ->then(function () {
                return new MergeResult(['name' => 'PHP', 'type' => 'Dynamic Language']);
            })
            ->handle();

        $this->assertEquals([
            'first_release' => '1995',
            'name' => 'PHP',
            'type' => 'Dynamic Language',
        ], $result);
    }

    function testDefaultParamsCanBeResolvedToCallables()
    {
        $defaultParams = [
            'view_renderer' => '<b>%s</b>',
            'layout'        => '<p>%s</p>',
        ];

        $result = ResponderChain::new($defaultParams)
            ->then(function($viewRenderer) use ($defaultParams) {
                $this->assertEquals($defaultParams['view_renderer'], $viewRenderer);
                return sprintf($viewRenderer, 'Welcome!');
            })
            // lastResult is result from previous call
            // layoutName resolved to callable from defaultParams
            ->then(function($lastResult, $layout) use ($defaultParams) {
                $this->assertEquals($defaultParams['layout'], $layout);
                return sprintf($layout, $lastResult);
            })
            ->handle();

        $this->assertEquals('<p><b>Welcome!</b></p>', $result);
    }

    function testParamsCanBeResolvedToNextCallable()
    {
        // array
        ResponderChain::new()
            ->then(function() {
                return ['name' => 'php'];
            })
            ->then(function() {
                return ['first_release' => '1995'];
            })
            ->then(function($name, $firstRelease) {
                $this->assertEquals('php', $name);
                $this->assertEquals('1995', $firstRelease);
                return strtoupper($name);
            })
            ->handle();


        // key/value
        ResponderChain::new()
            ->then(function() {
                return new KeyValueResult('name', 'php');
            })
            ->then(function() {
                return new KeyValueResult('first_release', '1995');
            })
            ->then(function($name, $firstRelease) {
                $this->assertEquals('php', $name);
                $this->assertEquals('1995', $firstRelease);
                return strtoupper($name);
            })
            ->handle();

        // aggregate
        ResponderChain::new()
            ->then(function() {
                return new AggregateResult(['name' => 'php']);
            })
            ->then(function() {
                return new AggregateResult(['first_release' => '1995']);
            })
            ->then(function($name, $firstRelease) {
                $this->assertEquals('php', $name);
                $this->assertEquals('1995', $firstRelease);
                return strtoupper($name);
            })
            ->handle();


        // merge
        ResponderChain::new()
            ->then(function() {
                return new MergeResult(['name' => 'php']);
            })
            ->then(function() {
                return new MergeResult(['first_release' => '1995']);
            })
            ->then(function($name, $firstRelease) {
                $this->assertEquals('php', $name);
                $this->assertEquals('1995', $firstRelease);
                return strtoupper($name);
            })
            ->handle();
    }

    function testOnFailureHandling()
    {
        $result = ResponderChain::new()
            ->then(function() {
                return ['designed_by' => 'Rasmus Lerdorf'];
            })
            ->then(function() {
                return new KeyValueResult('name', 'php');
            })
            ->then(function() {
                throw new \RuntimeException('Stop Executing Because Of Something');
            })
            ->then(function() {
                return 'Code wont reach here.';
            })
            ->onFailure(function(\Exception $error, $name, $designedBy) {
                return $error->getMessage() . ': ' . $name  . ' ' . $designedBy;
            })
            ->handle();

        $this->assertEquals('Stop Executing Because Of Something: php Rasmus Lerdorf', $result);
    }
}
