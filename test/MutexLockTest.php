<?php
namespace PoirotTest\Std;

use PHPUnit\Framework\TestCase;
use org\bovigo\vfs;
use Poirot\Std\Interfaces\iMutexLock;
use Poirot\Std\MutexLock;

/**
 * @see MutexLock
 */
class MutexLockTest
    extends TestCase
{
    protected $lockFilesVfsPath = 'nfs';
    protected $lockFilesRealPath;

    function setUp()
    {
        $this->lockFilesRealPath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'testmutex';

        vfs\vfsStreamWrapper::register();
        vfs\vfsStreamWrapper::setRoot( new vfs\vfsStreamDirectory($this->lockFilesVfsPath) );

        if (! is_dir(vfs\vfsStream::url($this->lockFilesVfsPath)) )
            mkdir(vfs\vfsStream::url($this->lockFilesVfsPath));

        if (! is_dir($this->lockFilesRealPath) )
            mkdir($this->lockFilesRealPath);
    }

    function tearDown()
    {
        foreach (new \DirectoryIterator( vfs\vfsStream::url($this->lockFilesVfsPath) ) as $file)
            if (! $file->isDot() )
                unlink( $file->getPathname() );

        rmdir( vfs\vfsStream::url($this->lockFilesVfsPath) );


        foreach (new \DirectoryIterator($this->lockFilesRealPath) as $file)
            if (! $file->isDot() )
                unlink( $file->getPathname() );

        rmdir($this->lockFilesRealPath);
    }


    /**
     * When mutex lock make a lock on a resource we let them to acquire lock on self
     * owned resource.
     *
     * @dataProvider lockImplementorProvider
     *
     * @param iMutexLock $mutexLock
     */
    function testAllowToAcquireLockOnSelfOwned(iMutexLock $mutexLock)
    {
        $mutexLock->acquire();
        $this->assertTrue($mutexLock->isLocked());
        // test if it's allow to acquire lock again
        $this->assertTrue($mutexLock->acquire());
    }

    /**
     * When resource is locked no lock can acquired
     */
    function testDisallowToAcquireLockOnOtherImplementedLocks()
    {
        $mutexLock = $this->provideFlockMockLock();
        $mutexLock->acquire();

        $otherMutexLock =$this->provideFlockMockLock();
        $this->assertFalse($otherMutexLock->acquire());

        $mutexLock->release();
    }

    /**
     * When we lock a resource the object with same setup will consider it as
     * locked
     */
    function testLockedResourceWillKeepLikeOnObjectWithSameResource()
    {
        $mutexLock = $this->provideFlockMockLock();
        $mutexLock->acquire();

        $otherMutexLock =$this->provideFlockMockLock();
        $this->assertTrue($otherMutexLock->isLocked());
    }

    /**
     * Different Objects with lock on same resources can release resource
     */
    function testAllowToReleaseLockAcquiredOnSameResources()
    {
        $mutexLock = $this->provideFlockMockLock();
        $mutexLock->acquire();

        $otherMutexLock =$this->provideFlockMockLock();
        $this->assertTrue($otherMutexLock->release());
        $this->assertFalse($mutexLock->isLocked());
    }

    /**
     * @dataProvider lockImplementorProvider
     *
     * @param iMutexLock $mutexLock
     */
    function testAcquireAndRelease(iMutexLock $mutexLock)
    {
        $this->assertTrue($mutexLock->acquire());
        $this->assertTrue($mutexLock->isLocked());
        $this->assertTrue($mutexLock->release());
        $this->assertFalse($mutexLock->isLocked());
    }

    /**
     * By destructing object lock will released
     */
    function testLockReleasedAfterImplementorDestruct()
    {
        $otherMutexLock = $this->provideFlockMockLock();
        $otherMutexLock->acquire();
        unset($otherMutexLock);

        $this->assertTrue($this->provideFlockMockLock()->acquire());
    }

    /**
     * By calling unset() the __destruct method of object will called
     * but it will not release the resource as the lock is not acquired by the same object
     */
    function testOnlyOwnedImplementedLockCanReleasedAfterDestruct()
    {
        $mutexLock = $this->provideFlockMockLock();
        $mutexLock->acquire();

        $otherMutexLock = $this->provideFlockMockLock();
        $this->assertTrue($otherMutexLock->isLocked());

        unset($otherMutexLock);
        $this->assertTrue($mutexLock->isLocked());
    }

    /**
     * @dataProvider lockImplementorProvider
     *
     * @param iMutexLock $mutexLock
     */
    function testExpiration(iMutexLock $mutexLock)
    {
        $expiration = 1;

        $mutexLock->acquire($expiration);
        $this->assertTrue($mutexLock->isLocked());

        sleep($expiration + 1);
        $this->assertFalse($mutexLock->isLocked());
        $this->assertTrue($mutexLock->acquire());
    }

    function testWhenTTLExpiredOtherImplementorsCanAcquireLock()
    {
        $expiration = 1;
        $otherMutexLock = $this->provideFlockMockLock();
        $otherMutexLock->acquire($expiration);

        $mutexLock = $this->provideFlockMockLock();
        $this->assertFalse($mutexLock->acquire());
        sleep($expiration + 1);
        $this->assertTrue($mutexLock->acquire());
    }

    // Providers:

    /**
     * @return array
     */
    function lockImplementorProvider()
    {
        return [
            // virtual fs mock
            [$this->provideFlockMockLock()],
            // real file lock
            [$this->provideFlockLock()],
        ];
    }

    protected function provideFlockMockLock()
    {
        return (new MutexLock('testing'))
            ->giveLockDirPath(vfs\vfsStream::url($this->lockFilesVfsPath . '/'));
    }

    protected function provideFlockLock()
    {
        $this->lockFilesRealPath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'testmutex';
        return (new MutexLock('testing'))
            ->giveLockDirPath($this->lockFilesRealPath);
    }
}
