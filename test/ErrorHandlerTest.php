<?php
namespace PoirotTest\Std;

use PHPUnit\Framework\TestCase;
use Poirot\Std\ErrorHandler;

/**
 * @see ErrorHandler
 */
class ErrorHandlerTest
    extends TestCase
{
    function setUp()
    {
       error_reporting(E_ALL);
    }

    function tearDown()
    {
        ErrorHandler::destroy();
    }


    function testRegisterAndRestoreHandler()
    {
        ErrorHandler::handle();
        $this->assertEquals(1, ErrorHandler::getNestedLevel());
        $this->assertErrorHandlerRegisteredProperly($this->getCurrentErrorHandler());

        // Test nested handler
        ErrorHandler::handle();
        $this->assertEquals(2, ErrorHandler::getNestedLevel());
        $this->assertErrorHandlerRegisteredProperly($this->getCurrentErrorHandler());

        ErrorHandler::destroy();
        $this->assertErrorHandlerRegisteredProperly($this->getCurrentErrorHandler(), true);

        // test wont register multiple handler with each nested handler
        ErrorHandler::handle();
        ErrorHandler::handle();
        restore_error_handler();
        $this->assertErrorHandlerRegisteredProperly($this->getCurrentErrorHandler(), true);

        // destroy doesnt care to handler change
        ErrorHandler::destroy();
        $this->assertErrorHandlerRegisteredProperly($this->getCurrentErrorHandler(), true);
    }

    function testHandlerWontReceiveSuppressErrors()
    {
        ErrorHandler::handle();
        @trigger_error('Hello', E_USER_WARNING);
        $this->assertEmpty(ErrorHandler::handleDone());
        error_clear_last();

        // get list of errors that happen when handling done
        ErrorHandler::handle();
        trigger_error('Hello', E_USER_WARNING);
        $this->assertIsIterable(ErrorHandler::handleDone());

        // assert that error handler wont handle errors anymore
        $this->assertErrorHandlerRegisteredProperly($this->getCurrentErrorHandler(), true);
    }

    function testHandlerCallableCanPassErrorToPhpDefault()
    {
        $this->expectOutputRegex('/Warning: Hello in/');
        ErrorHandler::handle(ErrorHandler::E_HandleAll, function() {
            // Do anything ..
            return false; // and then let php handle error
        });

        trigger_error('Hello', E_USER_WARNING);
    }

    function testHandleErrorLevel()
    {
        ob_start();

        // error handler will handle just set of defined error level
        ErrorHandler::handle(E_USER_WARNING | E_USER_NOTICE);
        $a .= $b; // generate dummy notice

        $output = ob_get_clean(); // php itself handled the notice error
        $this->assertRegExp('/Notice: /', $output);

        $this->assertEmpty(ErrorHandler::handleDone());
    }

    function testErrorHandlerNotCatchSilencedErrors()
    {
        // reporting error only on php warning errors
        error_reporting(E_WARNING);

        // By default error handler will not catch errors that is silenced by error_reporting level
        ErrorHandler::handle();
        $a .= $b; // generate dummy notice

        $this->assertEmpty(ErrorHandler::handleDone());
    }

    function testErrorHandlerRegardlessOfErrorReportingCatchAllErrors()
    {
        error_reporting(E_WARNING);

        ErrorHandler::handle(ErrorHandler::E_HandleAll | ErrorHandler::E_ScreamAll);
        $a .= $b; // generate dummy notice

        $messages = ['Undefined variable: a', 'Undefined variable: b'];
        foreach (ErrorHandler::handleDone() as $i => $error) {
            $this->assertInstanceOf(\ErrorException::class, $error);
            $this->assertEquals($messages[$i], $error->getMessage());
        }
    }

    function testNestedHierarchyErrorHandler()
    {
        $errorConcat = '';

        // Test PHP Errors Will not bubble up to prev level
        //
        ErrorHandler::handle(ErrorHandler::E_HandleAll, function() use (&$errorConcat) {
            $errorConcat .= 'Nested Level 1;'; });

        ErrorHandler::handle(ErrorHandler::E_HandleAll, function() use (&$errorConcat) {
            $errorConcat .= 'Nested Level 2;'; });

        trigger_error('Hello', E_USER_WARNING);
        $this->assertEquals('Nested Level 2;', $errorConcat);

        ErrorHandler::handleDone();
        ErrorHandler::handleDone();

        $this->assertErrorHandlerRegisteredProperly($this->getCurrentErrorHandler(), true);
    }

    function testConvertPhpErrorsToErrorException()
    {
        ErrorHandler::handle(E_ALL, function ($error) {
            throw $error;
        });


        try {
            include 'not_found_file'; // dummy warning error
            $this->fail('Exception expected');
        } catch (\ErrorException $e) {
            $this->assertEquals(E_WARNING, $e->getSeverity());
            $this->assertEquals(__FILE__, $e->getFile());
            $this->assertRegExp('/failed to open stream: No such file or directory/', $e->getMessage());

            $trace = $e->getTrace();
            $this->assertEquals(__FILE__, $trace[0]['file']);
            $this->assertEquals('include', $trace[0]['function']);

        } finally {
            ErrorHandler::handleDone();
        }
    }

    function testHandleExceptions()
    {
        $this->markTestSkipped('With PHPUnit we could not test custom exception handler.');


        $this->expectOutputString('Hello');

        ErrorHandler::handle(ErrorHandler::E_Exceptions, function(\ErrorException $e) use (&$errorConcat) {
            echo $e->getMessage();
        });

        throw new \Exception('Hello');
    }

    function testNestedHierarchyMakeThrowableErrorException()
    {
        $this->markTestSkipped('With PHPUnit we could not test custom exception handler.');


        $errorConcat = '';
        ErrorHandler::handle(ErrorHandler::E_HandleAll, function(\ErrorException $e) use (&$errorConcat) {
            $errorConcat .= $e->getMessage(); });

        ErrorHandler::handle(ErrorHandler::E_HandleAll, function($error) {
            // Throw an error exception that is caught by error handler
            throw $error; });


        trigger_error('Hello', E_USER_WARNING);
        $this->assertEquals('Hello', $errorConcat);

        ErrorHandler::destroy();
        $this->assertErrorHandlerRegisteredProperly($this->getCurrentErrorHandler(), true);
    }

    // ..

    function assertErrorHandlerRegisteredProperly($currErrHandler, $negative = false)
    {
        // Check the previous one is same handler?
        if ($currErrHandler instanceof \Closure) {
            $reflectionFunction = new \ReflectionFunction($currErrHandler);
            $currErrHandler = $reflectionFunction->getClosureScopeClass()
                ->getName();
        }

        if ($negative)
            $this->assertNotEquals(ErrorHandler::class, $currErrHandler);
        else
            $this->assertEquals(ErrorHandler::class, $currErrHandler);
    }

    protected function getCurrentErrorHandler()
    {
        $h = set_error_handler('var_dump');
        restore_error_handler();

        return $h;
    }

    protected function getCurrentExceptionHandler()
    {
        $h = set_exception_handler('var_dump');
        restore_exception_handler();

        return $h;
    }
}
