<?php
namespace PoirotTest\Std\Fixtures;

class SimpleClassFixture
{
    private $internalValue;

    function setInternalValue($internalValue = 1)
    {
        $this->internalValue = $internalValue;
        return $this;
    }

    function getInternalValue()
    {
        return $this->internalValue;
    }
}
