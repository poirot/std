<?php
namespace PoirotTest\Std\Fixtures;

class SimpleClassWithConstructorFixture
    extends SimpleClassFixture
{
    function __construct($x = null)
    {
        $this->setInternalValue($x);
    }
}
