<?php
namespace PoirotTest\Std\TestTypes;

use PHPUnit\Framework\TestCase;
use Poirot\Std\IteratorWrapper;
use Poirot\Std\Type\StdArray;

/**
 * @see StdArray
 */
class StdArrayTest
    extends TestCase
{
    function testConstructStrictly()
    {
        $stdArray = new StdArray([1, 2, 3, 4], true);
        $this->assertInstanceOf(StdArray::class, $stdArray);

        $stdArray = new StdArray(new StdArray([1, 2, 3, 4]), true);
        $this->assertInstanceOf(StdArray::class, $stdArray);

        $stdArray = new StdArray(new \ArrayIterator([1, 2, 3, 4]), true);
        $this->assertInstanceOf(StdArray::class, $stdArray);
    }

    function testConstructByDefaultSetToStrict()
    {
        $this->expectException(\UnexpectedValueException::class);
        new StdArray('invalid_value');
    }

    function testCanAccessToInternalArrayValue()
    {
        $stdArray = new StdArray([1, 2, 3, 4]);

        $this->assertEquals([1, 2, 3, 4], $stdArray->asInternalArray);
        $this->assertEquals([1, 2, 3, 4], $stdArray->value);
    }

    /**
     * @dataProvider provideTypeHintObjectData
     * @param $arrayProne \stdClass|object|string
     */
    function testHintingOtherTypesAsArray($arrayProne)
    {
        $stdArray = new StdArray($arrayProne, false);
        $this->assertEquals(['field' => 'value', 'otherField' => 'other-value'], $stdArray->asInternalArray);
    }

    function testMakeArrayFromStdClass()
    {
        $stdClass = $this->getDummyStdObject();

        $stdArray = StdArray::ofObject($stdClass);
        $this->assertEquals(['field' => 'value', 'otherField' => 'other-value'], $stdArray->asInternalArray);

        $stdClass->otherField = $this->getDummyStdObject();
        $stdArray = StdArray::ofObject($stdClass);
        $this->assertEquals(['field' => 'value', 'otherField' => ['field' => 'value', 'otherField' => 'other-value']],
            $stdArray->asInternalArray);
    }

    function testMakeArrayFromObject()
    {
        $stdArray = StdArray::ofObject($this->getDummyVariableObject());
        $this->assertEquals(['field' => 'value', 'otherField' => 'other-value'], $stdArray->asInternalArray);
    }

    function testMakeArrayFromJson()
    {
        $stdArray = StdArray::ofJson($this->getJsonString());
        $this->assertEquals(['field' => 'value', 'otherField' => 'other-value'], $stdArray->asInternalArray);
    }

    function testFailedWhenHintingUnknownTypes()
    {
        $this->expectException(\InvalidArgumentException::class);
        new StdArray('invalid_value', false);
    }

    function testItsTraversable()
    {
        $initialValue = ['a', 'b', 'c', 'd'];
        $stdArray = new StdArray($initialValue);
        $this->assertEquals($initialValue, $stdArray->asInternalArray);

        foreach ($stdArray as $i => $v)
            $this->assertEquals($initialValue[$i], $v);

        $this->assertEquals('d', $stdArray->current());
        $this->assertEquals(3, $stdArray->key());
    }

    function testItsIterable()
    {
        $initialValue = ['a', 'b', 'c', 'd'];
        $stdArray = new StdArray($initialValue);
        $this->assertEquals($initialValue, $stdArray->asInternalArray);

        $i = 0; do {
            $this->assertEquals($initialValue[$i], $stdArray->current());
        } while (++$i && $stdArray->next());

        $this->assertEquals('d', $stdArray->current());
        $this->assertEquals(3, $stdArray->key());

        $stdArray->rewind();

        $this->assertEquals('a', $stdArray->current());
        $this->assertEquals(0, $stdArray->key());
    }

    function testItsArrayAccessible()
    {
        $initialValue = [1, 2, 3, 4];
        $stdArray = new StdArray($initialValue);

        $this->assertEquals($initialValue, $stdArray->asInternalArray);
        foreach ($initialValue as $i => $v)
            $this->assertEquals($v, $stdArray[$i]);

        $this->assertNull($stdArray[100]);
        $this->assertFalse(isset($stdArray[100]));
        $this->assertTrue(isset($stdArray[1]));

        // Manipulate:
        $stdArray['field'] = 'value';
        $this->assertEquals('value', $stdArray['field']);

        unset($stdArray[3]);
        $this->assertEquals([1, 2, 3, 'field' => 'value'], $stdArray->asInternalArray);
    }

    function testLookup()
    {
        $stdArray = new StdArray([
            'Python' => [
                'name' => 'Python',
                "first_release" => "1991",
                "latest_release" => "3.8.0",
                "designed_by" => "Guido van Rossum",
                "description" => [
                    'name' => 'python',
                    "extension" => ".py",
                    'typing_discipline' => [
                        'tags' => ['Duck', 'dynamic', 'gradual'],
                        'description' => 'Python is an interpreted, object-oriented, '
                            .'high-level programming language with dynamic semantics. ',
                    ],
                    "license" => "Python Software Foundation License",
                ],
            ],
            'PHP' => [
                'name' => 'PHP',
                "first_release" => "1995",
                "latest_release" => "7.3.11",
                "designed_by" => "Rasmus Lerdorf",
                "description" => [
                    'name' => 'php',
                    "extension" => ".php",
                    "typing_discipline" => [
                        'tags' => ['Dynamic', 'weak'],
                        'description' => 'PHP (recursive acronym for PHP: Hypertext Preprocessor)'
                            .'is especially suited for web development and can be embedded into HTML.',
                    ],
                ],
            ],
        ]);

        // Fetch one by nested hierarchy lookup
        //
        $this->assertNull( $stdArray->lookup('/Missing/Path') );
        $this->assertNull( $stdArray->lookup('/PHP/first_release/not_found') );
        $this->assertSame('1995', $stdArray->lookup('/PHP/first_release') );

        // Regex lookup
        //
        $this->assertSame('1995', $stdArray->lookup('/PHP/~.+_release~') );

        // Fetch all, group of elements
        //
        $result = $stdArray->lookup('/PHP/*~.+_release~');
        $this->assertInstanceOf(\Generator::class, $result);
        $this->assertSame(
            ['first_release' => '1995', 'latest_release' => '7.3.11'],
            iterator_to_array($result)
        );

        // Specify the depth in expression
        //
        $this->assertSame('Guido van Rossum', $stdArray->lookup('/>/designed_by'));

        $result = $stdArray->lookup('/>/*designed_by');
        $this->assertInstanceOf(\Generator::class, $result);
        $this->assertSame(
            ['Guido van Rossum', 'Rasmus Lerdorf'],
            iterator_to_array($result, false)
        );

        // Depth + Regex + Group
        $result = $stdArray->lookup('/>2/*~extension|license~');
        $this->assertInstanceOf(\Generator::class, $result);
        $expected = [['extension'=>'.py'], ['license'=>'Python Software Foundation License'], ['extension'=>'.php']];
        $i = 0; foreach ($result as $k => $v) {
            $this->assertSame([$k=>$v], $expected[$i]);
            $i++;
        }

        $result = $stdArray->lookup('PHP/>/*~extension|license~');
        $this->assertInstanceOf(\Generator::class, $result);
        $expected = [['extension'=>'.php']];
        $i = 0; foreach ($result as $k => $v) {
            $this->assertSame([$k=>$v], $expected[$i]);
            $i++;
        }

        // Recursive Lookup in any depth
        //
        $result = $stdArray->lookup('//tags');
        $this->assertInstanceOf(\Generator::class, $result);
        $r = []; foreach ($result as $k => $v) {
            $this->assertSame('tags', $k);
            $r = array_merge($r, $v);
        }
        $this->assertSame(['Duck', 'dynamic', 'gradual', 'Dynamic', 'weak'], $r);


        // Combination of Variants query expressions
        //
        $result = $stdArray->lookup('/*~PHP|Python~//tags');
        $this->assertInstanceOf(\Generator::class, $result);
        $r = []; foreach ($result as $k => $v) {
            $this->assertSame('tags', $k);
            $r = array_merge($r, $v);
        }
        $this->assertSame(['Duck', 'dynamic', 'gradual', 'Dynamic', 'weak'], $r);

        $result = $stdArray->lookup('/~PHP~//tags');
        $this->assertInstanceOf(\Generator::class, $result);
        $r = []; foreach ($result as $k => $v) {
            $this->assertSame('tags', $k);
            $r = array_merge($r, $v);
        }
        $this->assertSame(['Dynamic', 'weak'], $r);

        $result = $stdArray->lookup('//tags/>/*0');
        $this->assertInstanceOf(\Generator::class, $result);
        $expected = [['Duck'], ['Dynamic']];
        $i = 0; foreach ($result as $k => $v) {
            $this->assertSame([$k=>$v], $expected[$i]);
            $i++;
        }

        // Re-index result
        //
        $result = $stdArray->lookup('//name/:');
        $this->assertInstanceOf(\Generator::class, $result);
        $this->assertSame(
            ['Python', 'python', 'PHP', 'php'],
            iterator_to_array($result)
        );

        $result = $stdArray->lookup('//tags/>/*~0|2~/:');
        $this->assertInstanceOf(\Generator::class, $result);
        $this->assertSame(
            ['Duck', 'gradual', 'Dynamic'],
            iterator_to_array($result)
        );


        $this->assertSame('Duck', $stdArray->lookup('//tags/:/>/0'));

        $result = $stdArray->lookup('//tags/:/>/*0/:');
        $this->assertInstanceOf(\Generator::class, $result);
        $this->assertSame(
            ['Duck', 'Dynamic'],
            iterator_to_array($result)
        );
    }

    function testLookupCanAccessByReference()
    {
        $stdArray = new StdArray([
            [
                'user' => 'user@domain.com',
                'name' => 'name a',
                'data' => [
                    'id' => '1234',
                ],
            ],
            [
                'user' => 'user2@domain.com',
                'name' => 'name b',
                'data' => [
                    'id' => '5678',
                ],
            ],
        ]);

        $firstUserByReference = &$stdArray->lookup('/0/user');
        $firstUserByReference = 'changed_user@domain.com';
        $this->assertEquals('changed_user@domain.com', $stdArray->asInternalArray[0]['user']);

        $names = &$stdArray->lookup('/>/*name');
        foreach ($names as &$name)
            $name = strtoupper($name);

        $expected = ['NAME A', 'NAME B'];
        $i = 0; foreach ($stdArray->asInternalArray as $usr) {
            $this->assertSame($expected[$i], $usr['name']);
            $i++;
        }

        $ids = &$stdArray->lookup('//id');
        foreach ($ids as &$id)
            $id = (int) $id;

        $expected = [1234, 5678];
        $i = 0; foreach ($stdArray->asInternalArray as $usr) {
            $this->assertSame($expected[$i], $usr['data']['id']);
            $i++;
        }

        $ids = &$stdArray->lookup('//id/:');
        $expected = [1234, 5678];
        foreach ($ids as $i => &$id) {
            $id = (int) $id;
            $this->assertSame($expected[$i], $stdArray->asInternalArray[$i]['data']['id']);
        }
    }

    function testFactoryByArrayReference()
    {
        $initialValue = [1, 2, 3, 4];
        $stdArray = StdArray::ofReference($initialValue);

        $stdArray->append(5);
        $this->assertEquals([1, 2, 3, 4, 5], $stdArray->asInternalArray);
    }

    function testArrayAccessibleByReference()
    {
        $initialValue = [1, 2, 3, 4];
        $stdArray = new StdArray($initialValue);
        $stdArray['field'] = 'value';

        $t = &$stdArray['field'];
        $t = 'new-value';

        $this->assertEquals('new-value', $stdArray['field']);

        // Create Reserved memory reference property
        $stdArray = new StdArray;
        $this->assertFalse(array_key_exists('otherData', $stdArray->asInternalArray));
        $dataRef = &$stdArray['otherData'];

        $this->assertFalse(array_key_exists('otherData', $stdArray->asInternalArray));
        $this->assertEquals(0, $stdArray->count());

        $dataRef = null;
        $this->assertFalse(array_key_exists('otherData', $stdArray->asInternalArray));
        $this->assertEquals(0, $stdArray->count());

        $dataRef = 'otherValue';
        $this->assertTrue(array_key_exists('otherData', $stdArray->asInternalArray));
        $this->assertEquals(1, $stdArray->count());
        $this->assertEquals('otherValue', $stdArray['otherData']);
    }

    function testInternalArrayStorageIsAccessibleByReference()
    {
        $initialValue = [1, 2, 3, 4];
        $stdArray = new StdArray($initialValue);

        $internalArray = &$stdArray->asInternalArray;
        array_pop($internalArray);

        $this->assertEquals([1, 2, 3], $stdArray->asInternalArray);
    }

    function testFilterIteratingAllArrayItems()
    {
        $initialValue = ['a', 'b', 'c', 'd', 'e'];
        $stdArray     = new StdArray($initialValue);
        $self = $this; $i = 0; $stdArray->filter(function($value, $index) use ($self, &$i, $initialValue) {
            $self->assertSame($i, $index);
            $self->assertSame($initialValue[$i], $value);
            $i++;

            return $value;
        });
    }

    function testFilterIteratingArrayItemsRecursively()
    {
        $initialValue = ['a', 'b', 'c', 'd' => [1, 2, 3, 4]];
        $stdArray     = new StdArray($initialValue);

        $self = $this;
        $filterArr = $stdArray->filter(function($value, $index, $depth) use ($self) {
            if ($depth === 0 && $index === 'd') {
                $self->assertSame([1, 2, 3, 4], $value);

                return StdArray::of($value)->filter(function($value) {
                    if ($value % 2 == 0)
                        $this->skipCurrentIteration();

                    return $value;
                })->asInternalArray;
            }

            return $value;

        }, 1);

        $this->assertSame([0 => 1, 2 => 3], $filterArr['d']);
    }

    function testFilterByCleanupValues()
    {
        $initialValue = ['a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5];
        $stdArray     = new StdArray($initialValue);

        $filterArr = $stdArray->filter(function($value, $index) {
            if (0 == ($value & 1)) {
                // whether the input integer is odd
                /** @var IteratorWrapper $this */
                $this->skipCurrentIteration();
            }

            return $value;
        });

        $this->assertInstanceOf(StdArray::class, $filterArr);
        $this->assertSame(['a' => 1, 'c' => 3, 'e' => 5], $filterArr->asInternalArray);
    }

    function testFilterByChangeValuesAndKeys()
    {
        $initialValue = ['a' => 1, 'b' => 2, 'c' => 3];
        $stdArray     = new StdArray($initialValue);

        $filterArr = $stdArray->filter(function($value, &$index) {
            $index = strtoupper($index);
            return $value * 2;
        });

        $this->assertInstanceOf(StdArray::class, $filterArr);
        $this->assertSame(['A' => 2, 'B' => 4, 'C' => 6], $filterArr->asInternalArray);
    }

    function testInsert()
    {
        // When key is not given the value appended to array
        //
        $stdArray = StdArray::of([])->insert('a');
        $this->assertSame([0 => 'a'], $stdArray->asInternalArray);

        $stdArray = StdArray::of([0 => 'a'])->insert('zz');
        $this->assertSame([0 => 'a', 1 => 'zz'], $stdArray->asInternalArray);

        // When we specify key the default behaviour is to replace value with new one
        //
        $stdArray = StdArray::of([0 => 'a', 1 => 'zz']);
        $stdArray->insert('b', 1);
        $this->assertSame([0 => 'a', 1 => 'b'], $stdArray->asInternalArray);

        // Skip setting value in array if given key has same value
        //
        $stdArray = StdArray::of(['words' => 'a']);
        $stdArray->insert('a', 'words', function ($givenValue, $currValue, $key) {
            if ($givenValue === $currValue)
                return StdArray::InsertDoSkip;
        });
        $this->assertSame(['words' => 'a'], $stdArray->asInternalArray);

        // Insert By push new values if key exists
        //
        $stdArray = StdArray::of(['words' => 'a']);
        $stdArray->insert('b', 'words', function ($givenValue, $currValue, $key) {
            if ($givenValue === $currValue)
                return StdArray::InsertDoSkip;

            return StdArray::InsertDoPush;
        });
        $this->assertSame(['words' => ['a', 'b']], $stdArray->asInternalArray);

        // Insert By replace new values if key exists
        //
        $stdArray = StdArray::of(['words' => 'a']);
        $stdArray->insert('a,b', 'words', function ($givenValue, $currValue, $key) {
            return StdArray::InsertDoReplace;
        });
        $this->assertSame(['words' => 'a,b'], $stdArray->asInternalArray);

    }

    /**
     * @dataProvider provideMergeData
     */
    function testMerge($a, $b, $mergeVariants)
    {
        foreach ($mergeVariants as $mergeStrategy => $expected) {
            $mergeStrategy = $mergeStrategy === '' ? null : $mergeStrategy;

            $this->assertEquals(
                $expected,
                StdArray::of($a)->merge($b, $mergeStrategy)->asInternalArray,
            );
        }
    }

    function testPrepend()
    {
        $stdArray = StdArray::of([1986 => 'wf', 1983 => 'me'])->prepend('a');
        $this->assertSame([0 => 'a', 1 => 'wf', 2 => 'me'], $stdArray->asInternalArray);

        $stdArray->prepend(1, 'b');
        $this->assertSame(['b' => 1, 0 => 'a', 1 => 'wf', 2 => 'me'], $stdArray->asInternalArray);

        $stdArray->prepend('c');
        $this->assertSame([0 => 'c', 'b' => 1, 1 => 'a', 2 => 'wf', 3 => 'me'], $stdArray->asInternalArray);

        $stdArray->prepend(0, 'b');
        $this->assertSame(['b' => 0, 0 => 'c', 1 => 'a', 2 => 'wf', 3 => 'me'], $stdArray->asInternalArray);
    }

    function testAppend()
    {
        $stdArray = StdArray::of([1986 => 'wf', 1983 => 'me']);

        $stdArray->append('a');
        $this->assertSame([1986 => 'wf', 1983 => 'me', 1987 => 'a'], $stdArray->asInternalArray);

        $stdArray->append(1, 'b');
        $this->assertSame([1986 => 'wf', 1983 => 'me', 1987 => 'a', 'b' => 1], $stdArray->asInternalArray);

        $stdArray->append('c');
        $this->assertSame([1986 => 'wf', 1983 => 'me', 1987 => 'a', 'b' => 1, 1988 => 'c'], $stdArray->asInternalArray);

        $stdArray->append(0, 'b');
        $this->assertSame([1986 => 'wf', 1983 => 'me', 1987 => 'a', 1988 => 'c', 'b' => 0], $stdArray->asInternalArray);
    }

    function testExcept()
    {
        $stdArray = StdArray::of($this->getDummyArray())
            ->except(...[1, 3]);

        $this->assertSame(['a' => 0, 'b' => 1, 'd' => 3], $stdArray->asInternalArray);
    }

    function testKeep()
    {
        $stdArray = StdArray::of($this->getDummyArray())
            ->keep(...['a', 'b', 'd']);

        $this->assertSame(['a' => 0, 'b' => 1, 'd' => 3], $stdArray->asInternalArray);
    }

    function testKeepIntersection()
    {
        $stdArray = StdArray::of(["a"=>"red","b"=>"green","c"=>"blue","yellow"])
            ->keepIntersection(StdArray::of(["A"=>"red","b"=>"GREEN","yellow","black"]))
            ->keepIntersection(["a"=>"green","b"=>"red","yellow","black"]);

        $this->assertSame(['a' => 'red', 0 => 'yellow'], $stdArray->asInternalArray);

        // ..

        $stdArray = StdArray::of(["a" => "green", "b" => "brown", "c" => "blue", "red"])
            ->keepIntersection(["a" => "GREEN", "B" => "brown", "yellow", "red"], function($a, $b) {
                return \strcasecmp($a, $b);
            });

        $this->assertSame(["a" => "green", "b" => "brown", 0 => "red"], $stdArray->asInternalArray);
    }

    function testGetColumn()
    {
        $rows = [0 => ['id' => '3', 'title' => 'Foo', 'date' => '2013-03-25']];

        $this->assertSame($rows, StdArray::of($rows)->getColumn(null, 0)->asInternalArray);
        $this->assertSame($rows, StdArray::of($rows)->getColumn(null)->asInternalArray);
        $this->assertSame($rows, StdArray::of($rows)->getColumn()->asInternalArray);
        $this->assertSame(['3'], StdArray::of($rows)->getColumn('id')->asInternalArray);

        // ..

        $rows = [
            456 => ['id' => '3', 'title' => 'Foo', 'date' => '2013-03-25'],
            457 => ['id' => '5', 'title' => 'Bar', 'date' => '2012-05-20'],
        ];

        $this->assertSame(['Foo', 'Bar'],
            StdArray::of($rows)->getColumn('title', null)->asInternalArray);

        $this->assertSame([3 => 'Foo', 5 => 'Bar'],
            StdArray::of($rows)->getColumn('title', 'id')->asInternalArray);

        $this->assertSame([
            3 => ['id' => '3', 'title' => 'Foo', 'date' => '2013-03-25'],
            5 => ['id' => '5', 'title' => 'Bar', 'date' => '2012-05-20'],
        ], StdArray::of($rows)->getColumn(null, 'id')->asInternalArray);

        $this->assertSame([
            ['id' => '3', 'title' => 'Foo', 'date' => '2013-03-25'],
            ['id' => '5', 'title' => 'Bar', 'date' => '2012-05-20'],
        ], StdArray::of($rows)->getColumn(null, 'foo')->asInternalArray);

        $this->assertSame([
            ['id' => '3', 'title' => 'Foo', 'date' => '2013-03-25'],
            ['id' => '5', 'title' => 'Bar', 'date' => '2012-05-20'],
        ], StdArray::of($rows)->getColumn(null)->asInternalArray);
    }

    function testGetFirst()
    {
        $initialValue = $this->getDummyArray();
        $stdArray = new StdArray($initialValue);
        $firstItem = $stdArray->getFirst();

        // test immutability
        $this->assertInstanceOf(StdArray::class, $firstItem);
        $this->assertNotSame($stdArray, $firstItem);

        $this->assertSame([1 => 'b'], $firstItem->asInternalArray);
        $this->assertSame(1, $firstItem->key());
        $this->assertSame('b', $firstItem->current());

        // check value of initial StdArray wont change
        $this->assertSame($initialValue, $stdArray->asInternalArray);
    }

    function testGetFirstAndRemove()
    {
        $initialValue = $this->getDummyArray();
        $stdArray = new StdArray($initialValue);
        $firstItem = $stdArray->getFirstAndRemove();

        // test immutability
        $this->assertInstanceOf(StdArray::class, $firstItem);
        $this->assertNotSame($stdArray, $firstItem);

        $this->assertSame([1 => 'b'], $firstItem->asInternalArray);
        $this->assertSame(1, $firstItem->key());
        $this->assertSame('b', $firstItem->current());

        $expected = array_slice($initialValue, 1, count($initialValue), true);
        $this->assertSame($expected, $stdArray->asInternalArray);
        // point to beginning of the array after then
        $this->assertSame(key($expected), $stdArray->key());
        $this->assertSame(reset($expected), $stdArray->current());
    }

    function testGetLast()
    {
        $initialValue = $this->getDummyArray();
        $stdArray = new StdArray($initialValue);
        $lastItem = $stdArray->getLast();

        // test immutability
        $this->assertInstanceOf(StdArray::class, $lastItem);
        $this->assertNotSame($stdArray, $lastItem);

        $this->assertSame(['d' => 3], $lastItem->asInternalArray);
        $this->assertSame('d', $lastItem->key());
        $this->assertSame(3, $lastItem->current());

        // check that main StdArray values not changed
        $this->assertSame($initialValue, $stdArray->asInternalArray);
    }

    function testGetLastAndRemove()
    {
        $initialValue = $this->getDummyArray();
        $stdArray = new StdArray($initialValue);
        $lastItem = $stdArray->getLastAndRemove();

        // test immutability
        $this->assertInstanceOf(StdArray::class, $lastItem);
        $this->assertNotSame($stdArray, $lastItem);

        $this->assertSame(['d' => 3], $lastItem->asInternalArray);
        $this->assertSame('d', $lastItem->key());
        $this->assertSame(3, $lastItem->current());

        $expected = array_slice($initialValue, 0, count($initialValue)-1, true);
        $this->assertSame($expected, $stdArray->asInternalArray);
        // point to beginning of the array after then
        $this->assertSame(key($expected), $stdArray->key());
        $this->assertSame(reset($expected), $stdArray->current());
    }

    function testGetKeys()
    {
        $stdArray = new StdArray($this->getDummyArray());
        $arrayKeys = $stdArray->getKeys();

        // test immutability
        $this->assertInstanceOf(StdArray::class, $arrayKeys);
        $this->assertNotSame($stdArray, $arrayKeys);

        $this->assertSame([1, 3, 'a', 'b', 'd'], $arrayKeys->asInternalArray);
    }

    function testGetValues()
    {
        $stdArray = new StdArray($this->getDummyArray());
        $arrayValues = $stdArray->getValues();

        // test immutability
        $this->assertInstanceOf(StdArray::class, $arrayValues);
        $this->assertNotSame($stdArray, $arrayValues);

        $this->assertSame(['b', 'd', 0, 1, 3], $arrayValues->asInternalArray);
    }

    function testReindex()
    {
        $stdArray = new StdArray([1 => 'b', 3 => 'd', 'a' => 0, 'd' => 3, 'b' => 1]);

        $this->assertSame([
            0 => 'b', 1 => 'd',           // re-indexed
            'a' => 0, 'b' => 1, 'd' => 3  // concat to array
        ], $stdArray->reindex()->asInternalArray);
    }

    /**
     * @dataProvider provideCleanArguments
     */
    function testClean($inputArray, $expected)
    {
        $stdArray = new StdArray($inputArray);
        $this->assertSame($expected, $stdArray->clean()->asInternalArray);
    }

    /**
     * @dataProvider provideFlattenArrayData
     */
    function testFlattenArray($separatorNotation, $inputArray, $expected)
    {
        $stdArray  = new StdArray($inputArray);
        $flatArray = $stdArray->getFlatten($separatorNotation);

        // test immutability
        $this->assertInstanceOf(StdArray::class, $flatArray);
        $this->assertNotSame($stdArray, $flatArray);

        $this->assertEquals($expected, $flatArray->asInternalArray);
    }

    function testIsAssoc()
    {
        $initialValue = [1, 2, 3, 4];
        $stdArray = new StdArray($initialValue);
        $this->assertFalse($stdArray->isAssoc());

        $initialValue = ['field' => 'value', 'otherField' => 'other-value'];
        $stdArray = new StdArray($initialValue);
        $this->assertTrue($stdArray->isAssoc());

        $initialValue = ['field' => 'value', 2, 3, 4];
        $stdArray = new StdArray($initialValue);
        $this->assertTrue($stdArray->isAssoc());
    }

    /**
     * @dataProvider provideIsHavingEqualValuesData
     */
    function testIsHavingEqualValues($a, $b)
    {
        $a = new StdArray($a);
        $this->assertTrue($a->isHavingEqualValuesWith($b));
    }

    function testIsEqual()
    {
        $initialValue = [1, 2, 3, 4];
        $stdArray = new StdArray($initialValue);
        $stdArrayCp = clone $stdArray;

        $this->assertTrue($stdArray->isEqualWith($stdArrayCp));
        $this->assertTrue($stdArray->isEqualWith($initialValue));
    }

    function testIsEmpty()
    {
        $stdArray = new StdArray();
        $this->assertTrue($stdArray->isEmpty());

        $stdArray = new StdArray(['a']);
        $this->assertFalse($stdArray->isEmpty());
    }

    function testItsCountable()
    {
        $initialValue = [1, 2, 3, 4];
        $stdArray = new StdArray($initialValue);
        $this->assertEquals(4, count($stdArray));
        $this->assertEquals(4, $stdArray->count());

        $stdArray = new StdArray();
        $this->assertEquals(0, count($stdArray));
        $this->assertEquals(0, $stdArray->count());
    }

    // ..

    function provideTypeHintObjectData()
    {
        yield [$this->getDummyStdObject()];
        yield [$this->getDummyVariableObject()];

        if ( function_exists('json_decode') )
            yield [$this->getJsonString()];
    }

    function provideMergeData()
    {
        return [
            [
                ['foo' => 'baz'],
                ['foo' => 'baz'],
                [
                    null => ['foo' => 'baz'],
                    StdArray::MergeDoMerging => ['foo' => ['baz', 'baz']],
                    StdArray::MergeDoPush => ['foo' => 'baz', 0 => 'baz'],
                    StdArray::MergeDoReplace => ['foo' => 'baz'],
                ],
            ],
            [
                ['foo' => [0 => 'baz']],
                ['foo' => [0 => 'baz']],
                [
                    null => ['foo' => ['baz']],
                    StdArray::MergeDoMerging => ['foo' => [0 => ['baz', 'baz']]],
                    StdArray::MergeDoPush => ['foo' => ['baz'], 0 => ['baz']],
                    StdArray::MergeDoReplace => ['foo' => ['baz']],
                ],
            ],
            [
                ['foo' => ['baz', 'bar']],
                ['foo' => ['baz']],
                [
                    null => ['foo' => ['baz', 'bar']],
                    StdArray::MergeDoMerging => ['foo' => [['baz', 'baz'], 'bar']],
                    StdArray::MergeDoPush => ['foo' => ['baz', 'bar'], ['baz']],
                    StdArray::MergeDoReplace => ['foo' => ['baz']],
                ],
            ],
            [
                ['foo' => ['baz']],
                ['foo' => 'baz'],
                [
                    null => ['foo' => ['baz', 'baz']],
                    StdArray::MergeDoMerging => ['foo' => ['baz', 'baz']],
                    StdArray::MergeDoPush => ['foo' => ['baz'], 'baz'],
                    StdArray::MergeDoReplace => ['foo' => 'baz'],
                ],
            ],
            [
                ['bar' => []],
                ['bar' => 'bat'],
                [
                    null => ['bar' => ['bat']],
                    StdArray::MergeDoMerging => ['bar' => ['bat']],
                    StdArray::MergeDoPush => ['bar' => [], 'bat'],
                    StdArray::MergeDoReplace => ['bar' => 'bat'],
                ],
            ],
            [
                // Left
                [ 'foo', 3 => 'bar', 'baz' => 'baz', 4 => ['a', 'b', 'c'] ],
                // Right, to be merged to left
                [ 'baz', 4 => ['d' => 'd'] ],
                // Expected variants with merge strategy
                [
                    null => [
                        0 => 'foo',
                        3 => 'bar',
                        'baz' => 'baz',
                        4 => ['a', 'b', 'c', 'd' => 'd'],
                        5 => 'baz',
                    ],
                    StdArray::MergeDoMerging => [
                        0 => ['foo', 'baz'],
                        3 => 'bar',
                        'baz' => 'baz',
                        4 => ['a', 'b', 'c', 'd' => 'd'],
                    ],
                    StdArray::MergeDoPush => [
                        0 => 'foo',
                        3 => 'bar',
                        'baz' => 'baz',
                        4 => ['a', 'b', 'c'],
                        5 => 'baz',
                        6 => ['d' => 'd'],
                    ],
                    StdArray::MergeDoReplace => [
                        0 => 'baz',
                        3 => 'bar',
                        'baz' => 'baz',
                        4 => ['d' => 'd'],
                    ],
                ],
            ],
        ];
    }

    function provideCleanArguments()
    {
        return [
            [[], []],
            [[null, false], []],
            [[0 => true], [0 => true]],
            [[0 => -9, 0], [0 => -9]],
            [[-8 => -9, 1, 2 => false], [-8 => -9, 0 => 1]],
            [[0 => 1.18, 1 => false], [0 => 1.18]],
            [['foo' => false, 'foo', 'lall'], ['foo', 'lall']],
            [
                [
                    'root' => [
                        '__index__' => [
                            'name' => '',
                        ],
                    ],
                ],
                []
            ],
        ];
    }

    function provideFlattenArrayData()
    {
        $nestedArray = [
            'key1' => 'value1',
            'key2' => [
                'subkey' => 'subkeyval'
            ],
            'key3' => 'value3',
            'key4' => [
                'subkey4' => [
                    'subsubkey4' => 'subsubkeyval4',
                    'subsubkey5' => 'subsubkeyval5',
                ],
                'subkey5' => 'subkeyval5'
            ]
        ];

        // (notation delimiter, nested array, expected)
        yield [
            '[%s]',
            $nestedArray,
            [
                'key1' => 'value1',
                'key2[subkey]' => 'subkeyval',
                'key3' => 'value3',
                'key4[subkey4][subsubkey4]' => 'subsubkeyval4',
                'key4[subkey4][subsubkey5]' => 'subsubkeyval5',
                'key4[subkey5]' => 'subkeyval5',
            ],
        ];

        yield [
            '.%s',
            $nestedArray,
            [
                'key1' => 'value1',
                'key2.subkey' => 'subkeyval',
                'key3' => 'value3',
                'key4.subkey4.subsubkey4' => 'subsubkeyval4',
                'key4.subkey4.subsubkey5' => 'subsubkeyval5',
                'key4.subkey5' => 'subkeyval5',
            ],
        ];
    }

    function getDummyArray()
    {
        return [1 => 'b', 3 => 'd', 'a' => 0, 'b' => 1, 'd' => 3];
    }

    function getDummyStdObject()
    {
        $initialValue = new \stdClass();
        $initialValue->field = 'value';
        $initialValue->otherField = 'other-value';

        return $initialValue;
    }

    function getDummyVariableObject()
    {
        $initialValue = new class {
            public $field = 'value';
            public $otherField = 'other-value';
        };

        return $initialValue;
    }

    function getJsonString()
    {
        return <<<JSON
        {
           "field": "value",
           "otherField": "other-value"
        }
        JSON;
    }

    function provideIsHavingEqualValuesData()
    {
        return [
            [
                [], []
            ],
            [
                ['BRANCH'], ['BRANCH']
            ],
            [
                ['x' => 'BRANCH'], ['BRANCH']
            ],
            [
                ['BRANCH','ADDRESS','MOBILE','NAME'], ['NAME','BRANCH','MOBILE','ADDRESS']
            ],
        ];
    }
}
