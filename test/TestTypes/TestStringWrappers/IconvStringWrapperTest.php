<?php
namespace PoirotTest\Std\TestTypes\TestStringWrappers;

use Poirot\Std\Type\StdString\Wrappers\IconvStringWrapper;


class IconvStringWrapperTest
    extends aStringWrapperTest
{
    protected $wrapperClass = IconvStringWrapper::class;

    function setUp()
    {
        if (! extension_loaded('iconv') )
            $this->markTestSkipped('Missing ext/iconv');

        parent::setUp();
    }
}
