<?php
namespace PoirotTest\Std\TestTypes\TestStringWrappers;

use Poirot\Std\Type\StdString\Wrappers\IntlStringWrapper;


class IntlStringWrapperTest
    extends aStringWrapperTest
{
    protected $wrapperClass = IntlStringWrapper::class;

    function setUp()
    {
        if (! extension_loaded('intl') )
            $this->markTestSkipped('Missing ext/intl');

        parent::setUp();
    }
}
