<?php
namespace PoirotTest\Std\TestTypes\TestStringWrappers;

use PHPUnit\Framework\TestCase;
use Poirot\Std\Interfaces\Type\iStringWrapper;

abstract class aStringWrapperTest
    extends TestCase
{
    /** @var string */
    protected $wrapperClass;

    /**
     * @return iStringWrapper|false
     */
    protected function getStringWrapper($encoding = null)
    {
        $wrapperClass = $this->wrapperClass;
        if (! call_user_func("$wrapperClass::isSupported", $encoding) )
            return false;


        return new $wrapperClass($encoding);
    }


    /**
     * @dataProvider provideStrlenData
     */
    function testStrlen($encoding, $str, $expected)
    {
        $wrapper = $this->getStringWrapper($encoding);
        $this->assertEncodingIsSupportedByWrapper($wrapper, $encoding);

        $this->assertSame($expected, $wrapper->strlen($str));
    }

    /**
     * @dataProvider provideSubstrData
     */
    function testSubstr($encoding, $str, $offset, $length, $expected)
    {
        $wrapper = $this->getStringWrapper($encoding);
        $this->assertEncodingIsSupportedByWrapper($wrapper, $encoding);

        $this->assertSame($expected, $wrapper->substr($str, $offset, $length));
    }

    /**
     * @dataProvider provideStrposData
     */
    function testStrpos($encoding, $haystack, $needle, $offset, $expected)
    {
        $wrapper = $this->getStringWrapper($encoding);
        $this->assertEncodingIsSupportedByWrapper($wrapper, $encoding);

        $this->assertSame($expected, $wrapper->strpos($haystack, $needle, $offset));
    }

    /**
     * @dataProvider provideConvertData
     */
    function testConvert($encoding, $convertEncoding, $str, $expected)
    {
        $wrapper = $this->getStringWrapper($encoding);
        $this->assertEncodingIsSupportedByWrapper($wrapper, $encoding);

        if (! $wrapper::isSupported($encoding, $convertEncoding) ) {
            $this->markTestSkipped("Encoding {$encoding} or {$convertEncoding} not supported");
            return;
        }

        $this->assertSame($expected, $wrapper->convert($str, $convertEncoding));

        // backward
        $wrapper = $this->getStringWrapper($convertEncoding);
        $this->assertSame($str, $wrapper->convert($expected, $encoding));
    }


    // ..

    function assertEncodingIsSupportedByWrapper($wrapper, $encoding) : iStringWrapper
    {
        if (! $wrapper )
            $this->markTestSkipped("Encoding {$encoding} not supported.");

        return $wrapper;
    }

    function provideStrlenData()
    {
        return [
            ['ascii', 'abcdefghijklmnopqrstuvwxyz', 26],
            ['utf-8', 'abcdefghijklmnopqrstuvwxyz', 26],
            ['utf-8', 'äöüß',                       4],
        ];
    }

    function provideSubstrData()
    {
        return [
            ['ascii', 'abcdefghijkl', 1, 5, 'bcdef'],
            ['ascii', 'abcdefghijkl', -2, null, 'kl'],
            ['utf-8', 'abcdefghijkl', 1, 5, 'bcdef'],
            ['utf-8', 'äöüß',         1, 2, 'öü'],
            ['utf-8', 'äöüß',         -1, null, 'ß'],
        ];
    }

    function provideStrposData()
    {
        return [
            ['ascii', 'abcdefghijkl', 'g', 3, 6],
            ['utf-8', 'abcdefghijkl', 'g', 3, 6],
            ['utf-8', 'äöüß',         'ü', 1, 2],
        ];
    }

    function provideConvertData()
    {
        return [
            ['ascii', 'ascii', 'abc', 'abc'],
            ['ascii', 'utf-8', 'abc', 'abc'],
            ['utf-8', 'ascii', 'abc', 'abc'],
            ['utf-8', 'iso-8859-15', '€',   "\xA4"],
            ['utf-8', 'iso-8859-16', '€',   "\xA4"], // ISO-8859-16 is wrong @ mbstring
        ];
    }
}
