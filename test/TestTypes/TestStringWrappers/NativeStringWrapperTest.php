<?php
namespace PoirotTest\Std\TestTypes\TestStringWrappers;

use Poirot\Std\Type\StdString\Wrappers\NativeStringWrapper;


class NativeStringWrapperTest
    extends aStringWrapperTest
{
    protected $wrapperClass = NativeStringWrapper::class;
}
