<?php
namespace PoirotTest\Std\TestTypes\TestStringWrappers;

use PHPUnit\Framework\TestCase;
use Poirot\Std\Type\StdString\Wrappers\ConverterStringWrapper;
use Poirot\Std\Type\StdString\Wrappers\NativeStringWrapper;

class ConvertStringWrapperTest
    extends TestCase
{
    protected $wrapperClass = NativeStringWrapper::class;

    /**
     * @dataProvider provideConvertData
     */
    function testConvert($encoding, $convertEncoding, $str)
    {
        $wrapper = new ConverterStringWrapper($encoding);
        $this->assertSame($str, \utf8_encode($wrapper->convert($str, $convertEncoding)));
    }

    function testConvertUnkwonChars()
    {
        $wrapper = new ConverterStringWrapper('UTF-8');
        $this->assertSame('?????? ?? ???????', $wrapper->convert('строка на русском', 'ISO-8859-1'));
        $this->assertSame('  -ABC-????-  ', $wrapper->convert('  -ABC-中文空白-  ', 'ISO-8859-1'));
    }

    // ..

    function provideConvertData()
    {
        return [
            ['utf-8', 'ISO-8859-1', 'foobar'],
            ['utf-8', 'ISO-8859-1', 'ÄÖÜ'],
        ];
    }
}
