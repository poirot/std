<?php
namespace PoirotTest\Std\TestTypes\TestStringWrappers;

use Poirot\Std\Type\StdString\Wrappers\MbStringWrapper;


class MbStringWrapperTest
    extends aStringWrapperTest
{
    protected $wrapperClass = MbStringWrapper::class;

    function setUp()
    {
        if (! function_exists('mb_strlen') )
            $this->markTestSkipped('Missing ext/mbstring');

        parent::setUp();
    }
}
