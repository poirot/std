<?php
namespace PoirotTest\Std\TestTypes\StdEnum;

use Poirot\Std\Type\StdEnum;

/**
 * @method static EnumFixture FOO();
 * @method static EnumFixture BAR();
 * @method static EnumFixture NUMBER();
 */
class EnumFixture
    extends StdEnum
{
    const FOO = "foo";
    const BAR = "bar";
    const NUMBER = 42;

    // considered as internal use
    private const PRIVATE_CONST = 'private';

    function __construct($initial_value = self::FOO)
    {
        parent::__construct($initial_value);
    }
}
