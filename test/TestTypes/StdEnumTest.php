<?php
namespace PoirotTest\Std\TestTypes;

use PHPUnit\Framework\TestCase;
use Poirot\Std\Type\StdEnum;
use PoirotTest\Std\TestTypes\StdEnum\EnumFixture;

/**
 * @see StdEnum
 */
class StdEnumTest
    extends TestCase
{
    function testConstructEnumValue()
    {
        $consEnum = new EnumFixture(EnumFixture::FOO);
        $makeEnum = EnumFixture::FOO();
        $this->assertInstanceOf(EnumFixture::class, $makeEnum);
        $this->assertEquals($consEnum, $makeEnum);

        $this->assertEquals($consEnum, new EnumFixture($makeEnum));
    }

    function testFailedConstructWithInvalidValue()
    {
        $this->expectException(\UnexpectedValueException::class);
        new EnumFixture('invalid');
    }

    function testFailedMakeUndefinedConst()
    {
        $this->expectException(\UnexpectedValueException::class);
        EnumFixture::UNDEFINED();
    }

    function testGetEnumValue()
    {
        $consEnum = new EnumFixture(EnumFixture::FOO);
        $this->assertEquals(EnumFixture::FOO, $consEnum());

        $makeEnum = EnumFixture::BAR();
        $this->assertEquals(EnumFixture::BAR, $makeEnum());
    }

    function testIsEqual()
    {
        $foo = EnumFixture::FOO();
        $bar = new EnumFixture(EnumFixture::BAR);

        $this->assertTrue( $foo->isEqualTo($foo) );
        $this->assertTrue( $foo->isEqualTo(new EnumFixture(EnumFixture::FOO)) );
        $this->assertFalse( $foo->isEqualTo($bar) );
        $this->assertFalse( $bar->isEqualTo($foo) );

        $class = $this->getExtendedEnumConstObject(EnumFixture::FOO);
        $this->assertTrue($foo->isEqualTo($class));

        $class = $this->getConflictedEnumConstObject(EnumFixture::FOO);
        $this->assertFalse($foo->isEqualTo($class));

        $class = $this->getExtendedEnumConstObject('42');
        $this->assertFalse(EnumFixture::NUMBER()->isEqualTo($class));
    }

    function testListConsts()
    {
        $enums = EnumFixture::listEnums();
        $this->assertEquals([
            'FOO' => new EnumFixture(EnumFixture::FOO),
            'BAR' => new EnumFixture(EnumFixture::BAR),
            'NUMBER' => new EnumFixture(EnumFixture::NUMBER),
        ], $enums);

        $this->assertEquals($enums['FOO'], EnumFixture::FOO());
        $this->assertEquals($enums['BAR'], EnumFixture::BAR());
        $this->assertEquals($enums['NUMBER'], EnumFixture::NUMBER());

        $this->assertEquals($enums['FOO'](), EnumFixture::FOO);
    }

    // ..

    function getConflictedEnumConstObject($value): StdEnum
    {
        return new class($value) extends StdEnum {
            const FOO = "foo";
            const BAR = "bar";
        };
    }

    function getExtendedEnumConstObject($value): StdEnum
    {
        return new class($value) extends EnumFixture {
            const BAZ = "baz";
            const SNUMBER = '42';
        };
    }
}
