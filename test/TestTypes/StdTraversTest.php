<?php
namespace PoirotTest\Std\TestTypes;

use PHPUnit\Framework\TestCase;
use Poirot\Std\IteratorWrapper;
use Poirot\Std\Type\StdTraversable;

/**
 * @see StdTraversable
 */
class StdTraversTest
    extends TestCase
{
    function testConstructStrictlyWithTraversable()
    {
        $stdTravers = new StdTraversable(new \ArrayIterator, true);
        $this->assertInstanceOf(StdTraversable::class, $stdTravers);

        $stdTravers = new StdTraversable($this->getDummyIterator(), true);
        $this->assertInstanceOf(StdTraversable::class, $stdTravers);
    }

    function testFailedConstructStrictlyWithNoneTraversable()
    {
        $this->expectException(\UnexpectedValueException::class);
        new StdTraversable([1, 2, 3, 4], true);
    }

    function testConstructByDefaultSetToStrict()
    {
        $this->expectException(\UnexpectedValueException::class);
        new StdTraversable('invalid_value');
    }

    function testHintingOtherTypesToTraversable()
    {
        $initialValue = [1, 2, 3, 4];
        $stdTravers = new StdTraversable($initialValue, false);
        foreach ($stdTravers as $i => $v)
            $this->assertEquals($initialValue[$i], $v);


        $initialValue  = $this->getObjectToMakeTraversable();
        $stdTravers    = new StdTraversable($initialValue, false);
        $expectedValue = ['a' => 1, 'b' => 2, 'c' => 3];
        foreach ($stdTravers as $i => $v)
            $this->assertEquals($expectedValue[$i], $v);

        $initialValue  = $this->getStdClassToMakeTraversable();
        $stdTravers    = new StdTraversable($initialValue, false);
        $expectedValue = ['a' => 1, 'b' => 2, 'c' => 3];
        foreach ($stdTravers as $i => $v)
            $this->assertEquals($expectedValue[$i], $v);

        $initialValue = 1;
        $stdTravers    = new StdTraversable($initialValue, false);
        $expectedValue = [ 1 ];
        foreach ($stdTravers as $i => $v)
            $this->assertEquals($expectedValue[$i], $v);
    }

    function testTraversableEach()
    {
        $initialValue = $this->getDummyIterator();
        $stdTravers   = new StdTraversable($initialValue, true);

        $x = $stdTravers->each(function ($v) {
            /** @var IteratorWrapper $this */
            if ($v == 1)
                $this->skipCurrentIteration();

            return $v;
        });

        $array = iterator_to_array($x);
        $this->assertEquals([2, 3, 4], array_values($array));
    }

    function testTraversableEachRecursively()
    {
        $initialValue    = $this->getObjectToMakeTraversable();
        $initialValue->a = $this->getDummyIterator();
        $stdTravers = new StdTraversable($initialValue, false);

        $x = $stdTravers->each(function ($v) {
            /** @var IteratorWrapper $this */
            if (is_int($v) && $v == 3)
                $this->skipCurrentIteration();

            return $v;
        }, true);

        $array = iterator_to_array($x);
        $this->assertInstanceOf(StdTraversable::class, $array['a']);

        $array['a'] = iterator_to_array($array['a']);
        $this->assertEquals(['a' => [0 => 1, 1 => 2, 3 => 4], 'b' => 2], $array);
    }

    function testDetermineProcessedIterableNodeValue()
    {
        $initialValue    = $this->getObjectToMakeTraversable();
        $initialValue->a = $this->getDummyIterator();
        $stdTravers = new StdTraversable($initialValue, false);

        $phpUnit = $this;
        $x = $stdTravers->each(function ($v, $_, $isTraversed) use ($phpUnit) {
            /** @var IteratorWrapper $this */
            if ($isTraversed) {
                $phpUnit->assertInstanceOf(StdTraversable::class, $v);
                $v = iterator_to_array($v);
                $v = array_values($v); // rebuild indexes

            } elseif ($v == 3)
                $this->skipCurrentIteration();

            return $v;

        }, true);

        $array = iterator_to_array($x);
        $this->assertEquals(['a' => [1, 2, 4], 'b' => 2], $array);
    }

    // ..

    protected function getDummyIterator()
    {
        return new class implements \IteratorAggregate {
            /**
             * @inheritDoc
             */
            function getIterator()
            {
                for ($i = 1; $i <= 4; $i++)
                    yield $i;
            }
        };
    }

    protected function getObjectToMakeTraversable()
    {
        return new class {
            public $a = 1;
            public $b = 2;
            public $c = 3;
        };
    }

    protected function getStdClassToMakeTraversable()
    {
        $stdClass = new \stdClass();
        $stdClass->a = 1;
        $stdClass->b = 2;
        $stdClass->c = 3;

        return $stdClass;
    }
}
