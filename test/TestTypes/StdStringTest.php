<?php
namespace PoirotTest\Std\TestTypes;

use PHPUnit\Framework\TestCase;
use Poirot\Std\Exceptions\Type\StdString\StringEncodingNotSupportedError;
use Poirot\Std\Type\StdString;
use PoirotTest\Std\TestTypes\StdString\CustomTestStringWrapperFixture;

/**
 * @see StdString
 */
class StdStringTest
    extends TestCase
{
    function testConstructStrictly()
    {
        $stdString = new StdString('', StdString::CharsetDefault, true);
        $this->assertInstanceOf(StdString::class, $stdString);

        $stdString = new StdString($this->getDummyStringify(), StdString::CharsetDefault, true);
        $this->assertInstanceOf(StdString::class, $stdString);
    }

    function testFailedConstructStrictlyWithNoneStringify()
    {
        try {
            new StdString(array(), StdString::CharsetDefault, true);
            $this->fail('Expect UnexpectedValueException.');
        } catch (\UnexpectedValueException $e) {
            $this->assertInstanceOf(\UnexpectedValueException::class, $e);
        }
    }

    function testConstructByDefaultSetToStrict()
    {
        $this->expectException(\UnexpectedValueException::class);
        new StdString(new \stdClass);
    }

    function testGetStringEncoding()
    {
        $stdStr = new StdString('', StdString\Encodings::CharsetSingleByte['ASCII']);
        $this->assertSame(StdString\Encodings::CharsetSingleByte['ASCII'], $stdStr->charset);
        $this->assertSame(StdString\Encodings::CharsetSingleByte['ASCII'], $stdStr->encoding);
    }

    function testStringsCanBeSerializedWithByteOrder()
    {
        foreach (StdString\Encodings::Charset as $charset) {
            $stdStr    = StdString::of('simple string value', $charset);
            $newStdStr = StdString::ofSerializedByteOrder(
                $stdStr->asSerializedByteOrder()
            );

            $this->assertEquals($charset, $newStdStr->encoding);
            $this->assertEquals((string) $stdStr, (string) $newStdStr);
        }
    }

    function testHintingOtherTypesToString()
    {
        $initialValue = 1234;
        $stdString    = new StdString($initialValue, StdString::CharsetDefault);
        $this->assertSame('1234', (string) $stdString);

        $initialValue = 1.234;
        $stdString    = new StdString($initialValue, StdString::CharsetDefault);
        $this->assertSame('1.234', (string) $stdString);

        $initialValue = -1234;
        $stdString    = new StdString($initialValue, StdString::CharsetDefault);
        $this->assertSame('-1234', (string) $stdString);

        $initialValue = null;
        $stdString    = new StdString($initialValue, StdString::CharsetDefault);
        $this->assertSame('', (string) $stdString);

        $initialValue = true;
        $stdString    = new StdString($initialValue, StdString::CharsetDefault, false);
        $this->assertSame('true', (string) $stdString);

        $initialValue = false;
        $stdString    = new StdString($initialValue, StdString::CharsetDefault, false);
        $this->assertSame('false', (string) $stdString);
    }

    function testRegisteredWrappersHasMostPriorities()
    {
        StdString::registerWrapper(CustomTestStringWrapperFixture::class);

        $str = StdString::of('test wrapper');
        $this->assertSame(
            'dummy_string_to_determine_wrapper',
            (string) $str->convert(StdString\Encodings::CharsetSingleByte['ASCII'])
        );

        StdString::unregisterWrapper(CustomTestStringWrapperFixture::class);
    }

    /**
     * @dataProvider provideSafeImplodeData
     */
    function testSafeImplode($initialValue, $delimiter, $concatArray, $expected)
    {
        // test immutability
        $stdInitial = StdString::of($initialValue);
        $stdString  = $stdInitial->concatSafeImplode($concatArray, $delimiter);

        $this->assertInstanceOf(StdString::class, $stdString);
        $this->assertNotSame($stdInitial, $stdString);

        // test functionality
        $this->assertSame($expected, (string) $stdString);
    }

    function testLowerFirstChar()
    {
        // test immutability
        $stdInitial = StdString::of('');
        $stdString  = $stdInitial->lowerFirstChar();

        $this->assertInstanceOf(StdString::class, $stdString);
        $this->assertNotSame($stdInitial, $stdString);

        // test functionality
        $this->assertSame('', (string) StdString::of('')->lowerFirstChar());
        $this->assertSame(' ', (string) StdString::of(' ')->lowerFirstChar());
        $this->assertSame('foo bar', (string) StdString::of('Foo bar')->lowerFirstChar());
        $this->assertSame("\t test", (string) StdString::of("\t test")->lowerFirstChar());

        try {
            if ( StdString::wrapper('UTF-8') ) {
                $this->assertSame('öäü', (string) StdString::of('Öäü')->lowerFirstChar());
                $this->assertSame('aBC-ÖÄÜ-中文空白', (string) StdString::of('ABC-ÖÄÜ-中文空白')->lowerFirstChar());
            }
        } catch (StringEncodingNotSupportedError $e) {
            $this->markTestSkipped('utf-8 string not supported.');
        }
    }

    function testUpperFirstChar()
    {
        // test immutability
        $stdInitial = StdString::of('');
        $stdString  = $stdInitial->upperFirstChar();

        $this->assertInstanceOf(StdString::class, $stdString);
        $this->assertNotSame($stdInitial, $stdString);

        // test functionality
        $this->assertSame('', (string) StdString::of('')->upperFirstChar());
        $this->assertSame(' ', (string) StdString::of(' ')->upperFirstChar());
        $this->assertSame('F', (string) StdString::of('F')->upperFirstChar());
        $this->assertSame('Foo bar', (string) StdString::of('foo bar')->upperFirstChar());
        $this->assertSame("\t test", (string) StdString::of("\t test")->upperFirstChar());

        try {
            if ( StdString::wrapper('UTF-8') ) {
                $this->assertSame('Öäü', (string) StdString::of('Öäü')->upperFirstChar());
                $this->assertSame('Öäü', (string) StdString::of('öäü')->upperFirstChar());
                $this->assertSame('Iñtërnâtiônàlizætiøn', (string) StdString::of('iñtërnâtiônàlizætiøn')->upperFirstChar());
            }
        } catch (StringEncodingNotSupportedError $e) {
            $this->markTestSkipped('utf-8 string not supported.');
        }
    }

    function testLower()
    {
        // test immutability
        $stdInitial = StdString::of('foO Bar BAZ');
        $stdString  = $stdInitial->lower();

        $this->assertInstanceOf(StdString::class, $stdString);
        $this->assertNotSame($stdInitial, $stdString);

        // test functionality
        $this->assertSame('foo bar baz', (string) StdString::of('foO Bar BAZ')->lower());

        try {
            if ( StdString::wrapper('UTF-8') )
                $this->assertSame('mąka', (string) StdString::of('mĄkA')->lower());
        } catch (StringEncodingNotSupportedError $e) {
            $this->markTestSkipped('utf-8 string not supported.');
        }
    }

    function testUpper()
    {
        // test immutability
        $stdInitial = StdString::of('foO Bar BAZ');
        $stdString  = $stdInitial->upper();

        $this->assertInstanceOf(StdString::class, $stdString);
        $this->assertNotSame($stdInitial, $stdString);

        // test functionality
        $this->assertSame('FOO BAR BAZ', (string) StdString::of('foO Bar BAZ')->upper());

        try {
            if ( StdString::wrapper('UTF-8') )
                $this->assertSame('UMLAUTE ÄÖÜ', (string) StdString::of('Umlaute äöü')->upper());
        } catch (StringEncodingNotSupportedError $e) {
            $this->markTestSkipped('utf-8 string not supported.');
        }
    }

    /**
     * @dataProvider providePascalCaseData
     */
    function testPascalCase($encoding, $initialValue, $expected)
    {
        // test immutability
        try {
            $stdInitial = StdString::of($initialValue, $encoding);
            $stdString  = $stdInitial->pascalCase();
        } catch (StringEncodingNotSupportedError $e) {
            $this->markTestSkipped('Missing support for UTF-8.');
            return;
        }

        $this->assertInstanceOf(StdString::class, $stdString);
        $this->assertNotSame($stdInitial, $stdString);

        // test functionality
        $this->assertSame($expected, (string) $stdString);
    }

    /**
     * @dataProvider provideCamelCaseData
     */
    function testCamelCase($encoding, $initialValue, $expected)
    {
        // test immutability
        try {
            $stdInitial = StdString::of($initialValue, $encoding);
            $stdString  = $stdInitial->camelCase();
        } catch (StringEncodingNotSupportedError $e) {
            $this->markTestSkipped('Missing support for UTF-8.');
            return;
        }

        $this->assertInstanceOf(StdString::class, $stdString);
        $this->assertNotSame($stdInitial, $stdString);

        // test functionality
        $this->assertSame($expected, (string) $stdString);
    }

    /**
     * @dataProvider provideSnakeCaseData
     */
    function testSnakeCase($encoding, $initialValue, $expected)
    {
        // test immutability
        try {
            $stdInitial = StdString::of($initialValue, $encoding);
            $stdString  = $stdInitial->snakeCase();
        } catch (StringEncodingNotSupportedError $e) {
            $this->markTestSkipped('Missing support for UTF-8.');
            return;
        }

        $this->assertInstanceOf(StdString::class, $stdString);
        $this->assertNotSame($stdInitial, $stdString);

        // test functionality
        $this->assertSame($expected, (string) $stdString);

    }

    function testHasStartedWith()
    {
        $str = StdString::of('foobarbaz');
        $this->assertSame('fo', (string) $str->hasStartedWith('fo'));
        $this->assertSame('fo', (string) $str->hasStartedWith('bo', 'fo'));

        $this->assertEmpty((string) $str->hasStartedWith('no'));


        $str = StdString::of("\n\t " . 'foo bar');
        $this->assertSame("\n\t ", (string) $str->hasStartedWith("\n\t "));

        $str = StdString::of('¬fòô bàř¬');
        $this->assertSame('¬fòô ', (string) $str->hasStartedWith('¬fòô '));
    }

    function testIsStartedWith()
    {
        $str = StdString::of('foobarbaz');
        $this->assertTrue($str->isStartedWith('fo'));
        $this->assertTrue($str->isStartedWith('bo', 'fo'));

        $this->assertFalse($str->isStartedWith('no'));

        $str = StdString::of("\n\t " . 'foo bar');
        $this->assertTrue($str->isStartedWith("\n\t "));

        $str = StdString::of('¬fòô bàř¬');
        $this->assertTrue($str->isStartedWith('¬fòô '));
    }

    /**
     * @dataProvider provideStripPrefixData
     */
    function testStripPrefix($initialValue, $strip, $removeWhitespaces, $expected)
    {
        // test immutability
        $stdInitial = StdString::of($initialValue);
        $stdString  = $stdInitial->stripPrefix($strip, $removeWhitespaces);

        $this->assertInstanceOf(StdString::class, $stdString);
        $this->assertNotSame($stdInitial, $stdString);

        // test functionality
        $this->assertSame($expected, (string) $stdString);
    }

    /**
     * @dataProvider provideStripPostfixData
     */
    function testStripPostfix($initialValue, $strip, $removeWhitespaces, $expected)
    {
        // test immutability
        $stdInitial = StdString::of($initialValue);
        $stdString  = $stdInitial->stripPostfix($strip, $removeWhitespaces);

        $this->assertInstanceOf(StdString::class, $stdString);
        $this->assertNotSame($stdInitial, $stdString);

        // test functionality
        $this->assertSame($expected, (string) $stdString);
    }

    /**
     * @dataProvider  provideSubStringData
     */
    function testSubString($encoding, $initialValue, $start, $length, $expected)
    {
        // test immutability
        try {
            $stdInitial = StdString::of($initialValue, $encoding);
            $stdString  = $stdInitial->subString($start, $length);

        } catch (StringEncodingNotSupportedError $e) {
            $this->markTestSkipped('Missing support for UTF-8.');
            return;
        }

        $this->assertInstanceOf(StdString::class, $stdString);
        $this->assertNotSame($stdInitial, $stdString);

        // test functionality
        $this->assertSame($expected, (string) $stdString);
    }

    function testContains()
    {
        $stdString = new StdString('abc def ghi', StdString\Encodings::Charset['ASCII']);
        $this->assertEquals('abc', (string) $stdString->hasContains('abc'));
        $this->assertEquals('f gh', (string) $stdString->hasContains('f gh'));
        $this->assertEquals('bc', (string) $stdString->hasContains('bc'));

        $this->assertEmpty((string) $stdString->hasContains('not'));


        try {
            $stdString = new StdString('щука 漢字', StdString\Encodings::Charset['UTF-8']);
            $this->assertEquals('щука', (string) $stdString->hasContains('щука'));
            $this->assertEquals('а 漢', (string) $stdString->hasContains('а 漢'));
            $this->assertEquals('字', (string) $stdString->hasContains('字'));

            $this->assertEmpty((string) $stdString->hasContains('not'));

        } catch (StringEncodingNotSupportedError $e) {
            $this->markTestSkipped('Missing support for UTF-8.');
        }
    }

    function testIsContains()
    {
        $stdString = new StdString('abc def ghi', StdString\Encodings::Charset['ASCII']);
        $this->assertTrue($stdString->isContains('abc'));
        $this->assertTrue($stdString->isContains('f gh'));
        $this->assertTrue($stdString->isContains('bc'));

        $this->assertFalse($stdString->isContains('not'));


        try {
            $stdString = new StdString('щука 漢字', StdString\Encodings::Charset['UTF-8']);
            $this->assertTrue($stdString->isContains('щука'));
            $this->assertTrue($stdString->isContains('а 漢'));
            $this->assertTrue($stdString->isContains('字'));

            $this->assertFalse($stdString->isContains('not'));

        } catch (StringEncodingNotSupportedError $e) {
            $this->markTestSkipped('Missing support for UTF-8.');
        }
    }

    function testNormalizeNewLines()
    {
        try {
            $stdString = new StdString("aaжи\r\n\r\n\r\n\r\nвπά\n\n\n\n\n우리をあöä\r\n");
            $this->assertSame("aaжи\r\n\r\nвπά\n\n우리をあöä\r\n", (string) $stdString->normalizeExtraNewLines());

        } catch (StringEncodingNotSupportedError $e) {
            $stdString = new StdString("aazz\r\n\r\n\r\n\r\nbcd\n\n\n\n\nefghi\r\n");
            $this->assertSame("aazz\r\n\r\nbcd\n\nefghi\r\n", (string) $stdString->normalizeExtraNewLines());
        }
    }

    function testNormalizeWhitespaces()
    {
        $stdString = new StdString('  foo   bar  ');
        $this->assertSame('foo bar', (string) $stdString->normalizeWhitespaces());

        $stdString = new StdString('   Ο     συγγραφέας  ');
        $this->assertSame('Ο συγγραφέας', (string) $stdString->normalizeWhitespaces());

        $stdString = new StdString("\tΟ \r\n   συγγραφέας  ");
        $this->assertSame('Ο συγγραφέας', (string) $stdString->normalizeWhitespaces());
    }

    function testItsArrayAccessible()
    {
        $stdString = StdString::of('foo bar');

        $this->assertFalse(isset($stdString[7]));
        $this->assertTrue(isset($stdString[1]));
        $this->assertEquals('o', $stdString[1]);


        try {
            if (StdString::wrapper('UTF-8')) {
                $stdString = StdString::of('中文空白');

                $this->assertFalse(isset($stdString[4]));
                $this->assertTrue(isset($stdString[1]));
                $this->assertEquals('文', $stdString[1]);
            }
        } catch (StringEncodingNotSupportedError $e) {
            $this->markTestSkipped('utf-8 string not supported.');
        }
    }

    function testItsCountable()
    {
        $stdString = StdString::of('foo bar');
        $this->assertEquals(7, $stdString->length());
        $this->assertEquals(7, count($stdString));

        $stdString = StdString::of('中文空白');
        $this->assertEquals(4, $stdString->length());
        $this->assertEquals(4, count($stdString));
    }

    // ..

    function provideSafeImplodeData()
    {
        # ($initialValue, $delimiter, $concatArray, $expected)
        return [
            ['',    '',  ['a', 'b', 'c'],   'abc'],
            ['',    '/', ['a', 'b', 'c'],   'a/b/c'],
            ['',    '/', ['a', '/b/', 'c'], 'a/b/c'],
            ['/a',  '/', ['b', 'c', 'd'],   '/a/b/c/d'],
            ['a/',  '/', ['b', 'c', 'd'],   'a/b/c/d'],
            ['/a/', '/', ['b/', 'c', 'd/'], '/a/b/c/d/'],
        ];
    }

    function provideStripPrefixData()
    {
        # ($initialValue, $strip, $removeWhitespaces, $expected)
        return [
            ['/foo/bar/baz',  '/',     true, 'foo/bar/baz'],
            ['/foo/bar/baz',  '/foo/', true, 'bar/baz'],
            ['  /foo/bar/',   '',      true, '/foo/bar/'],
            ['  /foo/bar/',   null,    true, '/foo/bar/'],
            ['  /foo/bar/',   '/f',    true, 'oo/bar/'],
            ['foo/bar/baz',   '/foo',  true, 'foo/bar/baz'],
            [" \tfoo/bar/baz", ' ',    false, "\tfoo/bar/baz"],

            ['آهایی دنیا سلام',  'آهایی', true, 'دنیا سلام'],
            ['آهایی دنیا سلام',  'آهایی', false, ' دنیا سلام'],
        ];
    }

    function provideStripPostfixData()
    {
        # ($initialValue, $strip, $removeWhitespaces, $expected)
        return [
            ['foo/bar/baz/',  '/',     true, 'foo/bar/baz'],
            ['foo/bar/baz/',  '/baz/', true, 'foo/bar'],
            ['/foo/bar/  ',   '',      true, '/foo/bar/'],
            ['/foo/bar/  ',   null,    true, '/foo/bar/'],
            ['/foo/bar/  ',   'r/',    true, '/foo/ba'],
            ['foo/bar/baz',   'baz/',  true, 'foo/bar/baz'],
            ["foo/bar/baz\t ", ' ',    false, "foo/bar/baz\t"],

            ['آهایی دنیا سلام',  'سلام', true, 'آهایی دنیا'],
            ['آهایی دنیا سلام',  'سلام', false, 'آهایی دنیا '],
        ];
    }

    function provideSubStringData()
    {
        # ($encoding, $initialValue, $start, $length, $expected)
        return [
            ['ascii', 'abcde', 1,  2,    'bc'],
            ['ascii', 'abcde', -2, null, 'de'],
            ['ascii', 'abcde', -3, 2,    'cd'],

            ['utf-8', '中文空白', 1,  2,    '文空'],
            ['utf-8', '中文空白', -2, null, '空白'],
            ['utf-8', '中文空白', -3, 2,    '文空'],
        ];
    }

    function providePascalCaseData()
    {
        # ($encoding, $initialValue, $expected)
        return [
            ['ascii', 'pascalCase',      'PascalCase'],
            ['ascii', 'pascal_case',     'PascalCase'],
            ['ascii', 'pascal__case',    'PascalCase'],
            ['ascii', '__pascal_case__', '__PascalCase__'],
            ['ascii', '  pascal  case',  'PascalCase'],
            ['ascii', 'pascal2case',     'Pascal2case'],
            ['ascii', 'pascal2_case',     'Pascal2Case'],

            ['utf-8', 'σascalCase',      'ΣascalCase'],
            ['utf-8', 'σascal_case',     'ΣascalCase'],
            ['utf-8', 'σascal__case',    'ΣascalCase'],
            ['utf-8', '__σascal_case__', '__ΣascalCase__'],
            ['utf-8', '  σascal  case',  'ΣascalCase'],
            ['utf-8', 'σascal2case',     'Σascal2case'],
        ];
    }

    function provideCamelCaseData()
    {
        # ($encoding, $initialValue, $expected)
        return [
            ['ascii', 'CascalCase',      'cascalCase'],
            ['ascii', 'cascal_case',     'cascalCase'],
            ['ascii', 'cascal__case',    'cascalCase'],
            ['ascii', '__Cascal_case__', '__cascalCase__'],
            ['ascii', '  cascal  case',  'cascalCase'],
            ['ascii', 'Cascal2case',     'cascal2case'],
            ['ascii', 'Cascal2_case',     'cascal2Case'],

            ['utf-8', 'ΣascalCase',      'σascalCase'],
            ['utf-8', 'σascal_case',     'σascalCase'],
            ['utf-8', 'σascal__case',    'σascalCase'],
            ['utf-8', '__σascal_case__', '__σascalCase__'],
            ['utf-8', '  σascal  case',  'σascalCase'],
            ['utf-8', 'σascal2case',     'σascal2case'],
        ];
    }

    function provideSnakeCaseData()
    {
        # ($encoding, $initialValue, $expected)
        return [
            ['ascii', 'SnakeCase',                      'snake_case'],
            ['ascii', 'Snake#Case',                     'snake#_case'],
            ['ascii', 'snake Case',                     'snake_case'],
            ['ascii', 'Snake Case',                     'snake_case'],
            ['ascii', 'snake  case',                    'snake_case'],
            ['ascii', 'snake2Case',                     'snake2_case'],
            ['ascii', '__snakeCase__',                  '__snake_case__'],
            ['ascii', "\n\t " . ' snake case  ' . "\n", 'snake_case'],

            ['utf-8', 'ΣτανιλCase',                      'στανιλ_case'],
            ['utf-8', 'Στανιλ#Case',                     'στανιλ#_case'],
            ['utf-8', 'στανιλ Case',                     'στανιλ_case'],
            ['utf-8', 'Στανιλ Case',                     'στανιλ_case'],
            ['utf-8', 'στανιλ  case',                    'στανιλ_case'],
            ['utf-8', '__στανιλCase__',                  '__στανιλ_case__'],
            ['ascii', "\n\t " . ' στανιλ case  ' . "\n", 'στανιλ_case'],
        ];
    }

    protected function getDummyStringify()
    {
        return new class {
            function __toString() {
                return 'dummy_string';
            }
        };
    }
}
