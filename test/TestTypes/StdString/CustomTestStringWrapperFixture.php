<?php
namespace PoirotTest\Std\TestTypes\StdString;

use Poirot\Std\Type\StdString\Wrappers\aStringWrapper;

final class CustomTestStringWrapperFixture
    extends aStringWrapper
{
    /**
     * @inheritDoc
     */
    static function isSupported(string $encoding, $convertEncoding = null) : bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    protected function doConvert($str, $fromEncoding, $toEncoding)
    {
        return 'dummy_string_to_determine_wrapper';
    }
}
