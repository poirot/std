<?php
namespace PoirotTest\Std;

use PHPUnit\Framework\TestCase;
use Poirot\Std\CriteriaLexer\Criteria;
use Poirot\Std\CriteriaLexer\ParsedCriteria;

/**
 * Test Criteria Lexer Parser
 *
 * @see Criteria
 * @see ParsedCriteria
 */
class CriteriaLexerTest
    extends TestCase
{
    function testCriteriaConstruction()
    {
        $criteria = '/simple/criteria/with/:variable~\d+~>';
        $criteriaObject = new Criteria($criteria);

        // get criteria same as passed to construct as an argument
        $this->assertEquals($criteria, $criteriaObject->getCriteria());

        // default literal characters defined is important
        $this->assertEquals('A-Za-z0-9_/@\+\-\.', $criteriaObject->getLiteralChars());
    }

    function testWithNewLiteralCharacters()
    {
        $criteria = '/simple/criteria/with/:variable~\d+~>';
        $criteriaObject = new Criteria($criteria);

        // after call it should be created new instance object of criteria
        $newCriteriaObj = $criteriaObject->withLiteralChars('A-Za-z0-9');
        $this->assertNotSame($newCriteriaObj, $criteriaObject);

        // and literal is now changed to given one
        $this->assertEquals('A-Za-z0-9', $newCriteriaObj->getLiteralChars());
    }

    function testFailedWithEmptyCriteria()
    {
        // when empty criteria given to the construct it will throw an exception
        $this->expectException(\InvalidArgumentException::class);

        new Criteria('');
    }

    function testParsedCriteria()
    {
        // parsed criteria is instance of ParsedCriteria object
        $criteria = new Criteria('simple_literal');

        $this->assertInstanceOf(
            ParsedCriteria::class,
            $criteria->parse()
        );
    }

    function testUnknownCriteriaGiven()
    {
        // criteria is unknown by parser and nothing is recognized in tokens array
        $criteria = new Criteria('|some word here|');

        $this->assertSame( [], $criteria->parse()->asArray());
    }

    function testAddedExpectedLiteralCharacters()
    {
        // define space character as an added expected literal
        $criteria = new Criteria('this is literal', Criteria::LiteralDefault . ' ');

        $this->assertSame(
            [
                [
                    '_literal_' => 'this is literal',
                ]
            ]
            , $criteria->parse()->asArray()
        );
    }

    function testParseCriteriaVariations()
    {
        $criteria = new Criteria('/+These_are-@AllLiterals.');

        $this->assertSame(
            '/+These_are-@AllLiterals.',
            $criteria->parse()->asString()
        );
    }

    /**
     * @dataProvider provideRegexCriteriaData
     * @param string $criteria
     * @param array $expected
     * @param string $expectedRegex
     */
    function testParseRegexCriteria($criteria, $expected, $expectedRegex)
    {
        // criteria that has regex part; criteria to setup to match an email address
        //
        $criteria = new Criteria($criteria);

        $this->assertEquals(
            $expected,
            $criteria->parse()->asArray()
        );

        $this->assertEquals(
            $expectedRegex,
            $criteria->parse()->asRegex()
        );
    }

    /**
     * @dataProvider provideVariableCriteriaData
     * @param string $criteria
     * @param array $expected
     * @param string $expectedRegex
     */
    function testParseVariableCriteria($criteria, $expected, $expectedRegex)
    {
        // expected parameter inside criteria
        //
        $criteria = new Criteria($criteria);

        $this->assertEquals(
            $expected,
            $criteria->parse()->asArray()
        );

        $this->assertEquals(
            $expectedRegex,
            $criteria->parse()->asRegex()
        );
    }

    /**
     * @dataProvider provideEscapeCharacterCriteriaData
     * @param string $criteria
     * @param array $expected
     * @param string $expectedRegex
     */
    function testParseEscapeCharacterCriteria($criteria, $expected, $expectedRegex)
    {
        $criteria = new Criteria($criteria);

        $this->assertEquals(
            $expected,
            $criteria->parse()->asArray()
        );

        // match with start and ending regex mitigate
        $this->assertEquals(
            "(^{$expectedRegex}$)",
            $criteria->parse()->asRegex(ParsedCriteria::RegexMatchExact)
        );
    }

    /**
     * @dataProvider provideOptionalPartCriteriaData
     * @param string $criteria
     * @param array $expected
     * @param string $expectedRegex
     */
    function testParseOptionalPartCriteria($criteria, $expected, $expectedRegex)
    {
        $criteria = new Criteria($criteria);

        $this->assertEquals(
            $expected,
            $criteria->parse()->asArray()
        );

        $this->assertEquals(
            $expectedRegex,
            $criteria->parse()->asRegex()
        );
    }

    /**
     * @dataProvider provideOptionalPartCriteriaData
     * @param string $criteria
     */
    function testMatchRegexAgainstGivenInput($criteria)
    {
        $criteria = new Criteria($criteria);

        // Match with <userid> part
        $this->assertEquals(
            1,
            preg_match($criteria->parse()->asRegex(ParsedCriteria::RegexMatchExact), '/-1234')
        );

        // Match with <username> part
        $this->assertEquals(
            1,
            preg_match($criteria->parse()->asRegex(ParsedCriteria::RegexMatchExact), '/u/anywords')
        );
    }

    /**
     * @dataProvider provideOptionalPartCriteriaData
     * @param string $criteria
     */
    function testBuildCriteriaByGivenParams($criteria)
    {
        $criteria = new Criteria($criteria);

        $this->assertEquals(
            '/u/anywords',
            $criteria->parse()->asString(['username' => 'anywords'])
        );

        $this->assertEquals(
            '/-1234',
            $criteria->parse()->asString(['userid' => '1234'])
        );
    }

    /**
     * @dataProvider provideOptionalPartCriteriaData
     * @param string $criteria
     */
    function testFailedBuildCriteriaByGivenParams($criteria)
    {
        $this->expectException(\InvalidArgumentException::class);

        // the given param value dosen't match the defined regex \d+
        $criteria = new Criteria($criteria);
        $criteria->parse()->asString(['userid' => 'this is not int']);
    }

    function testFailedWithUnbalancedBracketCriteria()
    {
        $this->expectException(\RuntimeException::class);

        // last ~ char is missing from the given criteria
        $criteria = new Criteria(':var_name~\w+');
        $criteria->parse();
    }

    // ..

    function provideRegexCriteriaData()
    {
        return [
            [
                // Given Criteria,
                '~[a-zA-z0-9.-]+\@[a-zA-z0-9.-]+.[a-zA-Z]+~',
                // Expected Parsed Structure
                [
                    [
                        '_regex_' => '[a-zA-z0-9.-]+\@[a-zA-z0-9.-]+.[a-zA-Z]+',
                    ]
                ],
                // Expected Regex Output
                '[a-zA-z0-9.-]+\@[a-zA-z0-9.-]+.[a-zA-Z]+',
            ]
        ];
    }

    function provideVariableCriteriaData()
    {
        return [
            [
                // Given Criteria,
                '/:variable',
                // Expected Parsed Structure
                [
                    [
                        '_literal_' => '/',
                    ],
                    [
                        '_parameter_' => 'variable',
                    ]
                ],
                // Expected Regex Output
                '/(?P<variable>[^.]+)',
            ],
            [
                // Given Criteria, expected parameter that has all numeric value
                '/:variable_name~\d+~',
                // Expected Parsed Structure
                [
                    [
                        '_literal_' => '/',
                    ],
                    [
                        '_parameter_' => 'variable_name',
                        'variable_name' => '\d+',
                    ]
                ],
                // Expected Regex Output
                '/(?P<variable_name>\d+)',
            ]
        ];
    }

    function provideEscapeCharacterCriteriaData()
    {
        return [
            [
                // Given Criteria, colon ":" character here will not considered as variable token, it's escaped by \
                'localhost\:port',
                // Expected Parsed Structure
                [
                    [
                        '_literal_' => 'localhost',
                    ],
                    [
                        '_literal_' => ':',
                    ],
                    [
                        '_literal_' => 'port',
                    ],
                ],
                // Expected Regex Output
                'localhost\:port',
            ]
        ];
    }

    function provideOptionalPartCriteriaData()
    {
        return [
            [
                // Optional part of criteria; it may given /u/username or /-12 as userid
                '/<u/:username~[a-zA-Z0-9._]+~><-:userid~\d+~>',
                // Expected Parsed Structure
                [
                    [
                        '_literal_' => '/',
                    ],
                    [
                        '_optional_' => [
                            [
                                '_literal_' => 'u/',
                            ],
                            [
                                '_parameter_' => 'username',
                                'username' => '[a-zA-Z0-9._]+',
                            ],
                        ],
                    ],
                    [
                        '_optional_' => [
                            [
                                '_literal_' => '-',
                            ],
                            [
                                '_parameter_' => 'userid',
                                'userid' => '\\d+',
                            ],
                        ],
                    ],
                ],
                // Expected Regex Output
                '/(?:u/(?P<username>[a-zA-Z0-9._]+))?(?:\-(?P<userid>\d+))?',
            ]
        ];
    }
}
