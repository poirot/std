<?php
namespace PoirotTest\Std;

use PHPUnit\Framework\TestCase;
use Poirot\Std\Glob;


/**
 * @see Glob
 */
class GlobTest
    extends TestCase
{
    function testFallback()
    {
        if (! defined('GLOB_BRACE') )
            $this->markTestSkipped('GLOB_BRACE not available');

        $this->assertEquals(
            glob(__DIR__ . '/_files/{alph,bet}a', GLOB_BRACE),
            Glob::glob(__DIR__ . '/_files/{alph,bet}a', Glob::GLOB_BRACE, true)
        );
    }

    function testNonMatchingGlobShouldReturnsArray()
    {
        $result = Glob::glob('/some/path/{,*.}{this,orthis}.php', Glob::GLOB_BRACE);
        $this->assertIsArray($result);
    }

    function testThrowExceptionOnError()
    {
        $this->expectException(\Exception::class);

        // run into a max path length error
        $path = '/' . str_repeat('a', 10000);
        Glob::glob($path);
    }

    /**
     * @dataProvider providePatterns
     *
     * @param string $pattern
     * @param array  $expectedSequence
     */
    function testPatterns($pattern, $expectedSequence)
    {
        $result = Glob::glob(__DIR__ . '/_files/' . $pattern, Glob::GLOB_BRACE, true);

        $this->assertCount(count($expectedSequence), $result);

        foreach ($expectedSequence as $i => $expectedFileName)
            $this->assertStringEndsWith($expectedFileName, $result[$i]);
    }

    // ..

    function providePatterns()
    {
        return [
            [
                "{{,*.}alph,{,*.}bet}a",
                [
                    'alpha', 'eta.alpha', 'zeta.alpha', 'beta', 'eta.beta',
                    'zeta.beta'
                ]
            ]
        ];
    }
}
