<?php
namespace PoirotTest\Std;

use PHPUnit\Framework\TestCase;
use Poirot\Std\Traits\tMixin;


/**
 * @see tMixin
 */
class MixinTest
    extends TestCase
{
    protected $mixin;

    function setUp()
    {
        $this->mixin = new class() {
            use tMixin;
            private $privateVariable = 'privateValue';
            private static function getPrivateStatic() {
                return 'privateStaticValue';
            }
        };
    }

    function testAddNewMixinMethod()
    {
        $this->mixin::addMixinMethod('newMethod', function () {
            return 'newValue';
        });

        $this->assertEquals('newValue', $this->mixin->newMethod());
    }

    function testAddNewInvokableCallable()
    {
        $this->mixin::addMixinMethod('newMethod', new class() {
            function __invoke() {
                return 'newValue';
            }
        });

        $this->assertEquals('newValue', $this->mixin->newMethod());
    }

    function testImportMethodsFromClass()
    {
        $this->mixin::importMethodsFrom(
            $this->getObjectHasMethodsToImport()
        );

        $this->assertContains('__invoke', $this->mixin::listMixinMethods());
        $this->assertContains('mixinMethodA', $this->mixin::listMixinMethods());
        $this->assertContains('mixinMethodB', $this->mixin::listMixinMethods());
        $this->assertContains('mixinMethodC', $this->mixin::listMixinMethods());

        $this->assertEquals('privateValue-test',  $this->mixin->mixinMethodA());
        $this->assertEquals('privateValue-new',  $this->mixin->mixinMethodA('new'));
        $this->assertEquals('privateValue-C',  $this->mixin->mixinMethodC('C'));
    }

    function testFailedRequiredParamOfImportedMethods()
    {
        $this->mixin::importMethodsFrom(
            $this->getObjectHasMethodsToImport()
        );

        $this->expectException(\ArgumentCountError::class);
        $this->mixin->mixinMethodC();
    }

    function testMixinPassesArgumentsCorrectly()
    {
        $this->mixin::addMixinMethod('concatenate', function (...$strings) {
            return implode('-', $strings);
        });

        $this->assertEquals('1-2-3', $this->mixin->concatenate(1, 2, 3));
    }

    function testMixinPassesOptionalArgumentsCorrectly()
    {
        $this->mixin::addMixinMethod('concatenate', function ($a, $b, $c = 3) {
            return implode('-', [$a, $b, $c]);
        });

        $this->assertEquals('1-2-3', $this->mixin->concatenate(1, 2));
    }

    function testRegisteredMethodsAreBoundToTheClass()
    {
        $this->mixin::addMixinMethod('newMethod', function () {
            return $this->privateVariable;
        });

        $this->assertEquals('privateValue', $this->mixin->newMethod());
    }

    function testMethodBoundWithClassCanCallStaticMethods()
    {
        $this->mixin::addMixinMethod('testStatic', function () {
            return $this::getPrivateStatic();
        });

        $this->assertEquals('privateStaticValue', $this->mixin->testStatic());
    }

    function testItWillThrowExceptionIfMethodDoesNotExist()
    {
        $this->expectException(\BadMethodCallException::class);

        $this->mixin->nonExistingMethod();
    }

    // ..

    private function getObjectHasMethodsToImport()
    {
        return new class {
            function __invoke() {
                return 'newValue';
            }

            function mixinMethodA($value = 'test') {
                return $this->mixinMethodB($value);
            }

            function mixinMethodB($value) {
                return $this->mixinMethodC($value);
            }

            function mixinMethodC() {
                return function ($value) {
                    /** @see self::setUp() */
                    return $this->privateVariable . '-' . $value;
                };
            }
        };
    }
}
