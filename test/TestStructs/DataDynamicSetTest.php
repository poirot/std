<?php
namespace PoirotTest\Std\TestStructs;

use Poirot\Std\Exceptions\Struct\PropertyError;
use Poirot\Std\Exceptions\Struct\PropertyIsGetterOnlyError;
use Poirot\Std\Exceptions\Struct\PropertyIsImmutableError;
use Poirot\Std\Exceptions\Struct\PropertyNotNullableError;
use Poirot\Std\Exceptions\Struct\PropertyIsSetterOnlyError;
use Poirot\Std\Struct\DataDynamicSet;
use PoirotTest\Std\TestStructs\TestDataSet\DataDynamicSetFixture;

/**
 * @see DataDynamicSet
 */
class DataDynamicSetTest
    extends aDataTest
{
    /**
     * Data Class Name
     *
     * @return string
     */
    function getDataClassName(): string
    {
        return DataDynamicSet::class;
    }

    function testFailedWhenProvideInvalidPropertyValue()
    {
        $dataClass = new DataDynamicSetFixture;

        // Invalid Data; Internal PHP Error will wrap into PropertyError
        try {
            $dataClass->created_date = '2020-01-14';
        } catch (\Exception $e) {
            $this->assertInstanceOf(PropertyError::class, $e);
            $this->assertNotInstanceOf(PropertyError::class, $e->getPrevious());
        }

        // Logical Error Thrown By Programmer should be instance of PropertyError
        try {
            $dataClass->created_date = new \DateTime();
            $dataClass->created_date = new \DateTime(); // cause immutable error
        } catch (\Exception $e) {
            $this->assertInstanceOf(PropertyError::class, $e);
            $this->assertInstanceOf(PropertyIsImmutableError::class, $e);
            $this->assertNull( $e->getPrevious() );
        }
    }

    function testCantSetValueForGetterOnlyProperty()
    {
        $this->expectException(PropertyIsGetterOnlyError::class);

        $dataClass = new DataDynamicSetFixture;
        $dataClass->personal_id = md5('Payam' . time());
    }

    function testCantGetValueForSetterOnlyProperty()
    {
        $this->expectException(PropertyIsSetterOnlyError::class);

        $dataClass = new DataDynamicSetFixture;
        $dataClass->birth_date = new \DateTime('1983-07-14');
        $birthDate = $dataClass->birth_date;
    }

    function testCantUnsetNotNullableSetterMethod()
    {
        $this->expectException(PropertyNotNullableError::class);

        $dataClass = new DataDynamicSetFixture;
        $dataClass->created_date = new \DateTime;
        unset($dataClass->created_date);
    }

    function testFailedWhenTryToUnsetGetterOnlyProperty()
    {
        $this->expectException(PropertyIsGetterOnlyError::class);

        $dataClass = new DataDynamicSetFixture;
        unset($dataClass->personal_id);
    }

    function testDataSetIsTraversable()
    {
        $expected = ['name', 'created_date', 'personal_id'];

        $dataClass = new DataDynamicSetFixture;
        $i = 0; foreach ($dataClass as $name => $value) {
            $this->assertEquals($name, $expected[$i]);
            $i++;
        }
        $this->assertEquals(3, $i);
    }

    function testManipulateAndCheckAvailabilityOfPropertyData()
    {
        $dataClass = new DataDynamicSetFixture;

        // Check data availability when it's not set yet.
        $this->assertTrue($dataClass->created_date instanceof \DateTime && isset($dataClass->created_date));
        $this->assertTrue(null === $dataClass->name);
        $this->assertTrue(null === $dataClass->personal_id);
        // when value is null it will considered as isset === false
        $this->assertFalse(isset($dataClass->name));
        $this->assertFalse(isset($dataClass->personal_id));

        // Setter/Getter Methods
        $dataClass->name = 'pAyaM';
        $this->assertTrue(isset($dataClass->name) && $dataClass->has('name'));
        $this->assertEquals('Payam', $dataClass->name);

        $this->assertNull($dataClass->personal_id);
        $dataClass->birth_date = new \DateTime('1983-07-14');
        $this->assertNotNull($dataClass->personal_id);

        // we can add extra data as we want
        $dataClass->extra_field = 'extra';
        $this->assertTrue(isset($dataClass->extra_field) && $dataClass->has('extra_field'));
        $this->assertEquals('extra', $dataClass->extra_field);

        // delete property
        unset($dataClass->name);
        $this->assertFalse(isset($dataClass->name));
        $this->assertFalse($dataClass->has('name'));
        $this->assertEquals(null, $dataClass->name);

        // set again
        $dataClass->name = 'pAyaM';
        $this->assertTrue(isset($dataClass->name) && $dataClass->has('name'));
        $this->assertEquals('Payam', $dataClass->name);
    }

    function testCountableSetterGetter()
    {
        $dataClass = new DataDynamicSetFixture;

        $this->assertEquals(3, $dataClass->count());
        $this->assertEquals(3, count($dataClass));
    }

    function testDropPropertyWhenForcedToUnset()
    {
        $dataClass = new DataDynamicSetFixture(['name' => 'payam']);
        unset($dataClass->name);

        $expected = ['created_date', 'personal_id'];
        $i = 0; foreach ($dataClass as $name => $value) {
            $this->assertEquals($name, $expected[$i]);
            $i++;
        }
        $this->assertEquals(2, $i);

        $this->assertEquals(2, $dataClass->count());
        $dataClass->name = 'payam';
        $this->assertEquals(3, $dataClass->count());

        $expected = ['name', 'created_date', 'personal_id'];
        $i = 0; foreach ($dataClass as $name => $value) {
            $this->assertEquals($name, $expected[$i]);
            $i++;
        }
        $this->assertEquals(3, $i);
    }

    function testSetPropertyToNullNotEqualAsUnset()
    {
        $dataClass = new DataDynamicSetFixture(['name' => 'payam']);
        $dataClass->name = null;

        $expected = ['name', 'created_date', 'personal_id'];
        $i = 0; foreach ($dataClass as $name => $value) {
            $this->assertEquals($name, $expected[$i]);
            $i++;
        }
        $this->assertEquals(3, $i);

        $this->assertEquals(3, $dataClass->count());
    }

    function testImportDataSkipOnError()
    {
        $dataClass  = new DataDynamicSetFixture(['name' => 'payam', 'birth_date' => new \DateTime]);
        $exportData = iterator_to_array($dataClass);

        $loadClass  = new DataDynamicSetFixture;

        try {
            $loadClass->import($exportData);
            $this->fail('Exception Expected.');
        } catch (\Exception $e) {
            $this->assertInstanceOf(PropertyIsGetterOnlyError::class, $e);
        }

        $loadClass = new DataDynamicSetFixture;
        $loadClass->import($exportData, [PropertyIsGetterOnlyError::class]);
        $loadClass->import($exportData, [PropertyIsGetterOnlyError::class, PropertyIsImmutableError::class]);
        $loadClass->import($exportData, [PropertyError::class]);
        $loadClass->import($exportData, [\Exception::class]);
    }
}
