<?php
namespace PoirotTest\Std\TestStructs;

use Poirot\Std\IteratorWrapper;
use Poirot\Std\Struct\CollectionPriorityObject;


/**
 * @see CollectionPriorityObject
 */
class CollectionPriorityObjectTest
    extends CollectionObjectTest
{
    function setUp()
    {
        $this->collectionObject = new CollectionPriorityObject;
    }

    function testPriorityOrder()
    {
        $this->collectionObject->add('C', ['__order' => '2']);
        $this->collectionObject->add('A', ['__order' => 4]);
        $this->collectionObject->add('D', ['__order' => 1]);
        $this->collectionObject->add('B', ['__order' => 3]);

        // Iterate all
        $self = $this;
        $fr = $this->collectionObject->find([], function ($object) use ($self) {
            $self->assertInstanceOf(IteratorWrapper::class, $this);
            return $object;
        });

        $expected = ['A', 'B', 'C', 'D'];
        $i = 0; foreach ($fr as $object) {
            $this->assertSame($expected[$i], $object);
            $i++;
        }

        $this->assertEquals(count($expected), $i); // be sure that we iterate all
    }

    function testPriorityOrderWith3rdArgument()
    {
        $this->collectionObject->add('C', [], 2);
        $this->collectionObject->add('A', [], 4);
        $this->collectionObject->add('D', [], 1);
        $this->collectionObject->add('B', [], 3);

        // Iterate all
        $self = $this;
        $fr = $this->collectionObject->find([], function ($object) use ($self) {
            $self->assertInstanceOf(IteratorWrapper::class, $this);
            return $object;
        });

        $expected = ['A', 'B', 'C', 'D'];
        $i = 0; foreach ($fr as $object) {
            $this->assertSame($expected[$i], $object);
            $i++;
        }

        $this->assertEquals(count($expected), $i); // be sure that we iterate all
    }
}
