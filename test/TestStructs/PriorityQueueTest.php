<?php
namespace PoirotTest\Std\TestStructs;

use PHPUnit\Framework\TestCase;
use Poirot\Std\Struct\PriorityQueue;

/**
 * @see PriorityQueue
 */
class PriorityQueueTest
    extends TestCase
{
    function testIteratingQueueWillNotRemoveItems()
    {
        $queue = new PriorityQueue;
        $testQueue = clone $queue; # empty state

        $queue->insert('foo', 3);
        $queue->insert('bar', 4);
        $queue->insert('baz', 2);
        $queue->insert('bat', 1);
        $this->assertNotEquals($testQueue, $queue);

        foreach ($queue as $_) {}
        $this->assertNotEquals($testQueue, $queue);
    }

    /**
     * @dataProvider provideOrderedQueue
     */
    function testIterateInExpectedOrder($queue, $expected)
    {
        for ($i = 0; $i < 3; $i++) {
            $test = []; foreach ($queue as $item)
                $test[] = $item;

            $this->assertEquals($expected, $test);
        }
    }

    // ..

    function provideOrderedQueue()
    {
        yield [
            $this->fillQueueWithDummyData(new PriorityQueue(PriorityQueue::Descending)),
            ['bar', 'foo', 'baz', 'bat'],
        ];

        yield [
            $this->fillQueueWithDummyData(new PriorityQueue(PriorityQueue::Ascending)),
            ['bat', 'baz', 'foo', 'bar'],
        ];
    }

    function fillQueueWithDummyData($queue)
    {
        $queue->insert('foo', 3);
        $queue->insert('bar', 4);
        $queue->insert('baz', 2);
        $queue->insert('bat', 1);
        return $queue;
    }
}
