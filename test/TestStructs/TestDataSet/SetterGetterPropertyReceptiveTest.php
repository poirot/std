<?php
namespace PoirotTest\Std\TestStructs\TestDataSet;

use PHPUnit\Framework\TestCase;
use Poirot\Std\Struct\DataSet\DataSetPropertyObject;
use Poirot\Std\Traits\tSetterGetterPropertyReceptive;

/**
 * @see tSetterGetterPropertyReceptive
 */
class SetterGetterPropertyReceptiveTest
    extends TestCase
{
    /** @var \ReflectionClass */
    protected $setterGetterObjectFixture;

    function setUp()
    {
        $class = new \ReflectionClass(SetterGetterPropertyReceptiveFixture::class);
        $this->setterGetterObjectFixture = $class;

        parent::setUp();
    }

    function testPropertyObjects()
    {
        // check that notation is supported
        $isAnnotationEnabled = $this->makeMethodAccessible('_isAnnotationEnabled')
            ->invoke(new SetterGetterPropertyReceptiveFixture);
        $this->assertTrue($isAnnotationEnabled);

        // get dataset property objects
        $dataSetProperties = $this->makeMethodAccessible('_getDataSetPropertyObjects')
            ->invoke(new SetterGetterPropertyReceptiveFixture);
        $this->assertInstanceOf(\Generator::class, $dataSetProperties);

        // expected properties
        $expectedPropertyMethods = [
            'property_immutable' => [
                'givePropertyImmutable' => DataSetPropertyObject::Writable,
            ],
            'property_getter' => [
                'getPropertyGetter' => DataSetPropertyObject::Readable,
            ],
            'property_getter_setter' => [
                'getPropertyGetterSetter' => DataSetPropertyObject::Readable,
                'setPropertyGetterSetter' => DataSetPropertyObject::Writable,
            ],
            'property_boolean' => [
                'isPropertyBoolean' => DataSetPropertyObject::Readable,
            ],
            'property_state' => [
                'hasPropertyState' => DataSetPropertyObject::Readable,
            ],
        ];

        foreach ($dataSetProperties as $property => $availableMethods)
        {
            $this->assertTrue( isset($expectedPropertyMethods[$property]) );

            /** @var DataSetPropertyObject $propObject */
            foreach ($availableMethods as $propObject) {
                // each available method is instance of Property Object
                // and we expected these method names
                $this->assertInstanceOf(DataSetPropertyObject::class, $propObject);
                $this->assertTrue( isset($expectedPropertyMethods[$property][$propObject->getMethodName()]) );

                // each method should match the expected method operator
                $methodOperate = $expectedPropertyMethods[$property][$propObject->getMethodName()];
                if ( DataSetPropertyObject::Readable === $methodOperate) {
                    $this->assertTrue($propObject->isReadable());
                } elseif (DataSetPropertyObject::Writable === $methodOperate) {
                    $this->assertTrue($propObject->isWritable());
                }
            }
        }
    }

    /**
     * @depends testPropertyObjects
     */
    function testIgnoredPropertyObjectsCanBeRead()
    {
        // get dataset property objects
        $propertyObject = new SetterGetterPropertyReceptiveFixture;
        $propertyObject->refuseExcludedPropertyMethod(
            'getMethodIgnoredByClassNotation',
            'setMethodIgnoredByClassNotation');

        $dataSetProperties = $this->makeMethodAccessible('_getDataSetPropertyObjects')
            ->invoke($propertyObject);

        $this->assertInstanceOf(\Generator::class, $dataSetProperties);

        $properties = iterator_to_array($dataSetProperties);
        $this->assertArrayHasKey('method_ignored_by_class_notation', $properties);
    }

    // ..

    function makeMethodAccessible($methodName): \ReflectionMethod
    {
        $method = $this->setterGetterObjectFixture->getMethod($methodName);
        $method->setAccessible(true);

        return $method;
    }
}
