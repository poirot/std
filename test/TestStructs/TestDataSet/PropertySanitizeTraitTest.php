<?php
namespace PoirotTest\Std\TestStructs\TestDataSet;

use PHPUnit\Framework\TestCase;
use Poirot\Std\Traits\tPropertySanitizeKey;


/**
 * @see tPropertySanitizeKey
 */
class PropertySanitizeTraitTest
    extends TestCase
{
    /** @var \ReflectionClass */
    protected $sanitizeKeyFixture;

    function setUp()
    {
        $class = new \ReflectionClass(PropertySanitizeFixture::class);
        $this->sanitizeKeyFixture = $class;

        parent::setUp();
    }

    /**
     * @dataProvider provideSanitizePropertyKeyData
     */
    function testSanitizeToPropertyKey($methodNameCase, $expectedPropertyKey)
    {
        $sanitizedProperty = $this->makeMethodAccessible('_sanitizeToPropertyKey')
            ->invoke(new PropertySanitizeFixture, $methodNameCase);

        $this->assertEquals($expectedPropertyKey, $sanitizedProperty);
    }

    /**
     * @dataProvider provideSanitizeBackFromPropertyKeyData
     */
    function testSanitizeBackFromPropertyKey($propertyKey, $expected)
    {
        $sanitizedProperty = $this->makeMethodAccessible('_sanitizeBackFromPropertyKey')
            ->invoke(new PropertySanitizeFixture, $propertyKey);

        $this->assertEquals($expected, $sanitizedProperty);
    }

    // ..

    function makeMethodAccessible($methodName): \ReflectionMethod
    {
        $method = $this->sanitizeKeyFixture->getMethod($methodName);
        $method->setAccessible(true);

        return $method;
    }

    function provideSanitizePropertyKeyData()
    {
        return [
            ['SnakeCase',                      'snake_case'],
            ['Snake#Case',                     'snake#_case'],
            ['snake Case',                     'snake_case'],
            ['Snake Case',                     'snake_case'],
            ['snake  case',                    'snake_case'],
            ['snake2Case',                     'snake2_case'],
            ['__snakeCase__',                  '__snake_case__'],
            ["\n\t " . ' snake case  ' . "\n", 'snake_case'],
        ];
    }

    function provideSanitizeBackFromPropertyKeyData()
    {
        return [
            ['pascal_case',     'PascalCase'],
            ['pascal__case',    'PascalCase'],
            ['__pascal_case__', '__PascalCase__'],
            ['pascal2case',     'Pascal2case'],
            ['pascal2_case',    'Pascal2Case'],
        ];
    }
}
