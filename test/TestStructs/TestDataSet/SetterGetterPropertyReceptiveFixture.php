<?php
namespace PoirotTest\Std\TestStructs\TestDataSet;

use Poirot\Std\Traits\tSetterGetterPropertyReceptive;

/**
 * @excludeByNotation enabled
 * Is Mandatory to activate notation doc to ignore methods.
 *
 * @method getMethodIgnoredByClassNotation @ignore
 * @method setMethodIgnoredByClassNotation @ignore
 */
class SetterGetterPropertyReceptiveFixture
{
    use tSetterGetterPropertyReceptive;

    protected $propertyGetterSetter;
    protected $propertyBoolean = true;
    protected $propertyImmutable;
    protected $propertyState = false;


    // Internal Methods that is not considered as property method because of visibility:

    protected function setProtectedInternalMethodForClass($default = null)
    { }

    private function giveProtectedInternalMethodForClass($default = null)
    { }

    protected function getProtectedInternalMethodForClass()
    {
        return 'Internal method use inside the class';
    }

    protected function hasProtectedInternalMethodForClass()
    {
        return true;
    }

    private function isProtectedInternalMethodForClass()
    {
        return true;
    }

    // Property Methods, they should prefixed with get,is,has,set,give with public visibility:

    // Immutable Property Setter:

    // Note: Immutable property may have not defined as nullable
    function givePropertyImmutable($value)
    {
        $this->propertyImmutable = $value;
    }

    // Getter Only

    function getPropertyGetter()
    {
        return 'getter';
    }

    // Getter/Setter

     function getPropertyGetterSetter(): string
    {
        return $this->propertyGetterSetter;
    }

    // Note: Setter method should define as nullable, used when unset property
    function setPropertyGetterSetter(?string $propertyGetterSetter): void
    {
        $this->propertyGetterSetter = $propertyGetterSetter;
    }

    // Boolean Getter:

    function isPropertyBoolean()
    {
        return $this->propertyBoolean;
    }

    // State Getter:

    function hasPropertyState()
    {
        return $this->propertyState;
    }

    // Excluded Method to not considered as Property; they might be kind of internal class method or comes
    // from interface but should'nt be a property:

    function setMethodIgnoredByClassNotation()
    { }

    function getMethodIgnoredByClassNotation()
    { }

    /**
     * @ignore
     */
    function isMethodIgnoredByDocNotation()
    { }

    /**
     * @ignore
     */
    function giveMethodIgnoredByDocNotation()
    { }
}
