<?php
namespace PoirotTest\Std\TestStructs\TestDataSet;

use Poirot\Std\Traits\tPropertySanitizeKey;


class PropertySanitizeFixture
{
    use tPropertySanitizeKey;
}
