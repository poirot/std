<?php
namespace PoirotTest\Std\TestStructs\TestDataSet;

use Poirot\Std\Exceptions\Struct\PropertyIsImmutableError;
use Poirot\Std\Struct\DataDynamicSet;

/**
 * @property string    $name;
 * @property \DateTime $created_date;
 * @property string    $personal_id;
 *
 * @property \DateTime $birth_date; Setter only
 */
class DataDynamicSetFixture
    extends DataDynamicSet
{
    protected $name;
    /** @var \DateTime|null */
    protected $birthDate;
    /** @var \DateTime */
    protected $createdDate;


    protected function setName(?string $name): void
    {
        if ($name !== null)
            $name = ucfirst(strtolower($name));

        $this->name = $name;
    }

    protected function getName(): ?string
    {
        return $this->name;
    }

    protected function giveCreatedDate(\DateTime $date)
    {
        if ($this->createdDate)
            throw new PropertyIsImmutableError(sprintf(
                'Property "created_date" is immutable.'
            ));

        $this->createdDate = $date;
    }

    protected function getCreatedDate(): \DateTime
    {
        if (! $this->createdDate )
            $this->createdDate = new \DateTime;

        return $this->createdDate;
    }

    // Setter only
    protected function setBirthDate(?\DateTime $date): void
    {
        $this->birthDate = $date;
    }

    // Getter only
    protected function getPersonalId(): ?string
    {
        if (!$this->birthDate)
            return null;

        return md5($this->getName() . $this->birthDate->getTimestamp());
    }
}
