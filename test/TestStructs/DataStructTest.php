<?php
namespace PoirotTest\Std\TestStructs;

use Poirot\Std\Struct\DataStruct;

/**
 * @see DataStruct
 */
class DataStructTest
    extends aDataTest
{
    /**
     * Data Class Name
     *
     * @return string
     */
    function getDataClassName(): string
    {
        return DataStruct::class;
    }

    function testManipulateAndCheckAvailabilityOfPropertyData()
    {
        $dataClass = $this->newDummyData();

        // check availability
        $this->assertTrue(isset($dataClass->name) && $dataClass->has('name'));
        $this->assertTrue(isset($dataClass->{'latest release'}) && $dataClass->has('latest release'));
        $this->assertTrue(isset($dataClass->designed_by) && $dataClass->has('designed_by'));

        // check access
        $this->assertEquals('PHP', $dataClass->name);
        $this->assertEquals('7.3.11', $dataClass->{'latest release'});
        $this->assertEquals('Rasmus Lerdorf', $dataClass->designed_by);

        // check remove
        $dataClass = $this->newDummyData();

        $dataClass->del('name');
        $this->assertFalse(isset($dataClass->name) || $dataClass->has('name'));
        $this->assertSame(null, $dataClass->name);

        unset($dataClass->{'latest release'});
        $this->assertFalse(isset($dataClass->{'latest release'}) || $dataClass->has('latest release'));
        $this->assertSame(null, $dataClass->{'latest release'});
        $this->assertFalse((bool)$dataClass->{'latest release'});

        // remove and check variadic arguments
        $dataClass = $this->newDummyData();
        $dataClass->del('name', 'latest release');
        $this->assertTrue($dataClass->has('designed_by'));
        $this->assertFalse($dataClass->has('name', 'latest release'));
    }
}
