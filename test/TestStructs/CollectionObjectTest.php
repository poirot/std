<?php
namespace PoirotTest\Std\TestStructs;

use PHPUnit\Framework\TestCase;
use Poirot\Std\IteratorWrapper;
use Poirot\Std\Struct\Collection\DatumId;
use Poirot\Std\Struct\CollectionObject;


/**
 * @see CollectionObject
 */
class CollectionObjectTest
    extends TestCase
{
    /** @var CollectionObject */
    protected $collectionObject;


    function setUp()
    {
        $this->collectionObject = new CollectionObject;
    }

    /**
     * @dataProvider provideDifferentDatumObjects
     */
    function testCollectionObject($datum)
    {
        // add datum into collection:
        $objectId = $this->collectionObject->add($datum);
        $this->assertInstanceOf(DatumId::class, $objectId);

        // objectId will be unique every time you add same object
        $this->assertEquals($objectId, $this->collectionObject->add($datum));

        // we can add meta data to same object
        $this->collectionObject->add($datum, ['test' => 'test data']);
        $this->collectionObject->add($datum, ['extra' => 'extra data']);

        // get stored object:
        $object = $this->collectionObject->get($objectId);
        $this->assertSame($datum, $object);

        // get stored object with additional data through callback
        [$object, $objData] = $this->collectionObject->get($objectId, function($obj, $meta) {
            return [$obj, $meta];
        });
        $this->assertSame($datum, $object);
        $this->assertSame(['test' => 'test data', 'extra' => 'extra data'], $objData);

        // get none-existence object id
        $this->assertNull(
            $this->collectionObject->get(DatumId::of([1, 'a', 2, 3]))
        );

        // when object not exists callback won't executed
        $self = $this;
        $this->assertNull(
            $this->collectionObject->get(DatumId::of([1, 'a', 2, 3]), function () use ($self) {
                $self->fail('Callback shouldnt executed when there is no object found.');
            })
        );
    }

    /**
     * @depends testCollectionObject
     */
    function testFindAndIterateObjects()
    {
        foreach ($dataObjects = $this->getIterableObjects() as ['object' => $object, 'data' => $metaData]) {
            $this->collectionObject->add($object, $metaData);
        }

        // find all
        $fr = $this->collectionObject->find([]);
        $this->assertInstanceOf(IteratorWrapper::class, $fr);

        $i = 0; foreach ($fr as $objHash => $object) {
            $this->assertSame($objHash, (string) DatumId::of($object));
            $this->assertSame($dataObjects[$i]['object'], $object);
            $i++;
        }

        $this->assertEquals(count($dataObjects), $i); // be sure that we iterate all
    }

    /**
     * @depends testCollectionObject
     */
    function testFindByMetaDataAndIterateObjects()
    {
        foreach ($dataObjects = $this->getIterableObjects() as ['object' => $object, 'data' => $metaData]) {
            $this->collectionObject->add($object, $metaData);
        }

        // find by given meta data
        $fr = $this->collectionObject->find(['name' => 'PHP']);
        $this->assertInstanceOf(IteratorWrapper::class, $fr);

        $this->assertSame($dataObjects[0]['object'], $fr->current());

        $fr->next();
        $this->assertNull($fr->current());
    }

    /**
     * @depends testCollectionObject
     */
    function testPriorityOrder()
    {
        foreach ($dataObjects = $this->getIterableObjects() as ['object' => $object, 'data' => $metaData]) {
            $this->collectionObject->add($object, $metaData);
        }

        // find all
        $self = $this;
        $fr = $this->collectionObject->find([], function ($object, $data, $objHash) use ($self) {
            $self->assertInstanceOf(IteratorWrapper::class, $this);
            return [$object, $data];
        });
        $this->assertInstanceOf(IteratorWrapper::class, $fr);

        $i = 0; foreach ($fr as $objHash => [$object, $data]) {
            $this->assertSame($objHash, (string) DatumId::of($object));
            $this->assertSame($dataObjects[$i]['data'], $data);
            $this->assertSame($dataObjects[$i]['object'], $object);
            $i++;
        }

        $this->assertEquals(count($dataObjects), $i); // be sure that we iterate all
    }

    /**
     * @depends testCollectionObject
     */
    function testRemoveItemFromCollection()
    {
        $datum    = new \stdClass;
        $objectId = $this->collectionObject->add($datum);
        $object   = $this->collectionObject->get($objectId);
        $this->assertSame($datum, $object);

        $this->collectionObject->del($objectId);
        $this->assertNull(
            $this->collectionObject->get($objectId)
        );

        // del can be called even if no object exists
        $this->collectionObject->del($objectId);
    }

    /**
     * @depends testCollectionObject
     */
    function testSetMetaDataToExistingObject()
    {
        $objectId = $this->collectionObject->add(new \stdClass);
        $this->collectionObject->setMetaData($objectId, ['meta' => 'data']);

        $objData = $this->collectionObject->get($objectId, function($_, $meta) {
            return $meta;
        });

        $this->assertSame(['meta' => 'data'], $objData);
    }

    function testFailSetMetaDataWhenObjectNotExists()
    {
        $this->expectException(\Exception::class);

        $objectId = DatumId::of([1, 'a', 2, 3]);
        $this->collectionObject->setMetaData($objectId, ['meta' => 'data']);
    }

    function testCountable()
    {
        $count = rand(1, 10); for ($i=1; $i<=$count; $i++) {
            $this->collectionObject->add(uniqid());
        }

        $this->assertEquals($count, $this->collectionObject->count());
        $this->assertEquals($count, count($this->collectionObject));
    }

    // ..

    function provideDifferentDatumObjects()
    {
        return [
            [12345],
            ['abcde'],
            [new \stdClass],
            [pack("nvc*", 0x1234, 0x5678, 65, 66)],
        ];
    }

    function getIterableObjects()
    {
        $data = [
            [
                'name' => 'PHP',
                'designed_by' => 'Rasmus Lerdorf',
            ],
            [
                'name' => 'NodeJs',
                'designed_by' => 'Ryan Dahl',
            ],
            [
                'name' => 'Golang',
                'designed_by' => 'Rob Pike',
            ],
        ];

        $result = [];
        foreach ($data as $d) {
            $result[] = [
                'object' => (object) $d,
                'data'   => $d,
            ];
        }

        return $result;
    }
}
