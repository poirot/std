<?php
namespace PoirotTest\Std\TestStructs;

use PHPUnit\Framework\TestCase;
use Poirot\Std\Exceptions\ValueObject\ObjectInitializeError;
use Poirot\Std\Exceptions\ValueObject\PropertyMissingOrNotFulFilledError;
use Poirot\Std\Struct\aValueObject;
use PoirotTest\Std\TestStructs\ValueObject\GeoValueObjectFixture;
use PoirotTest\Std\TestStructs\ValueObject\InvalidGeoValueObjectFixture;

/**
 * @see aValueObject
 */
class ValueObjectTest
    extends TestCase
{
    function testFailValueObjectInvalidDefinedConstructor()
    {
        $this->expectException(ObjectInitializeError::class);

        new InvalidGeoValueObjectFixture([
            '37.339724',
            '-121.873848',
        ]);
    }

    function testValueObjectFactoryWillResolveArgumentsToConstructor()
    {
        $geoObject = GeoValueObjectFixture::of([
            'caption' => 'Home',
            'geo' => [
                '37.339724',
                '-121.873848',
            ],
        ]);

        $this->assertInstanceOf(GeoValueObjectFixture::class, $geoObject);
    }

    function testFailOnTypeMismatchOfConstructArguments()
    {
        $this->expectException(PropertyMissingOrNotFulFilledError::class);

        GeoValueObjectFixture::of([
            'geo' => 'invalid_value',
        ]);
    }

    function testFailWhenMandatoryArgumentsMissing()
    {
        $this->expectException(PropertyMissingOrNotFulFilledError::class);

        GeoValueObjectFixture::of([
            'caption' => 'Home',
        ]);
    }

    function testPropertiesAreAccessibleByPropertyMethods()
    {
        $geoObject = new GeoValueObjectFixture([
            '37.339724',
            '-121.873848',
        ]);

        $geoObject = $geoObject
            ->withCaption('New Entitle')
            ->withGeo([
                '41.570950',
                '-73.292610'
            ]);

        $this->assertEquals('New Entitle', $geoObject->getCaption());
        $this->assertEquals('-73.292610', $geoObject->getLat());
        $this->assertEquals('41.570950', $geoObject->getLon());
    }

    function testValueObjectIsImmutable()
    {
        $geoObject = new GeoValueObjectFixture([
            '37.339724',
            '-121.873848',
        ], 'New Entitle');

        $newObject = $geoObject->withCaption('New Entitle');

        $this->assertEquals($geoObject, $newObject);
        $this->assertNotSame($geoObject, $newObject);
    }

    function testValueObjectIsTraversable()
    {
        $geoObject = GeoValueObjectFixture::of([
            'geo' => [
                '37.339724',
                '-121.873848',
            ],
        ]);

        $this->assertEquals([
            'geo' => [
                '37.339724',
                '-121.873848'
            ],
            'caption' => null,
            'lat' => '-121.873848',
            'lon' => '37.339724',
        ], iterator_to_array($geoObject));
    }

    function testEquality()
    {
        $a = GeoValueObjectFixture::of([
            'geo' => [
                '37.339724',
                '-121.873848',
            ],
        ]);

        $b = new GeoValueObjectFixture([
            '37.339724',
            '-121.873848',
        ]);

        $this->assertTrue($a->isEqualTo($b));
    }

    function testValueObjectIsSerializable()
    {
        $geoObject = GeoValueObjectFixture::of([
            'geo' => [
                '37.339724',
                '-121.873848',
            ],
        ]);

        $serialized = serialize($geoObject);
        $geoUnserialized = unserialize($serialized);

        $this->assertEquals($geoObject, $geoUnserialized);
    }
}
