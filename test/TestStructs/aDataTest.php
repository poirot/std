<?php
namespace PoirotTest\Std\TestStructs;

use PHPUnit\Framework\TestCase;
use Poirot\Std\Interfaces\Struct\iData;
use Poirot\Std\Struct\DataStruct;


abstract class aDataTest
    extends TestCase
{
    /**
     * Data Class Name
     *
     * @return string
     */
    abstract function getDataClassName(): string;


    function testConstructedPropertiesIsAccessible()
    {
        $properties = [
            'name' => 'PHP',
            'latest release' => '7.3.11',
            'designed_by' => 'Rasmus Lerdorf' ];

        /** @var iData $dataClass */
        $class = $this->getDataClassName();
        $dataClass = new $class($properties);

        $this->assertEquals($properties['name'], $dataClass->name);
        $this->assertEquals($properties['latest release'], $dataClass->{'latest release'});
        $this->assertEquals($properties['designed_by'], $dataClass->designed_by);
    }

    function testImportedPropertiesIsAccessible()
    {
        $properties = [
            'name' => 'PHP',
            'latest release' => '7.3.11',
            'designed_by' => 'Rasmus Lerdorf' ];

        /** @var iData $dataClass */
        $class = $this->getDataClassName();
        $dataClass = new $class();
        $dataClass->import($properties);

        $this->assertEquals($properties['name'], $dataClass->name);
        $this->assertEquals($properties['latest release'], $dataClass->{'latest release'});
        $this->assertEquals($properties['designed_by'], $dataClass->designed_by);
    }

    /**
     * @dataProvider provideImportIterable
     */
    function testCanImportAnyIterable($iterable, $expected)
    {
        /** @var iData $dataClass */
        $class = $this->getDataClassName();
        $dataClass = new $class();
        $dataClass->import($iterable);

        $this->assertEquals($expected['name'], $dataClass->name);
        $this->assertEquals($expected['latest release'], $dataClass->{'latest release'});
        $this->assertEquals($expected['designed_by'], $dataClass->designed_by);
    }

    function testDataIsIterable()
    {
        $expected = [['name' => 'PHP'], ['latest release' => '7.3.11'], ['designed_by' => 'Rasmus Lerdorf']];
        $dataClass = $this->newDummyData();

        foreach (range(0, 2) as $_) {
            // object data can iterated multiple time
            $i = 0; foreach ($dataClass as $name => $value) {
                $this->assertEquals([$name => $value], $expected[$i]);
                $i++;
            }
            $this->assertEquals(3, $i);
        }
    }

    function testDataObjectGeneralManipulation()
    {
        $dataClass = $this->newDummyData();

        // check access
        $this->assertEquals('PHP', $dataClass->name);
        $this->assertEquals('7.3.11', $dataClass->{'latest release'});
        $this->assertEquals('Rasmus Lerdorf', $dataClass->designed_by);

        // check remove
        unset($dataClass->designed_by);
        $this->assertFalse(isset($dataClass->designed_by) || $dataClass->designed_by);
        $this->assertSame(null, $dataClass->designed_by);
        $this->assertFalse((bool)$dataClass->designed_by);

        // check set
        $dataClass->designed_by = 'Rasmus Lerdorf';
        $this->assertEquals('Rasmus Lerdorf', $dataClass->designed_by);

        // check availability
        $this->assertTrue(isset($dataClass->name));
        $this->assertTrue(isset($dataClass->{'latest release'}));
        $this->assertTrue(isset($dataClass->designed_by));
    }

    function testCanContainsAnyRandomProperties()
    {
        /** @var iData $dataClass */
        $class = $this->getDataClassName();
        $dataClass = new $class();

        for ($i=0; $i<=10; $i++) {
            $key = uniqid();
            $val = uniqid();
            $dataClass->{$key} = $val;
            $this->assertTrue($dataClass->has($key));
            $this->assertEquals($val, $dataClass->$key);
        }
    }

    function testDataIsAccessibleByReference()
    {
        /** @var iData $dataClass */
        $class = $this->getDataClassName();
        $dataClass = new $class();

        $dataClass->data = 'value';
        $dataRef = &$dataClass->data;
        $dataRef.= '-changed';
        $this->assertEquals('value-changed', $dataClass->data);

        // Create Reserved memory reference property
        $this->assertFalse($dataClass->has('otherData'));
        $dataRef = &$dataClass->otherData;

        $this->assertFalse($dataClass->has('otherData'));
        $this->assertEquals(1, $dataClass->count());

        $dataRef = null;
        $this->assertFalse($dataClass->has('otherData'));
        $this->assertEquals(1, $dataClass->count());

        $dataRef = 'otherValue';
        $this->assertTrue($dataClass->has('otherData'));
        $this->assertEquals(2, $dataClass->count());
        $this->assertEquals('otherValue', $dataClass->otherData);
    }

    function testConstructFromArrayReference()
    {
        $data = [ 'name' => null ];
        $dataClass = DataStruct::ofReference($data);
        $this->assertInstanceOf(DataStruct::class, $dataClass);

        $this->assertTrue($dataClass->has('name'));

        $dataClass->name = 'Creativity';
        $data[] = 'An Other Member';     # manipulating data array itself change content of entity
        $name = &$dataClass->name;
        $name = 'Power';

        $expected = ['name' => 'Power', 0 => 'An Other Member'];
        $this->assertEquals($expected, $data);
        $this->assertEquals($expected, iterator_to_array($dataClass));
    }

    function testEmpty()
    {
        /** @var iData $dataClass */
        $dataClass = $this->newDummyData();
        $dataClass->del('name', 'latest release', 'designed_by');
        $this->assertTrue($dataClass->isEmpty());

        $dataClass = $this->newDummyData();
        $dataClass->empty();
        $this->assertTrue($dataClass->isEmpty());
    }

    function testCountable()
    {
        /** @var iData $dataClass */
        $class = $this->getDataClassName();
        $dataClass = new $class();
        $count = rand(1, 10); for ($i=1; $i<=$count; $i++) {
            $key = uniqid();
            $dataClass->{$key} = uniqid();
        }

        $this->assertEquals($count, $dataClass->count());
        $this->assertEquals($count, count($dataClass));
    }

    // ..

    final function newDummyData()
    {
        $className = $this->getDataClassName();
        $dataClass = new $className;
        $dataClass->name = 'PHP';
        $dataClass->{'latest release'} = '7.3.11';
        $dataClass->designed_by = 'Rasmus Lerdorf';

        return $dataClass;
    }

    final function provideImportIterable()
    {
        $expected = [
            'name' => 'PHP',
            'latest release' => '7.3.11',
            'designed_by' => 'Rasmus Lerdorf' ];

        yield [$expected, $expected];
        yield [new \ArrayIterator($expected), $expected];
        yield [$this->newDummyData(), $expected];
        yield [(function() use ($expected) {
            foreach ($expected as $k => $v)
                yield $k => $v;
        })(), $expected];
    }
}
