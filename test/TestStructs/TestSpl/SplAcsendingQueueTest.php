<?php
namespace PoirotTest\Std\TestStructs\TestSpl;

use Poirot\Std\Struct\Spl\SplAscendingQueue;

/**
 * @see SplAscendingQueue
 */
class SplAscendingQueueTest
    extends SplDescendingQueueTest
{
    /** @var SplAscendingQueue */
    protected $queue;

    function setUp()
    {
        parent::setUp();

        $this->queue = new SplAscendingQueue;
    }

    // ..

    function provideOrderedQueue()
    {
        $this->setUp();
        
        $queue = $this->queue;
        $this->fillQueueWithDummyData($queue);

        yield [
            $queue,
            ['bat', 'baz', 'foo', 'bar'],
        ];
    }
}
