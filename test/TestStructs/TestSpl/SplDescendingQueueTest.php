<?php
namespace PoirotTest\Std\TestStructs\TestSpl;

use PHPUnit\Framework\TestCase;
use Poirot\Std\Struct\Spl\SplDescendingQueue;

/**
 * @see SplDescendingQueue
 */
class SplDescendingQueueTest
    extends TestCase
{
    /** @var SplDescendingQueue */
    protected $queue;

    function setUp()
    {
        parent::setUp();

        $this->queue = new SplDescendingQueue;
    }

    function testWithIteratingQueueSetToInitState()
    {
        $queue = $this->queue;
        $testQueue = clone $queue; # empty state

        $this->fillQueueWithDummyData($queue);
        $this->assertNotEquals($queue, $testQueue);

        foreach ($queue as $_) {}
        $this->assertEquals($queue, $testQueue);
    }

    function testQueueIsIterableOnce()
    {
        $queue = $this->queue;
        $this->fillQueueWithDummyData($queue);

        for ($i = 1; $i < 2; $i++) {
            $test = []; foreach ($queue as $item)
                $test[] = $item;

            if ($i == 1) {
                $this->assertIsArray($test);
            } else {
                $this->assertEmpty($test);
            }
        }
    }

    /**
     * @dataProvider provideOrderedQueue
     */
    function testIterateInExpectedOrder($queue, $expected)
    {
        $test = []; foreach ($queue as $item)
            $test[] = $item;

        $this->assertEquals($expected, $test);
    }

    // ..

    function provideOrderedQueue()
    {
        $this->setUp();

        $queue = $this->queue;
        $this->fillQueueWithDummyData($queue);

        yield [
            $queue,
            ['bar', 'foo', 'baz', 'bat'],
        ];
    }

    function fillQueueWithDummyData($queue)
    {
        $queue->insert('foo', 3);
        $queue->insert('bar', 4);
        $queue->insert('baz', 2);
        $queue->insert('bat', 1);
    }
}
