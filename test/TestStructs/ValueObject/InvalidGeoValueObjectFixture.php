<?php
namespace PoirotTest\Std\TestStructs\ValueObject;

class InvalidGeoValueObjectFixture
    extends GeoValueObjectFixture
{
    /**
     * Constructor Dosen't met all necessary property setter methods available in object
     *
     * @param $geo
     */
    function __construct(array $geo)
    {
        parent::__construct($geo);
    }
}
