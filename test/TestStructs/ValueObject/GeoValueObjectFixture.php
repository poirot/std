<?php
namespace PoirotTest\Std\TestStructs\ValueObject;

use Poirot\Std\Struct\aValueObject;

/**
 * @method GeoValueObjectFixture withGeo(array $location)
 * @method GeoValueObjectFixture withCaption(?string $entitle)
 */
class GeoValueObjectFixture
    extends aValueObject
{
    /** @var array [lon, lat] */
    protected $geo;
    /** @var string Geo Lookup Caption  */
    protected $caption;

    /**
     * @inheritDoc
     */
    static function parseWith($origin, $strict = true): iterable
    {
        if (! is_string($origin))
            return parent::parseWith($origin);


        if ( strpbrk($origin, '.,') ) {
            // "37.339724,-121.873848"
            $geo = explode(',', $origin);
            $origin = [
                'geo' => [
                    trim($geo[0]),
                    trim($geo[1])
                ]
            ];
        }

        return self::parseWith($origin);
    }

    /**
     * Constructor
     *
     * @param $geo
     * @param string|null $caption
     */
    function __construct(array $geo, ?string $caption = null)
    {
        parent::__construct();

        $this->withGeo($geo);
        if (null !== $caption)
            $this->withCaption($caption);
    }

    function __toString()
    {
        return sprintf('%s,%s', $this->getGeo('lat'), $this->getGeo('lon'));
    }

    // Setter Properties:
    // we should have setter methods which match constructor arguments of value object

    /**
     * Set GeoLocation
     *
     * @param array $location
     *        [28.5122077, 53.5818702]
     *        ["lon": 28.5122077, "lat": 53.5818702]
     *
     * @return GeoValueObjectFixture
     */
    protected function withGeo(array $location)
    {
        if ( empty($location) )
            return $this;

        if ($location === null)
            $this->geo = null;


        if ( isset($location['lon']) )
            $this->geo = array($location['lon'], $location['lat']);
        else
            $this->geo = array($location[0], $location[1]);

        return $this;
    }

    /**
     * Set Geo Lookup Caption
     *
     * @param string $entitle
     *
     * @return GeoValueObjectFixture
     */
    protected function withCaption(?string $entitle)
    {
        $this->caption = $entitle;
        return $this;
    }

    // Getter Properties:
    // will present as a property when we iterate object

    protected function getCaption()
    {
        return $this->caption;
    }

    protected function getLat()
    {
        return $this->getGeo('lat');
    }

    protected function getLon()
    {
        return $this->getGeo('lon');
    }

    protected function getGeo($lonLat = null)
    {
        if ($lonLat === null)
            return $this->geo;

        # Geo Property (lon, lat)
        $lonLat = strtolower( (string) $lonLat );
        switch ($lonLat) {
            case 'lon':
            case 'long':
            case 'longitude':
                return $this->geo[0];
            case 'lat':
            case 'latitude':
                return $this->geo[1];
        }

        throw new \Exception(sprintf('Unknown Geo Property (%s).', $lonLat));
    }
}
