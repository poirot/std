<?php
namespace PoirotTest\Std\TestStructs;

use Poirot\Std\Struct\DataHashMap;

/**
 * @see DataHashMap
 */
class DataHashMapTest
    extends aDataTest
{
    /**
     * Data Class Name
     *
     * @return string
     */
    function getDataClassName(): string
    {
        return DataHashMap::class;
    }

    function testManipulateAndCheckAvailabilityOfPropertyData()
    {
        $dataClass = $this->newDummyData();

        // check availability
        $this->assertTrue($dataClass->has('name'));
        $this->assertTrue($dataClass->has('latest release'));
        $this->assertTrue($dataClass->has('designed_by'));

        // check access
        $this->assertEquals('PHP', $dataClass->get('name'));
        $this->assertEquals('7.3.11', $dataClass->get('latest release'));
        $this->assertEquals('Rasmus Lerdorf', $dataClass->get('designed_by'));

        // check remove
        $dataClass->del('name');
        $this->assertFalse($dataClass->has('name'));
        $this->assertSame(null, $dataClass->get('name'));
        $this->assertSame('PHP', $dataClass->get('name', 'PHP'));

        // check set
        $dataClass->set('name', 'PHP');
        $this->assertEquals('PHP', $dataClass->get('name'));

        // remove and check variadic arguments
        $dataClass = $this->newDummyData();
        $dataClass->del('name', 'latest release');
        $this->assertTrue($dataClass->has('designed_by'));
        $this->assertFalse($dataClass->has('name', 'latest release'));
    }

    function testDataKeyCanBeNoneScalarValue()
    {
        $dataClass = new DataHashMap;

        $dataClass->set([1, 2, 3], 6);
        $this->assertEquals(6, $dataClass->get([1, 2, 3]));
        $this->assertEquals(null, $dataClass->get([1, '2', 3]));

        $object = new \stdClass;
        $dataClass->set($object, [1, 2, 3]);
        $this->assertEquals([1, 2, 3], $dataClass->get($object));
        $this->assertEquals(null, $dataClass->get(new \stdClass));

        $closure = function () {};
        $dataClass->set($closure, 'value');
        $this->assertEquals('value', $dataClass->get($closure));
        $this->assertEquals(null, $dataClass->get(function () {}));

        // Non-Scalar Values Are Iterable
        $expectedKey = [[1, 2, 3], $object, $closure];
        $expectedVal = [6, [1, 2, 3], 'value'];
        $i = 0; foreach ($dataClass as $key => $val) {
            $this->assertEquals($expectedKey[$i], $key);
            $this->assertEquals($expectedVal[$i], $val);
            $i++;
        }

        $this->assertTrue($dataClass->has([1, 2, 3]));
        $dataClass->del([1, 2, 3]);
        $this->assertFalse($dataClass->has([1, 2, 3]));

        $this->assertTrue($dataClass->has($object));
        $dataClass->del($object);
        $this->assertFalse($dataClass->has($object));

        $this->assertTrue($dataClass->has($closure));
        $dataClass->del($closure);
        $this->assertFalse($dataClass->has($closure));
    }
}
