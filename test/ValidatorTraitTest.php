<?php
namespace PoirotTest\Std;

use PHPUnit\Framework\TestCase;
use Poirot\Std\Exceptions\Validator\ValidationError;
use Poirot\Std\Interfaces\Pact\ipValidator;
use Poirot\Std\Validator\aValidator;
use Poirot\Std\Validator\MessageProcessor\TruncateLength;
use Poirot\Std\Validator\tValidator;


/**
 * @see tValidator
 * @see aValidator
 */
class ValidatorTraitTest
    extends TestCase
{
    function tearDown()
    {
        ValidationError::resetMessageProcessorsToDefault();
    }


    function testFailedAssertValidation()
    {
        $validator = new class implements ipValidator {
            use tValidator;
            protected $value = -1;

            /**
             * @inheritDoc
             */
            function doAssertValidate(&$exceptions) {
                if ($this->value < 0)
                    $exceptions[] = $this->createError(
                        'Negative value is given.',
                        ValidationError::NotInRangeError,
                        null,
                        $this->value);
            }
        };

        $this->expectException(ValidationError::class);
        $validator->assertValidate();
    }

    function testCreateValidationErrorFromExceptions()
    {
        $validator = new class implements ipValidator {
            use tValidator;

            /**
             * @inheritDoc
             */
            function doAssertValidate(&$exceptions) {
                $exceptions[] = new \Exception(
                    'Error Validation Message.');
            }
        };

        try {
            $validator->assertValidate();
        } catch (ValidationError $e) {
            // skip root validation error exception
            do {
                $this->assertInstanceOf(ValidationError::class, $e);
            } while($e = $e->getPrevious());

        }

    }

    function testErrorChains()
    {
        $validator = new class implements ipValidator {
            use tValidator;
            protected $value = -1;

            /**
             * @inheritDoc
             */
            function doAssertValidate(&$exceptions) {
                if ($this->value < 0)
                    $exceptions[] = $this->createError(
                        'Negative value is given.',
                        ValidationError::NotInRangeError,
                        null,
                        $this->value);
            }
        };

       try {
           $validator->assertValidate();
       } catch (ValidationError $e) {
           $this->assertEquals(
               'Negative value is given.',
               $e->getMessage());
       }
    }

    function testWalkThroughChainedErrors()
    {
        $validator = new class implements ipValidator {
            use tValidator;

            /**
             * @inheritDoc
             */
            function doAssertValidate(&$exceptions) {
                $exceptions[] = $this->createError(
                    'Username is already reserved.',
                    'user-exists',
                    'username');

                $exceptions[] = $this->createError(
                    'Password should me more than 8 characters.',
                    ValidationError::NotInRangeError,
                    'password');

                $exceptions[] = $this->createError(
                    'Registration Failed.',
                    'failed');
            }
        };

        // Simply Iterate All Errors
        try {
            $validator->assertValidate();

        } catch (ValidationError $e) {
            $i = 0;
            $expectedErrors = ['failed', ValidationError::NotInRangeError, 'user-exists'];
            do {
                $this->assertEquals($expectedErrors[$i], $e->getError());
                $i ++;
            } while($e = $e->getPrevious());
        }
    }

    function testAllErrorMessages()
    {
        $validator = new class implements ipValidator {
            use tValidator;

            /**
             * @inheritDoc
             */
            function doAssertValidate(&$exceptions)
            {
                $exceptions[] = $this->createError(
                    'Error Message.',
                    ValidationError::NotInRangeError,
                    'labeled-param1',
                    -1);

                $exceptions[] = $this->createError(
                    'Error Message 1.',
                    ValidationError::NotInRangeError,
                    'labeled-param2',
                    1000);

                $exceptions[] = $this->createError(
                    'Error Message 2.',
                    ValidationError::RequiredError,
                    'labeled-param2',
                    1000);

                $exceptions[] = $this->createError(
                    'Required Conditions Doesnt Match.',
                    ValidationError::RequiredError);
            }
        };

        try {
            $validator->assertValidate();
        } catch (ValidationError $e) {
            $this->assertEquals([
                0 => [
                    'type' => 'required',
                    'message' => 'Required Conditions Doesnt Match.',
                    'value' => null,
                ],
                'labeled-param2' => [
                    'type' => [
                        'required',
                        'not-in-range',
                    ],
                    'message' => [
                        'Error Message 2.',
                        'Error Message 1.',
                    ],
                    'value' => 1000,
                ],
                'labeled-param1' => [
                    'type' => 'not-in-range',
                    'message' => 'Error Message.',
                    'value' => -1,
                ]],
                $e->asErrorsArray()
            );
        }
    }

    function testDefaultMessageProcessors()
    {
        $validator = new class implements ipValidator {
            use tValidator;

            /**
             * @inheritDoc
             */
            function doAssertValidate(&$exceptions) {
                $exceptions[] = $this->createError(
                    'Parameter (%param%) has wrong value (%value%) as input.',
                    ValidationError::NotInRangeError,
                    'namedParam',
                    'invalid-value');
            }
        };

        try {
            $validator->assertValidate();
        } catch (ValidationError $e) {
            $this->assertEquals(
                'Parameter (namedParam) has wrong value (\'invalid-value\') as input.',
                $e->getMessage());
        }
    }

    function testRegisterNewMessageProcessors()
    {
        $validator = new class implements ipValidator {
            use tValidator;

            /**
             * @inheritDoc
             */
            function doAssertValidate(&$exceptions) {
                $exceptions[] = $this->createError(
                    'Parameter (%param%) has wrong value (%value%) as input.',
                    ValidationError::NotInRangeError,
                    'namedParam',
                    'invalid-value');
            }
        };

        ValidationError::addMessageProcessor(new TruncateLength(50), PHP_INT_MIN);

        try {
            $validator->assertValidate();
        } catch (ValidationError $e) {
            $this->assertEquals(
                'Parameter (namedParam) has wrong value (\'invali...',
                $e->getMessage());
        }
    }
}
