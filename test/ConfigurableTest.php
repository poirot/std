<?php
namespace PoirotTest\Std;

use PHPUnit\Framework\TestCase;
use Poirot\Std\Configurable\aConfigurable;
use Poirot\Std\Configurable\aConfigurableSetter;
use Poirot\Std\Configurable\Params;
use Poirot\Std\Exceptions\Configurable\ConfigurationError;
use Poirot\Std\Exceptions\Configurable\ConfigurationPropertyTypeError;
use Poirot\Std\Exceptions\Configurable\UnknownConfigurationPropertyError;
use Poirot\Std\Exceptions\Configurable\UnknownResourceError;
use Poirot\Std\Interfaces\Pact\ipConfigurable;
use Poirot\Std\Traits\tConfigurable;
use Poirot\Std\Traits\tConfigurableSetter;

/**
 * @see tConfigurable
 * @see aConfigurable
 * @see tConfigurableSetter
 * @see aConfigurableSetter
 */
class ConfigurableTest
    extends TestCase
{
    /**
     * @dataProvider provideConfigurableClasses
     */
    function testConfigurableWithArray(ipConfigurable $configurable)
    {
        $configurable->setConfigs([
            'a' => 1,
            'b' => 2,
        ]);

        $this->assertEquals(1, $configurable->a);
        $this->assertEquals(2, $configurable->b);
        $this->assertEquals(3, $configurable->c);
    }

    /**
     * @dataProvider provideConfigurableClasses
     */
    function testConfigurableWithArrayResourceToParse(ipConfigurable $configurable)
    {
        $configurable->setConfigs($configurable::parse(new \ArrayIterator([
            'a' => 1,
            'b' => 2,
        ])));

        $this->assertEquals(1, $configurable->a);
        $this->assertEquals(2, $configurable->b);
        $this->assertEquals(3, $configurable->c);
    }

    /**
     * @dataProvider provideConfigurableClasses
     */
    function testFailedWithUnavailableOptions(ipConfigurable $configurable)
    {
        $this->expectException(UnknownConfigurationPropertyError::class);

        $options = [
            'a' => 1,
            'b' => 2,
            'c' => 3,
            'd' => 'not_used',
            'e' => 'not_used',
        ];

        $configurable->setConfigs($options);
    }

    /**
     * @dataProvider provideConfigurableClasses
     */
    function testFailedConfigurableWithInvalidOptionValue(ipConfigurable $configurable)
    {
        $this->expectException(ConfigurationPropertyTypeError::class);

        $configurable->setConfigs(['c' => 'invalid_value_as_string']);
    }

    function testConfigurableShouldNotFailedWithInvalidOptionValue()
    {
        $configurable = $this->getConfigurableInstance();
        $configurable->setConfigs(['c' => 'invalid_value_as_string'], [
            ConfigurationError::class
        ]);

        $this->assertEquals(3, $configurable->c);
    }

    function testConfigurableSetterCanSetMultiParamsSetters()
    {
        $configurableClass = new class($this) extends aConfigurableSetter
        {
            /** @var TestCase */
            private $testCase;
            public $isMethodCalled = false;

            function __construct(TestCase $testCase) {
                $this->testCase = $testCase;
            }

            function setMyMethod($paramOne, $paramTwo) {
                $this->testCase->assertEquals('one', $paramOne);
                $this->testCase->assertEquals('two', $paramTwo);

                $this->isMethodCalled = true;
            }
        };


        $configurableClass->isMethodCalled = false;
        $configurableClass->setConfigs([
            'my_method' => [
                'param_one' => 'one',
                'param_two' => 'two',
            ]
        ]);
        $this->assertTrue($configurableClass->isMethodCalled);

        $configurableClass->isMethodCalled = false;
        $configurableClass->setConfigs([
            'my_method' => new Params([
                'param_one' => 'one',
                'param_two' => 'two',
            ])
        ]);
        $this->assertTrue($configurableClass->isMethodCalled);
    }

    function testConfigurableSetterCanSetMultiParamsSettersOnNonePublicMethods()
    {
        $configurableClass = new class($this) extends aConfigurableSetter
        {
            /** @var TestCase */
            private $testCase;
            public $isMethodCalled = false;

            function __construct(TestCase $testCase) {
                $this->testCase = $testCase;
            }

            protected function setMyMethod($paramOne, $paramTwo) {
                $this->testCase->assertEquals('one', $paramOne);
                $this->testCase->assertEquals('two', $paramTwo);

                $this->isMethodCalled = true;
            }
        };

        $configurableClass->isMethodCalled = false;
        $configurableClass->setConfigs([
            'my_method' => [
                'param_one' => 'one',
                'param_two' => 'two',
            ]
        ]);
        $this->assertTrue($configurableClass->isMethodCalled);
    }

    function testConfigurableSetterWillResolveParamToMethods()
    {
        $configurableClass = new class($this) extends aConfigurableSetter
        {
            /** @var TestCase */
            private $testCase;
            public $isMethodCalled = false;

            function __construct(TestCase $testCase) {
                $this->testCase = $testCase;
            }

            protected function setMyMethod($paramOne) {
                $this->testCase->assertEquals('one', $paramOne);
                $this->isMethodCalled = true;
            }
        };

        $configurableClass->isMethodCalled = false;
        $configurableClass->setConfigs([
            'my_method' => new Params([
                'one',
            ])
        ]);
        $this->assertTrue($configurableClass->isMethodCalled);
    }

    /**
     * @dataProvider provideConfigurableClasses
     */
    function testSkipExceptionsWhenUnavailableOptions(ipConfigurable $configurable)
    {
        $options = [
            'a' => 1,
            'b' => 2,
            'c' => 3,
            'd' => 'not_used',
            'e' => 'not_used',
        ];

        $configurable->setConfigs($options, [
            UnknownConfigurationPropertyError::class,
        ]);

        $this->assertTrue(true);
    }

    /**
     * @dataProvider provideConfigurableClasses
     */
    function testFailedConfigurableWithUnknownResourceToParse(ipConfigurable $configurable)
    {
        $this->expectException(UnknownResourceError::class);

        $optionsResource = new \stdClass();
        $optionsResource->a = 1;

        $configurable->setConfigs($configurable::parse($optionsResource));
    }

    // ..

    function provideConfigurableClasses()
    {
        return [
            [ $this->getConfigurableInstance() ],
            [ $this->getConfigurableSetterInstance() ],
        ];
    }

    protected function getConfigurableInstance()
    {
        return new class extends aConfigurable
        {
            public $a;
            public $b;
            public $c = 3; // cant be null

            /**
             * @inheritDoc
             */
            function setConfigs(array $options, array $skipExceptions = [])
            {
                $classVariables = get_object_vars($this);
                foreach ($options as $k => $v) {
                    try {
                        if (! array_key_exists($k, $classVariables) )
                            throw new UnknownConfigurationPropertyError(
                                'Setter Not Found.');

                        if ($k == 'c' && !is_int($v)) {
                            throw new ConfigurationPropertyTypeError(
                                '"C" value must be integer; none integer value is given.');
                        }

                        $this->{$k} = $v;
                    } catch (\Exception $e) {
                        if ($this->_shouldThrowConfigurationException($e, $skipExceptions))
                            throw $e;
                    }
                }
            }
        };
    }

    protected function getConfigurableSetterInstance()
    {
        return new class extends aConfigurableSetter
        {
            protected $a;
            protected $b;
            protected $c = 3; // cant be null

            function setA(?int $val) {
                $this->a = $val;
            }

            function setB(?int $val) {
                $this->b = $val;
            }

            function setC(int $val) {
                $this->c = $val;
            }

            function __get($key) {
                return $this->{$key};
            }
        };
    }
}
