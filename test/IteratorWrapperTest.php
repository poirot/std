<?php
namespace PoirotTest\Std;

use PHPUnit\Framework\TestCase;
use Poirot\Std\IteratorWrapper;


/**
 * @see IteratorWrapper
 */
class IteratorWrapperTest
    extends TestCase
{
    function testGeneratorIsIterator()
    {
        list($generator) = $this->provideSimpleArrayIteration();
        $this->assertInstanceOf(\Iterator::class, $generator);
    }

    function testAccessToFirstKeyVal()
    {
        list($generator, $expected) = $this->provideSimpleArrayIteration();
        $this->assertEquals(current($expected), $generator->current());

        list($generator, $expected) = $this->provideSimpleArrayIteration();
        $this->assertEquals(key($expected), $generator->key());
    }

    function testIterationOverAnArray()
    {
        list($generator, $expected) = $this->provideSimpleArrayIteration();

        foreach ($generator as $i => $v)
            $this->assertEquals($expected[$i], $v);
    }

    function testIterationTwiceOverAnArray()
    {
        list($generator, $expected) = $this->provideSimpleArrayIteration();

        foreach ($generator as $i => $v)
            $this->assertEquals($expected[$i], $v);

        foreach ($generator as $i => $v)
            $this->assertEquals($expected[$i], $v);
    }

    function testWrapIterationOverAnIteratorAggregate()
    {
        list($generator, $expected) = $this->provideIteratorAggregateIteration();

        foreach ($generator as $i => $v)
            $this->assertEquals($expected[$i], $v);

        foreach ($generator as $i => $v)
            $this->assertEquals($expected[$i], $v);
    }

    function testSkipAnItem()
    {
        list($generator, $expected) = $this->provideSkippedItems();

        foreach ($generator as $i => $v)
            $this->assertEquals($expected[$i], $v);
    }

    function testStopIteration()
    {
        list($generator, $expected) = $this->provideStoppedIteration();

        foreach ($generator as $i => $v)
            $this->assertEquals($expected[$i], $v);
    }

    function testStopAndSkipCurrentItemIteration()
    {
        list($generator, $expected) = $this->provideStopAndSkipIteration();

        foreach ($generator as $i => $v)
            $this->assertEquals($expected[$i], $v);
    }

    function testIteratorCallableCanChangeTheIndex()
    {
        list($generator, $expected) = $this->provideChangeableIndexIteration();

        foreach ($generator as $i => $v)
            $this->assertEquals($expected[$i], $v);
    }

    function testCachingIterator()
    {
        $oneTimeTraversable = $this->provideOneTimeTraversable();

        try {
            foreach ($oneTimeTraversable as $_) {
                // Do something ...
            }

            foreach ($oneTimeTraversable as $_) {
                // Continue Execution ...
            }

            $this->fail('Exception expected.');

        } catch (\Throwable $e) {
            $this->assertContains('Cannot traverse an already closed generator', $e->getMessage());
        }

        $iterator = new IteratorWrapper($this->provideOneTimeTraversable(), function ($value) {
            return $value;
        });

        $c = 0;
        foreach ($iterator as $i) {
            $c++;
        }

        foreach ($iterator as $i) {
            $c++;
        }

        // we count whole the item inside iterator * 2 times
        $this->assertEquals(8, $c);
    }

    // ..

    function provideSimpleArrayIteration()
    {
        return [
            new IteratorWrapper([0, 1, 2, 3, 4], function ($value) {
                return $value * 2;
            }),
            [0, 2, 4, 6, 8]
        ];
    }

    function provideIteratorAggregateIteration()
    {
        $myIterator = new class implements \IteratorAggregate
        {
            function getIterator()
            {
                foreach ([0, 1, 2, 3, 4] as $i => $v) {
                    yield $i => $v;
                }
            }
        };

        return [
            new IteratorWrapper($myIterator, function ($value) {
                return $value * 2;
            }),
            [0, 2, 4, 6, 8]
        ];
    }

    function provideSkippedItems()
    {
        return [
            new IteratorWrapper([0, 1, 2, 3, 4], function ($value, $index) {
                if ($index % 2 == 0)
                    /** @var IteratorWrapper $this */
                    $this->skipCurrentIteration();

                return $value;
            }),
            [1 => 1, 3 => 3]
        ];
    }

    function provideStoppedIteration()
    {
        return [
            new IteratorWrapper([0, 1, 2, 3, 4], function ($value, $index) {
                if ($index >= 3)
                    /** @var IteratorWrapper $this */
                    $this->stopAfterThisIteration();

                return $value;
            }),
            [0, 1, 2, 3]
        ];
    }

    function provideStopAndSkipIteration()
    {
        return [
            new IteratorWrapper([0, 1, 2, 3, 4], function ($value, $index) {
                if ($index >= 3)
                    /** @var IteratorWrapper $this */
                    $this
                        ->skipCurrentIteration()
                        ->stopAfterThisIteration();

                return $value;
            }),
            [0, 1, 2]
        ];
    }

    function provideChangeableIndexIteration()
    {
        return [
            new IteratorWrapper([0, 1, 2], function ($value, &$index) {
                $index = chr(97 + $index); // 97 is ascii code char for "a"
                return $value;
            }),
            ['a' => 0, 'b' => 1, 'c' => 2]
        ];
    }

    function provideOneTimeTraversable()
    {
        $oneTimeTraversable = (function() {
            static $isIterated;
            if (null === $isIterated) {
                foreach ([1, 2, 3, 4] as $i => $v)
                    yield $i => $v;
            }

            $isIterated = true;
        })();

        return $oneTimeTraversable;
    }
}
