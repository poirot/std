<?php
namespace PoirotTest\Std;

use PHPUnit\Framework\TestCase;
use Poirot\Std\ArgumentsResolver;
use Poirot\Std\Exceptions\ArgumentResolver\CantResolveParameterDependenciesError;
use PoirotTest\Std\Fixtures\SimpleClassFixture;
use PoirotTest\Std\Fixtures\SimpleClassWithConstructorFixture;
use function Poirot\Std\Invokable\makeReflectFromCallable;


/**
 * @see ArgumentsResolver
 */
class ArgumentsResolverTest
    extends TestCase
{
    /*
     * To Resolve Arguments For a Callable We First Make Reflection Object
     * From Given Callable.
     *
     * Callable May Be In a Various Format Before Achieve Related Reflection.
     */

    function testMakeReflectionMethodFromCallableArray()
    {
        $callable   = ['\DateTime', 'createFromFormat'];
        $reflection = makeReflectFromCallable($callable);

        $this->assertInstanceOf(\ReflectionMethod::class, $reflection);
    }

    function testMakeReflectionMethodFromString()
    {
        $callable   = '\DateTime::createFromFormat';
        $reflection = makeReflectFromCallable($callable);

        $this->assertInstanceOf(\ReflectionMethod::class, $reflection);


        $callable   = 'printf';
        $reflection = makeReflectFromCallable($callable);

        $this->assertInstanceOf(\ReflectionFunction::class, $reflection);
    }

    function testMakeReflectionMethodFromInvokable()
    {
        $reflection = makeReflectFromCallable(function () {
            // closure callable
        });

        $this->assertInstanceOf(\ReflectionFunction::class, $reflection);


        $reflection = makeReflectFromCallable(new class {
            // Invokable Reflection
            function __invoke() { }
        });

        $this->assertInstanceOf(\ReflectionMethod::class, $reflection);
    }

    /*
     * Resolvers Variants
     *
     */

    function testCallableResolver()
    {
        $resolver = new ArgumentsResolver\CallableResolver(function($argument) {
            return $argument;
        });

        $this->assertInstanceOf(\ReflectionFunction::class, $resolver->getReflectionMethod());

        $this->assertInstanceOf(\Closure::class, $resolver->resolve(['arg1']));
        $this->assertEquals('arg1', $resolver->resolve(['arg1'])->__invoke());
    }

    function testInstantiatorResolver()
    {
        $resolver = new ArgumentsResolver\InstantiatorResolver(SimpleClassFixture::class);
        $this->assertInstanceOf(\ReflectionFunction::class, $resolver->getReflectionMethod());
        // SimpleClassFixture has no need any argument on constructor to resolve
        $this->assertInstanceOf(SimpleClassFixture::class, $resolver->resolve([]));

        $resolver = new ArgumentsResolver\InstantiatorResolver(SimpleClassWithConstructorFixture::class);
        $this->assertInstanceOf(\ReflectionMethod::class, $resolver->getReflectionMethod());

        /** @var SimpleClassWithConstructorFixture $simpleClass */
        $simpleClass = $resolver->resolve([]);
        $this->assertEquals(null, $simpleClass->getInternalValue());

        $simpleClass = $resolver->resolve([10]);
        $this->assertEquals(10, $simpleClass->getInternalValue());
    }

    function testReflectionResolver()
    {
        $resolver = new ArgumentsResolver\ReflectionResolver(
            makeReflectFromCallable(function($argument) { return $argument;}));

        $this->assertInstanceOf(\ReflectionFunction::class, $resolver->getReflectionMethod());

        $this->assertInstanceOf(\Closure::class, $resolver->resolve(['arg1']));
        $this->assertEquals('arg1', $resolver->resolve(['arg1'])->__invoke());
    }

    /*
     * Argument Resolver
     *
     */

    /**
     * @depends testMakeReflectionMethodFromInvokable
     */
    function testCanResolverCallableByDifferentResolvers()
    {
        $callable = function(int $argument) {
            return $argument;
        };

        $resolvers = [
            new ArgumentsResolver\CallableResolver($callable),
            new ArgumentsResolver\ReflectionResolver(
                makeReflectFromCallable($callable)
            ),
        ];

        foreach ($resolvers as $resolver) {
            $argumentsResolver = new ArgumentsResolver($resolver);
            $this->assertEquals(
                ['argument' => 10],
                $argumentsResolver
                    ->withOptions(['argument' => 10])
                    ->getResolvedArguments()
            );
        }
    }

    /**
     * @dataProvider provideReflectionMethodToSanitize
     * @depends testMakeReflectionMethodFromInvokable
     *
     * @param ArgumentsResolver\aResolver $resolver
     * @param array $options
     */
    function testSanitizeGivenParamsToArgumentName(ArgumentsResolver\aResolver $resolver, array $options)
    {
        $argumentsResolver = new ArgumentsResolver($resolver);

        $resolvedArgs = $argumentsResolver
            ->withOptions($options)
            ->getResolvedArguments();

        $this->assertIsArray($resolvedArgs);
        $this->assertNotEmpty($resolvedArgs);
    }

    /**
     * @dataProvider providePreparedReflectionCallable
     * @depends      testMakeReflectionMethodFromInvokable
     *
     * @param \ReflectionFunction $reflection
     */
    function testResolveArgumentsFromAssociativeArray(\ReflectionFunction $reflection)
    {
        $argumentsResolver = new ArgumentsResolver(
            new ArgumentsResolver\ReflectionResolver($reflection));

        $resolvedArgs = $argumentsResolver
            ->withOptions([
                'arg2' => 'Value2',
                'argOptional1' => 1234,
                'arg1'   => 'Value1',
            ], false)
            ->getResolvedArguments();

        $this->assertSame([
            'arg1' => 'Value1',
            'arg2' => 'Value2',
            'argOptional1' => 1234]
            , $resolvedArgs
        );
    }

    /**
     * @dataProvider providePreparedReflectionCallable
     * @depends      testMakeReflectionMethodFromInvokable
     *
     * @param \ReflectionFunction $reflection
     */
    function testResolveArgumentsFromArrayList(\ReflectionFunction $reflection)
    {
        $argumentsResolver = new ArgumentsResolver(
            new ArgumentsResolver\ReflectionResolver($reflection));

        $resolvedArgs = $argumentsResolver
            ->withOptions([
                'Value1',
                'Value2',
                1234,
            ], false)
            ->getResolvedArguments();

        $this->assertSame([
            'arg1' => 'Value1',
            'arg2' => 'Value2',
            'argOptional1' => 1234]
            , $resolvedArgs
        );
    }

    /**
     * @dataProvider providePreparedReflectionCallable
     * @depends      testMakeReflectionMethodFromInvokable
     *
     * @param \ReflectionFunction $reflection
     */
    function testResolveArgumentsFromParamsDicList(\ReflectionFunction $reflection)
    {
        $argumentsResolver = new ArgumentsResolver(
            new ArgumentsResolver\ReflectionResolver($reflection));

        $resolvedArgs = $argumentsResolver
            ->withOptions([
                'arg2' => 'Value2',
                'Value1',
                'argOptional1' => 1234,
            ], false)
            ->getResolvedArguments();

        $this->assertSame([
            'arg1' => 'Value1',
            'arg2' => 'Value2',
            'argOptional1' => 1234]
            , $resolvedArgs
        );
    }

    /**
     * @param \ReflectionFunction $reflection
     *
     * @dataProvider providePreparedReflectionCallable
     * @depends      testMakeReflectionMethodFromInvokable
     */
    function testResolvedArgumentsWithDefaultValue(\ReflectionFunction $reflection)
    {
        $argumentsResolver = new ArgumentsResolver(
            new ArgumentsResolver\ReflectionResolver($reflection));

        $resolvedArgs = $argumentsResolver
            ->withOptions([
                'arg1' => 'Value1',
                'arg2' => 'Value2',
            ], false)
            ->getResolvedArguments();

        $this->assertSame([
            'arg1' => 'Value1',
            'arg2' => 'Value2',
            'argOptional1' => null]
            , $resolvedArgs
        );
    }

    /**
     * @param \ReflectionFunction $reflection
     *
     * @dataProvider providePreparedReflectionCallable
     * @depends      testMakeReflectionMethodFromInvokable
     */
    function testFailedResolveArgument(\ReflectionFunction $reflection)
    {
        $argumentsResolver = new ArgumentsResolver(
            new ArgumentsResolver\ReflectionResolver($reflection));


        $this->expectException(CantResolveParameterDependenciesError::class);

        $argumentsResolver
            ->withOptions([
                'arg1' => 'Value1',
            ])
            ->getResolvedArguments();
    }

    /*
     * Resolve Arguments From Given List By Type Hint Match
     */

    /**
     * @dataProvider provideTypeHintedReflectionCallable
     * @depends      testMakeReflectionMethodFromInvokable
     *
     * @param \ReflectionFunction $reflection
     */
    function testResolveArgumentsByTypeHint(\ReflectionFunction $reflection)
    {
        $argumentsResolver = new ArgumentsResolver(
            new ArgumentsResolver\ReflectionResolver($reflection));


        $time = new \DateTime;
        $resolvedArgs = $argumentsResolver
            ->withOptions([
                5,
                $time,
                'Value1',
                'Value2',
            ], true)
            ->getResolvedArguments();

        $this->assertSame(
            [
                'arg1'     => 'Value1',
                'arg2'     => 'Value2',
                'datetime' => $time,
                'int'      => 5
            ]
            , $resolvedArgs
        );
    }

    /**
     * @param \ReflectionFunction $reflection
     *
     * @dataProvider provideTypeHintedReflectionCallable
     * @depends      testMakeReflectionMethodFromInvokable
     */
    function testResolveArgumentsFromTypeHintWithDefault(\ReflectionFunction $reflection)
    {
        $argumentsResolver = new ArgumentsResolver(
            new ArgumentsResolver\ReflectionResolver($reflection));


        $time = new \DateTime;
        $resolvedArgs = $argumentsResolver
            ->withOptions([
                $time,
                'Value1',
                'Value2',
            ], true)
            ->getResolvedArguments();

        $this->assertSame(
            [
                'arg1'     => 'Value1',
                'arg2'     => 'Value2',
                'datetime' => $time,
                'int'      => 1234
            ]
            , $resolvedArgs
        );
    }

    /**
     * @param \ReflectionFunction $reflection
     *
     * @dataProvider provideTypeHintedReflectionCallable
     * @depends      testMakeReflectionMethodFromInvokable
     */
    function testFailedInvalidTypeArgument(\ReflectionFunction $reflection)
    {
        $argumentsResolver = new ArgumentsResolver(
            new ArgumentsResolver\ReflectionResolver($reflection));


        $this->expectException(CantResolveParameterDependenciesError::class);

        $time = 'invalid_datetime_object';
        $argumentsResolver
            ->withOptions([
                $time,
                'Value1',
                'Value2',
            ], true)
            ->getResolvedArguments();
    }

    /**
     * @dataProvider provideTypeHintedReflectionCallable
     * @depends      testMakeReflectionMethodFromInvokable
     *
     * @param \ReflectionFunction $reflection
     */
    function testFailedResolveArguments(\ReflectionFunction $reflection)
    {
        $argumentsResolver = new ArgumentsResolver(
            new ArgumentsResolver\ReflectionResolver($reflection));


        $this->expectException(CantResolveParameterDependenciesError::class);

        $argumentsResolver
            ->withOptions([ 'arg1' => 'Value1' ], true)
            ->getResolvedArguments();
    }

    /*
     * Resolve Arguments For Callable and Then Create a Closure With Resolved Arguments
     * Inside.
     *
     * We can just call the resulted closure instead.
     */

    function testResolveClosureWithResolveArguments()
    {
        $argumentsResolver = new ArgumentsResolver(
            new ArgumentsResolver\CallableResolver(function ($resolveMe) { return $resolveMe; }));

        $resolvedCallable = $argumentsResolver
            ->withOptions([
                'resolve_me' => 'resolved', // resolve by sanitizing name
            ])
            ->resolve();

        $this->assertInstanceOf(\Closure::class, $resolvedCallable);
        $this->assertEquals('resolved', $resolvedCallable());
    }

    function testResolveInstantiatedClass()
    {
        $argumentsResolver = new ArgumentsResolver(
            new ArgumentsResolver\InstantiatorResolver(SimpleClassWithConstructorFixture::class)
        );

        /** @var SimpleClassWithConstructorFixture $resolvedClass */
        $resolvedClass = $argumentsResolver
            ->withOptions([
                'x' => 10,
            ])
            ->resolve();

        $this->assertInstanceOf(SimpleClassWithConstructorFixture::class, $resolvedClass);
        $this->assertEquals(10, $resolvedClass->getInternalValue());
    }

    // Providers:

    function provideReflectionMethodToSanitize()
    {
        $reflectionResolver = new ArgumentsResolver\ReflectionResolver(
            makeReflectFromCallable(function(bool $resolveMe) {}));

        $reflection_resolver = new ArgumentsResolver\ReflectionResolver(
            makeReflectFromCallable(function(bool $resolve_me) {}));

        return [
            [$reflectionResolver, ['resolveMe' => true]],
            [$reflectionResolver, ['resolveme' => true]],
            [$reflectionResolver, ['resolve_me' => true]],

            [$reflectionResolver, ['ResolveMe' => true]],
            [$reflectionResolver, ['ResoLveMe' => true]],

            [$reflection_resolver, ['resolve_me' => true]],
            [$reflection_resolver, ['resolveMe' => true]],
            [$reflection_resolver, ['ResolveMe' => true]],
        ];
    }

    /**
     * Provide Reflection From Callable That Has Some Mandatory Arguments To Resolve
     *
     * @return array
     */
    function providePreparedReflectionCallable()
    {
        $closure    = function ($arg1, $arg2, $argOptional1 = null) { };
        yield [makeReflectFromCallable($closure)];
    }

    /**
     * Provide Reflection From Callable That Has Type Hint Defined Arguments
     *
     * @return array
     */
    function provideTypeHintedReflectionCallable()
    {
        $closure    = function (string $arg1, string $arg2, \DateTime $datetime, int $int = 1234) { };
        yield [makeReflectFromCallable($closure)];
    }
}
