<?php
namespace PoirotTest\Std\TestHydrator;

use PHPUnit\Framework\TestCase;
use Poirot\Std\Exceptions\Configurable\UnknownConfigurationPropertyError;
use Poirot\Std\Exceptions\Configurable\ResourceParseError;
use Poirot\Std\Exceptions\Configurable\UnknownResourceError;
use Poirot\Std\Hydrator\aHydrateEntity;
use Poirot\Std\Hydrator\HydrateGetters;
use Poirot\Std\Hydrator\Parser\FunctorHydrateParser;
use PoirotTest\Std\TestHydrator\Fixtures\HydrateEntityFixture;

/**
 * @see aHydrateEntity
 */
class HydrateEntityTests
    extends TestCase
{
    /**
     * @dataProvider provideDummyData()
     */
    function testConstructInitializing($data)
    {
        $hydrator = new HydrateEntityFixture($data);
        $this->assertEquals($data['fullname'], $hydrator->getFullName());
        $this->assertEquals($data['credential'], $hydrator->getPassword());
        $this->assertEquals($data['email'], $hydrator->getEmail());
        $this->assertEquals($data['user_agent'], $hydrator->getMeta()['userAgent']);
    }

    /**
     * @dataProvider provideDummyData()
     */
    function testBuildMethodInitializing($data)
    {
        $hydrator = new HydrateEntityFixture;
        $hydrator->setConfigs(HydrateEntityFixture::parse($data));

        $this->assertEquals($data['fullname'], $hydrator->getFullName());
        $this->assertEquals($data['credential'], $hydrator->getPassword());
        $this->assertEquals($data['email'], $hydrator->getEmail());
        $this->assertEquals($data['user_agent'], $hydrator->getMeta()['userAgent']);
    }

    /**
     * @dataProvider provideDummyData()
     */
    function testHydratorIsIterable($data)
    {
        $hydrator = new HydrateEntityFixture($data);
        $this->assertEquals([
            'fullname' => 'John Doe',
            'password' => '******',
            'email' => 'email@mail.com',
            'meta' => [
                'userAgent' => 'Chrome',
            ],
        ], iterator_to_array($hydrator));
    }

    function testHydratorIterableWillExcludeNullValuesFromProperties()
    {
        $hydrator = new HydrateEntityFixture([
            'fullname' => 'John Doe',
        ]);

        $this->assertEquals([
            'fullname' => 'John Doe',
        ], iterator_to_array($hydrator));
    }
    
    /**
     * @depends testHydratorIterableWillExcludeNullValuesFromProperties
     */
    function testConstructWillNotFailedByUnknownProperty()
    {
        $hydrator = new HydrateEntityFixture([
            'fullname' => 'John Doe',
            'unknown_property' => 'value',
        ]);

        $this->assertEquals([
            'fullname' => 'John Doe',
        ], iterator_to_array($hydrator));
    }

    /**
     * @depends testHydratorIterableWillExcludeNullValuesFromProperties
     */
    function testBuildWillNotFailedByUnknownProperty()
    {
        $hydrator = new HydrateEntityFixture;
        $hydrator->setConfigs([
            'fullname' => 'John Doe',
            'unknown_property' => 'value',
        ]);

        $this->assertEquals([
            'fullname' => 'John Doe',
        ], iterator_to_array($hydrator));
    }

    function testConfigurableSkippedExceptionsCanBeOverride()
    {
        try {
            new HydrateEntityFixture([
                'fullname' => 'John Doe',
                'unknown_property' => 'value',
            ], null, []);

            $this->fail('Exception Expected.');
        } catch (\Exception $e) {
            $this->assertInstanceOf(UnknownConfigurationPropertyError::class, $e);
        }

        try {
            $hydrator = new HydrateEntityFixture;
            $hydrator->setConfigs([
                'fullname' => 'John Doe',
                'unknown_property' => 'value',
            ], []);

            $this->fail('Exception Expected.');
        } catch (\Exception $e) {
            $this->assertInstanceOf(UnknownConfigurationPropertyError::class, $e);
        }
    }

    function testHydratorDefaultValues()
    {
        $reqBody = ['fullname' => 'John Doe', 'credential' => '******'];
        $hydrator = new HydrateEntityFixture($reqBody, $this->getPersistedEntityData());

        // changed values
        $this->assertEquals('John Doe', $hydrator->getFullName());
        $this->assertEquals('******', $hydrator->getPassword());
        // from default values
        $this->assertEquals('my@mail.com', $hydrator->getEmail());
        $this->assertEquals(null, $hydrator->getMeta());

        $this->assertEquals([
            // changed values
            'fullname' => 'John Doe',
            'password' => '******',
            // from default values
            'email' => 'my@mail.com',
        ], iterator_to_array($hydrator));
    }

    function testRegisterHydratorParser()
    {
        $jsonParser = $this->getSimpleParser();

        aHydrateEntity::clearAllParsers();
        aHydrateEntity::registerParser($jsonParser);

        $registeredParsers = aHydrateEntity::listParsers();
        $this->assertIsArray($registeredParsers);
        $this->assertSame($registeredParsers[0], $jsonParser);

        aHydrateEntity::clearAllParsers();
        $this->assertEmpty(aHydrateEntity::listParsers());
    }

    /**
     * @dataProvider provideDummyData()
     */
    function testRegisteredParserCanParseResource($data)
    {
        HydrateEntityFixture::clearAllParsers();
        HydrateEntityFixture::registerParser($this->getSimpleParser());

        $hydrator = new HydrateEntityFixture(
            serialize($data)
        );

        $this->assertEquals($data['fullname'], $hydrator->getFullName());
        $this->assertEquals($data['credential'], $hydrator->getPassword());
        $this->assertEquals($data['email'], $hydrator->getEmail());
        $this->assertEquals($data['user_agent'], $hydrator->getMeta()['userAgent']);
    }

    function testFailedParseResource()
    {
        $this->expectException(UnknownResourceError::class);

        new HydrateEntityFixture('unknown resource');
    }

    function testFailedOnCustomParseResource()
    {
        $this->expectException(ResourceParseError::class);

        HydrateEntityFixture::registerParser(new FunctorHydrateParser(function() {
            throw new \Exception('Parse Error Happen.');
        }));

        new HydrateEntityFixture('unknown resource');
    }

    // ..

    function getPersistedEntityData()
    {
        yield 'fullname' => 'John Smith';
        yield 'credential' => '123456';
        yield 'email' => 'my@mail.com';
    }

    function provideDummyData()
    {
        yield [
            [
                'fullname' => 'John Doe',
                'credential' => '******',
                'email' => 'email@mail.com',
                'user_agent' => 'Chrome',
            ]
        ];
    }

    function getSimpleParser()
    {
        return new FunctorHydrateParser(function($resource) {
            $unserialized = unserialize($resource);
            if (is_array($unserialized))
                return $unserialized;

            return null;
        });
    }
}
