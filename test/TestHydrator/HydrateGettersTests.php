<?php
namespace PoirotTest\Std\TestHydrator;

use PHPUnit\Framework\TestCase;
use Poirot\Std\Hydrator\HydrateGetters;
use PoirotTest\Std\TestHydrator\Fixtures\HydrateGettersFixture;

/**
 * @see HydrateGetters
 */
class HydrateGettersTests
    extends TestCase
{
    function testHydrateGetterIsIterable()
    {
        $hydGetter = new HydrateGetters(new HydrateGettersFixture);
        $this->assertIsIterable($hydGetter);
    }

    function testIterableProperties()
    {
        $hydGetter = new HydrateGetters(new HydrateGettersFixture);
        $this->assertEquals([
            'classname' => HydrateGettersFixture::class,
            'snake_case_property' => 'snake_case_property',
            'nullable' => null,
        ], iterator_to_array($hydGetter));
    }

    function testCanExcludeMethodWithNotations()
    {
        $hydGetter = new HydrateGetters(new HydrateGettersFixture);
        $this->assertArrayNotHasKey('ignored_method', iterator_to_array($hydGetter));
    }

    function testCanExcludeIndividualMethods()
    {
        $hydGetter = new HydrateGetters(new HydrateGettersFixture);
        $hydGetter->excludePropertyMethod('getNullable');

        $this->assertEquals([
            'classname' => HydrateGettersFixture::class,
            'snake_case_property' => 'snake_case_property',
        ], iterator_to_array($hydGetter));

        // through constructor

        $hydGetter = new HydrateGetters(new HydrateGettersFixture, [
            HydrateGetters::ExcludeIgnoreMethods => [
                'getNullable',
            ],
        ]);

        $this->assertEquals([
            'classname' => HydrateGettersFixture::class,
            'snake_case_property' => 'snake_case_property',
        ], iterator_to_array($hydGetter));
    }

    function testCanRefuseExcludedIndividualMethods()
    {
        try {
            $hydGetter = new HydrateGetters(new HydrateGettersFixture);
            $hydGetter->refuseExcludedPropertyMethod('getIgnoredMethod');
            iterator_to_array($hydGetter);

            $this->fail('Expect Exception.');
        } catch (\LogicException $e) {
            $this->assertInstanceOf(\LogicException::class, $e);
        }

        // through constructor

        try {
            $hydGetter = new HydrateGetters(new HydrateGettersFixture, [
                HydrateGetters::IncludeIgnoredMethods => [
                    'getIgnoredMethod',
                ],
            ]);
            iterator_to_array($hydGetter);

            $this->fail('Expect Exception.');
        } catch (\LogicException $e) {
            $this->assertInstanceOf(\LogicException::class, $e);
        }
    }
}
