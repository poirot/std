<?php
namespace PoirotTest\Std\TestHydrator\Fixtures;

use Poirot\Std\Hydrator\aHydrateEntity;

class HydrateEntityFixture
    extends aHydrateEntity
{
    protected $fullname;
    protected $credential;
    protected $email;
    protected $meta = [];

    // Hydrate Setter

    function setFullname($fullname)
    {
        $this->fullname = trim( (string) $fullname );
    }

    function setCredential($credential)
    {
        $this->credential = (string) $credential;
    }

    function setEmail($email)
    {
        $this->email = (string) $email;
    }

    function setMeta($meta)
    {
        $this->meta = $meta;
    }

    function setUserAgent($userAgent)
    {
        $this->addMeta('userAgent', (string) $userAgent);
    }

    function addMeta($key, $val)
    {
        $this->meta[$key] = $val;
    }

    // Hydrate Getters

    function getFullname()
    {
        return $this->fullname;
    }

    function getPassword()
    {
        return $this->credential;
    }

    function getEmail()
    {
        return $this->email;
    }

    function getMeta()
    {
        return !empty($this->meta) ? $this->meta : null;
    }
}
