<?php
namespace PoirotTest\Std\TestHydrator\Fixtures;

/**
 * @excludeByNotation enabled to ignore some getter methods
 */
class HydrateGettersFixture
{
    function getClassname()
    {
        return static::class;
    }

    function getSnakeCaseProperty()
    {
        return 'snake_case_property';
    }

    function getNullable()
    {
        return null;
    }

    /**
     * @ignore considered as property method
     */
    function getIgnoredMethod()
    {
        throw new \LogicException('Method is ignored by annotations.');
    }
}
