# Error Handler

It's a wrapper around default php [set_error_handler](https://www.php.net/manual/en/function.set-error-handler.php) and [set_exception_handler](https://www.php.net/manual/en/function.set-exception-handler.php) functions. it can be used to create a custom error handler that lets you control how PHP behave when some runtime errors happens. With different error types custom error handling can handle only errors were expecting and take the appropriate action based on that.

## Usage sample

```php
ErrorHandler::handle();
    
10/0; // Warning
echo $foo; // Notice
trigger_error('User error', E_USER_WARNING);
@trigger_error('User error', E_USER_WARNING); // ignored by @ error control operator

if ($errors = ErrorHandler::handleDone()) {
    foreach ($errors as $err) {
        echo get_class($err) . ' - ' . $err->getMessage() . PHP_EOL;
    }
}

/*
ErrorException - User error
ErrorException - Undefined variable: foo
ErrorException - Division by zero
*/
```

## Custom error handler

`ErrorHandler::handle` takes the callable as it's second parameter, and it servers to notify PHP that if there are any php runtime errors. Callable expected to get `\ErrorException` object as all errors will converted to by error handler.

```php
ErrorHandler::handle(E_ALL, function (\ErrorException $e) {
    print sprintf(
        "Encountered error %s in %s, line %s: %s\n",
        $e->getSeverity(), $e->getFile(), $e->getLine(), $e->getMessage()
    );
    
    // if you wish let error pop up and handle with
    // php default error handler.
    #return false;
});

10/0;
// Encountered error 2 in index.php, line 23: Division by zero
echo $foo; // Notice Error
// Encountered error 8 in index.php, line 24: Undefined variable: foo

ErrorHandler::handleDone();
```

## Error level handling

First argument of ErrorHandler lets you choose what errors should handled, and it works like the error_reporting directive in php.ini. However, it's important to remember that you can only have one active error handler at any time, not one for each level of error.

```php
ErrorHandler::handle(E_NOTICE, function ($e) {
    echo get_class($e) . ' - ' . $e->getMessage() . PHP_EOL;
});

    // Indentation added for more readability 
    ErrorHandler::handle(E_WARNING | E_ERROR, function ($e) {
        print "This will not triggered.";
    });
    echo $foo; # Notice Error triggered and show to user by php default
    ErrorHandler::handleDone();


    echo $_SERVER[1];
    # ErrorException - Undefined index: NOT_EXISTS 
    
// ...
```
   
By default error handler will not catch errors that is silenced by error_reporting level otherwise we use a specific bit control flag `ErrorHandler::E_ScreamAll`

```php
error_reporting(E_ALL & ~E_WARNING);
    
ErrorHandler::handle(E_ALL, function ($e) {
   print "This will not triggered.";
});

10/0; // Warning error will stay silence because of error_reporting level

ErrorHandler::handle(E_ALL|ErrorHandler::E_ScreamAll, function ($e) {
   print "This will triggered.";
});

10/0;
// This will triggered.
```

There is a special bit mask flag `ErrorHandler::E_ScreamAll` that can be added to error level handling to catch all errors if accrued regardless of what php error reporting level is.

```php
error_reporting(E_ALL & ~E_WARNING);

ErrorHandler::handle(E_ALL | ErrorHandler::E_ScreamAll, function ($e) {
    echo get_class($e) . ' - ' . $e->getMessage() . PHP_EOL;
});

10/0;
// ErrorException - Division by zero
```

## Handle Exceptions

As exceptions terminate the execution flow there is no much control over Exception handling the only thing is triggering callable registered in chain.

ErrorHandler::handle(ErrorHandler::E_HandleAll, function(\Throwable $e) {
    // This will triggered second. 
    error_log($e->getMessage());
});

```php
ErrorHandler::handle(ErrorHandler::E_HandleAll, function(\Throwable $e) {
    // This will triggered first.
    echo get_class($e) . ' - ' . $e->getMessage() . PHP_EOL;
    // Throw an error exception that is
    // caught by error handler to catch by other handlers
    throw $e;
});

throw new \RuntimeException('error happen.');
// RuntimeException - error happen.
// execution terminated.
```


