# Arguments Resolver

‌It helps to automatically call a callable (function, method or closure) or create an instance of a class with providing necessary arguments from list of available options.

Resolver can resolve arguments by argument name, or type hint provided to the argument. Argument name has the most priority, if name is not match within provided options then try to match based on the type hint of argument by searching from the first element to last options and find the match.

## Usage sample

```php
function foo(array $array, stdClass $object, callable $callable, array $secondArray) {
    // doing something with arguments ... 
}

$ar = new ArgumentsResolver(new CallableResolver('foo'));
$args = $ar->withOptions([
    function () {},
    ['second' => ''],
    new stdClass(),
    'array' => [],
])->getResolvedArguments();

/*
[
  'array' => [],
  'object' => object(stdClass)
  'callable' => object(Closure)
  'secondArray' => ['second' => '']
]
*/
```

## Class Instantiator

Create an instance of class and resolve the arguments needed to constructing object by available given options.

```php
class SimpleClass
{
    public $internalValue;

    function __construct($value)
    {
        $this->internalValue = $x;
    }
}

// Resolving class construct arguments from available options
// 
$ar = new ArgumentsResolver(new InstantiatorResolver(
    SimpleClass::class
));

$simple = $ar->withOptions(['value' => 5, 'not_used' => 'any'])
    ->resolve()
    
echo $simple->internalValue; // 5
```

or simply use the global function available to create instance of class:

```php
use function Poirot\Std\Invokable\resolveInstantClass;
    
$simple = resolveInstantClass(SimpleClass::class, ['value' => 5]);
echo $simple->internalValue; // 5
```

## ‌Callable Resolver

Allows to determine the arguments to pass to a function or method and call it. The input to the resolver can be any [callable](http://php.net/manual/en/language.types.callable.php).

```php
new CallableResolver([new MyClass(), 'myMethod']);
new CallableResolver(['MyClass', 'myStaticMethod']);
new CallableResolver('MyClass::myStaticMethod');
new CallableResolver(new MyInvokableClass());
new CallableResolver(function ($foo) {});
new CallableResolver('MyNamespace\my_function');
```

**Get resolved arguments to a callable** as an array, which can be used to call the callable:

```php
function foo($foo, $bar = 'baz') {
    return $foo . $bar;
}

// Resolve Arguments
//
$ar = new ArgumentsResolver(
    new ArgumentsResolver\CallableResolver('foo')
);

$arguments = $ar->withOptions(['foo' => 'foo'])
   ->getResolvedArguments(); // Array ( [foo] => foo [bar] => baz )
   
call_user_func_array('foo', $arguments);
```

**Call prepared resolved callable dynamically** by list of available arguments:
this will resolve the arguments list needed to execute callable and wrap it to the \Closure that can be invoked directly.

The codes below is equivalent as the code block that you can find above. 

```php
function foo($foo, $bar = 'baz') {
    return $foo . $bar;
}

// Resolve Arguments
//
$ar = new ArgumentsResolver(
    new ArgumentsResolver\CallableResolver('foo')
);

$ar->withOptions(['foo' => 'foo'])
   ->resolve()->__invoke();
```

or simply use the available global function helper:

```php
resolveCallable('foo', ['foo' => 'foo'])();
```

## Reflection Resolver

```php
$ar = new ArgumentsResolver(new ArgumentsResolver\ReflectionResolver(
    new \ReflectionFunction(function ($foo, $bar = 'baz') {
        return $foo . $bar;
    })
));

$ar->withOptions(['foo' => 'foo'])
   ->resolve()();
```

There is also an utility class which helps in creating a reflection instance: The input can be any [callable](http://php.net/manual/en/language.types.callable.php).

```php
$reflection = \Poirot\Std\Invokable\makeReflectFromCallable($callable);
```

