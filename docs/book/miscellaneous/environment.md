# Environment Provisioning

To ease apply different PHP runtime configurations at once the `iEnvironmentContext` interface is defined which can hold various possible runtime configuration values. This contexts can be considered as different environments with different setup for a specific purpose. such as `Development`, `Test` or `Production` setup.

## Usage sample

```php
EnvRegistry::apply(EnvRegistry::Development);
echo EnvRegistry::currentEnvironment(); // development
```

## Available configurations

| Attribute | Value | Equivalent PHP Command |
|-----------|--------|-----------------------|
| **define_const** | array ['const_name' => 'value'] | `define((string) $const, $value);` |
| **display_errors** | (int) 0, 1 | `ini_set('display_errors', $value);` |
| **display_startup_errors** | (int) 0, 1 | `ini_set('display_startup_errors', $value);` |
| **env_global** | array ['env_name' => 'value'] | `$_ENV[$name] = $value //simplified` |
| **error_reporting** | (string) 'E_ALL' or (int) E_ALL & ~E_NOTICE [predefined constants](https://www.php.net/manual/en/errorfunc.constants.php) | `error_reporting(E_ALL)` |
| **html_errors** | (int) 0, 1 | `ini_set('html_errors', $value)` |
| **time_zone** | (string) [List of Supported Timezones](https://www.php.net/manual/en/timezones.php) | `date_default_timezone_set('UCT')` |
| **max_execution_time** | (int) 30 | `set_time_limit($seconds)` |


## PreDefined Contexts

**Development**, Will enable all error reporting level to show to end-user.

```php
class DevelopmentContext
    extends aEnvironmentContext
{
    protected $displayErrors  = 1;
    /** PHP 5.3 or later, the default value is E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED */
    protected $errorReporting = E_ALL;
    protected $displayStartupErrors = 1;
}
```

**Production**, to mitigate all error messages to display to end user.

```php
class ProductionContext
    extends aEnvironmentContext
{
    protected $displayErrors  = 0;
    protected $errorReporting = 0;
    protected $displayStartupErrors = 0;
}
```

**PhpServer**, will give the current values from php server configuration.

```php
class PhpServerContext
    extends aEnvironmentContext
{
    function getDisplayErrors()
    {
        if($this->errorReporting === null)
            $this->setDisplayErrors( (int) ini_get('display_errors'));

        return $this->displayErrors;
    }

    // Doing the same for all attributes
    // ...
}
```

## Environment Context Registry

Environment context values can be override while registry provision configurations.

```php
#file: .env.development.php
return [
   'max_executuin_time' => 0,
   'env_global' => [
      'PDO_USER' => 'root',
      'PDO_PASS' => 'secret',
      'PDO_SERVER' => 'mariadb',
    ],
];
``` 
```php
#file: index.php
$envName = $_SERVER['APP_ENV'];
$envFile = __DIR__ . '/.env.' . $envName . '.php';
EnvRegistry::apply($envName, file_exists($envFile) ? include $envFile : []);
```

after applying environment context it's accessible from registry class anywhere.

```php
printf('Environment %s were applied and active.',
  (string) EnvRegistry::getCurrentEnvironment()
);

printf('Pdo User is:%s and executuion time limited to %d.',
  EnvRegistry::currentEnvironment()->getContext()->getEnvGlobal()['PDO_USER'],
  EnvRegistry::currentEnvironment()->getContext()->getMaxExecutionTime()
);
```


## Custom environment context

```php
if (! EnvRegistry::getContextNameByAlias('local')) {
    EnvRegistry::setAliases(EnvRegistry::Development, 'local', 'localhost');
}

EnvRegistry::apply('localhost');
echo EnvRegistry::currentEnvironment(); // development
```

Custom context implementation:

```php
if ($_SERVER['APP_DEBUG']) {
    EnvRegistry::register(MyCustomDebugModeContext::class, 'debug_mode');
    EnvRegistry::apply('debug_mode');
}
```
