# IteratorWrapper

This iterator wrapper allows the conversion of anything that is [Traversable](https://www.php.net/manual/en/class.traversable.php) into an Iterator. This class provide a wrapper around any php Traversable and adding extra functionalities to that.

```php
$iterator = new IteratorWrapper([0, 1, 2, 3, 4], function ($value) {
   return $value;
});

while ($iterator->valid()) {
   $key = $iterator->key();
   $value = $iterator->current();
    
   $iterator->next();
}
```

## Manipulating key and values

On iterator callback the returned value will considered as a current value of iterator item:

```php
// Multiply all values by 2 in given array set 
$iterator = new IteratorWrapper([0, 1, 2], function ($value) {
   return $value * 2;
});
```

we also can change the key if we need to do this:

```php
$iterator = new IteratorWrapper([0, 1, 2], function ($value, &$index) {
   $index = chr(97 + $index); // 97 is ascii code char for "a"
   return $value * 2;
});

$result = iterator_to_array($iterator); // ['a' => 0, 'b' => 1, 'c' => 2]
```

## Filter items

With iterator wrapper we can skip unwanted items from the iterator with provide Closure control how we iterate over object.

```php
$iterator = new IteratorWrapper([0, 1, 2, 3, 4], function ($value, $index) {
    if ($index % 2 == 0)
        /** @var IteratorWrapper $this */
        $this->skipCurrentIteration();

    return $value;
});

$result = iterator_to_array($iterator); // [1 => 1, 3 => 3]
```

## Cutout items

Sometimes it's needed to stop iterating over items when specific conditions met before we get to the end of iterator items. like when we have limit on database result items.

```php
$iterator = new IteratorWrapper([0, 1, 2, 3, 4], function ($value, $index) {
    if ($index >= 3)
        /** @var IteratorWrapper $this */
        $this->stopAfterThisIteration();

    return $value;
});

$result = iterator_to_array($iterator); // [0, 1, 2, 3]
```

```php
$iterator = new IteratorWrapper([0, 1, 2, 3, 4], function ($value, $index) {
    if ($index >= 2) {
        /** @var IteratorWrapper $this */
        $this->skipCurrentIteration()
        $this->stopAfterThisIteration();
    }
    
    return $value;
});

$result = iterator_to_array($iterator); // [0, 1, 2]
```

## Caching Iterator

As we have built-in support for [CachingIterator](https://www.php.net/manual/en/class.cachingiterator.php) but IteratorWrapper itself comes with the internal caching support to the iteration. Some times you have an iterator which is not iterable multiple times for instance it could be the case when you fetch result from databases like MangoDB which you cant rewind the iteration.

here is the example can illustrating this better:

```php
// One time traversable problem
$oneTimeTraversable = (function() {
    static $isIterated;
    if (null === $isIterated) {
        foreach ([1, 2, 3, 4] as $i => $v)
            yield $i => $v;
    }

    $isIterated = true;
})();

foreach ($oneTimeTraversable as $i) {
    // Do something ...
}

// Will throw:
// Exception: Cannot traverse an already closed generator
foreach ($oneTimeTraversable as $i) {
    // Do Something ...
}
```

```php
$oneTimeTraversable = (function() {
    static $isIterated;
    if (null === $isIterated) {
        foreach ([1, 2, 3, 4] as $i => $v)
            yield $i => $v;
    }

    $isIterated = true;
})();

$wrapIterator = new IteratorWrapper($oneTimeTraversable, function ($value) {
    return $value;
});

foreach ($wrapIterator as $i) {
    // Do something ...
}

foreach ($wrapIterator as $i) {
    // Continue Execution ...
}

// Reach here without throwing Exception
```
