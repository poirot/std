# MutexLock

**The Mutex allows mutual execution of concurrent processes in order to prevent "race conditions".** This is achieved by using a "lock" mechanism. Each possibly concurrent thread cooperates by acquiring a lock before accessing the corresponding data.
‌
## Usage sample

```php
if ($mutexLock->acquire()) {
    // resource is free and lock acquired successfully.
    // business logic execution
} else {
    // when resource is allready locked, blocked!
}
```

How to instantiate lock for specific resource:

```php
$realm = 'your_lock_realm';
$lockDir = sys_get_temp_dir();
$mutexLock = new MutexLock($realm)
   ->giveLockDirPath($lockDir);
```

here we created an instantiate of lock object which is try to make lock file on given `$lockDir` path, the `$realm` is an indicator and unique name to our lock resource. `MutexLock::giveLockDirPath` is an Immutable method so when we gave the path we couldn't change the value later on.

## Object can acquire lock multiple times

When object is already acquire lock on resource all further tries to lock from same object will be success:

```php
if ($mutexLock->acquire()) {
    // do something ...
    if ($mutexLock->acquire()) {
       // somewhere else on code base it will acquired if 
       // we try to get lock on already locked resource.
    }
    
    // we done just realease the lock
   $mutexLock->release();
}
```

## Different lock object instances can't acquire lock on same resource

If we create different lock objects with same setup lock will not acquired on second calls if resource is already got locked.

```php
$mutexLock = new MutexLock('your_lock_realm')
   ->giveLockDirPath($lockDir);

$otherMutexLock = new MutexLock('your_lock_realm')
   ->giveLockDirPath($lockDir);

if ($mutexLock->acquire()) {
   if ($otherMutexLock->acquire()) {
      throw new \Exception('It will not reach here.');
   }
}

// rest of execution
```

## Releasing lock, cleanup and destructing object

As the crash could happen to PHP while we acquired lock the resource might kept as locked as execution didn't reach the `__destruct` method of lock object. we always allow MutexLock to release on resources. We need a system that cleans the garbage in case of crash, just like PHP does for everything else. [register_shutdown_function](http://www.php.net/register_shutdown_function)() could cover the cases of [exit](http://www.php.net/exit)(), die() and other exceptions in the code code, but it wouldn’t be enough for a crash or an interruption.

```php
// Lock File
$mutexLock = new MutexLock('your_lock_realm')
   ->giveLockDirPath($lockDir);

while($mutexLock->acquire()) {
  // php died while doing concurrent process incidintally.
  // ...
  
  // job is done.
  break; 
}

// by destructing object the lock will release.
unset($mutexLock);
```

```php
// Cleanup garabge lock file(s)
$otherMutexLock = new MutexLock('your_lock_realm')
   ->giveLockDirPath($lockDir);

if ($otherMutexLock->isLocked()) {
   // Note:
   // We cant use unset() or __destruct to release the lock.
   // just the release() method should be called directly if the same
   // object didnt create lock on resource.
   $otherMutexLock->release();
}
```

## Expiration TTL on lock

The TTL expiration on seconds amount of time can be defined to keep lock on resource.

```php
$mutexLock->acquire(3);
while ($mutexLock->isLocked()) {
   // deny other concurences to access script for 3 seconds.
   
   // do something ...
   sleep(1);
}

// continue executuin of script
```
