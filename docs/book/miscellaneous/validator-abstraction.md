# Validator abstraction

Validation is a very common task while we dealing with Data, Data can come from forms submitted by users or data before it is written into a database or passed to other web service.

Any object can implement Validator abstraction interface and trait which makes validation task transparent by providing an abstraction layer to generalize the validation. any Validation libraries can still be use out of the box to ease validation task or just simple php code block.

## Usage example

The only task of objects implementing validation abstract is to add `ValidationError` object as error(s) which found during validation.

```php
class Author
    implements ipValidator
{
    use tValidator;

    public $name;

    protected function doAssertValidate(&$exceptions)
    {
        if (empty($this->name)) {
            $exceptions[] = $this->createError(
                'Author name is required.',
                'required',
                'name',
                $this->name,
            );
        } elseif (strlen($this->name) > 70) {
            $exceptions[] = $this->createError(
                'Name length exceed maximum allowed characters.',
                'length',
                'name',
                $this->name
            );
        }
    }
}


$author = new Author;
$author->name = '';

try {
    $author->assertValidate();
} catch (ValidationError $e) {
    echo json_encode([
        'errors' => $e->asErrorsArray()
    ]);
}

/*
{
   "errors":{
      "name":{
         "type":"required",
         "message":"Author name is required.",
         "value":""
      }
   }
}
*/
```

## Error chain

Each error caught by validator will chained together as an exception which is traversable by getting the previous exception chain.

```php
class SimpleObject
    implements ipValidator
{
    use tValidator;

    function doAssertValidate(&$exceptions) {
        $exceptions[] = $this->createError('Username is already reserved.');
        $exceptions[] = $this->createError('Password should me more than 8 characters.');
        $exceptions[] = $this->createError('Registration Failed.');
    }
};


$simpleObject = new SimpleObject;

try {
    $simpleObject->assertValidate();
} catch (ValidationError $e) {
    do {
        print $e->getMessage() . PHP_EOL;
    } while($e = $e->getPrevious());
}

/*
Registration Failed.
Password should me more than 8 characters.
Username is already reserved.
*/
```

## Error message processor

Validation error messages are usually shows to users directly, depends of different scenarios we might wanted to show different messages to end user like translating error messages.

There is some samples of error messages processors which give you the whole idea of how to use them.

Default registered message processor will replace `%param%` and `%value%` with `ValidatorError` into error message.

```php
class SimpleObject
    implements ipValidator
{
    use tValidator;

    function doAssertValidate(&$exceptions) {
        $exceptions[] = $this->createError(
            'Parameter (%param%) has wrong value (%value%) as input.',
            ValidationError::NotInRangeError,
            'namedParam',
            'invalid-value');
    }
};


$simpleObject = new SimpleObject;

try {
    $simpleObject->assertValidate();
} catch (ValidationError $e) {
    print $e->getMessage();
}

// Parameter (namedParam) has wrong value ('invalid-value') as input.
```

Any custom message processor can be attached statically to the `ValidationError` class.

```php
// Register this to truncate error message to specified size with minimum priority
ValidationError::addMessageProcessor(new  TruncateLength(50),  PHP_INT_MIN);
```
‌