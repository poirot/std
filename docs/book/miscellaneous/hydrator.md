# Hydrator

Hydrator provide a mechanisms both for mostly extracting data sets from objects, as well as manipulating objects data. A simple example of when hydrators came handy is when user send form data from website these data can be in different naming as we expect in domain logic and probably doing some filter over data as they are not trusted usually.

## Entity Hydrator

Entity Hydrator abstract basically separate manipulation and extraction to two different job, with defined setter methods hydrator object can be manipulated with given data set then based on the given data we have getter methods which is expected to filter and extract prepared data for next stage to receiver of data.

```php
class ProfileHydrator
    extends aHydrateEntity
{
    protected $fullname;
    protected $email;

    // Hydrate Setter

    function setFullname($fullname)
    {
        $this->fullname = trim( (string) $fullname );
    }

    function setEmail($email)
    {
        $this->email = (string) $email;
    }

    // Hydrate Getters

    function getFullname()
    {
        return $this->fullname;
    }

    function getEmailAddress()
    {
        return $this->email;
    }
}

$hydrator = new ProfileHydrator(ProfileHydrator::parse($_POST));
echo $hydrator->getFullname();
print_r(iterator_to_array($hydrator));
```

‌**Register global input data parser:** with data parsers it's possible to parse different data types to an iterator to feed into entity hydrators to related setter method.

```php
HydrateEntityFixture::registerParser(new FunctorHydrateParser(
  function($resource) {
    $unserialized = unserialize($resource);
    if (is_array($unserialized))
       return $unserialized;
}));

$hydrator = new HydrateEntityFixture(
   serialize(['fullname' => 'Full Name'])
);
```

## Getters hydrator

extract data from an object with getter methods, generally getter hydrator will make an Reflection object of class find getter methods and iterate them by calling them method and return the sanitaized property name associated with the value returned from method call.

```php
class SimpleGetterClass
{
    function getClassname()
    {
        return static::class;
    }

    function getSnakeCaseProperty()
    {
        return 'snake_case_property';
    }

    function getNullable()
    {
        return null;
    }
}

$hydGetter = new HydrateGetters(new SimpleGetterClass);
print_r(iterator_to_array($hydGetter));
/*
[
    [classname] => SimpleGetterClass
    [snake_case_property] => snake_case_property
    [nullable] => 
]
 */
```

Some property methods can be excluded to not considered as data:

```php
$hydGetter = new HydrateGetters(new SimpleGetterClass);
$hydGetter->excludePropertyMethod('getClassname', 'getNullable');

print_r(iterator_to_array($hydGetter));
/*
[
    [snake_case_property] => snake_case_property
]
*/
```

Excluding properties can be defined as well internally into class by notations:

```php
/**
 * @excludeByNotation enabled
 */
class SimpleGetterClass
{
    /**
     * @ignore considered as property method
     */
    function getClassname()
    {
        return static::class;
    }

    function getSnakeCaseProperty()
    {
        return 'snake_case_property';
    }

    /**
     * @ignore considered as property method
     */
    function getNullable()
    {
        return null;
    }
}

print_r(iterator_to_array($hydGetter));
/*
[
    [snake_case_property] => snake_case_property
]
*/
```
