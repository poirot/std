# ResponderChain

The `ResponderChain` allows to run multiple callable one after other and pass the result from each callable to others to attain to the final result.

## Default Chaining Result Behaviour

### Merging values

By default if value returned from callable is `array` or `StdArray` object will get merged to previous result regardless if the previous result is `array`, `StdArray` or not.

```php
$result = ResponderChain::new()
    ->then(function () {
        return 'first';
    })
    ->then(function () {
        return StdArray::of(['second' => 2]);
    })
    ->then(function () {
        return ['third' => 3];
    })
    ->handle();

/*
[
    'first',
    'second' => 2,
    'third' => 3,
]
*/
```

### Replacing values

Any value other than `array` or `StdArray` returned by callable will replace the result from previous callable regardless if the previous result is `array`or not.

```php
$result = ResponderChain::new()
    ->then(function () {
        return 'first';
    })->then(function() {
        return 'second';
    })->then(function() {
        return 'third';
    })->handle();
    
// 'third'
```

Another example to demonstrate merge and replace together:

```php
$result = ResponderChain::new()
    ->then(function () {
        return ['first' => 1];
    })
    ->then(function () {
        return 'second will replaced first';
    })
    ->then(function () {
        return ['third' => 3]; // get merged, because it's array
    })
    ->handle();

/*
[
    'second will replaced first',
    'third' => 3,
]
*/
```

## Key Value Pair Result

The `KeyValueResult` object can be returned by the callable to indicate that value is an key, value pair result which a key holding the value associated with that. 
The `KeyValueResult` will not get merged by the previous value and always replace the last one and will cast to a single key associative array at the end.

```php
$result = ResponderChain::new()
    ->then(function () {
        return new KeyValueResult('name', 'PHP');
    })
    ->handle();
    
// ['name' => 'PHP']
```

Instead regular array this result won't get merged by last result from chain:

```php
$result = ResponderChain::new()
    ->then(function () {
        return 'first';
    })
    ->then(function () {
        return new KeyValueResult('name', 'Python');
    })
    ->then(function () {
        return new KeyValueResult('name', 'PHP');
    })
    ->handle();
    
// ['name' => 'PHP']
```

## Aggregate Result

The `AggregateResult` will accept a variadic arguments of any data result implementation or any PHP default data type and merge them together, the final result is a multi key / value pair array.

The `AggregateResult` will not get merged by the previous value and always replace the last result.

```php
$result = ResponderChain::new()
    ->then(function () {
        return new AggregateResult(
            new AggregateResult('PHP', 'Dynamic Language', ['name' => 'PHP']),
            new KeyValueResult('designed_by', 'Rasmus Lerdorf'),
            ['first_release' => '1995']
        );
    })
    ->handle();
    
/*
[
    'PHP',
    'Dynamic Language',
    'name' => 'PHP',
    'designed_by' => 'Rasmus Lerdorf',
    'first_release' => '1995',
]
*/
```

The `AggregateResult` respond result won't get merged by last result from chain:

```php
$result = ResponderChain::new()
    ->then(function() {
        return ['first_release' => '1995'];
    })
    ->then(function () {
        return new AggregateResult([
           'name' => 'PHP',
           'type' => 'Dynamic Language'
       ]);
    })
    ->handle();

/*
[
    'name' => 'PHP',
    'type' => 'Dynamic Language',
];
*/
```

## Merge Result

When result from callable is `MergeResult` will merge the data from last result, it's similar to simple array type behaviour when it's returned by callable.

```php
$result = ResponderChain::new()
    ->then(function () {
        return 'first';
    })->then(function() {
        return new MergeResult(['second']);
    })->then(function() {
        return new MergeResult(['third']);
    })
    ->handle();

// ['first', 'second', 'third']


$result = ResponderChain::new()
    ->then(function() {
        return ['first_release' => '1995'];
    })
    ->then(function () {
        return new MergeResult(['name' => 'PHP', 'type' => 'Dynamic Language']);
    })
    ->handle();

/*
[
    'first_release' => '1995',
    'name' => 'PHP',
    'type' => 'Dynamic Language',
]
*/
```

## Passing Parameters Through Call Chain

After each callable execution returned result get merged to params and will get resolved by type and name of argument to next callable.

### Define Default Parameters

default parameters can be set on creation time, these parameters can be resolved to callable which required the parameter by defining an argument with same type or name to callable. to read more about resolving arguments see [ArgumentsResolver](/miscellaneous/arguments-resolver/).

```php
$defaultParams = [
   'view_renderer' => new ViewModelRenderer,
   'layout_name'   => 'main', 
];

$result = ResponderChain::new($defaultParams)
    ->then(function(ViewModelRenderer $view) {
        return $view->capture('welcome_page');
    })
    // lastResult is result from previous call
    // layoutName resolved to callable from defaultParams
    ->then(function($lastResult, $layoutName) {
        return $view->capture($layoutName, ['content' => $lastResult])
    })
    ->handle();
```

### Parameters get updated based on each result from callable

Every result returned by each callable will get merged to the default parameters and will replace if the current parameters with same name exists then these parameters can be resolved to next callable.

```php
$result = ResponderChain::new()
    ->then(function() {
       return ['designed_by' => 'Rasmus Lerdorf'];
    })
    ->then(function() {
        return new KeyValueResult('name', 'php');
    })
    ->then(function() {
        return new AggregateResult(['first_release' => '1995']);
    })
    ->then(function($name, $firstRelease, $designedBy) {
        return strtoupper($name) . ' ' . $firstRelease . ' ' . $designedBy;
    })
    ->handle();

// PHP 1995 Rasmus Lerdorf
```
    