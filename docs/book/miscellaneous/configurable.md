# Configurable

Configurable objects are classes which implemented `ipConfigurable` pact interface, which allow object to parse configuration properties from a resource to an iterator and build the object itself with this given properties.

```php
abstract class aConfigurable
    implements ipConfigurable
{
    use tConfigurable;


    function __construct($options = null, array $skipExceptions = [])
    {
        if ($options !== null)
            $this->setConfigs(static::parse($options), $skipExceptions);
    }

    /**
     * Build Object With Provided Options
     *
     * @param array $options        Usually associated array
     * @param array $skipExceptions List of exception classes to skip on error
     *
     * @return $this
     * @throws ConfigurationError Invalid Option(s) value provided
     */
    abstract function setConfigs(array $options, array $skipExceptions = []);
}
``` 

## Configurable setter

Built-in configurable which map whole properties to a setter method defined inside the class and feed them by calling setter method and associated value of given property.

This is the simple demonstration of what is expected of a configurable object:

```php
class SimpleConfigurable
    extends aConfigurableSetter
{
    protected $a;
    protected $c = 3; // cant be null

    function setA(?int $val) {
        $this->a = $val;
    }

    function setC(int $val) {
        $this->c = $val;
    }

    function __get($key) {
        return $this->{$key};
    }

    static function parse($optionsResource)
    {
        if (is_string($optionsResource)) {
            $optionsResource = json_decode($optionsResource, true);
        }

        return parent::parse($optionsResource);
    }
};

$configurableClass = new SimpleConfigurable;
$configurableClass->setConfigs(SimpleConfigurable::parse('{
  "a": 1,
  "c": 5
}'));

echo $configurableClass->a;
echo $configurableClass->c;
// 15
```

## Multi Parameter Setter Methods

If the setter method needs to have more than one value as a parameter the easiest way is to pass required parameters as an `iterable` or `array` to a method. with `configurableSetter` it's possible to define parameters needed for a setting as a separate arguments to the setter method.

```php
class SimpleConfigurable
    extends aConfigurableSetter
{
    function setCredentials($username, $password)
    {
       // ...
    }
};

$configurableClass = new SimpleConfigurable;
$configurableClass->setConfigs([
  'credentials' => [
     'username' => 'us1000',
     'password' => 'secret',  
  ]
]);
```

## Avoid configuration exceptions

Exceptions of type `ConfigurationError` expected to thrown when something is not correct with given configuration properties. for example when given property is unknown `UnknownConfigurationPropertyError` for configurable object or the given value has not correct type `ConfigurationPropertyTypeError`.

During building object throwing these known exceptions can be eliminated and skipped.

```php
$configurableClass = new simpleConfigurable('{
  "a": 1,
  "d": 5
}');

// Fatal error: Uncaught UnknownConfigurationPropertyError: 
// Configuration Setter option "d" not found.
```
```php
$configurableClass = new simpleConfigurable('{
  "a": 1,
  "d": 5
}', [UnknownConfigurationPropertyError::class]);

echo $configurableClass->a;
echo $configurableClass->c;
// 13
```
