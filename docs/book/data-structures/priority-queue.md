# PriorityQueue

A `PriorityQueue` is very similar to a Queue. Values are pushed into the queue with an assigned priority, and the value with the highest priority will always be at the front of the queue.

The PHP Default [SplPriorityQueue](https://www.php.net/manual/en/class.splpriorityqueue.php) acts as a heap; on iteration, each item is removed from the queue. This class will allow to iterate multiple time over internal queue which is implemented default php `SplPriorityQueue`.

> **Note:** Iterating over a Queue and successive pop operations will not remove items from the queue.

## Usage sample

The `PriorityQueue` only has a method `insert` and provide Traversable object to travers over the items in queue without deleting them.

```php
$queue = new PriorityQueue(PriorityQueue::Ascending);
$queue->insert('C', 3);
$queue->insert('D', 4);
$queue->insert('B', 2);
$queue->insert('A', 1);

foreach ($queue as $value) {
    echo $value;
}

// ABCD
```

## SplQueue

There is some workaround and fixes for default PHP `SplPriorityQueue` which is available as these classes. these two classes are extend `SplPriorityQueue` and used internally by `PriorityQueue`.

### `SplDescendingQueue`
### `SplAscendingQueue`
