# `iData` Interface

The `iData` interface provide simple interface around objects which considered as providing data properties with all objects implementing iData interface it's possible to exchange data between any data entity object.

Data Objects are:

- Traversable 
- Countable 
- Implementing `iObject` [property overloading](https://www.php.net/manual/en/language.oop5.overloading.php#language.oop5.overloading.members) interface.

```php
interface iData
    extends iObject, \Countable
{
    /**
     * Set Struct Data From Array
     *
     * @param iterable $data
     *
     * @return $this
     */
    function import(iterable $data);

    /**
     * If a variable is declared
     *
     * @param mixed $key
     *
     * @return bool
     */
    function has(...$key);

    /**
     * Unset a given variable
     *
     * @param mixed $key
     *
     * @return $this
     */
    function del(...$key);

    /**
     * Clear all values
     *
     * @return $this
     */
    function empty();

    /**
     * Is Empty?
     *
     * @return bool
     */
    function isEmpty();
}
```
