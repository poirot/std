# CollectionObject

The `CollectionObject` is using to store any kind of data type in a collection set, it's possible to define set of meta data to it when data is stored, which can used to query the whole data based on theses associated meta data. for every same data a unique datum id will be generated which is used as a reference to fetch data again.

## It's Countable

```php
// Add random data amount
for ($i=1; $i<=rand(1, 10); $i++) {
   $collectionObject->add(uniqid());
}

$collectionObject->count();
count($collectionObject);
```

## ObjectID for data type

Every object when stored to the collection will get a unique ID and can be used to access to the object. here is how the uniqueID can be generated in forehand for an object.

```php
$objectToStore = [1, 2, 3, 4, 5];
$objectId = DatumId::of($objectToStore);
echo $objectId->__toString();
// 00d34736902d23bd0da1e4de9e79d94a


$objectToStore = pack("nvc*", 0x1234, 0x5678, 65, 66);
$objectId = DatumId::of($objectToStore);
echo $objectId->__toString();
// 3f5145ad7bb6fe230a3badefc32f90e7
```

## `add` - Adding data

Add data to the collection, data can be any value type. with the second argument it's possible to define set of meta data to the stored data which can fetched later or used as a term when querying the collection.

> _**Note:** when same Data is exists in collection the meta data will be replaced with the given meta data in_ _`add`_ _method._

```php
$collectionObject = new CollectionObject;
    
$objectId = $collectionObject->add([1, 2, 3, 4, 5]);
$objectId = $collectionObject->add('abcdef');


// Associated meta data to stored object
//
$objectId = $collectionObject->add(new User(1), [
  'username' => 'mail@email.com'
]);

$objectId = $collectionObject->add(
   pack("nvc*", 0x1234, 0x5678, 65, 66),
   [
     'pack_format' => 'nvc*',
   ]
);
```

## `get` - Get stored object by objectId

Get stored object data in collection by its unique object id, will return `null` if not found.

```php
$object = $collectionObject->get(
  new DatumId('00d34736902d23bd0da1e4de9e79d94a')
);
```

### Get additional meta data associated with the object:
with a second argument the meta data and object itself is available to the callback function. if object with given id not exists in collection null will returned without calling the given callback function.

```php
$objectId = $collectionObject->add(new User(1), [
  'username' => 'mail@email.com'
]);

[$object, $objData] = $collectionObject->get($objectId,
   function($obj, $meta) {
      return [$obj, $meta];
   }
);

print_r($objectData);
// [ 'username' => 'mail@email.com' ]
```

## `find` - Find an object by meta data

Search for stored objects that match given accurate meta data. The result returned is the [IteratorWrapper](/miscellaneous/iterator-wrapper/#iteratorwrapper) instance include all the object matched with the criteria data.

Fetch all objects inside collection:

```php
foreach ($collectionObject->find([]) as $hashIdAsString => $object) {
    // Do anything with all stored objects
}
```

Find based on the criteria matching the metadata associated with each object:

```php
$data = ['name' => 'PHP', 'designed_by' => 'Rasmus Lerdorf' ];
$collectionObject->add((object) $data, $data);

$object = $collectionObject->find(['name' => 'PHP'])->current();
/*
object(stdClass)
  public 'name' => string 'PHP'
  public 'designed_by' => string 'Rasmus Lerdorf'
*/
```

## `del` - Remove object from collection

Remove an object by the ObjectID. if no object with same ObjectID exists on collection it will do nothing.

```php
$collectionObject->del(
   DatumId::of([1, 2, 3, 4, 5])
);
```

## `setMetaData` - Set object meta data

Set meta data associated with the existence object. It will `thrown` an Exception if object not found.

```php
$objectId = $collectionObject->add(new User(1));
$collectionObject->setMetaData($objectId, ['meta' => 'data']);

$objData = $collectionObject->get($objectId, function($_, $meta) {
    return $meta;
});

print_r($objData);
// ['meta' => 'data']
```
‌