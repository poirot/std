# DataStruct

The `DataStruct` is implementing [iData](/data-structures/idata-interface) interface and is a simplest Poirot data entity object.

## Constructing

The `DataStructure` can accept sets of data as a constructor argument. These data is an [iterable](https://www.php.net/manual/en/language.types.iterable.php#language.types.iterable) object with key as data property name which hold the value of the property.

```php
$dataClass = new DataStruct([
   'name' => 'PHP',
   'designed_by' => 'Rasmus Lerdorf',
 ]);
```

## Constructing by variable reference

When there is an array variable, this can be referenced as a construct to the data object, make any change to the data object properties will reflect the reference variable.

```php
$data = [ 'name' => null ];
$dataClass = DataStruct::ofReference($data);
$dataClass->name = 'Power';

print_r($data);
// ['name' => 'Power']
```

## It's Traversable

```php
$dataClass = new DataStruct([
    'name' => 'PHP',
    'designed_by' => 'Rasmus Lerdorf',
]);

foreach ($dataClass as $key => $val)
    echo $key . ': '. $val . PHP_EOL;

// name: PHP
// designed_by: Rasmus Lerdorf
```

## It's Countable

```php
count($dataClass);
$dataClass->count();
```

## Data Manipulation, property overloading

### Set Properties:

```php
$dataClass = new DataStruct;
    
$dataClass->name = 'PHP';
$dataClass->designed_by = 'Rasmus Lerdorf';
```

### Get Properties value:

```php
echo $dataClass->name;        // PHP
echo $dataClass->designed_by; // Rasmus Lerdorf
```

### Remove property:

```php
unset($dataClass->name);
```

### Check availability of property:

> **Note:** if property exists and has `null` as value it also considered as available property and this method will return `true`.

```php
isset($dataClass->name);
```

## Properties are accessible by memory reference

```php
$dataClass = new DataStruct([
  'data' => 'value'
]);

$dataRef = &$dataClass->data;
$dataRef.= '-changed';

echo $dataClass->data; 
// value-changed
```

### Data property can be reserved by it's memory reference:

the property with not assigned value will not considered as property until take a value. 

```php
$dataClass = new DataStruct;
$dataReference = &$dataClass->data;

if (!isset($dataClass->data) && 0 === count($dataClass)) {
   $dataReference = value;
}

echo $dataClass->data; 
// value
```

## `import` - Set data to object

The `import` method will replace all existing property values by new values provided to method.

```php
$dataClass = new DataStruct();
$dataClass->import([
   'name' => 'PHP',
   'designed_by' => 'Rasmus Lerdorf',
 ]);
```

## `del` - Remove property

Any given properties to method will be removed from data object.

```php
$dataClass->del('name');
$dataClass->del('none_exists_prop', 'designed_by');
```

## `has` - Check property exists

Check whether any properties given as argument exists or not? If any of given properties exists will return `true` otherwise `false` will be returned.

> **Note:** if property exists and has `null` as value it also considered as available property and this method will return `true`.

```php
$dataClass->has('name'); // true
$dataClass->has('none_exists_prop', 'designed_by'); // true
```

## `empty` - Remove all properties

```php
$dataClass->empty();
```

## `isEmpty` - Check has any properties

The `isEmpty` method will check if object has any property and return `boolean` value.

```php
$dataClass->isEmpty();
```
