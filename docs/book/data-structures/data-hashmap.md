# DataHashMap

The `DataHashMap` is extending [DataStruct](/data-structures/data-struct/#datastruct) and have all similar functionalities from that. what specific is about hash map data object is allow to set [none scalar](https://www.php.net/manual/en/function.is-scalar.php) keys for data.

```php
‌$dataClass = new DataHashMap;
$dataClass->set([1, 2, 3], 6);

echo $dataClass->get([1, 2, 3]); // 6
```

As none scalar types can be assigned as a key holding values there is no possibility to use property accessors, so two methods are added to `DataHashMap` object:

## `set` - Set property

```php
$dataClass = new DataHashMap;
    
$dataClass->set('name', 'PHP');

$closure = function () {};
$dataClass->set($closure, 'value');
```

## `get` - Get property

```php
$dataClass->get('name');
$dataClass->get($closure);
$dataClass->get([1, 2, 3]);
```

## `del` - Remove property

```php
$dataClass->del('name');
$dataClass->del($closure);
$dataClass->del([1, 2, 3]);
```

## `has` - Check property exists

```php
$dataClass->has('name');
$dataClass->has($closure);
$dataClass->has([1, 2, 3]);
```

## more methods ...  
  
The `DataHashMap` is extending [DataStruct](/data-structures/data-struct/#datastruct) and they have same functionalities except methods mentioned above here, to know more about available methods check the documentation from [DataStruct](/data-structures/data-struct/#datastruct)