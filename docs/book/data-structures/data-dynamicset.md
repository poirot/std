# DataDynamicSet

The `DataDynamicSet` is extending [DataStruct](/data-structures/data-struct/#datastruct) and have all functionalities from that and in addition have some unique functionalities in general can be used to define custom property accessors which known as Getter/Setter methods. A getter is a method that gets the value of a specific property. On the other hand, a setter is a method that sets the value of a specific property.

You don't need to use getter/setter for everything and they should used wisely only when it's needed. They might needed to be used when you have for example a property were accept only a specific PHP type. The `date_time_created` property can be one of those which only accept a `\DateTime` object.

```php
$dataClass = new BlogPost;
$dataClass->created_date = '2020-01-14';
// Throw PropertyError Exception

$dataClass = new BlogPost;
$dataClass->created_date = new \DateTime('2020-01-14');
```

## Read-only properties

Read-only properties are created by defining only a getter method for a property. A `PropertyIsGetterOnlyError` exception is thrown in attempt to set a read-only property.

Methods with `protected` visibility which are prefixed with `get`, `has` and `is` all determined as the property getter. like: `getPropertyName()`, `hasAnyOrder()`, `isAdministration()`.

_**Note:** adding docblock for property is not mandatory but it's recommended._

```php
/**
 * @property-read $property
 */
class ReadOnlyProperty
    extends DataDynamicSet
{
    protected function getProperty()
    {
        return 'value';
    }
}

$a = new ReadOnlyProperty;
echo $a->property;     // value
$a->property = null;   // throws PropertyIsGetterOnlyError
```

## Protect construct level properties by Read-only

Read-only properties are often used to provide read access to a property that was provided during _construct_, which should stay unchanged during the life time of an instance.

In this example below, we can have access to the connection used by Model class afterward and options is used by class itself for some internal usage inside the class and it's not a property accessor.

```php
/**
 * @property-read Connection $connection
 */
class Model
    extends DataDynamicSet
{
    /** @var Connection */
    private $connection;
    
    protected $options;

    function __construct(Connection $connection, array $options = [])
    {
        $this->connection = $connection;
        $this->options = $options;
    }
    
    // Getter method
    
    protected function getConnection()
    {
        return $this->connection;
    }
}

$model = new Model(new Connection);

$model->connection = null; // throws PropertyIsGetterOnlyError
unset($model->connection); // throws PropertyIsGetterOnlyError
```

## Write-only properties

Read-only properties are created by defining only a setter method for a property. A `PropertyIsSetterOnlyError` exception is thrown in attempt to get a write-only property.

Methods with `protected` visibility which are prefixed with `set`, `give` all determined as the property getter. like: `setPropertyName($name)`, `giveConnection($connection)`.

_**Note:** adding docblock for property is not mandatory but it's recommended._

```php
/**
 * @property-write $property
 */
class WriteOnlyProperty
    extends DataDynamicSet
{
    private $property;
    
    protected function setProperty($value)
    {
        return $this->property = $value;
    }
}

$a = new WriteOnlyProperty;
$a->property = 'value';
echo $a->property;           // throws PropertyIsSetterOnlyError
```

## Property Type declaration

‌Type declarations allow property to require a certain type at setter/getter level but mostly it would define on setters.
On set property value the `PropertyError` will thrown if value type is mismatch by method arguments definition. When type declaration not happen on method argument and check happen inside the method block for given type and it's not match the `PropertyTypeError` expected to thrown as well.

> **Note:** When using Type Declaration the method argument should be nullable. it's needed when unset or remove property the underlying value will set to `null`.

```php
/**
 * @property-write $created_date
 */
class BlogPost
    extends DataDynamicSet
{
    protected $date_created;
    protected $tags;
    
    protected function setCreatedDate(?\DateTime $datetime)
    {
        return $this->date_created = $datetime;
    }
    
    protected function setTags($tags)
    {
       if (!is_array($tags) || empty($tags))
           throw new Struct\PropertyTypeError(
              'tags should be none empty array.'
           );
    }
    
    // ...
}
```

## `import` - Set data to object

The `import` method will replace all existing property values by new values provided to method.

> **Note:** As DataDynamicSet could be sensitive to data given to properties couple of exceptions can happen when importing data.

Here is an example how the exceptions can be ignored while importing data:

```php
$DataDynamicSet = new ErrorProneDataDynamicSet;

// Ignore All Errors Related To Getter Only Property
$DataDynamicSet->import($someData, [PropertyIsGetterOnlyError::class]);

// Ignore All Errors Related To Getter Only Property and Immutable Property
$DataDynamicSet->import($someData, [
   PropertyIsGetterOnlyError::class, 
   PropertyIsImmutableError::class
]);

// Ignore All Property Error, including getter only, immutable, etc.
$DataDynamicSet->import($someData, [PropertyError::class]);

// Ignore All Exceptions regarding if it's property error or anything
$DataDynamicSet->import($someData, [\Exception::class]);
```

## more methods ...

The `DataDynamicSet` is extending [DataStruct](/data-structures/data-struct/#datastruct) and they have same functionalities except methods mentioned above here, to know more about available methods check the documentation from [DataStruct](/data-structures/data-struct/#datastruct)

