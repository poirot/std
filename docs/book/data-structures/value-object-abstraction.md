# Value Object Abstraction

‌In practical terms you can think of [value objects](https://martinfowler.com/eaaCatalog/valueObject.html) as your own primitive types. And they should behave as ones. It means that value objects should always be [immutable](https://en.wikipedia.org/wiki/Immutable_object)! This is crucial otherwise it’s hard to avoid issues with values changing unexpectedly (values should be replaced, not modified!).

> **Note:** It’s important to remember about immutability (none of these methods can modify internal state) and to not to put any actual business logic inside value objects – they should be as self-contained as possible.

## Declaration

To define a value object of any type it should extends the `aValueObject` abstract class, and you should follow these simple rules when working with ValueObject in Poirot:

### Constructor:

-   Value Object will have `__construct` method.
    
-   Constructor method should have all possible Value Object needed properties as arguments in constructor method.
    
-   Each constructor arguments should have an accessor method with same name of argument. for example, if you have `$amount` variable it should have one protected accessor method named `withAmount($value)` which can change the state of object for the property.
    
-   The parent `__construct` method call on constructing object to initialize the init state.
‌

```php
class Money
  extends aValueObject
{
    private $amount;
    protected $currency;

    function __construct(int $amount, string $currency = 'EUR')
    {
        parent::__construct();

        $this->withAmount($amount);
        $this->withCurrency($currency);
    }

    protected function withAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    protected function withCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }
    
    // Rest of implementation ...
}
```

When creating a new value object we need to make sure that it has a valid state on constructing. when values are not correct Exception should thrown on creating object. To read more see [Validation](/data-structures/value-object-abstraction/#validation) section.

### Getting current state properties:

To know what state Value Object is and to get property values that we need to be expose to outside the getter methods can be defined:

```php
class Money
  extends aValueObject
{
    private $amount;
    protected $currency;

    function __construct(int $amount, string $currency = 'EUR')
    {
        parent::__construct();

        $this->withAmount($amount);
        $this->withCurrency($currency);
    }

    protected function withAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    protected function withCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }
    
    // Getter Methods:
    
    protected function getAmount()
    {
        return $this->amount;
    }
    
    protected function getCurrency()
    {
        return $this->currency;
    }
}

$moneySpent = new Money(100, 'USD');
echo $moneySpent->getAmount() . ' ' . $moneySpent->getCurrency();
// 100 USD
```

### Change object state and immutability:

Changing the object state and immutability will handled by abstract class the only thing is to define an accessor protected method that is prefixed by `with`. Check the example below and how it's been used:

```php
$eur = new Money(100, 'EUR');
$usd = $eur->withCurrency('USD'); // change the state and return new object

echo $eur->getCurrency(); // EUR
echo $usd->getCurrency(); // USD
```

to make it understandable to most IDEs some notations can be added to class:

```php
/**
 * @method Money  withAmount($amount)
 * @method Money  withCurrency($currency)
 *
 * @method int    getAmount()
 * @method string getCurrency()
 */
class Money
  extends aValueObject
{
    private $amount;
    protected $currency;

    function __construct(int $amount, string $currency = 'EUR')
    {
        parent::__construct();

        $this->withAmount($amount);
        $this->withCurrency($currency);
    }

    protected function withAmount($amount)
    {
        $this->amount = $amount;
        
        // This is not mandatory to have but can help with auto-completion 
        // in code editors
        return $this;
    }
    
    // Rest of implementation here
    // ...
```

## It's Traversable

```php
$money = new Money(100, 'EUR');
    
$asArray = iterator_to_array($money);
/*
['amount' => 100, 'currency' => 'EUR'];
*/
```

## It's Serializable

```php
$money = new Money(100, 'EUR');
$serialized = serialize($money);

$moneyUnserialized = unserialize($serialized);
echo $moneyUnserialized->getAmount(); 
// 100
```

## Check equality

Two value objects are equal not when they refer to the same object, but when the value that they hold is equal, e.g. two different one hundred dollar bills have the same value when we use them to buy something (class Money would be a value object).

To check equality of two value object the abstract object will check the whole values one by one and the class implementation to see if the given object is extend the current value object, which mean something similar to this method:

```php
function isEqualTo(iValueObject $object): bool
{
   return get_class($this) === get_class($object) && $this == $object;
}
```

The `isEqualTo` method can be override by object extending abstract value object, for the Money object it could be re written:

```php
class Money
  extends aValueObject
{
    private $amount;
    protected $currency;

    function __construct(int $amount, string $currency = 'EUR')
    {
        parent::__construct();

        $this->withAmount($amount);
        $this->withCurrency($currency);
    }
    
    function isEqualTo(iValueObject $object): bool
    {
        if (! $object instanceof Money) 
           return false;
           
        return $this->getAmount() === $object->getAmount()
            && $this->getCurrency() === $object->getCurrency();
    }
    
    // Rest of implementation here
    // ...
}


$eur = new Money(100);
$usd = $eur->withCurrency('USD');

var_dump($eur->isEqualTo($usd)); // false
```

## Validation

Let’s take currency from our Money object, Currency value should always consist of three uppercase letters, so why not to convert it to its own Value Object:

> **Note:** Validation should happen on **construct** level and when value object state got changed by **property accessor** as well.

Here we only make sure that currency code has a correct format, but as an improvement it could also validated against [ISO 4217](http://www2.1010data.com/documentationcenter/discover/FunctionReference/DataTypesAndFormats/currencyUnitCodes.html) currency list (or its subset).

```php
class Currency
  extends aValueObject 
{
    private $currency;
 
    function __construct(string $currency)
    {
        // The exception will thrown if currency is invalid.
        $this->withCurrency($currency);
    }
 
    function withCurrency(string $currency)
    {
       $currency = strtoupper($currency);
        if (strlen($currency) !== 3 || !ctype_alpha($currency))
            throw new InvalidArgumentException(
              'Currency has to consist of three letters'
            );
            
        $this->currency = $currency;
        return $this;
    }
    
    // Rest of the definition should be here
    // ...
}
```

## Additional helper methods

In addition to standard getter methods for internal state retrieval, VOs can have helper methods with various functionality. This includes creating new values (either entirely new or based on the current one, e.g. result of adding some amount to Money object), providing different representation for its internal state (e.g. IPAddress VO can present IP address as string or binary data), checking if value object meets some condition (e.g. DateRange VO can have a method to find out if it overlaps another DateRange object), etc.

```php
class Money
  extends aValueObject
{
    // Money definition from previous example should be here
 
    function add(Money $toAdd): Money
    {
        if ($this->getCurrency() !== $toAdd->getCurrency())
            throw new InvalidArgumentException(
                'You can only add money with the same currency'
            );
        
        if ($toAdd->getAmount() === 0)
            return $this;
        
        return new Money(
           $this->getAmount() + $toAdd->getAmount(), 
           $this->getCurrency()
        );
    }
}
```
‌