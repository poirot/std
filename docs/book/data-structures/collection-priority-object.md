# CollectionPriorityObject

The `CollectionPriorityObject` is extending `CollectionObject` to allow add objects with priority order. To read about all available methods see the [CollectionObject](/data-structures/collection-object/#collectionobject).

## `add` - Adding data

The third argument of add method indicate the priority order of the object.

```php
$collectionObject = new CollectionPriorityObject;
$collectionObject->add('C', [], 2);
$collectionObject->add('A', [], 4);
$collectionObject->add('D', [], 1);
$collectionObject->add('B', [], 3);

foreach($collectionObject->find([]) as $storedObj) {
    echo $storedObj;
}

// ABCD
```

priority order also can be defined as the meta data:

```php
$collectionObject->add('C', ['__order' => '2']);
$collectionObject->add('A', ['__order' => 4]);
$collectionObject->add('D', ['__order' => 1]);
$collectionObject->add('B', ['__order' => 3]);
```

## more methods ...

More methods is similar with `CollectionObject`, To read about all available methods see the [CollectionObject documentation](/data-structures/collection-object/#collectionobject).
