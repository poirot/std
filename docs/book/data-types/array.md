# Arrays

Additionally to the [rich set of PHP array functions](https://secure.php.net/manual/en/book.array.php), there is a collection of convenience helper methods array helper provides extra methods allowing you to deal with arrays more efficiently.

> **Method name convention:** 
> Any method from `StdArray` which prefixed with get like `getKeys`, `getLast`, ... has no effect on the current object and will return new instance of object include the expected result.

## Instantiate and Construct

`StdArray` can be instantiated around existing array or from an initial empty array:

```php
$arrOne   = new StdArray();
$arrTwo   = new StdArray([1, 2, 3, 4]);
$arrThree = new StdArray(new \ArrayIterator([1, 2, 3, 4]));

// Through factory static method:
$arr = StdArray::of([1, 2, 3, 4]);
```

`StdArray` can built around different data sources such as `Traversable`, `Object`, `json string`, reference memory address to array variable.

All accepted different data types can be constructed through construct method and none-strict mode by passing second argument to constructor.

```php
$initialValue = (object) ['field' => 'value', 'otherField' => 'other-value']

// can be constructed on none-strict mode
$stdArr = new StdArray($initialValue, false);
```

## Converting To Array

### Traversable:

```php
$initialValue = function() {
  yield field => 'value';
  yield otherField => 'other-value';
}();

$stdArr = StdArray::ofTraversable($initialValue);
```

### stdClass object:

```php
$initialValue = new \stdClass();
$initialValue->field = 'value';
$initialValue->otherField = 'other-value';

$stdArr = StdArray::ofObject($initialValue);
```

### class object:

```php
$initialValue = new class {
  public $field = 'value';
  public $otherField = 'other-value';
};

$stdArr = StdArray::ofObject($initialValue);
```

### json string:

```php
$initialValue = '{
  "field": "value",
  "otherField": "other-value"
}';

$stdArr = StdArray::ofJson($initialValue);
```

### memory address reference:

```php
$initialValue = [1, 2, 3, 4];
$stdArray = StdArray::ofReference($initialValue);
$stdArray->append(5);

print_r($initialValue); // [1, 2, 3, 4, 5]
```

## It's Iterable

`StdArray` is iterable object and items are accessible through `\Iterator`interface.

```php
$stdArray = new StdArray(['a', 'b', 'c', 'd']);
while ($stdArray->valid()) {
    echo $stdArray->key() . '=' . $stdArray->current() . PHP_EOL;
    $stdArray->next();
}
```

## It's Array Accessible

The object items are accessible just like a regular array, the only difference is when key is undefined instead of trigger `E_NOTICE` error of Array key does not exist, it will just return `null` as a value.

```php
$stdArray = new StdArray(['a', 'b', 'c', 'd']);
    
// when key dosen't exists
var_dump( $stdArray[100] );        # null
var_dump( isset($stdArray[100]) ); # false

echo $stdArray[1];                 # b

$stdArray['field'] = 'value';
echo $stdArray['field'];           # value
```

## It's Countable

```php
$initialValue = [1, 2, 3, 4];
$stdArray = new StdArray($initialValue);
count($stdArray); // 4
```

## Array keys accessible by reference

```php
$stdArray = new StdArray([1, 2, 3, 4]);
$stdArray['new-item'] = 'value';

$t = &$stdArray['new-item'];
$t = 'new-value';
echo $stdArray['new-item']; // new-value

// Internal array:
$stdArray = new StdArray([1, 2, 3, 4]);

$internalArray = &$stdArray->asInternalArray;
array_pop($internalArray);

// [1, 2, 3]
var_dump(
  $stdArray->asInternalArray
); 
```

## `lookup` - Search array elements by query path
‌
The `lookup` method search array to matching against given query path `expression` and will return `\Generator` traversable of matching elements or exact value of an element based on the given query path `expression`type.

Any expression which does not return nodes will return `null`.

These path expressions look very much like the path expressions you use with traditional computer file systems:

![](https://gblobscdn.gitbook.com/assets%2F-LkFIsrp32T2osZZ-hee%2F-M4AALm2_KBJ_NUqWok7%2F-M4DVl9FiuHdc5l9TdtT%2Fimg_path_folders.jpg?alt=media&token=65157915-03c8-4f2c-aafb-4aa06e9f6041)
‌
### Fetch one by nested hierarchy lookup:

Selects from the root by hierarchy descendant.

```php
$arr = [
    'books' => [
        [
            'title' => 'Professional PHP Design Patterns',
            'author' => [
                'firstname' => 'Aaron',
                'lastname' => 'Saray',
            ],
        ],
    ],
];

$result = StdArray::of($arr)->lookup('/books/0/title');
// Professional PHP Design Patterns

$result = StdArray::of($arr)->lookup('/books/0/not_exist_field');
// null
```

First `slash` given to expression can be removed, so this query will do the same:

```php
$result = StdArray::of($arr)->lookup('books/0/title');
```

### Fetch all by nested hierarchy lookup:

By adding `*` beside the element in query, simply expected path to match with more than one element or make the result to `\Generator` iterable object.

```php
$result = StdArray::of($arr)->lookup('/books/0/*title');
foreach ($result as $k => $v) {
   print $k .': ' . $v;
}

// title: Professional PHP Design Patterns
```

### Regex lookup:

Regex matching expression can be defined between `~` characters:

```php
$arr = [
    'books' => [
        [
            'title' => 'Professional PHP Design Patterns',
            'author' => [
                'firstname' => 'Aaron',
                'lastname' => 'Saray',
            ],
        ],
    ],
];

## First element key which match with given regex
#
$result = StdArray::of($arr)->lookup('/books/0/author/~.+name$~');
// Aaron

## All elements which matches with given regex
#
$result = StdArray::of($arr)->lookup('/books/0/author/*~.+name$~');
// or
$result = StdArray::of($arr)->lookup('/books/0/author/*~firstname|lastname~');
foreach ($result as $k => $v) {
   print $k .': ' . $v . PHP_EOL;
}
// firstname: Aaron
// lastname: Saray
```

### Match lookup in specified depth only:

The depth are counting from 1 for the next level of hierarchy elements inside root. check this out:

```php
$arr = [
    'Python' => [
        // Depth 1
        'name' => 'Python',
        "designed_by" => "Guido van Rossum",
        "details" => [
            // Depth 2
            'name' => 'python language',
            'typing_discipline' => [
                'tags' => ['Duck', 'dynamic', 'gradual'],
            ],
        ],
    ],
    'PHP' => [
        // Depth 1
        'name' => 'PHP',
        "designed_by" => "Rasmus Lerdorf",
        "details" => [
            // Depth 2
            'name' => 'php language',
            "typing_discipline" => [
                'tags' => ['Dynamic', 'weak'],
            ],
        ],
    ],
];

## First element in depth level ONE matching with given expression
#
$result = StdArray::of($arr)->lookup('/>/name');
// Python

## All elements in depth level TWO matching with given expression
#
$result = StdArray::of($arr)->lookup('/>2/*name');
foreach ($result as $k => $v) {
   print $k .': ' . $v . PHP_EOL;
}
// name: python language
// name: php language
```
‌
### Recursive lookup - Match lookup expression within any depth:

The result of `//` expression is always an `\Generator` traversable.

```php
$arr = [
    'Python' => [
        'name' => 'Python',
        'details' => [
            'name' => 'python language',
            'typing_discipline' => [
                'tags' => ['Duck', 'dynamic', 'gradual'],
            ],
        ],
    ],
    'PHP' => [
        'name' => 'PHP',
        'details' => [
            'name' => 'php language',
            'typing_discipline' => [
                'tags' => ['Dynamic', 'weak'],
            ],
        ],
    ],
];

$result = StdArray::of($arr)->lookup('//name');
foreach ($result as $k => $v) {
    print $k .': ' . $v . PHP_EOL;
}

// name: Python
// name: python language
// name: PHP
// name: php language
```

### Considerations about lookup resulting in same keys:

As the result of query lookup is a `Generator` some times we have duplicated key in the result so using some functions like `iterator_to_array` will not give us the expected result.

**check this sample:** 
as you see here we have two similar key in the iterator each came from the nested element on main array. when use query take this into consideration.

```php
## Fetch first tag in every item
#
$result = StdArray::of($arr)->lookup('//tags/>/*0')
foreach ($result as $k => $v) {
  print $k .': ' . $v . PHP_EOL;
}
// 0: dynamic 0: dynamic
```

### Reindex lookup result:

By re-index expression token the result will get re-indexed on numeric values started from zero, it could be a solution the the same key on the traversable result which also mentioned.

here are the examples:
to re-index `:` should be comes after `/`

```php
$result = StdArray::of($arr)->lookup('//tags/>/*0/:');
iterator_to_array($result);
// ['dynamic', 'dynamic']
```

### Samples on Combination of variant query expressions:

```php
$arr = [
    'Python' => [
        'name' => 'Python',
        'details' => [
            'name' => 'python language',
            'typing_discipline' => [
                'tags' => ['dynamic', 'duck', 'gradual'],
            ],
        ],
    ],
    'PHP' => [
        'name' => 'PHP',
        'details' => [
            'name' => 'php language',
            'typing_discipline' => [
                'tags' => ['dynamic', 'weak'],
            ],
        ],
    ],
];

## Fetch any tags inside PHP element
#
$result = StdArray::of($arr)->lookup('~PHP~//tags');
foreach ($result as $k => $v) {
    print $k .': ' . implode(', ', $v) . PHP_EOL;
}
// tags: Dynamic, weak


## Fetch All available tags
#
$result = StdArray::of($arr)->lookup('//tags/:');
foreach ($result as $v) {
    print implode(', ', $v) . PHP_EOL;
}
// dynamic, duck, gradual
// dynamic, weak


## Fetch Re-Indexed result of First(0) and Third(2) tag from each element
#
$result = StdArray::of($arr)->lookup('//tags/>/*~0|2~/:');
iterator_to_array($result);
// ['dynamic', 'gradual', 'dynamic']

$result = StdArray::of($arr)->lookup('//tags/:/>/*~0|2~/:')
// ['dynamic', 'gradual', 'dynamic']
```

## `lookup` - Access by reference

The `lookup` method can access elements by their memory reference address.

```php
$stdArray = new StdArray([
    [
        'user' => 'user@domain.com',
        'name' => 'name a',
        'data' => ['id' => '1234'],
    ],
    [
        'user' => 'user2@domain.com',
        'name' => 'name b',
        'data' => ['id' => '5678'],
    ],
]);

$firstUserByReference = &$stdArray->lookup('/0/user');
$firstUserByReference = 'changed_user@domain.com';

$names = &$stdArray->lookup('/>/*name');
foreach ($names as &$name)
    $name = strtoupper($name);

$ids = &$stdArray->lookup('//id/:');
foreach ($ids as $i => &$id)
    $id = (int) $id;

/*
[
  [
    'user' => 'changed_user@domain.com',
    'name' => 'NAME A',
    'data' => ['id' => 1234],
  ],
  [
    'user' => 'user2@domain.com',
    'name' => 'NAME B',
    'data' => ['id' => 5678],
  ],
];
*/
```

## `filter` - Filter Array Items

This method will affect current elements inside array object, check out the following code. We'll use the `filter` helper to capitalise each string element with `strtoupper` and remove all none string and empty elements.

```php
$initialValue = ['a', 'b', 'c', '', 0];
$stdArray     = new StdArray($initialValue);

$stdArray->filter(function($value) {
    if (!is_string($value) || $value === '')
        /** @var IteratorWrapper $this */
        $this->skipCurrentIteration();

    if (is_string($value))
        $value = strtoupper($value);

    return $value;
});

// ['A', 'B', 'C']
print_r(
  $stdArray->asInternalArray
);
```

Array keys can change as well with `filter` method:

```php
$initialValue = ['a', 'b', 'c'];
$stdArray     = new StdArray($initialValue);

$stdArray->filter(function($value, &$key) {
    // 65 is ascii code for 'A' char
    $key = chr(65 + $key);
    
    // actual value always should be returned by filter
    return $value;
});

// ['A' => 'a', 'B' => 'b', 'C' => 'c']
print_r(
   $stdArray->asInternalArray
);
```

### Further reading

As `filter` method use `IteratorWrapper` internally to provide the functionalities for filter elements to know better about all available features see `IteratorWrapper` [documentation](/miscellaneous/iterator-wrapper) for more.

## `append` - Append to end of array

The `append` method add a given value to the end of array, if given array key as a second argument exists will replaced by new value and will moved to the end of array elements.

```php
$stdArray = StdArray::of([1986 => 'a', 1983 => 'b']);
    
$stdArray->append('c');
// [1986 => a, 1983 => b, 1987 => c]

$stdArray->append(1, 'one');
// [1986 => a, 1983 => b, 1987 => c, 'one' => 1]

$stdArray->append('d');
// [1986 => a, 1983 => b, 1987 => c, 'one' => 1, 1988 => 'd']

$stdArray->append('e', 1986);
// [1983 => b, 1987 => c, 'one' => 1, 1988 => 'd', 1986 => 'e']
```

## `prepend` - Prepend to beginning of array

The `prepend` method add and element to the beginning of array and re-index array keys. if given array key as a second argument exists will replaced by new value and will moved to the beginning of array elements.

```php
$stdArray = StdArray::of([1986 => 'b', 1983 => 'c']);
    
$stdArray->prepend('a');
// [0 => 'a', 1 => 'b', 2 => 'c']

$stdArray->prepend(1, 'one');
// ['one' => 1, 0 => 'a', 1 => 'b', 2 => 'c']

$stdArray->prepend('x');
// [0 => 'x', 'one' => 1, 1 => 'a', 2 => 'b', 3 => 'c']

$stdArray->prepend(1, 'one');
// ['one' => 1, 0 => 'x', 1 => 'a', 2 => 'b', 3 => 'c']
```

## `insert` - Insert an element to array

The `insert` method will append an element to the array if not any specific key given as second argument or set value of the element if key exists.

When key is not given the value will appended to array:

```php
$stdArray = StdArray::of([])->insert('a');
// [0 => 'a']

$stdArray = $stdArray->insert('b');
// [0 => 'a', 1 => 'b']
```

When we specify key the default behaviour is to replace value with new one:

```php
$stdArray = StdArray::of([0 => 'a', 1 => 'zz']);
$stdArray->insert(1, 'b');
// [0 => 'a', 1 => 'b']
```
    
By comparison callable as third arguments it's possible to define the insert behaviour when given key is exists in array:

```php
$comprator = function ($givenValue, $currValue, $key) {
    if ($givenValue === $currValue)
        return StdArray::InsertDoSkip;
    elseif ($key == 'words')
        return StdArray::InsertDoPush;

    // otherwise when no insert instruction returned 
    // the default behaviour is to replace value
    // which is considered as StdArray::InsertDoReplace
};

$stdArray = StdArray::of(['words' => 'a', 'time' => 1585909377]);
$stdArray
  ->insert('b', 'words', $comprator)
  ->insert(time(), 'time', $comprator);
  
 // ['words' => ['a', 'b'], 'time' => 1585909389]
```

## `merge` - merges the given array recursively

The `merge` method gives flexibility on how to merges the given array into the current array by introducing merge strategies.

By default when element has an integer key it will be appended to array.

```php
$stdArray = StdArray::of([ 0 => 'foo'])
   ->merge([ 0 => 'bar']);
   
// [0 => foo, 1 => bar]
```

The way that merge behaves can be changed with providing merge strategy as a second argument: **Merge Strategy** is a callable or integer value of compare result which instruct the merge how to behave when same element with key exists, the returned number is `-1, 0, 1` which actually has equality constant defined in the class in order as `MergeDoReplace`, `MergeDoMerging`, `MergeDoPush`.

The code below changes the merge behaviour in this way when there is an existing integer key try to merge their value together and for the rest default strategy is called.

```php
$a = [ 0 => 'foo'];
$b = [ 0 => 'bar'];

$stdArray = StdArray::of($a)
    ->merge($b, function ($value, $currValue, $key) {
        if (is_int($key))
            return StdArray::MergeDoMerging;

        return StdArray\DefaultMergeStrategy::call($value, $currValue, $key);
    });
    
// [0 => ['foo', 'bar']]
```

If the array not consist of other different mix of structures which is not important how it's gonna merge, the codes above can be simplified to this:

```php
$stdArray = StdArray::of($a)
    ->merge($b, StdArray::MergeDoMerging);
```

### Other examples to see how merge works in default:

```php
$stdArray = StdArray::of([ 0 => ['foo']])
    ->merge([ 0 => 'bar']);
// [['foo'], 'bar']

$stdArray = StdArray::of([ 0 => 'foo'])
    ->merge([ 0 => ['bar']]);
// ['foo', ['bar']]

$stdArray = StdArray::of(['key' => 'foo'])
    ->merge(['key' => 'bar']);
// ['key' => 'bar']

$stdArray = StdArray::of(['key' => ['foo']])
    ->merge(['key' => 'bar']);
// ['key' => ['foo', 'bar']]

$stdArray = StdArray::of(['key' => 'foo'])
    ->merge(['key' => ['bar']]);
// ['key' => ['bar', 'foo']]


$a = [ 'key' => [['foo', 'a' => null], 'a' => 1], 'foo'];
$b = [ 'key' => [[['bar'], 'a' => 1, 'b' => 2], 'b' => 2], 'bar'];

$stdArray = StdArray::of($a)
    ->merge($b);
    
/*
[
    'key' => [
        [
            'foo',
            'a' => 1,
            ['bar'],
            'b' => 2,
        ],
        'a' => 1,
        'b' => 2,
    ],
    'foo',
    'bar',
]
*/
```

### Another merge strategy example:

If any field has a same value it's not gonna merge otherwise the values getting merged for the rest. In this sample `'value'` field has same value in both array and will not get merged.

```php
$a = ['field' => [
  'type' => 'invalid-type',
  'message' => 'Error message 1.',
  'value' => 'invalid_value',
]];

$b = ['field' => [
  'type' => 'not-in-range',
  'message' => 'Error message 2.',
  'value' => 'invalid_value',
]];

$messages = StdArray::of($a)
   ->merge($b, function ($value, $currVal) {
        if ($value === $currVal)
           return StdArray::MergeDoReplace;
        return StdArray::MergeDoMerging;
})->asInternalArray;

/*
[
    'field' => [
        'type' => [
            'invalid-type',
            'not-in-range',
        ],
        'message' => [
            'Error message 1.',
            'Error message 2.',
        ],
        'value' => 'invalid_value',
    ],
]
*/
```

## `except` - Remove array element(s) by key(s)

The `except` method will remove all given key(s) element from array if key exists.

```php
$initialValue = [1 => 'b', 3 => 'd', 'a' => 0, 'b' => 1, 'd' => 3];
$stdArray = StdArray::of($initialValue)
    ->except(...[1, 3]);

// ['a' => 0, 'b' => 1, 'd' => 3]
```

## `keep` - Keep only given key(s) in array

The `keep` method only keeps the given keys in array if they exists and rest of array will be cleared from unwanted keys.

```php
$initialValue = [1 => 'b', 3 => 'd', 'a' => 0, 'b' => 1, 'd' => 3];
$stdArray = StdArray::of($initialValue)
    ->keep(...[1, 3]);

// [1 => 'b', 3 => 'd'];
```

## `keepIntersection` - Keep values that are present in all the arguments.

Computes the intersection of arrays, compares data by a callback function.

```php
$initialValue = ["a" => "green", "b" => "brown", "c" => "blue", "red"];
$stdArray = StdArray::of($initialValue)
    ->keepIntersection(["a" => "GREEN", "B" => "brown", "yellow", "red"], 
      function($a, $b) {
        return \strcasecmp($a, $b);
      });

// ['a' => 'green', 'b' => 'brown', 0 => 'red']
```

## `getFirst` - Get first array element

The `getFirst` method return first array element of array which the result is another `StdArray` object with the single element from beginning of current array. if current array is empty the result will be an empty array.

```php
$initialValue = [1 => 'b', 3 => 'd', 'a' => 0, 'b' => 1, 'd' => 3];
$firstElement = StdArray::of($initialValue)
    ->getFirst();

echo $firstElement->key() . ' = ' . $firstElement->value();
// 1 = b
```

## `getFirstAndRemove` - Pop an element off the beginning of array

The `getFirstAndRemove` shift element off from beginning of array by removing the element from array and return new `StdArray` object containing the element.

> **Note:** This method will reset the array pointer of the input array to the beginning.

```php
$stdArray = StdArray::of([1 => 'b', 3 => 'd', 'a' => 0, 'b' => 1, 'd' => 3]);
$firstElement = $stdArray
    ->getFirstAndRemove();

echo $firstElement->key() . ' = ' . $firstElement->value();
// 1 = b

$stdArray->reset();
echo $stdArray->key() . ' = ' . $stdArray->value();
// 3 = d
```

## `getLast` - Get last array element

The `getLast` method return last array element of array which the result is another `StdArray` object with the single element from end of current array. if current array is empty the result will be an empty array.

```php
$initialValue = [1 => 'b', 3 => 'd', 'a' => 0, 'b' => 1, 'd' => 3];
$lastElement = StdArray::of($initialValue)
    ->getLast();

echo $lastElement->key() . ' = ' . $lastElement->value();
// d = 3
```

## `getLastAndRemove` - Pop an element off the end of array

The `getLastAndRemove` pop element off from end of array by removing the element from array and return new `StdArray` object containing the element.

> **Note:** This method will reset the array pointer of the input array to the beginning.

```php
$stdArray = StdArray::of([1 => 'b', 3 => 'd', 'a' => 0, 'b' => 1, 'd' => 3]);
$lastElement = $stdArray
    ->getLastAndRemove();

echo $lastElement->key() . ' = ' . $lastElement->value();
// d = 3

echo $stdArray->key() . ' = ' . $stdArray->value();
// 1 = b
```

## `getKeys` - List Array keys

The `getKeys` method returns an instance of `StdArray` with values contains all the keys from current array.

```php
$stdArray = StdArray::of([1 => 'b', 3 => 'd', 'a' => 0, 'b' => 1, 'd' => 3]);
$arrayKeys = $stdArray->getKeys();
// [1, 3, 'a', 'b', 'd']
```

## `getValues` - List Array values

The `getValues` method returns an instance of `StdArray` contains all the values from current array indexed from zero.

```php
$stdArray = StdArray::of([1 => 'b', 3 => 'd', 'a' => 0, 'b' => 1, 'd' => 3]);
$arrayValues = $stdArray->getValues();
// ['b', 'd', 0, 1, 3]
```

## `getColumn` - representing a single column from array

The `getColumn` method retrieves all of the values for a given key(column), it possible to specify how the resulting values from column to be keyed by defining another key as second argument.

```php
$rows = [
  ['id' => '3', 'title' => 'Foo', 'date' => '2013-03-25'],
  ['id' => '5', 'title' => 'Bar', 'date' => '2012-05-20'],
];

$stdArray = StdArray::of($rows)->getColumn('title');
// ['Foo', 'Bar']

$stdArray = StdArray::of($rows)->getColumn('title', 'id');
// [3 => 'Foo', 5 => 'Bar']

$stdArray = StdArray::of($rows)->getColumn(null, 'id');
// [
//   3 => ['id' => '3', 'title' => 'Foo', 'date' => '2013-03-25'],
//   5 => ['id' => '5', 'title' => 'Bar', 'date' => '2012-05-20'],
// ]
```

## `getFlatten` - Get flatten face of multi-dimensional array

The `getFlatten` method flattens a multi-dimensional array into a single level array that uses given notation to indicate depth and return new `StdArray` object containing the element.

```php
$nestedArray = [
    'key1' => 'value1',
    'key2' => [
        'subkey' => 'subkeyval'
    ],
    'key3' => 'value3',
    'key4' => [
        'subkey4' => [
            'subsubkey4' => 'subsubkeyval4',
            'subsubkey5' => 'subsubkeyval5',
        ],
        'subkey5' => 'subkeyval5'
    ]
];

$flattenArr = StdArray::of($nestedArray)
    ->getFlatten('.');
    
/*
array (
  'key1' => 'value1',
  'key2.subkey' => 'subkeyval',
  'key3' => 'value3',
  'key4.subkey4.subsubkey4' => 'subsubkeyval4',
  'key4.subkey4.subsubkey5' => 'subsubkeyval5',
  'key4.subkey5' => 'subkeyval5',
)
*/
```

```php
$flattenArr = StdArray::of($nestedArray)
    ->getFlatten('[%s]');

/*
array (
  'key1' => 'value1',
  'key2[subkey]' => 'subkeyval',
  'key3' => 'value3',
  'key4[subkey4][subsubkey4]' => 'subsubkeyval4',
  'key4[subkey4][subsubkey5]' => 'subsubkeyval5',
  'key4[subkey5]' => 'subkeyval5',
)
*/
```

## `reindex` - ReIndex array keys

The `reindex` method will index array values from zero based on the current position of the item, if the key is an integer then take none-integer keys sort keys and concat them to the end of array as final result.

```php
$stdArray = StdArray::of([1 => 'b', 3 => 'd', 'a' => 0, 'd' => 3, 'b' => 1]);
$arrayKeys = $stdArray->reindex();
// [0 => b, 1 => d, 'a' => 0, 'b' => 1, 'd' => 3]
```

## `clean` - Clean elements with empty values

The `clean` method Clean all elements with [falsy](https://www.php.net/manual/en/language.types.boolean.php#language.types.boolean.casting) values from the current array.

```php
$stdArray = StdArray::of([null, false, true, -9, 
  'foo' => false, 'foo', 'lall' => [], null => '^']);
  
$stdArray->clean();
// [2 => 1, 3 => -9, 4 => foo, null => ^]
```

## `isEqualWith` - Check whether origin array is equal to given value

The `isEqualWith` method check equality of current array with the given value can be any type which can constructed by `StdArray`

```php
$stdArray = new StdArray([1, 2, 3, 4])
    ->isEqualWith([1, 2, 3, 4]);
    
// true
```

## `isHavingEqualValuesWith` - Check whether origin array has equal values to given value

The `isHavingEqualValuesWith` method check equality of values inside two arrays regardless of position of element in array, the given value can be any type which can constructed by `StdArray`

```php
$stdArray = new StdArray(['x' => 'BRANCH'])
    ->isHavingEqualValuesWith(['BRANCH']);
// true

$stdArray = new StdArray(['BRANCH','ADDRESS','MOBILE','NAME'])
    ->isHavingEqualValuesWith(['NAME','BRANCH','MOBILE','ADDRESS']);
// true
```

## `isEmpty` - Check if array is empty

The `isEmpty` method returns `true` if the collection is empty; otherwise, `false` is returned.

```php
StdArray::of([])->isEmpty();
// true
```

## `isAssoc` - Check if array is Associative array

```php
$initialValue = ['field' => 'value', 'otherField' => 'other-value'];
$stdArray = new StdArray($initialValue);

$stdArray->isAssoc();
// true
```
