# Strings

The `StdString` is easy to use and easy to bundle library include convenience methods to handle strings with different character set encoding support. This library will also auto-detect server environment and will use the available installed php-extensions and pick them as a fallback to do the task.

> **Note:** The `StdString` is immutable object which means any modification method to the current object will apply changes on the new instance of the object and the current one will stay same without any changes.

It's a wrapper around existence several extensions or libraries like `mbstring`, `iconv`, `intl`, ... in addition any custom user implementation can be registered as a fallback to the library through the available interfaces.

If Symfony Polyfills for `mbstring` is available (required in composer) or classes are loaded or autoloadable by PHP it might be selected as a fallback by `StdString` if needed.([https://github.com/symfony/polyfill](https://github.com/symfony/polyfill))

## Register custom wrapper

Any custom implementation string wrapper can be registered to `StdString` by implementing `iStringWrapper` interface and then simply can be registered.

Registered wrapper will take the higher priority when `StdString` taking a wrapper from registry basically depends on the implemented methods it would take the job.

```php
StdString::registerWrapper(CustomTestStringWrapper::class);
    
// Do the string manuplation job ....

StdString::unregisterWrapper(CustomTestStringWrapper::class);
```

## Usage sample

```php
echo StdString::of('iñtërnâtiônàlizætiøn')->upperFirstChar();
// Iñtërnâtiônàlizætiøn

echo (string) StdString::of('mĄkA')->lower();
// mąka

echo StdString::of('/foo/bar/baz')
    ->stripPrefix('/')
    ->upperFirstChar();
// Foo/bar/baz

$str = StdString::of('¬fòô bàř¬');
if ($str->isStartedWith('¬fòô ')) {
    echo 'String has started with the expected prefix.';
}
```

## Instantiate and Construct

When string object constructed the object should know about the encoding charset of string, the default charset is `UTF-8`, depend on the string representing content any other encoding can be chosen.

The `Encodings` class contains all available charsets which can be used to get more consistency when using different charset on code base.

```php
StdString\Encodings::Charset['UTF-32'];
StdString\Encodings::CharsetSingleByte['ISO-8859-1'];
```

Values given as initial value to the class will casting to string type if it's not string. On Any object which can't cast to string `\UnexpectedValueException` will thrown.

```php
$stdString = new StdString('Hello!');
$stdString = new StdString($toStrigableObject);

$stdString = new StdString(3.14, StdString::CharsetDefault);
// '3.14'

$stdString = new StdString(-1234, StdString::CharsetDefault);
// '-1234'

$stdString = new StdString(true, StdString::CharsetDefault);
// 'true'
```

## It's serializable

The serialized string is a 3 byte sequence in the beginning of the string which indicate the encoding charset of the string and the rest is the origin string byte sequences means the whole original string.

For instance the serialized string `中文空白` with encoding `UTF-16` represented like this: `\x00\x25 中文空白` the string part is also should be considered as byte sequences but here just to make it more understandable the string is used.

> **Note:** Serialized string is safe to store in DB and other storages and can be searchable and can be converted and used by`StdString` object with same encoding again.

```php
$stdStr    = StdString::of('中文空白', StdString\Encodings::Charset['UTF-32']);
$newStdStr = StdString::ofSerializedByteOrder(
    $stdStr->asSerializedByteOrder()
);

echo $newStdStr->encoding;
// UTF-32
```

## It's array like accessible

Characters within strings may be only accessed by specifying the zero-based offset of the desired character after the string using square array brackets, as in `$str[0]`.

> **Note:** Instead of default PHP behaviour of accessing elements which is consider the string as series of 8bit data for each element, here each element represent a character in string. in multibyte encodings one character may consist more than 8bit.

```php
$str = '中文空白';
$stdString = StdString::of($str);

echo $stdString[1];
// 文

var_dump(isset($str[6]));       // true
var_dump(isset($stdString[4])); // false
```

## It's countable

```php
$stdString = StdString::of('中文空白');
$stdString->length();
// or
count($stdString);
// 4
```

## Get current encoding

To know which charset encoding object is instantiated with there are two readonly property which hold the same value.

```php
$stdStr = new StdString('', StdString\Encodings::CharsetSingleByte['ASCII']);
$stdStr->charset;
$stdStr->encoding;
// ASCII
```

## `lower`

Convert all alphabetic characters within string to lowercase.

```php
echo StdString::of('foO Bar BAZ')->lower();
// foo bar baz

echo StdString::of('mĄkA')->lower();
// mąka
```

## `lowerFirstChar`

Makes string's first char lowercase, if that character is alphabetic.

```php
echo StdString::of('Foo bar')->lowerFirstChar();
// foo bar

echo StdString::of('Öäü')->lowerFirstChar();
// 'öäü'
```

## `upper`

Convert all alphabetic characters within string to uppercase.

```php
echo StdString::of('foO Bar BAZ')->upper();
// FOO BAR BAZ

echo StdString::of('Umlaute äöü')->upper();
// UMLAUTE ÄÖÜ
```

## `upperFirstChar`

Makes string's first char uppercase, if that character is alphabetic.

```php
echo StdString::of('foo bar')->upperFirstChar();
// Foo bar

echo StdString::of('öäü')->upperFirstChar();
// Öäü
```

## `camelCase`

Note that whitespaces will be removed from the string. only `_` character will be considered as a separator char and will be used in creating camelCase result of string other like numbers will be considered as part of words.

```php
echo StdString::of('camelCase')->camelCase();
// camelCase

echo StdString::of('camel_case')->camelCase();
// camelCase

echo StdString::of('  camel  case')->camelCase();
// camelCase

echo StdString::of('Camel2case')->camelCase();
// camel2case

echo StdString::of('Camel2_case')->camelCase();
// camel2Case

echo StdString::of('ΣamelCase')->camelCase();
// σamelCase
```

The delimiter can given to the method and that will be remain if string at the beginning or end has it.

```php
echo StdString::of('__Camel_case__')->camelCase('_');
// __cascalCase__
```

## `pascalCase`

```php
echo StdString::of('pascalCase')->pascalCase();
// PascalCase

echo StdString::of('pascal__case')->pascalCase();
// PascalCase

echo StdString::of('  pascal  case')->pascalCase();
// PascalCase

echo StdString::of('pascal2case')->pascalCase();
// Pascal2case

echo StdString::of('pascal2_case')->pascalCase();
// Pascal2Case

echo StdString::of('σascalCase')->pascalCase();
// ΣascalCase
```

The delimiter can given to the method and that will be remain if string at the beginning or end has it.

```php
echo StdString::of('__σascal_case__')->pascalCase('_');
// __ΣascalCase__
```

## `snakeCase`

```php
echo StdString::of('SnakeCase')->snakeCase();
// snake_case

echo StdString::of('Snake#Case')->snakeCase();
// snake#_case

echo StdString::of('snake  case')->snakeCase();
// snake_case

echo StdString::of('snake2Case')->snakeCase();
// snake2_case

echo StdString::of('ΣτανιλCase')->snakeCase();
// στανιλ_case
```

The delimiter can given to the method and that will be remain if string at the beginning or end has it.

```php
echo StdString::of('__snakeCase__')->snakeCase('_');
// __snake_case__
```

## `subString` - Return part of a string

Returns the portion of string specified by the start and length parameters. to see about all possible values to the parameters check [here](https://www.php.net/manual/en/function.substr.php).

```php
$stdString = StdString::of('abcde');
echo $stdString->subString(1, 2);  // bc
echo $stdString->subString(-2);    // de
echo $stdString->subString(-3, 2); // cd

$stdString = StdString::of('中文空白');
echo $stdString->subString(1, 2);  // 文空
echo $stdString->subString(-2);    // 空白
```

## `stripPrefix` - remove string from beginning

Remove given string if only it's found at the beginning of the original string.

```php
$stdString = StdString::of('/foo/bar/baz');
echo $stdString->stripPrefix('/');      // foo/bar/baz
echo $stdString->stripPrefix('/foo/');  // bar/baz

$stdString = StdString::of('آهایی دنیا سلام');
echo $stdString->stripPrefix('آهایی');  
// دنیا سلام
```

Second argument to the method will strip any whitespace from beginning of array before strip given string when it has `true` value. which is default value.

```php
echo StdString::of('  /foo/bar/')->stripPrefix();
// '/foo/bar/'
```

## `stripPostfix` - remove string from end

Remove given string if only it's found at the end of the original string.

```php
$stdString = StdString::of('foo/bar/baz/');
echo $stdString->stripPostfix('/');      // foo/bar/baz
echo $stdString->stripPostfix('/baz/');  // foo/bar

$stdString = StdString::of('آهایی دنیا سلام');
echo $stdString->stripPostfix('سلام');  
// آهایی دنیا
```

Second argument to the method will strip any whitespace from end of array before strip given string when it has `true` value. which is default value.

```php
echo StdString::of('/foo/bar/  ')->stripPostfix();
// '/foo/bar/'
```

## `hasStartedWith` - Get prefixed string

Get the given string as a result if the origin string is started with that otherwise return empty string. Method will accept [variadic argument](https://www.php.net/manual/en/functions.arguments.php#functions.variable-arg-list) of string values and will return the one which match first with the origin string prefix.

```php
$stdStr = StdString::of('foobarbaz');
$prefix = $str->hasStartedWith('bo', 'fo');

echo $prefix->__toString(); // fo 
```

## `isStartedWith` - Check prefix of string

Method will accept [variadic argument](https://www.php.net/manual/en/functions.arguments.php#functions.variable-arg-list) of string values and will check if the string has one of the given values in the beginning.

```php
$stdStr = StdString::of('foobarbaz');
$str->hasStartedWith('fo');
// true
```

## `hasContains` - Has string contains a substring

Determine existence of a substring in origin string, if it has the substring will be returned as the result otherwise empty string will be returned.

```php
$stdString = new StdString('щука 漢字');
echo $stdString->hasContains('щука');
// щука
```

## `isContains` - Check string contains a substring

Check if string contains given substring and return boolean value `true` if has substring otherwise return `false`.

```php
$stdString = new StdString('щука 漢字');
$stdString->isContains('щука');
// true
```

## `concatSafeImplode` - Safe join strings

Join string elements from array with a delimiter glue, joining will be safe to not include extra glue character if concat string has already contain the delimiter glue.

```php
$stdInitial = StdString::of('/a/');
$stdString  = $stdInitial->concatSafeImplode(['b/', 'c', 'd/'], '/');

echo $stdString;
// /a/b/c/d/
```

## `normalizeExtraNewLines` - Remove extra line breaks

Remove extra new lines and line breaks from string.

```php
$stdString = new StdString("aaжи\r\n\r\n\r\n\r\nвπά\n\n\n\n\n우리をあöä\r\n");
echo $stdString->normalizeExtraNewLines();
// aaжи\r\n\r\nвπά\n\n우리をあöä\r\n
```

## `normalizeWhitespaces` - Strip and remove whitespaces

```php
$stdString = new StdString("\tΟ \r\n   συγγραφέας  ");
echo $stdString->normalizeWhitespaces();
// Ο συγγραφέας
```
