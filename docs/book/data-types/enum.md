# Enums

An enumeration type, "enum" for short, is a data type to categorise named values. Enums can be used instead of hard coded strings to represent, for example, the status of a blog post in a structured and typed way.

For example, a Post object supposed to have three different values for status of the post, it could be one of three strings: `draft`, `published` or `archived`.

```php
class Post
{
    function setStatus(PostStatus $status): void
    {
        $this->status = $status(); // Invoke StdEnum to get value
    }
}
```

With `StdEnum` the enumeration type can be defined as follow:

```php
class PostStatus extends StdEnum
{
    const Draft = 'draft';
    const Published = 'published';
    const Archived = 'archived';
}
```

And Named values can be accessed by calling as static methods:

```php
$post->setStatus(PostStatus::Draft());
```

## Declaration

To define new enumeration type it's needed to just extend the `StdEnum` and define needed named values as public `constant` inside the class.

The `StdEnum` will automatically recognize the possible values from defined constants and will proxy all static method calls to related constant name to build new instance with value equal to const.

To get IDE support on autocompletion on the class the Docblock notation can be defined but it's not necessary but highly recommended.

```php
/**
 * @method static EnumFixture Draft();
 * @method static EnumFixture Published();
 * @method static EnumFixture Archived();
 */
class PostStatus extends StdEnum
{
    const Draft = 'draft';
    const Published = 'published';
    const Archived = 'archived';
    
    // 'draft' as a default value
    function __construct($initial_value = self::Draft)
    {
        parent::__construct($initial_value);
    }
}
```

## Constructing object

Instantiating object is possible by `new` syntax and by calling statically the value name like `::NamedConst`.

```php
$postStatus = PostStatus::Archived();
// which is equal to
$postStatus = new PostStatus(PostStatus::Archived);
```

When the named value is not valid to class the `\UnexpectedValueException` will thrown.

```php
new PostStatus('invalid');
// Exception: \UnexpectedValueException

PostStatus::UNDEFINED();
// Exception: \UnexpectedValueException
```

## Get Value of enum object

The `StdEnum` is [invokable](https://www.php.net/manual/en/language.oop5.magic.php#object.invoke) and by calling the object like functions will return the value which is holding.

```php
$postStatus = PostStatus::Draft();
if ($postStatus() === PostStatus::Draft) {
   echo 'Post is on Draft.';
}

// Post is on Draft.
```

An example on How it's used in classes which are needed these values can be [found here](/data-types/enum/#enums).

## Equality check

The `isEqualTo` method will tests whether enum instances are equal (returns `true` if enum values are equal, `false` otherwise).

```php
$archiveStatus = PostStatus::Archived();

// true
$archivedStatus->isEqualTo(new PostStatus(PostStatus::Archived));
$archivedStatus->isEqualTo(PostStatus::Archived());

// false
$archivedStatus->isEqualTo(PostStatus::Draft());
```

### Check on classes which extends from base enum class

The equality check on different object can be `true` only for the classes which extended from base enum class with same value. otherwise result check for different objects with the same value is always `false`.

```php
class ExtendedPostStatus extends PostStatus
{
    const Banned = 'banned';
}


$archiveStatus = ExtendedPostStatus::Archived();
$archivedStatus->isEqualTo(PostStatus::Archived());
// true
```

## List all possible enums

Return an array of enum name to the Enum object initialized with the internal value of related constant.

```php
$enums = PostStatus::listEnums();
    
/*
[
   'Draft' => (PostStatus) 'draft',
   // ..
]
*/
```

