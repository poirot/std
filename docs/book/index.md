# Introduction  

Poirot SPL is sets of useful basic libraries and general purpose utility classes for different scopes like:

- argument resolver;
- error handling
- environment setup
- mutex lock
- validator abstraction
- etc.

Libraries is grouped into different scopes just to organize the documentation topics.

**Miscellaneous Core Libraries** is sets of useful basic libraries and helpers.

**Standard Data Types Libraries** is wrapper around existence PHP data types which adding more convenient methods and useful helpers to them.

**Standard Data Structures** is sets of data manipulation objects and data entities structure which provide functionalities to ease working with data sets.
  
The component are distributed under the "**Poirot/Std**" namespace and has a [git repository](https://gitlab.com/poirot), and, for the php components, a light **autoloader** and [composer](https://getcomposer.org/) support.  
  
*All Poirot components following the [PSR-4 convention](https://www.php-fig.org/psr/psr-4/) for namespace related to directory structure and so, can be loaded using any modern framework autoloader. All packages are well tested and using modern coding standards.*
